#include "RTXetrisStage.h"
#include "NxT/Direct3D12/D3D12ConstantBuffer.h"
#include "NxT/IGraphics.h"
#include <DirectXTex.h>
#include <time.h>
#include <random>
using namespace sweeper;
using namespace nxt;

#define EPSILON 0.0001f

void RTXetrisStage::CheckInput(int mouseButtons) {


}

void RTXetrisStage::GenerateStage() {
	
	//iInstanceCrystal = iWorldRenderer->iWorkContexts[0]->CreateInstanceBuffer("iic", crystals.data(), sizeof(Crystals), crystals.size());
	//iInstanceBuffer = iWorldRenderer->iWorkContexts[0]->CreateInstanceBuffer("iiWc", whiteCubeTransforms.data(), sizeof(Transforms), whiteCubeTransforms.size());
	//iInstanceMet = iWorldRenderer->iWorkContexts[0]->CreateInstanceBuffer("iiMc", metCubeTransforms.data(), sizeof(Transforms), metCubeTransforms.size());
}

void RTXetrisStage::BuildRTXData(std::vector<unsigned int>& vertexSizes, std::vector<unsigned int>& rtxIndiceSizes, std::vector<IIndexBuffer*>& rtxIndexBuffers, std::vector<IVertexBuffer*>& rtxVertexBuffers, std::vector<RTXetrisPiece*>& pieceVec) {
	for (int i = 0; i < 11; i++) {

		const nxt::IMesh* piece = pieceVec.at(i)->getIMesh();
		if (iWorldRenderer->isDXRSupported) {
			tang.insert(tang.end(), piece->tang.begin(), piece->tang.end());
			bitang.insert(bitang.end(), piece->bitang.begin(), piece->bitang.end());
			pos.insert(pos.end(), piece->pos.begin(), piece->pos.end());
			indice.insert(indice.end(), piece->indice.begin(), piece->indice.end());
			materialIndice.insert(materialIndice.end(), piece->materialIndice.begin(), piece->materialIndice.end());
			normal.insert(normal.end(), piece->normal.begin(), piece->normal.end());
			uv.insert(uv.end(), piece->uv.begin(), piece->uv.end());
			offsets.insert(offsets.end(), piece->offsets.begin(), piece->offsets.end());
			textures.insert(textures.end(), piece->textures.begin(), piece->textures.end());

			vertexSizes.insert(vertexSizes.end(), piece->rtxVertexSizes.begin(), piece->rtxVertexSizes.end());
			rtxIndexBuffers.insert(rtxIndexBuffers.end(), piece->iRTXIndexBuffers.begin(), piece->iRTXIndexBuffers.end());
			rtxVertexBuffers.insert(rtxVertexBuffers.end(), piece->iRTXVertexBuffers.begin(), piece->iRTXVertexBuffers.end());
			rtxIndiceSizes.insert(rtxIndiceSizes.end(), piece->rtxIndexSizes.begin(), piece->rtxIndexSizes.end());
		}
	}
}

void RTXetrisStage::LoadModels(std::vector<RTXetrisPiece*>& pieceVec, PieceType pieceType, std::string modelPath) {

	//Yea, I know this is a little weird, but given the nature of acceleration structures, and DXR Im not sure how to avoid it
	for (int i = 0; i < 11; i++) {
		IMesh* piece = iWorldRenderer->LoadModel(modelPath.c_str(), MeshType::GLTF);
		pieceVec.push_back(
			new RTXetrisPiece(
				0.0f,
				0.0f,
				0.0f,
				piece,
				pieceType
			)
		);
	}
}

void BuildInstanceMap(std::vector<RTXetrisPiece*> pieces) {
	for (int i = 0; i < pieces.size(); i++) {
		pieces[i]->BuildInstanceMap();
	}
}

void RTXetrisStage::Initialize() {

	pieceRotation.resize(10);
	for (int i = 0; i < 10; i++) pieceRotation[i] = PieceType::NO_PIECE;

	IMesh::rtxIndiceOffset = 0;
	IMesh::rtxMatIndex = 0;
	IMesh::rtxIndiceOffset = 0;
	IMesh::vbVerticesRtxOffsetSize = 0;
	IMesh::ibIndicesRtxOffsetSize = 0;
	IMesh::rtxOffset = 0;
	IMesh::rtxNameMap = {};
	IMesh::rtxMats = {};
	IMesh::rtxTextureNameMap = {};
	IMesh::rtxTextures = {};
	IMesh::nameMap = {};
	IMesh::rtxSubsetIndex = 0;

	iConstantBufferHelper = new D3D12ConstantBufferHelper<wvp>(iWorldRenderer->iGraphics, iWorldRenderer->iWorkContexts[0]);
	iConstantBufferHelper->Initialize(1024);

	iConstantBufferHelperView = new D3D12ConstantBufferHelper<View>(iWorldRenderer->iGraphics, iWorldRenderer->iWorkContexts[0]);
	iConstantBufferHelperView->Initialize(9);

	iConstantBufferHelperPBR = new D3D12ConstantBufferHelper<PBRAppSettings>(iWorldRenderer->iGraphics, iWorldRenderer->iWorkContexts[0]);
	iConstantBufferHelperPBR->Initialize(1024);

	iConstantBufferHelperBorderColor = new D3D12ConstantBufferHelper<BorderColorModulation>(iWorldRenderer->iGraphics, iWorldRenderer->iWorkContexts[0]);
	iConstantBufferHelperBorderColor->Initialize(32);

	colorValues.push_back(Vec3(0.0f, 1.0f, 0.2f));
	colorValues.push_back(Vec3(0.0f, 1.0f, 0.8f));
	colorValues.push_back(Vec3(1.0f, 0.0f, 0.8f));

	currentColorIndex = 0.0f;
	colorTransisitionDT = 0.0f;

	rtxetrisStage = iWorldRenderer->LoadModel("Assets/Models/rtxetris/stage.gltf", MeshType::GLTF);
	rtxetrisReflectPlane = iWorldRenderer->LoadModel("Assets/Models/rtxetris/reflectplane.gltf", MeshType::GLTF);

	const uint8_t MAX_NUMBER_OF_UNIQUE_PIECES = 7;
	const uint8_t MAX_NUMBER_OF_DUPLICATE_PIECES = 11;

	//rtxRtxetrisStage = iWorldRenderer->LoadModel("Assets/Models/rtxetris/world.gltf", MeshType::GLTF);

	angleRot = 0.0f;
	world.SetIdentity();
	viewProj.SetIdentity();
	worldZ.SetIdentity();

	GenerateStage();

	LoadModels(elbow_left, PieceType::ELBOW_LEFT, "Assets/Models/rtxetris/elbowLeft.gltf");
	LoadModels(elbow_right, PieceType::ELBOW_RIGHT, "Assets/Models/rtxetris/elbowRight.gltf");
	LoadModels(noodle, PieceType::NOODLE, "Assets/Models/rtxetris/noodle.gltf");
	LoadModels(zig_left, PieceType::ZIG_LEFT, "Assets/Models/rtxetris/zigLeft.gltf");
	LoadModels(zig_right, PieceType::ZIG_RIGHT, "Assets/Models/rtxetris/zigRight.gltf");
	LoadModels(square, PieceType::SQUARE, "Assets/Models/rtxetris/square.gltf");
	LoadModels(tee, PieceType::TEE, "Assets/Models/rtxetris/tee.gltf");

	rtxBorder = iWorldRenderer->LoadModel("Assets/Models/rtxetris/border.gltf", MeshType::GLTF);
	rtxPieceBorder = iWorldRenderer->LoadModel("Assets/Models/rtxetris/pieceBorder.gltf", MeshType::GLTF);
	sinModel = iWorldRenderer->LoadModel("Assets/Models/rtxetris/sin.gltf", MeshType::GLTF);

	if (iWorldRenderer->isDXRSupported) {
		std::vector<unsigned int> vertexSizes;
		std::vector<IIndexBuffer*> rtxIndexBuffers;
		std::vector<IVertexBuffer*> rtxVertexBuffers;
		std::vector<unsigned int> rtxIndiceSizes;
		std::vector<Matrix4x4> ms;

		vertexSizes.insert(vertexSizes.end(), rtxetrisStage->rtxVertexSizes.begin(), rtxetrisStage->rtxVertexSizes.end());
		rtxIndexBuffers.insert(rtxIndexBuffers.end(), rtxetrisStage->iRTXIndexBuffers.begin(), rtxetrisStage->iRTXIndexBuffers.end());
		rtxVertexBuffers.insert(rtxVertexBuffers.end(), rtxetrisStage->iRTXVertexBuffers.begin(), rtxetrisStage->iRTXVertexBuffers.end());

		rtxIndiceSizes.insert(rtxIndiceSizes.end(), rtxetrisStage->rtxIndexSizes.begin(), rtxetrisStage->rtxIndexSizes.end());

		tang.insert(tang.end(), rtxetrisStage->tang.begin(), rtxetrisStage->tang.end());
		bitang.insert(bitang.end(), rtxetrisStage->bitang.begin(), rtxetrisStage->bitang.end());
		pos.insert(pos.end(), rtxetrisStage->pos.begin(), rtxetrisStage->pos.end());
		indice.insert(indice.end(), rtxetrisStage->indice.begin(), rtxetrisStage->indice.end());
		materialIndice.insert(materialIndice.end(), rtxetrisStage->materialIndice.begin(), rtxetrisStage->materialIndice.end());
		normal.insert(normal.end(), rtxetrisStage->normal.begin(), rtxetrisStage->normal.end());
		uv.insert(uv.end(), rtxetrisStage->uv.begin(), rtxetrisStage->uv.end());
		offsets.insert(offsets.end(), rtxetrisStage->offsets.begin(), rtxetrisStage->offsets.end());
		textures.insert(textures.end(), rtxetrisStage->textures.begin(), rtxetrisStage->textures.end());

		vertexSizes.insert(vertexSizes.end(), rtxetrisReflectPlane->rtxVertexSizes.begin(), rtxetrisReflectPlane->rtxVertexSizes.end());
		rtxIndexBuffers.insert(rtxIndexBuffers.end(), rtxetrisReflectPlane->iRTXIndexBuffers.begin(), rtxetrisReflectPlane->iRTXIndexBuffers.end());
		rtxVertexBuffers.insert(rtxVertexBuffers.end(), rtxetrisReflectPlane->iRTXVertexBuffers.begin(), rtxetrisReflectPlane->iRTXVertexBuffers.end());

		rtxIndiceSizes.insert(rtxIndiceSizes.end(), rtxetrisReflectPlane->rtxIndexSizes.begin(), rtxetrisReflectPlane->rtxIndexSizes.end());

		tang.insert(tang.end(), rtxetrisReflectPlane->tang.begin(), rtxetrisReflectPlane->tang.end());
		bitang.insert(bitang.end(), rtxetrisReflectPlane->bitang.begin(), rtxetrisReflectPlane->bitang.end());
		pos.insert(pos.end(), rtxetrisReflectPlane->pos.begin(), rtxetrisReflectPlane->pos.end());
		indice.insert(indice.end(), rtxetrisReflectPlane->indice.begin(), rtxetrisReflectPlane->indice.end());
		materialIndice.insert(materialIndice.end(), rtxetrisReflectPlane->materialIndice.begin(), rtxetrisReflectPlane->materialIndice.end());
		normal.insert(normal.end(), rtxetrisReflectPlane->normal.begin(), rtxetrisReflectPlane->normal.end());
		uv.insert(uv.end(), rtxetrisReflectPlane->uv.begin(), rtxetrisReflectPlane->uv.end());
		offsets.insert(offsets.end(), rtxetrisReflectPlane->offsets.begin(), rtxetrisReflectPlane->offsets.end());
		textures.insert(textures.end(), rtxetrisReflectPlane->textures.begin(), rtxetrisReflectPlane->textures.end());

		cbMaterialRTX = new D3D12ConstantBufferHelper<nxt::MetallicRoughness[128]>(iWorldRenderer->iGraphics, iWorldRenderer->iWorkContexts[0], true);
		cbMaterialRTX->Initialize(6);

		BuildRTXData(vertexSizes, rtxIndiceSizes, rtxIndexBuffers, rtxVertexBuffers, elbow_left);
		BuildRTXData(vertexSizes, rtxIndiceSizes, rtxIndexBuffers, rtxVertexBuffers, elbow_right);
		BuildRTXData(vertexSizes, rtxIndiceSizes, rtxIndexBuffers, rtxVertexBuffers, noodle);
		BuildRTXData(vertexSizes, rtxIndiceSizes, rtxIndexBuffers, rtxVertexBuffers, zig_left);
		BuildRTXData(vertexSizes, rtxIndiceSizes, rtxIndexBuffers, rtxVertexBuffers, zig_right);
		BuildRTXData(vertexSizes, rtxIndiceSizes, rtxIndexBuffers, rtxVertexBuffers, square);
		BuildRTXData(vertexSizes, rtxIndiceSizes, rtxIndexBuffers, rtxVertexBuffers, tee);

		tangs = iWorldRenderer->iWorkContexts[0]->CreateSrvBuffer("", tang.data(), sizeof(Tangent), tang.size());
		bitangs = iWorldRenderer->iWorkContexts[0]->CreateSrvBuffer("", bitang.data(), sizeof(Bitangent), bitang.size());
		positions = iWorldRenderer->iWorkContexts[0]->CreateSrvBuffer("", pos.data(), sizeof(Positions), pos.size());
		indices = iWorldRenderer->iWorkContexts[0]->CreateSrvBuffer("", indice.data(), sizeof(unsigned int), indice.size());
		materialIndices = iWorldRenderer->iWorkContexts[0]->CreateSrvBuffer("", materialIndice.data(), sizeof(unsigned), materialIndice.size());
		normals = iWorldRenderer->iWorkContexts[0]->CreateSrvBuffer("", normal.data(), sizeof(Normal), normal.size());
		uvs = iWorldRenderer->iWorkContexts[0]->CreateSrvBuffer("", uv.data(), sizeof(UV), uv.size());
		instanceOffsets = iWorldRenderer->iWorkContexts[0]->CreateSrvBuffer("", offsets.data(), sizeof(unsigned), offsets.size());

		for (int j = 0; j < rtxVertexBuffers.size(); j++) {
			Matrix4x4 matrix;
			matrix = nxt::RotationY(1.57f);

			std::string name = rtxVertexBuffers[j]->name;

			if (name.find("Black") != std::string::npos) {
				matrix._r[3] = 0.0f;
				matrix._r[7] = -7.0f;
				matrix._r[11] = 0.0f;
			}
			else if (name.find("stone") == std::string::npos) {
				matrix._r[3] = 0.0f;
				matrix._r[7] = 0.0f;
				matrix._r[11] = -50.0f;
			}

			if (name.find("elbow_left") != std::string::npos) {
				int index = (j - 2) / 4;
				elbow_left[index]->instanceIndices.push_back(j);
			}
			else if (name.find("elbow_right") != std::string::npos) {
				int index = ((j - 2) - 44) / 4;
				elbow_right[index]->instanceIndices.push_back(j);
			}
			else if (name.find("noodle") != std::string::npos) {

				int index = ((j - 2) - 44 * 2) / 4;
				noodle[index]->instanceIndices.push_back(j);
			}
			else if (name.find("zig_left") != std::string::npos) {
				int index = ((j - 2) - 44 * 3) / 4;
				zig_left[index]->instanceIndices.push_back(j);
			}
			else if (name.find("zig_right") != std::string::npos) {
				int index = ((j - 2) - 44 * 4) / 4;
				zig_right[index]->instanceIndices.push_back(j);
			}
			else if (name.find("square") != std::string::npos) {
				int index = ((j - 2) - 44 * 5) / 4;
				square[index]->instanceIndices.push_back(j);
			}
			else if (name.find("tee") != std::string::npos) {
				int index = ((j - 2) - 44 * 6) / 4;
				tee[index]->instanceIndices.push_back(j);
			}

			ms.push_back(matrix);
		}

		BuildInstanceMap(elbow_left);
		BuildInstanceMap(elbow_right);
		BuildInstanceMap(noodle);
		BuildInstanceMap(tee);
		BuildInstanceMap(zig_left);
		BuildInstanceMap(zig_right);
		BuildInstanceMap(square);


		unsigned int idx = 0;
		for (const auto& entry : IMesh::rtxMats) {
			cbMaterialRTX->type[idx] = IMesh::rtxMats[idx];
			idx++;
		}

		rtxModelData = new nxt::RTXModelData();
		rtxModelData->numberOfModels = rtxVertexBuffers.size();
		rtxModelData->scene = nullptr;
		rtxModelData->verticeCount = vertexSizes;
		rtxModelData->indexBuffers = rtxIndexBuffers;
		rtxModelData->vertexBuffers = rtxVertexBuffers;
		rtxModelData->indiceCount = rtxIndiceSizes;
		rtxModelData->matrices = ms;
	}

	srand(time(NULL));
	currentPiece = nullptr;
	piecesAtPlay.resize(7);

	iWorldRenderer->Initialize(rtxModelData);
	iAS = iWorldRenderer->GetIAS();
}

void UpdateAccelerationStructure(IAccelerationStructure* iAS, std::vector<RTXetrisPiece*> pieces) {
	for (int i = 0; i < pieces.size(); i++) {
		if (pieces[i]->onStage) {
			std::vector<bool> componentsOnMap = pieces[i]->GetComponentsOnMap();
			int idx = 0;
			for (auto& mapItem : pieces[i]->instanceIndicesMap) {
				for (int instanceIndex : mapItem.second) {
					float z = 0.0f;
					if (componentsOnMap[idx] == false) {
						z = 50.0f;
					}

					Matrix4x4 rtxWorldMat;
					rtxWorldMat.SetIdentity();
					memcpy(rtxWorldMat._r, pieces[i]->GetRTXWorldMatrix()._r, sizeof(float) * 16);
					rtxWorldMat._r[11] = z;

					memcpy(iAS->instances[instanceIndex].transform._r, rtxWorldMat._r, sizeof(float) * 16);
					idx++;
				}
			}
		}
	}
}

void RTXetrisStage::Update(float dt, int mouseButtons) {
	this->dt = dt;

	std::vector<uint8_t> lines;
	for (int y = 0; y < 14; y++) {
		bool lineFound = true;
		for (int x = 0; x < 9; x++) {
			int arrIndex = y * 9 + x;

			if (stage[arrIndex] != 1) {
				lineFound = false;
			}
		}

		if (lineFound) {
			lines.push_back(y);
		}
	}



	bool transform = false;
	if (currentPiece == nullptr) {

		if (pieceRotationCount == 7) {
			
			pieceRotation[0] = pieceRotation[7];
			pieceRotation[1] = pieceRotation[8];
			pieceRotation[2] = pieceRotation[9];

			for (int i = 3; i < 10; i++)
				pieceRotation[i] = PieceType::NO_PIECE;

			pieceRotationCount = 0;
			firstRotation = false;
		}

		uint8_t pieceType;
		if (pieceRotationCount == 0 && firstRotation) {
			for (int i = 0; i < 10; i++) {
				while (1) {
					bool newPiece = true;
					pieceType = rand() % 7;

					if (i < 7) {
						for (int j = 0; j < 7; j++) {
							if (pieceType == pieceRotation[j]) {
								newPiece = false;
								break;
							}
						}
					}
					else {
						for (int j = 3; j < 10; j++) {
							if (pieceType == pieceRotation[j]) {
								newPiece = false;
								break;
							}
						}
					}

					if (newPiece) {
						pieceRotation[i] = (PieceType)pieceType;
						break;
					}
				}
			}
		}
		else if (pieceRotationCount == 0 && !firstRotation) {

			for (int i = 3; i < 10; i++) {
				while (1) {
					bool newPiece = true;
					pieceType = rand() % 7;
					
					if (i < 7) {
						for (int j = 0; j < 7; j++) {
							if (pieceType == pieceRotation[j]) {
								newPiece = false;
								break;
							}
						}
					}
					else {
						for (int j = 7; j < 10; j++) {
							if (pieceType == pieceRotation[j]) {
								newPiece = false;
								break;
							}
						}
					}


					if (newPiece) {
						pieceRotation[i] = (PieceType)pieceType;
						break;
					}
				}
			}
		}

		pieceType = pieceRotation[pieceRotationCount];

		switch (pieceType) {
		case ELBOW_RIGHT:
			currentPiece = elbow_right[piecesAtPlay[pieceType]];
			break;
		case ELBOW_LEFT:
			currentPiece = elbow_left[piecesAtPlay[pieceType]];
			break;
		case NOODLE:
			currentPiece = noodle[piecesAtPlay[pieceType]];
			break;
		case ZIG_LEFT:
			currentPiece = zig_left[piecesAtPlay[pieceType]];
			break;
		case ZIG_RIGHT:
			currentPiece = zig_right[piecesAtPlay[pieceType]];
			break;
		case SQUARE:
			currentPiece = square[piecesAtPlay[pieceType]];
			break;
		case TEE:
			currentPiece = tee[piecesAtPlay[pieceType]];
			break;
		}
		piecesAtPlay[pieceType]++;
		pieceRotationCount++;
		currentPiece->PlacePiece(stage);
		currentPiece->active = true;
		transform = true;
	}


	if (lines.size() > 0) {
		for (int i = 0; i < elbow_right.size(); i++) {
			if (elbow_right[i]->onStage)
				elbow_right[i]->CheckInLine(lines.data(), lines.size());
		}

		for (int i = 0; i < elbow_left.size(); i++) {
			if (elbow_left[i]->onStage)
				elbow_left[i]->CheckInLine(lines.data(), lines.size());
		}

		for (int i = 0; i < noodle.size(); i++) {
			if (noodle[i]->onStage)
				noodle[i]->CheckInLine(lines.data(), lines.size());
		}

		for (int i = 0; i < square.size(); i++) {
			if (square[i]->onStage)
				square[i]->CheckInLine(lines.data(), lines.size());
		}

		for (int i = 0; i < zig_left.size(); i++) {
			if (zig_left[i]->onStage)
				zig_left[i]->CheckInLine(lines.data(), lines.size());
		}

		for (int i = 0; i < zig_right.size(); i++) {
			if (zig_right[i]->onStage)
				zig_right[i]->CheckInLine(lines.data(), lines.size());
		}

		for (int i = 0; i < tee.size(); i++) {
			if (tee[i]->onStage)
				tee[i]->CheckInLine(lines.data(), lines.size());
		}


		for (int i = 0; i < lines.size(); i++) {
			for (int x = 0; x < 9; x++) {
				int arrIndex = lines[i] * 9 + x;

				stage[arrIndex] = 0;
			}
		}
	}

	view.SetIdentity();
	//view.SetRotationY(-1.57f);
	view.SetTranslation(0.0f, -1.0f, 20.0f);
	worldX.SetIdentity();
	worldY.SetIdentity();
	proj = ProjectionLH(3840.0f / 2160.0f, 60.0f * 3.14f / 180.0f, 0.1f, 200.0f);

	lightView.SetIdentity();

	Matrix4x4 rotY, rotZ;
	rotY.SetRotationY(0.3f);
	rotZ.SetRotationX(0.3f);

	lightView = MatrixMultiply(rotY, rotZ);
	lightView.SetTranslation(-1.0f, -1.0f, 5.0f);
	lightProj = OrthoLH(15, 15, 0.01f, 15);

	iConstantBufferHelperView->type.cameraPosition[0] = 0.0f; iConstantBufferHelperView->type.cameraPosition[1] = 0.0f;
	iConstantBufferHelperView->type.cameraPosition[2] = 20.0f; iConstantBufferHelperView->type.cameraPosition[3] = 0.0f;

	iConstantBufferHelperPBR->type.ambient = 0.15f;
	iConstantBufferHelperPBR->type.applyOcclusion = 0;
	iConstantBufferHelperPBR->type.iblCubeMapRotationY = 0.0f;
	iConstantBufferHelperPBR->type.maxReflectanceLOD = 9.0f;
	iConstantBufferHelperPBR->type.modulateEmission = 0;

	colorTransisitionDT += dt;
	float t = colorTransisitionDT / 20.0f;
	if (t > 1.0f) {
		colorTransisitionDT = 0.0f;
		currentColorIndex++;
		if (currentColorIndex >= colorValues.size()) {
			currentColorIndex = 0;
		}
	}

	uint8_t nextColorIndex = 0;
	if (currentColorIndex + 1 < colorValues.size()) {
		nextColorIndex = currentColorIndex + 1;
	}

	Vec3 intermediateColor = Vec3Lerp(colorValues[currentColorIndex], colorValues[nextColorIndex], t);

	iConstantBufferHelperBorderColor->type.borderColor[0] = intermediateColor.x;
	iConstantBufferHelperBorderColor->type.borderColor[1] = intermediateColor.y;
	iConstantBufferHelperBorderColor->type.borderColor[2] = intermediateColor.z;
	iConstantBufferHelperBorderColor->type.borderColor[3] = 1.0f;

	viewProj = view * proj;
	lightViewProj = lightView * lightProj;

	if (currentPiece) {
		if ((MouseButtons::DKDN & mouseButtons) == MouseButtons::DKDN) {
			currentPiece->MoveDown(stage);
			transform = true;
			pieceMoveDownDelta = 0.0f;
		}

		if ((MouseButtons::LKDN & mouseButtons) == MouseButtons::LKDN) {
			currentPiece->MoveLeft(stage);
			transform = true;
		}

		if ((MouseButtons::RKDN & mouseButtons) == MouseButtons::RKDN) {
			currentPiece->MoveRight(stage);
			transform = true;
		}

		if ((MouseButtons::KUP & mouseButtons) == MouseButtons::KUP) {
			currentPiece->RotateRight(stage);
			transform = true;		
		}

		pieceMoveDownDelta += dt;
		if (pieceMoveDownDelta > 1.0f) {
			if (!currentPiece->MoveDown(stage)) {
				pieceDisableTimerStart = true;
			}
			else {
				pieceDisableTimerStart = false;
				pieceDisableTimer = 0.0f;
			}

			transform = true;
			pieceMoveDownDelta = 0.0f;
		}

		if (pieceDisableTimerStart) {
			pieceDisableTimer += dt;
			if (pieceDisableTimer > 0.25f) {
				pieceDisableTimer = 0.0f;
				pieceDisableTimerStart = false;
				currentPiece = nullptr;
				transform = false;
			}
		}

		if (iWorldRenderer->isDXRSupported && transform) {
			for (int i = 0; i < currentPiece->instanceIndices.size(); i++) {
				
				std::vector<bool> componentsOnMap = currentPiece->GetComponentsOnMap();
				
				float z = 0.0f;
				if (componentsOnMap[i] == false) {
					if (currentPiece->getPieceType() == SQUARE) {
						int x = 12;
					}
					z = 50.0f;
				}
				
				memcpy(iAS->instances[currentPiece->instanceIndices[i]].transform._r, currentPiece->GetRTXWorldMatrix()._r, sizeof(float) * 16);
				iAS->instances[currentPiece->instanceIndices[i]].transform._r[11] = z;
			}
			iAS->dirty = true;
		}
		
		if (iWorldRenderer->isDXRSupported && lines.size() > 0) {

			UpdateAccelerationStructure(iAS, elbow_right);
			UpdateAccelerationStructure(iAS, elbow_left);
			UpdateAccelerationStructure(iAS, noodle);
			UpdateAccelerationStructure(iAS, square);
			UpdateAccelerationStructure(iAS, zig_left);
			UpdateAccelerationStructure(iAS, zig_right);
			UpdateAccelerationStructure(iAS, tee);

			iAS->dirty = true;
		}
	}

	if (currentPiece && !currentPiece->active)
		currentPiece = nullptr;
}

void RTXetrisStage::Render() {

	//world.SetTranslation(6, 0, 10);

	world = world;
	world.SetRotationY(-1.57f);
	//world.SetTranslation(0, 0, 0);
	iConstantBufferHelperPBR->type.specularContribution = 1.0f;

	iConstantBufferHelper->type.worldRotate = lightViewProj;
	iConstantBufferHelper->type.world = world;
	iConstantBufferHelper->type.viewProj = viewProj;
	iConstantBufferHelper->type.wvp = world * viewProj;

	IConstantBuffer* viewCB = iConstantBufferHelperView->Update();
	iConstantBufferHelperPBR->type.ambient = 0.0f;
	iConstantBufferHelperPBR->type.modulateEmission = 0.015f;
	iConstantBufferHelperPBR->type.applyOcclusion = 2;
	iConstantBufferHelperPBR->type.maxReflectanceLOD = 1.5f;
	iConstantBufferHelperPBR->type.specularContribution = 3.0f;
	/*
	iWorldRenderer->SubmitMesh(
		rtxetrisStage,
		{
			{0, iConstantBufferHelper->Update()},
			{1, viewCB},
			{2, iConstantBufferHelperPBR->Update()}
		},
		{},
		1,
		nullptr,
		false
	);
	*/

	iConstantBufferHelperBorderColor->type.sine = 1;

	iWorldRenderer->SubmitMesh(
		rtxBorder,
		{
			{0, iConstantBufferHelper->Update()},
			{2, iConstantBufferHelperBorderColor->Update()}
		},
		{},
		{true, true, true, true},
		1,
		nullptr,
		false
	);
	
	iConstantBufferHelperPBR->type.maxReflectanceLOD = 0.0f;




	for (int i = 0; i < tee.size(); i++) {
		if (tee[i]->onStage) {
			iConstantBufferHelperPBR->type.specularContribution = 2.25f;
			//iConstantBufferHelper->type.worldRotate = worldRot;
			iConstantBufferHelper->type.world = tee[i]->GetWorldMatrix();
			iConstantBufferHelper->type.viewProj = viewProj;
			iConstantBufferHelper->type.wvp = tee[i]->GetWorldMatrix() * viewProj;
			iConstantBufferHelperPBR->type.modulateEmission = 0.009f;

			iConstantBufferHelperPBR->type.ambient = 0.075f;
			iConstantBufferHelperPBR->type.applyOcclusion = 2;

			iWorldRenderer->SubmitMesh(
				tee[i]->rtxetrisPiece,
				{
					{0, iConstantBufferHelper->Update()},
					{1, viewCB},
					{2, iConstantBufferHelperPBR->Update()}
				},
				{},
				tee[i]->GetComponentsOnMap()
			);

			iConstantBufferHelperPBR->type.specularContribution = 3.0f;
		}
	}

	for (int i = 0; i < noodle.size(); i++) {
		if (noodle[i]->onStage) {

			//iConstantBufferHelper->type.worldRotate = worldRot;
			iConstantBufferHelper->type.world = noodle[i]->GetWorldMatrix();
			iConstantBufferHelper->type.viewProj = viewProj;
			iConstantBufferHelper->type.wvp = noodle[i]->GetWorldMatrix() * viewProj;

			iConstantBufferHelperPBR->type.ambient = 0.20f;
			iConstantBufferHelperPBR->type.modulateEmission = 0.009f;
			iConstantBufferHelperPBR->type.applyOcclusion = 2;

			iWorldRenderer->SubmitMesh(
				noodle[i]->rtxetrisPiece,
				{
					{0, iConstantBufferHelper->Update()},
					{1, viewCB},
					{2, iConstantBufferHelperPBR->Update()}
				},
				{},
				noodle[i]->GetComponentsOnMap()
			);
		}
	}

	for (int i = 0; i < elbow_right.size(); i++) {
		if (elbow_right[i]->onStage) {

			iConstantBufferHelperPBR->type.specularContribution = 3.0f;
			//iConstantBufferHelper->type.worldRotate = worldRot;
			iConstantBufferHelper->type.world = elbow_right[i]->GetWorldMatrix();
			iConstantBufferHelper->type.viewProj = viewProj;
			iConstantBufferHelper->type.wvp = elbow_right[i]->GetWorldMatrix() * viewProj;

			iConstantBufferHelperPBR->type.ambient = 0.2f;
			iConstantBufferHelperPBR->type.modulateEmission = 0.009f;
			iConstantBufferHelperPBR->type.applyOcclusion = 2;

			iWorldRenderer->SubmitMesh(
				elbow_right[i]->rtxetrisPiece,
				{
					{0, iConstantBufferHelper->Update()},
					{1, viewCB},
					{2, iConstantBufferHelperPBR->Update()}
				},
				{},
				elbow_right[i]->GetComponentsOnMap()
			);
		}

	}

	for (int i = 0; i < elbow_left.size(); i++) {
		if (elbow_left[i]->onStage) {
			iConstantBufferHelperPBR->type.specularContribution = 2.5f;

			//iConstantBufferHelper->type.worldRotate = worldRot;
			iConstantBufferHelper->type.world = elbow_left[i]->GetWorldMatrix();
			iConstantBufferHelper->type.viewProj = viewProj;
			iConstantBufferHelper->type.wvp = elbow_left[i]->GetWorldMatrix() * viewProj;

			iConstantBufferHelperPBR->type.ambient = 0.075f;
			iConstantBufferHelperPBR->type.modulateEmission = 0.009f;
			iConstantBufferHelperPBR->type.applyOcclusion = 2;

			iWorldRenderer->SubmitMesh(
				elbow_left[i]->rtxetrisPiece,
				{
					{0, iConstantBufferHelper->Update()},
					{1, viewCB},
					{2, iConstantBufferHelperPBR->Update()}
				},
				{},
				elbow_left[i]->GetComponentsOnMap()
			);

			iConstantBufferHelperPBR->type.specularContribution = 3.0f;
		}
	}

	for (int i = 0; i < square.size(); i++) {
		if (square[i]->onStage) {

			//iConstantBufferHelper->type.worldRotate = worldRot;
			iConstantBufferHelper->type.world = square[i]->GetWorldMatrix();
			iConstantBufferHelper->type.viewProj = viewProj;
			iConstantBufferHelper->type.wvp = square[i]->GetWorldMatrix() * viewProj;

			iConstantBufferHelperPBR->type.ambient = 0.05f;
			iConstantBufferHelperPBR->type.modulateEmission = 0.009f;

			iConstantBufferHelperPBR->type.applyOcclusion = 4;

			iWorldRenderer->SubmitMesh(
				square[i]->rtxetrisPiece,
				{
					{0, iConstantBufferHelper->Update()},
					{1, viewCB},
					{2, iConstantBufferHelperPBR->Update()}
				},
				{},
				square[i]->GetComponentsOnMap()
			);
		}
	}

	for (int i = 0; i < zig_right.size(); i++) {
		if (zig_right[i]->onStage) {


			//iConstantBufferHelper->type.worldRotate = worldRot;
			iConstantBufferHelper->type.world = zig_right[i]->GetWorldMatrix();
			iConstantBufferHelper->type.viewProj = viewProj;
			iConstantBufferHelper->type.wvp = zig_right[i]->GetWorldMatrix() * viewProj;

			iConstantBufferHelperPBR->type.ambient = 0.2f;
			iConstantBufferHelperPBR->type.modulateEmission = 0.009f;

			iConstantBufferHelperPBR->type.applyOcclusion = 2;

			iWorldRenderer->SubmitMesh(
				zig_right[i]->rtxetrisPiece,
				{
					{0, iConstantBufferHelper->Update()},
					{1, viewCB},
					{2, iConstantBufferHelperPBR->Update()}
				},
				{},
				zig_right[i]->GetComponentsOnMap()
			);
		}

	}

	for (int i = 0; i < zig_left.size(); i++) {
		if (zig_left[i]->onStage) {

			iConstantBufferHelperPBR->type.ambient = 0.2f;

			//iConstantBufferHelper->type.worldRotate = worldRot;
			iConstantBufferHelper->type.world = zig_left[i]->GetWorldMatrix();
			iConstantBufferHelper->type.viewProj = viewProj;
			iConstantBufferHelper->type.wvp = zig_left[i]->GetWorldMatrix() * viewProj;

			iConstantBufferHelperPBR->type.modulateEmission = 0.009f;
			iConstantBufferHelperPBR->type.applyOcclusion = 2;

			iWorldRenderer->SubmitMesh(
				zig_left[i]->rtxetrisPiece,
				{
					{0, iConstantBufferHelper->Update()},
					{1, viewCB},
					{2, iConstantBufferHelperPBR->Update()}
				},
				{},
				zig_left[i]->GetComponentsOnMap()
			);
		}
	}

	
	int loopCount = 0;
	for (int i = pieceRotationCount; i < (pieceRotationCount) + 1; i++) {

		uint8_t index = 0;
		if (i >= pieceRotation.size()) {
			index = i - 7;
		}
		else {
			index = i;
		}

		float distance = 2.0f;
		float y = 7.25;
		Matrix4x4 worldMatrix, translateMatrix, scaleMatrix;

		if (pieceRotation[i] == TEE || pieceRotation[i] == ZIG_LEFT || pieceRotation[i] == ZIG_RIGHT || pieceRotation[i] == ELBOW_LEFT) {
			y = 6.65f;
		}
		else if (pieceRotation[i] == ELBOW_RIGHT) {
			y = 6.25f;
		}
		else if (pieceRotation[i] == SQUARE) {
			y = 6.5f;
		}
		else if (pieceRotation[i] == NOODLE) {
			y = 6.75f;
		}

		translateMatrix = Translation(6.15f + (loopCount * distance), y, 0.0f);

		scaleMatrix.SetIdentity();
		scaleMatrix.SetScale(0.6f, 0.6f, 0.6f);
		worldMatrix = nxt::MatrixMultiply(scaleMatrix, translateMatrix);

		Matrix4x4 rotY;
		rotY.SetRotationY(-1.57f);

		worldMatrix = nxt::MatrixMultiply(rotY, worldMatrix);

		iConstantBufferHelper->type.world = worldMatrix;
		iConstantBufferHelper->type.viewProj = viewProj;
		iConstantBufferHelper->type.wvp = worldMatrix * viewProj;

		switch (pieceRotation[index]) {
		case ZIG_LEFT:
			iConstantBufferHelperPBR->type.ambient = 0.2f;
			iConstantBufferHelperPBR->type.modulateEmission = 0.009f;
			iConstantBufferHelperPBR->type.applyOcclusion = 2;

			iWorldRenderer->SubmitMesh(
				zig_left[0]->rtxetrisPiece,
				{
					{0, iConstantBufferHelper->Update()},
					{1, viewCB},
					{2, iConstantBufferHelperPBR->Update()}
				},
				{},
				{true, true, true, true},
				1,
				nullptr,
				false
			);
			break;
		case ZIG_RIGHT:
			iConstantBufferHelperPBR->type.ambient = 0.2f;
			iConstantBufferHelperPBR->type.modulateEmission = 0.009f;

			iConstantBufferHelperPBR->type.applyOcclusion = 2;

			iWorldRenderer->SubmitMesh(
				zig_right[0]->rtxetrisPiece,
				{
					{0, iConstantBufferHelper->Update()},
					{1, viewCB},
					{2, iConstantBufferHelperPBR->Update()}
				},
				{},
				{ true, true, true, true },
				1,
				nullptr,
				false
			);
			break;
		case NOODLE:

			iConstantBufferHelperPBR->type.ambient = 0.20f;
			iConstantBufferHelperPBR->type.modulateEmission = 0.009f;
			iConstantBufferHelperPBR->type.applyOcclusion = 2;

			iWorldRenderer->SubmitMesh(
				noodle[i]->rtxetrisPiece,
				{
					{0, iConstantBufferHelper->Update()},
					{1, viewCB},
					{2, iConstantBufferHelperPBR->Update()}
				},
				{},
				{ true, true, true, true },
				1,
				nullptr,
				false
			);
			break;
		case SQUARE:
			iConstantBufferHelperPBR->type.ambient = 0.05f;
			iConstantBufferHelperPBR->type.modulateEmission = 0.009f;

			iConstantBufferHelperPBR->type.applyOcclusion = 4;

			iWorldRenderer->SubmitMesh(
				square[0]->rtxetrisPiece,
				{
					{0, iConstantBufferHelper->Update()},
					{1, viewCB},
					{2, iConstantBufferHelperPBR->Update()}
				},
				{},
				{ true, true, true, true },
				1,
				nullptr,
				false
			);
			break;
		case ELBOW_LEFT:
			iConstantBufferHelperPBR->type.specularContribution = 2.5f;
			iConstantBufferHelperPBR->type.ambient = 0.075f;
			iConstantBufferHelperPBR->type.modulateEmission = 0.009f;
			iConstantBufferHelperPBR->type.applyOcclusion = 2;

			iWorldRenderer->SubmitMesh(
				elbow_left[0]->rtxetrisPiece,
				{
					{0, iConstantBufferHelper->Update()},
					{1, viewCB},
					{2, iConstantBufferHelperPBR->Update()}
				},
				{},
				{ true, true, true, true },
				1,
				nullptr,
				false
			);

			iConstantBufferHelperPBR->type.specularContribution = 3.0f;
			break;
		case ELBOW_RIGHT:

			iConstantBufferHelperPBR->type.specularContribution = 3.0f;
			//iConstantBufferHelper->type.worldRotate = worldRot;

			iConstantBufferHelperPBR->type.ambient = 0.2f;
			iConstantBufferHelperPBR->type.modulateEmission = 0.009f;
			iConstantBufferHelperPBR->type.applyOcclusion = 2;

			iWorldRenderer->SubmitMesh(
				elbow_right[0]->rtxetrisPiece,
				{
					{0, iConstantBufferHelper->Update()},
					{1, viewCB},
					{2, iConstantBufferHelperPBR->Update()}
				},
				{},
				{ true, true, true, true },
				1,
				nullptr,
				false
			);
			break;
		case TEE:
			iConstantBufferHelperPBR->type.specularContribution = 2.25f;
			//iConstantBufferHelper->type.worldRotate = worldRot;
			iConstantBufferHelperPBR->type.modulateEmission = 0.009f;

			iConstantBufferHelperPBR->type.ambient = 0.075f;
			iConstantBufferHelperPBR->type.applyOcclusion = 2;

			iWorldRenderer->SubmitMesh(
				tee[i]->rtxetrisPiece,
				{
					{0, iConstantBufferHelper->Update()},
					{1, viewCB},
					{2, iConstantBufferHelperPBR->Update()}
				},
				{},
				{ true, true, true, true },
				1,
				nullptr,
				false
			);

			iConstantBufferHelperPBR->type.specularContribution = 3.0f;
			break;
		}
		loopCount++;
	}

	if (iWorldRenderer->isDXRSupported) {
		rtxModelData->bitangs = bitangs;
		rtxModelData->uvs = uvs;
		rtxModelData->normals = normals;
		rtxModelData->indices = indices;
		rtxModelData->positions = positions;
		rtxModelData->materialIndices = materialIndices;
		rtxModelData->instanceOffsets = instanceOffsets;
		rtxModelData->tangs = tangs;
		rtxModelData->iCB = cbMaterialRTX->Update();
		rtxModelData->textures = textures;
		//rtxModelData->bitangs = bitangs;


		iWorldRenderer->SubmitRTXDispatchRequest(iAS, rtxModelData);
	}
	iWorldRenderer->Draw(dt, viewCB, worldRot);
}
