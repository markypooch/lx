#pragma once

#include "NxT/IGraphics.h"
#include "NxT/IMesh.h"
#include "NxT/IWorldRenderer.h"

namespace nxt {

	struct PassInput {
		IRenderTarget** renderTargets;
		ITexture** iTextures;
		IWorldRenderer::WorldRendererMesh** worldRendererMeshes;

		unsigned int numberOfRenderTargets;
		unsigned int numberOfTextures;
		unsigned int numberOfMeshes;
	};

	class Pass {
	public:
		virtual void run() = 0;
	public:
		bool ran = false;
	};
};