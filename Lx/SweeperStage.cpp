#include "SweeperStage.h"
#include "NxT/Direct3D12/D3D12ConstantBuffer.h"
#include "NxT/IGraphics.h"
#include <iostream>
#include <DirectXTex.h>
#include <random>
using namespace sweeper;
using namespace nxt;

#define EPSILON 0.0001f

ITexture* LoadDDSTexture(const char* name, WCHAR* filePath, IWorldRenderer* iWorldRenderer) {
	
	DirectX::ScratchImage* imageData = new DirectX::ScratchImage();
	HRESULT loadResult = DirectX::LoadFromDDSFile(filePath, DirectX::DDS_FLAGS_NONE, nullptr, *imageData);

	void* pData           = imageData->GetImage(0, 0, 0)->pixels;
	unsigned int width    = imageData->GetImage(0, 0, 0)->width;
	unsigned int height   = imageData->GetImage(0, 0, 0)->height;
	unsigned int mipCount = imageData->GetMetadata().mipLevels;
	DXGI_FORMAT format    = imageData->GetImage(0, 0, 0)->format;
	size_t rowPitch       = imageData->GetImage(0, 0, 0)->rowPitch;
	size_t slicePitch     = imageData->GetImage(0, 0, 0)->slicePitch;
	size_t arrSize        = imageData->GetImageCount() / mipCount;

	ITexture* ibl_diffuse = iWorldRenderer->iWorkContexts[0]->CreateTexture(name, pData, width, height, rowPitch, slicePitch, DXGIToNXTFormat(format), UsageFlags::NONE, mipCount, arrSize);

	for (int i = 0; i < mipCount; i++) {
		for (int j = 0; j < arrSize; j++) {
			const DirectX::Image* image = imageData->GetImage(i, j, 0);
			size_t totalSlizePitch =0;
			if (image) {
				void* pData = image->pixels;
				unsigned int width = image->width;
				unsigned int height = image->height;
				DXGI_FORMAT format = image->format;
				size_t rowPitch = image->rowPitch;
				size_t slicePitch = image->slicePitch;
				unsigned int mipIndex = i;

				iWorldRenderer->iWorkContexts[0]->UpdateTextureMip(ibl_diffuse, pData, i, rowPitch, slicePitch, j, mipCount, arrSize, totalSlizePitch);
				totalSlizePitch += slicePitch;
			}
		}
	}

	return ibl_diffuse;
}

void SweeperStage::CheckInput(int mouseButtons) {
	
	
}

void SweeperStage::GenerateStage() {
	float z = -13.25f;
	float y = -13.25f;
	float x = -13.25f;

	int metCubeProbability = 1 / 10;
	int crystalProbability = 1 / 20;

	for (int i = 0; i < 1000; i++) {
		if (i % 10 == 0) {
			z += 2.6f;
			x = -13.25f;
		}

		if (i % 100 == 0)
		{
			x = -13.25f;
			z = -13.25f;
			y += 2.6f;
		}

		metCubeProbability = rand() % 20 + 1;
		crystalProbability = rand() % 50 + 1;
		///if (metCubeProbability == 1)
		//	metCubeTransforms.push_back(Transforms{ x, y, z, 0.0f });
		if (crystalProbability == 1) {
			int isRed = rand() % 3 + 1;
			int isBlue = rand() % 3 + 1;
			float color[4]{};

			if (isRed == 1)
				color[0] = 0.7f;
			else if (isBlue == 1)
				color[2] = 0.7f;
			else
				color[1] = 0.7f;

			crystals.push_back(Crystals{
				x, y, z, 0.0f, 
				color[0], color[1], color[2], color[3]
			});
		}
		else
			whiteCubeTransforms.push_back(Transforms{ x, y, z, 0.0f });

		x += 2.6f;

		cubes.push_back(SweeperCube(x, y, z));
	}

	iInstanceCrystal = iWorldRenderer->iWorkContexts[0]->CreateInstanceBuffer("iic", crystals.data(), sizeof(Crystals), crystals.size());
	iInstanceBuffer = iWorldRenderer->iWorkContexts[0]->CreateInstanceBuffer("iiWc", whiteCubeTransforms.data(), sizeof(Transforms), whiteCubeTransforms.size());
	//iInstanceMet = iWorldRenderer->iWorkContexts[0]->CreateInstanceBuffer("iiMc", metCubeTransforms.data(), sizeof(Transforms), metCubeTransforms.size());
}

void SweeperStage::Initialize() {
	
	iConstantBufferHelper = new D3D12ConstantBufferHelper<wvp>(iWorldRenderer->iGraphics, iWorldRenderer->iWorkContexts[0]);
	iConstantBufferHelper->Initialize(1024);

	iConstantBufferHelperView = new D3D12ConstantBufferHelper<View>(iWorldRenderer->iGraphics, iWorldRenderer->iWorkContexts[0]);
	iConstantBufferHelperView->Initialize(9);

	iConstantBufferHelperPBR = new D3D12ConstantBufferHelper<PBRAppSettings>(iWorldRenderer->iGraphics, iWorldRenderer->iWorkContexts[0]);
	iConstantBufferHelperPBR->Initialize(1024);

	whiteCube = iWorldRenderer->LoadModel("Assets/Models/techcube/techcubelet.gltf", MeshType::GLTF);
	metCube = iWorldRenderer->LoadModel("Assets/Models/MetallicCube/metCube.gltf", MeshType::GLTF);
	//mine = iWorldRenderer->LoadModel("Assets/Models/Mine/Mine.gltf", MeshType::GLTF);
	crystal = iWorldRenderer->LoadModel("Assets/Models/Crystal/crystal.gltf", MeshType::GLTF);

	ibl_diffuse  = LoadDDSTexture("ibl_diffuse",  L"Assets/Textures/ibl_diffuse.dds",  iWorldRenderer);
	ibl_specular = LoadDDSTexture("ibl_specular", L"Assets/Textures/ibl_specular.dds", iWorldRenderer);
	brdf_lut     = LoadDDSTexture("brdf_lut",     L"Assets/Textures/brdf_lut.dds",     iWorldRenderer);

	angleRot = 0.0f;
	world.SetIdentity();
	viewProj.SetIdentity();
	worldZ.SetIdentity();

	GenerateStage();

	std::vector<unsigned int> vertexSizes;
	std::vector<IIndexBuffer*> rtxIndexBuffers;
	std::vector<IVertexBuffer*> rtxVertexBuffers;
	std::vector<unsigned int> rtxIndiceSizes;
	std::vector<Matrix4x4> ms;

	for (int i = 0; i < 256; i++) {
		vertexSizes.insert(vertexSizes.end(), whiteCube->rtxVertexSizes.begin(), whiteCube->rtxVertexSizes.end());
		//vertexSizes.insert(vertexSizes.end(), whiteCube->rtxVertexSizes.begin(), whiteCube->rtxVertexSizes.end());
		//vertexSizes.insert(vertexSizes.end(), whiteCube->rtxVertexSizes.begin(), whiteCube->rtxVertexSizes.end());
		//vertexSizes.insert(vertexSizes.end(), whiteCube->rtxVertexSizes.begin(), whiteCube->rtxVertexSizes.end());

		rtxIndexBuffers.insert(rtxIndexBuffers.end(), whiteCube->iRTXIndexBuffers.begin(), whiteCube->iRTXIndexBuffers.end());
		//rtxIndexBuffers.insert(rtxIndexBuffers.end(), whiteCube->iRTXIndexBuffers.begin(), whiteCube->iRTXIndexBuffers.end());
		//rtxIndexBuffers.insert(rtxIndexBuffers.end(), whiteCube->iRTXIndexBuffers.begin(), whiteCube->iRTXIndexBuffers.end());
		//rtxIndexBuffers.insert(rtxIndexBuffers.end(), whiteCube->iRTXIndexBuffers.begin(), whiteCube->iRTXIndexBuffers.end());

		rtxVertexBuffers.insert(rtxVertexBuffers.end(), whiteCube->iRTXVertexBuffers.begin(), whiteCube->iRTXVertexBuffers.end());
		//rtxVertexBuffers.insert(rtxVertexBuffers.end(), whiteCube->iRTXVertexBuffers.begin(), whiteCube->iRTXVertexBuffers.end());
		//rtxVertexBuffers.insert(rtxVertexBuffers.end(), whiteCube->iRTXVertexBuffers.begin(), whiteCube->iRTXVertexBuffers.end());
		//rtxVertexBuffers.insert(rtxVertexBuffers.end(), whiteCube->iRTXVertexBuffers.begin(), whiteCube->iRTXVertexBuffers.end());

		rtxIndiceSizes.insert(rtxIndiceSizes.end(), whiteCube->rtxIndexSizes.begin(), whiteCube->rtxIndexSizes.end());
		//rtxIndiceSizes.insert(rtxIndiceSizes.end(), whiteCube->rtxIndexSizes.begin(), whiteCube->rtxIndexSizes.end());
		//rtxIndiceSizes.insert(rtxIndiceSizes.end(), whiteCube->rtxIndexSizes.begin(), whiteCube->rtxIndexSizes.end());
		//rtxIndiceSizes.insert(rtxIndiceSizes.end(), whiteCube->rtxIndexSizes.begin(), whiteCube->rtxIndexSizes.end());

		Matrix4x4 matrix;
		matrix.SetIdentity();

		matrix._r[3] = cubes[i].getX();
		matrix._r[7] = cubes[i].getY();
		matrix._r[11] = cubes[i].getZ();

		ms.push_back(matrix);
		ms.push_back(matrix);
	}

	nxt::RTXModelData *rtxModelData = new nxt::RTXModelData();
	rtxModelData->numberOfModels = 2;
	rtxModelData->scene = whiteCube;
	rtxModelData->verticeCount = vertexSizes;
	rtxModelData->indexBuffers = rtxIndexBuffers;
	rtxModelData->vertexBuffers = rtxVertexBuffers;
	rtxModelData->indiceCount = rtxIndiceSizes;
	rtxModelData->matrices = ms;

	//iWorldRenderer->InitializeRTX(rtxModelData, ibl_diffuse, ibl_specular, brdf_lut);
}

float viewPosZ = 45.0f;
void SweeperStage::Update(float dt, int mouseButtons) {
	angleRot += 0.00045f;

	view.SetIdentity();
	view.SetTranslation(0.0f, 0.0f, viewPosZ);
	worldX.SetIdentity();
	worldY.SetIdentity();
	proj = ProjectionLH(3840.0f / 2160.0f, 60.0f * 3.14f / 180.0f, 0.1f, 200.0f);

	iConstantBufferHelperView->type.cameraPosition[0] = 0.0f; iConstantBufferHelperView->type.cameraPosition[1] = 0.0f;
	iConstantBufferHelperView->type.cameraPosition[2] = 0.0f; iConstantBufferHelperView->type.cameraPosition[3] = 0.0f;

	iConstantBufferHelperPBR->type.ambient = 0.15f;
	iConstantBufferHelperPBR->type.applyOcclusion = 0;
	iConstantBufferHelperPBR->type.iblCubeMapRotationY = 0.0f;
	iConstantBufferHelperPBR->type.maxReflectanceLOD = 9.0f;
	iConstantBufferHelperPBR->type.modulateEmission = 0;

	viewProj = view * proj;
	const float maxRotationAmount = 0.003f;
	float denomX = 0.0f;
	float denomY = 0.0f;
	float dX = 0.0f;
	float dY = 0.0f;
	if ((MouseButtons::LMBD & mouseButtons) == MouseButtons::LMBD || setCursorOrigin) {
		if (!setCursorOrigin) {
			GetCursorPos(&lastCursorPos);
			setCursorOrigin = true;
		}
		POINT currentCursorPos{};
		GetCursorPos(&currentCursorPos);

		//Get the absolute difference, and normalize
		denomX = (currentCursorPos.x - lastCursorPos.x);
		denomY = (currentCursorPos.y - lastCursorPos.y);
		dX = 0.0f;
		dY = 0.0f;
		if (abs(denomX) > 0.01f) {
			dX = denomX / (3840.0f/4.0f);
			if (dX > 1.0f) dX = 1.0f;
			else if (dX < -1.0f) dX = -1.0f;
		}

		if (abs(denomY) > 0.01f) {
			dY = denomY / (2160.0f/4.0f);
			if (dY > 1.0f) dY = 1.0f;
			else if (dY < -1.0f) dY = -1.0f;
		}

		std::cout << dX;
		std::cout << dY;

		if (abs(dY) > 0.05) {//deadzone 
			if (abs(worldRotY) - EPSILON >= 1.15 && abs(worldRotY) - EPSILON <= 1.85) {
				//worldRotX -= maxRotationAmount * dY;
				worldRotX += maxRotationAmount * dY;
			}
			else {
				worldRotX += maxRotationAmount * dY;
			}
		}
		if (abs(dX) > 0.05) {
			if (abs(worldRotY) - EPSILON >= 1.15 && abs(worldRotY) - EPSILON <= 1.85) {
				//worldRotZ += maxRotationAmount * dX;
				worldRotY += maxRotationAmount * dX;
			}
			else {
				worldRotY += maxRotationAmount * dX;
			}
		}

		//worldRotZ = worldRotY - 1.57f;
	}
	
	if ((MouseButtons::LMBU & mouseButtons) == MouseButtons::LMBU) {
		pick = true;
		setCursorOrigin = false;
		GetCursorPos(&lastCursorPos);
	}

	if ((MouseButtons::WHEELU & mouseButtons) == MouseButtons::WHEELU) {
		viewPosZ -= 0.95f;
		if (viewPosZ < 25.0f) viewPosZ = 25.0f;
	}
	if ((MouseButtons::WHEELD & mouseButtons) == MouseButtons::WHEELD) {
		viewPosZ += 0.95f;
		if (viewPosZ > 70.0f) viewPosZ = 70.0f;
	}

	worldX.SetRotationX(worldRotX);
	worldY.SetRotationY(worldRotY);

	if (abs(worldRotX) - EPSILON >= 1.15 && abs(worldRotX) - EPSILON <= 1.85) {
		worldRot = worldX * worldY;
	}
	else {

		worldRot = worldY * worldX;
	}
}

void SweeperStage::Render() {

	//world.SetTranslation(6, 0, 10);

	world = world;
	world.SetTranslation(0, 0, 0);
	iConstantBufferHelperPBR->type.specularContribution = 1.0f;

	iConstantBufferHelper->type.worldRotate = worldRot;
	iConstantBufferHelper->type.world = world;
	iConstantBufferHelper->type.viewProj = viewProj;
	iConstantBufferHelper->type.wvp = world * viewProj;


	IConstantBuffer* viewCB = iConstantBufferHelperView->Update();

	iWorldRenderer->SubmitMesh(
		whiteCube,
		{
			{0, iConstantBufferHelper->Update()},
			{1, viewCB},
			{2, iConstantBufferHelperPBR->Update()}
		},
		{
			{5, ibl_diffuse},
			{6, ibl_specular},
			{7, brdf_lut}
		},
		{},
		whiteCubeTransforms.size(),
		iInstanceBuffer
	);
	/*
	iConstantBufferHelperPBR->type.specularContribution = 1.0f;

	//world.SetTranslation(-6, 0, 10);
	iConstantBufferHelperPBR->type.maxReflectanceLOD = 8.0f;
	iConstantBufferHelperPBR->type.ambient = 0.0f;

	iConstantBufferHelper->type.world = world;
	iConstantBufferHelper->type.viewProj = viewProj;
	iConstantBufferHelper->type.wvp = world * viewProj;
	iWorldRenderer->SubmitMesh(
		metCube,
		{
			{0, iConstantBufferHelper->Update()},
			{1, iConstantBufferHelperView->Update()},
			{2, iConstantBufferHelperPBR->Update()}
		},
		{
			{5, ibl_diffuse},
			{6, ibl_specular},
			{7, brdf_lut}
		},
		metCubeTransforms.size(),
		iInstanceMet
	);*/

	/*
	iConstantBufferHelperPBR->type.maxReflectanceLOD = 8.0f;
	iConstantBufferHelperPBR->type.ambient = 0.0f;

	world.SetRotationY(angleRot);
	world.SetTranslation(0, 0, 10);

	iConstantBufferHelperPBR->type.specularContribution = 0.75f;
	iConstantBufferHelper->type.world = world;
	iConstantBufferHelper->type.viewProj = viewProj;
	iConstantBufferHelper->type.wvp = world * viewProj;
	iWorldRenderer->SubmitForwardMesh(
		crystal,
		{
			{0, iConstantBufferHelper->Update()},
			{1, iConstantBufferHelperView->Update()},
			{2, iConstantBufferHelperPBR->Update()}
		},
		{
			{0, iWorldRenderer->materialTexture},
			{5, ibl_diffuse},
			{6, ibl_specular},
			{7, brdf_lut}
		},
		crystals.size(),
		iInstanceCrystal
	);*/
	
	/*
	world.SetRotationY(0.0f);
	world.SetTranslation(0, 0, 10);
	iConstantBufferHelperPBR->type.maxReflectanceLOD = 9.0f;
	iConstantBufferHelperPBR->type.ambient = 0.5f;
	iConstantBufferHelperPBR->type.specularContribution = 1.0f;

	iConstantBufferHelper->type.world = world;
	iConstantBufferHelper->type.viewProj = viewProj;
	iConstantBufferHelper->type.wvp = world * viewProj;
	iWorldRenderer->SubmitMesh(
		mine,
		{
			{0, iConstantBufferHelper->Update()},
			{1, iConstantBufferHelperView->Update()},
			{2, iConstantBufferHelperPBR->Update()}
		},
		{
			{5, ibl_diffuse},
			{6, ibl_specular},
			{7, brdf_lut}
		}
	);
	*/
	/*
	world.SetTranslation(-10, 0, 20);
	iConstantBufferHelperPBR->type.maxReflectanceLOD = 9.0f;
	iConstantBufferHelperPBR->type.ambient = 0.5f;
	iConstantBufferHelperPBR->type.specularContribution = 1.0f;

	iConstantBufferHelper->type.world = world;
	iConstantBufferHelper->type.viewProj = viewProj;
	iConstantBufferHelper->type.wvp = world * viewProj;
	iWorldRenderer->SubmitMesh(
		mine,
		{
			{0, iConstantBufferHelper->Update()},
			{1, iConstantBufferHelperView->Update()},
			{2, iConstantBufferHelperPBR->Update()}
		},
		{
			{5, ibl_diffuse},
			{6, ibl_specular},
			{7, brdf_lut}
		}
		);

	world.SetTranslation(0, -5, 10);
	iConstantBufferHelperPBR->type.maxReflectanceLOD = 9.0f;
	iConstantBufferHelperPBR->type.ambient = 0.5f;
	iConstantBufferHelperPBR->type.specularContribution = 1.0f;

	iConstantBufferHelper->type.world = world;
	iConstantBufferHelper->type.viewProj = viewProj;
	iConstantBufferHelper->type.wvp = world * viewProj;
	iWorldRenderer->SubmitMesh(
		mine,
		{
			{0, iConstantBufferHelper->Update()},
			{1, iConstantBufferHelperView->Update()},
			{2, iConstantBufferHelperPBR->Update()}
		},
		{
			{5, ibl_diffuse},
			{6, ibl_specular},
			{7, brdf_lut}
		}
		);*/
	/*
	iWorldRenderer->SubmitMesh(t0, 
		{
			{0, iConstantBufferHelper->Update()}
		}, 
		{
			{0, t0->diffuseMap},
			{1, t0->normalMap}
		}
	);

	world.SetTranslation(-2, 0, 5);
	iConstantBufferHelper->type.world = world;

	iWorldRenderer->SubmitMesh(t1, 
		{
			{0, iConstantBufferHelper->Update()}
		}, 
		{
			{0, t1->diffuseMap},
			{1, t1->normalMap}
		}
	);

	world.SetIdentity();
	iConstantBufferHelper->type.world = world;

	iWorldRenderer->SubmitMesh(shadowPlane,
		{
			{0, iConstantBufferHelper->Update()}
		},
		{
			{0, shadowPlane->diffuseMap},
			{1, shadowPlane->normalMap}
		}
	);*/

	//iWorldRenderer->Draw(this->dt, viewCB, worldRot);
}
