#pragma once
#include "NxT/Matrix.h"
namespace nxt {
	class SweeperCube
	{
	public:
		SweeperCube(float posX, float posY, float posZ) {
			this->posX = posX;
			this->posY = posY;
			this->posZ = posZ;
		}

		float getX() const { return posX; }
		float getY() const { return posY; }
		float getZ() const { return posZ; }
	private:
		float posX, posY, posZ;
		nxt::Matrix4x4 m;
	};
};
