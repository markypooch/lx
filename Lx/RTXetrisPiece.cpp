#include "RTXetrisPiece.h"
using namespace sweeper;

RTXetrisPiece::RTXetrisPiece(float posX, float posY, float posZ, nxt::IMesh* rtxetrisPiece, uint8_t pieceType) {
	this->posX = posX;
	this->posY = posY;
	this->posZ = posZ;
	this->rtxetrisPiece = rtxetrisPiece;
	this->pieceType = pieceType;
	this->instanceIndices = {};

	startPosX = 0.0f;
	startPosZ = 0.0f;

	switch (pieceType) {
	case ELBOW_LEFT:
		startPosY = 6.5f;

		coordinates.resize(4);
		coordinates[0] = { -1.0f, 1.0f };
		coordinates[1] = { 0.0f, 1.0f };
		coordinates[2] = { 0.0f, 0.0f };
		coordinates[3] = { 0.0f,-1.0f };

		mapIndices.resize(4);
		mapIndices[0] = { 3.0f, 0.0f };
		mapIndices[1] = { 4.0f, 0.0f };
		mapIndices[2] = { 4.0f, 1.0f };
		mapIndices[3] = { 4.0f, 2.0f };

		subsetBoolMask[0] = { true, true };
		subsetBoolMask[1] = { true };
		subsetBoolMask[2] = { true };

		rtxetrisPiece->modelData->orderedSubsets[0] = { &rtxetrisPiece->modelData->subsets[0], &rtxetrisPiece->modelData->subsets[1] };
		rtxetrisPiece->modelData->orderedSubsets[1] = { &rtxetrisPiece->modelData->subsets[2] };
		rtxetrisPiece->modelData->orderedSubsets[2] = { &rtxetrisPiece->modelData->subsets[3] };

		break;
	case ELBOW_RIGHT:
		startPosY = 6.5f;

		coordinates.resize(4);
		coordinates[0] = { -1.0f, 1.0f };
		coordinates[1] = { -1.0f, 0.0f };
		coordinates[2] = {  0.0f, 0.0f };
		coordinates[3] = {  1.0f, 0.0f };

		mapIndices.resize(4);
		mapIndices[0] = { 3.0f, 0.0f };
		mapIndices[1] = { 3.0f, 1.0f };
		mapIndices[2] = { 4.0f, 1.0f };
		mapIndices[3] = { 5.0f, 1.0f };

		subsetBoolMask[0] = { true };
		subsetBoolMask[1] = { true, true, true };

		rtxetrisPiece->modelData->orderedSubsets[0] = { &rtxetrisPiece->modelData->subsets[0] };
		rtxetrisPiece->modelData->orderedSubsets[1] = { &rtxetrisPiece->modelData->subsets[1], &rtxetrisPiece->modelData->subsets[2], &rtxetrisPiece->modelData->subsets[3] };
		
		break;
	case ZIG_LEFT:
		startPosY = 7.5f;

		coordinates.resize(4);
		coordinates[0] = { -1.0f, 0.0f };
		coordinates[1] = { 0.0f, 0.0f };
		coordinates[2] = { 0.0f, -1.0f };
		coordinates[3] = {  1.0f, -1.0f };

		mapIndices.resize(4);
		mapIndices[0] = { 3.0f, 0.0f };
		mapIndices[1] = { 4.0f, 0.0f };
		mapIndices[2] = { 4.0f, 1.0f };
		mapIndices[3] = { 5.0f, 1.0f };

		subsetBoolMask[0] = { true, true };
		subsetBoolMask[1] = { true, true };

		rtxetrisPiece->modelData->orderedSubsets[0] = { &rtxetrisPiece->modelData->subsets[0],  &rtxetrisPiece->modelData->subsets[1] };
		rtxetrisPiece->modelData->orderedSubsets[1] = { &rtxetrisPiece->modelData->subsets[2],  &rtxetrisPiece->modelData->subsets[3] };

		break;
	case ZIG_RIGHT:
		startPosY = 7.5f;

		coordinates.resize(4);
		coordinates[0] = {  1.0f, 0.0f };
		coordinates[1] = { 0.0f, 0.0f };
		coordinates[2] = { 0.0f,-1.0f };
		coordinates[3] = { -1.0f, -1.0f };

		mapIndices.resize(4);
		mapIndices[0] = { 5.0f, 0.0f };
		mapIndices[1] = { 4.0f, 0.0f };
		mapIndices[2] = { 4.0f, 1.0f };
		mapIndices[3] = { 3.0f, 1.0f };

		subsetBoolMask[0] = { true, true };
		subsetBoolMask[1] = { true, true };

		rtxetrisPiece->modelData->orderedSubsets[0] = { &rtxetrisPiece->modelData->subsets[0],  &rtxetrisPiece->modelData->subsets[1] };
		rtxetrisPiece->modelData->orderedSubsets[1] = { &rtxetrisPiece->modelData->subsets[2],  &rtxetrisPiece->modelData->subsets[3] };
		break;
	case NOODLE:
		startPosY = 6.5f;

		coordinates.resize(4);
		coordinates[0] = { 0.0f, 1.0f };
		coordinates[1] = { 0.0f, 0.0f };
		coordinates[2] = { 0.0f,-1.0f };
		coordinates[3] = { 0.0f,-2.0f };

		mapIndices.resize(4);
		mapIndices[0] = { 4.0f, 0.0f };
		mapIndices[1] = { 4.0f, 1.0f };
		mapIndices[2] = { 4.0f, 2.0f };
		mapIndices[3] = { 4.0f, 3.0f };

		subsetBoolMask[0] = { true };
		subsetBoolMask[1] = { true };
		subsetBoolMask[2] = { true };
		subsetBoolMask[3] = { true };

		rtxetrisPiece->modelData->orderedSubsets[0] = { &rtxetrisPiece->modelData->subsets[0] };
		rtxetrisPiece->modelData->orderedSubsets[1] = { &rtxetrisPiece->modelData->subsets[1] };
		rtxetrisPiece->modelData->orderedSubsets[2] = { &rtxetrisPiece->modelData->subsets[2] };
		rtxetrisPiece->modelData->orderedSubsets[3] = { &rtxetrisPiece->modelData->subsets[3] };

		break;
	case SQUARE:
		startPosX = -0.5f;
		startPosY = 7.0f;

		coordinates.resize(4);
		coordinates[0] = { -1.0f, -1.0f };
		coordinates[1] = { -1.0f,  1.0f };
		coordinates[2] = {  1.0f,  1.0f };
		coordinates[3] = {  1.0f, -1.0f };

		mapIndices.resize(4);
		mapIndices[0] = { 3.0f, 1.0f };
		mapIndices[1] = { 3.0f, 0.0f };
		mapIndices[2] = { 4.0f, 0.0f };
		mapIndices[3] = { 4.0f, 1.0f };

		subsetBoolMask[0] = { true, true };
		subsetBoolMask[1] = { true, true };
	
		rtxetrisPiece->modelData->orderedSubsets[0] = { &rtxetrisPiece->modelData->subsets[0],  &rtxetrisPiece->modelData->subsets[1] };
		rtxetrisPiece->modelData->orderedSubsets[1] = { &rtxetrisPiece->modelData->subsets[2],  &rtxetrisPiece->modelData->subsets[3] };

		break;
	case TEE:
		startPosY = 6.5f;

		coordinates.resize(4);
		coordinates[0] = { 0.0f, 1.0f };
		coordinates[1] = { 0.0f, 0.0f };
		coordinates[2] = {-1.0f, 0.0f };
		coordinates[3] = { 0.0f,-1.0f };

		mapIndices.resize(4);
		mapIndices[0] = { 4.0f, 0.0f };
		mapIndices[1] = { 4.0f, 1.0f };
		mapIndices[2] = { 3.0f, 1.0f };
		mapIndices[3] = { 4.0f, 2.0f };

		subsetBoolMask[0] = { true };
		subsetBoolMask[1] = { true, true };
		subsetBoolMask[2] = { true };

		rtxetrisPiece->modelData->orderedSubsets[0] = { &rtxetrisPiece->modelData->subsets[0] };
		rtxetrisPiece->modelData->orderedSubsets[1] = { &rtxetrisPiece->modelData->subsets[1],  &rtxetrisPiece->modelData->subsets[2] };
		rtxetrisPiece->modelData->orderedSubsets[2] = { &rtxetrisPiece->modelData->subsets[3] };
		break;
	}

	currentRotation = 0.0f;
	rotY.SetRotationY(1.57f);

	rasterRotY.SetRotationY(-1.57f);
	world = nxt::MatrixMultiply(rotY, world);
	rasterWorld = nxt::MatrixMultiply(rasterRotY, rasterWorld);

	pieceComponentsLineified = 0;
}

void RTXetrisPiece::BuildInstanceMap() {
	switch (pieceType) {
	case ELBOW_LEFT:
		
		instanceIndicesMap[0] = { instanceIndices[0], instanceIndices[1] };
		instanceIndicesMap[1] = { instanceIndices[2] };
		instanceIndicesMap[2] = { instanceIndices[3] };

		break;
	case ELBOW_RIGHT:
		
		instanceIndicesMap[0] = { instanceIndices[0] };
		instanceIndicesMap[1] = { instanceIndices[1], instanceIndices[2], instanceIndices[3] };

		break;
	case ZIG_LEFT:

		instanceIndicesMap[0] = { instanceIndices[0], instanceIndices[1] };
		instanceIndicesMap[1] = { instanceIndices[2], instanceIndices[3] };

		break;
	case ZIG_RIGHT:
	
		instanceIndicesMap[0] = { instanceIndices[0], instanceIndices[1] };
		instanceIndicesMap[1] = { instanceIndices[2], instanceIndices[3] };

		break;
	case NOODLE:

		instanceIndicesMap[0] = { instanceIndices[0] };
		instanceIndicesMap[1] = { instanceIndices[1] };
		instanceIndicesMap[2] = { instanceIndices[2] };
		instanceIndicesMap[3] = { instanceIndices[3] };

		break;
	case SQUARE:
		
		instanceIndicesMap[0] = { instanceIndices[0], instanceIndices[1] };
		instanceIndicesMap[1] = { instanceIndices[2], instanceIndices[3] };
		break;
	case TEE:
	
		instanceIndicesMap[0] = { instanceIndices[0] };
		instanceIndicesMap[1] = { instanceIndices[1], instanceIndices[2] };
		instanceIndicesMap[2] = { instanceIndices[3] };

		break;
	}
}

void RTXetrisPiece::RestorePiecePositionOnMap(uint8_t* stageMap) {
	for (int i = 0; i < mapIndices.size(); i++) {
		int arrIndexY = mapIndices[i].y * 9;
		int arrIndexX = mapIndices[i].x;

		//If another piece is in this spot, prevent rotation
		stageMap[arrIndexY + arrIndexX] = 1;
	}
}

void RTXetrisPiece::ClearPiecePositionOnMap(uint8_t* stageMap) {
	for (int i = 0; i < mapIndices.size(); i++) {
		int arrIndexY = mapIndices[i].y * 9;
		int arrIndexX = mapIndices[i].x;

		//If another piece is in this spot, prevent rotation
		stageMap[arrIndexY + arrIndexX] = 0;
	}
}

bool RTXetrisPiece::MovePiece(nxt::Vec2& direction, uint8_t* stageMap) {


	ClearPiecePositionOnMap(stageMap);

	std::vector<nxt::Vec2> tempMapIndices;
	for (int i = 0; i < mapIndices.size(); i++) {
		

		tempMapIndices.push_back({ mapIndices[i].x + direction.x, mapIndices[i].y + direction.y });
		nxt::Vec2 tempMapIndex = tempMapIndices[tempMapIndices.size() - 1];

		//If out of bounds, prevent rotation
		if (tempMapIndex.x < 0 || tempMapIndex.x > 8) {
			RestorePiecePositionOnMap(stageMap);
			return false;
		}

		if (tempMapIndex.y < 0 || tempMapIndex.y > 13) {
			RestorePiecePositionOnMap(stageMap);
			return false;
		}

		int arrIndexY = tempMapIndices[i].y * 9;
		int arrIndexX = tempMapIndices[i].x;

		//If another piece is in this spot, prevent rotation
		if (stageMap[arrIndexY + arrIndexX] != 0) {
			RestorePiecePositionOnMap(stageMap);
			return false;
		}

	}

	//We are not out of bounds, and there are no pieces in the way blocking the rotation.
	UpdateCoordinatesAndMapIndices(stageMap, {}, tempMapIndices);

}

void  RTXetrisPiece::CheckInLine(uint8_t* line, uint8_t lineCount) {
	for (auto& mapItem : subsetBoolMask) {
		for (int i = 0; i < lineCount; i++) {
			if (mapItem.first == line[i]) {
				for (int j = 0; j < mapItem.second.size(); j++) {
					pieceComponentsLineified++;
					mapItem.second[j] = false;
				}
				this->active = false;
			}
		}
	}
}

void RTXetrisPiece::UpdateCoordinatesAndMapIndices(uint8_t* stageMap, const std::vector<nxt::Vec2>& tempCoordinates, const std::vector<nxt::Vec2>& tempMapIndices) {

	auto cachedMap = subsetBoolMask;
	subsetBoolMask.clear();

	auto cachedRenderingOrder = this->rtxetrisPiece->modelData->orderedSubsets;
	this->rtxetrisPiece->modelData->orderedSubsets.clear();

	instanceIndicesMap.clear();

	for (int i = 0; i < tempMapIndices.size(); i++) {

		int oldArrIndexY = mapIndices[i].y * 9;
		int oldArrIndexX = mapIndices[i].x;

		stageMap[oldArrIndexY + oldArrIndexX] = 0;
	}

	for (int i = 0; i < tempMapIndices.size(); i++) {

		int newArrIndexY = tempMapIndices[i].y * 9;
		int newArrIndexX = tempMapIndices[i].x;

		stageMap[newArrIndexY + newArrIndexX] = 1;

		mapIndices[i].x = tempMapIndices[i].x;
		mapIndices[i].y = tempMapIndices[i].y;

		subsetBoolMask[mapIndices[i].y].push_back(true);
		
		this->rtxetrisPiece->modelData->orderedSubsets[mapIndices[i].y].push_back(&this->rtxetrisPiece->modelData->subsets[i]);
		this->instanceIndicesMap[mapIndices[i].y].push_back(instanceIndices[i]);
	}

	for (int i = 0; i < tempCoordinates.size(); i++) {
		coordinates[i].x = tempCoordinates[i].x;
		coordinates[i].y = tempCoordinates[i].y;
	}
}

bool RTXetrisPiece::RotatePiece(const nxt::Matrix2x2& m, uint8_t* stageMap) {

	ClearPiecePositionOnMap(stageMap);

	std::vector<nxt::Vec2> tempCoordinates;
	std::vector<nxt::Vec2> tempMapIndices;

	//Rotate our map indices and check each block position
	for (int i = 0; i < coordinates.size(); i++) {
		nxt::Vec2 out = m.Matrix2x2MultiplyVector(coordinates[i]);

		tempCoordinates.push_back({ roundf(out.x), roundf(out.y) });

		int diffCoordinateX = tempCoordinates[i].x - coordinates[i].x;
		int diffCoordinateY = coordinates[i].y - tempCoordinates[i].y;

		tempMapIndices.push_back({ mapIndices[i].x + diffCoordinateX,  mapIndices[i].y + diffCoordinateY });

		nxt::Vec2 tempMapIndex = tempMapIndices[tempMapIndices.size() - 1];

		//If out of bounds, prevent rotation
		if (tempMapIndex.x < 0 || tempMapIndex.x > 8) {
			RestorePiecePositionOnMap(stageMap);
			return false;
		}

		if (tempMapIndex.y < 0 || tempMapIndex.y > 13) {
			RestorePiecePositionOnMap(stageMap);
			return false;
		}

		int arrIndexY = tempMapIndex.y * 9;
		int arrIndexX = tempMapIndex.x;

		//If another piece is in this spot, prevent rotation
		if (stageMap[arrIndexY + arrIndexX] != 0) {
			RestorePiecePositionOnMap(stageMap);
			return false;
		}
	}

	//We are not out of bounds, and there are no pieces in the way blocking the rotation.
	UpdateCoordinatesAndMapIndices(stageMap, tempCoordinates, tempMapIndices);

	
	return true;
}

bool RTXetrisPiece::RotateLeft(uint8_t* stageMap) {

	nxt::Matrix2x2 m;
	m.SetIdentity();
	m.SetRotationY(-1.5708f);
	
	if (!RotatePiece(m, stageMap))
		return false;

	currentRotation -= 1.57f;
	currentRasterRotation += 1.57f;
	world.SetRotationZ(currentRotation);
	rasterWorld.SetRotationZ(currentRasterRotation);

	return true;

}

bool RTXetrisPiece::RotateRight(uint8_t* stageMap) {

	nxt::Matrix2x2 m;
	m.SetIdentity();
	m.SetRotationY(1.5708f);

	if (!RotatePiece(m, stageMap))
		return false;

	currentRotation += 1.57f;
	currentRasterRotation -= 1.57f;
	nxt::Matrix4x4 rotZ, rasterRotZ;
	rotZ.SetRotationX(currentRotation);
	rasterRotZ.SetRotationX(currentRasterRotation);

	world.SetIdentity();
	rasterWorld.SetIdentity();

	world = nxt::MatrixMultiply(rotZ, world);
	world = nxt::MatrixMultiply(rotY, world);

	rasterWorld = nxt::MatrixMultiply(rasterRotY, rasterWorld);
	rasterWorld = nxt::MatrixMultiply(rasterRotZ, rasterWorld);

	world._r[3] = posX + startPosX;
	world._r[7] = posY + startPosY;
	world._r[11] = posZ + startPosZ;

	rasterWorld.SetTranslation(posX + startPosX, posY + startPosY, posZ + startPosZ);

	return true;
}

bool RTXetrisPiece::MoveRight(uint8_t* stageMap) {

	nxt::Vec2 direction = { 1.0f, 0.0f };
	if (!MovePiece(direction, stageMap))
		return false;

	
	posX += 1.0f;

	//world.SetTranslation(posX + startPosX, posY + startPosY, posZ + startPosZ);
	world._r[3] = posX + startPosX;
	world._r[7] = posY + startPosY;
	world._r[11] = posZ + startPosZ;

	rasterWorld.SetTranslation(posX + startPosX, posY + startPosY, posZ + startPosZ);

	return true;
}

bool RTXetrisPiece::MoveLeft(uint8_t* stageMap) {

	nxt::Vec2 direction = { -1.0f, 0.0f };
	if (!MovePiece(direction, stageMap))
		return false;

	posX -= 1.0f;

	//world.SetTranslation(posX + startPosX, posY + startPosY, posZ + startPosZ);
	world._r[3] = posX + startPosX;
	world._r[7] = posY + startPosY;
	world._r[11] = posZ + startPosZ;

	rasterWorld.SetTranslation(posX + startPosX, posY + startPosY, posZ + startPosZ);

	return true;
}

bool RTXetrisPiece::MoveDown(uint8_t* stageMap) {

	nxt::Vec2 direction = { 0.0f, 1.0f };
	if (!MovePiece(direction, stageMap))
		return false;

	posY -= 1.0f;
	//world.SetTranslation(posX + startPosX, posY + startPosY, posZ + startPosZ);

	world._r[3]  = posX + startPosX;
	world._r[7]  = posY + startPosY;
	world._r[11] = posZ + startPosZ;

	rasterWorld.SetTranslation(posX + startPosX, posY + startPosY, posZ + startPosZ);

	return true;
}

bool RTXetrisPiece::PlacePiece(uint8_t* stageMap) {
	for (int i = 0; i < mapIndices.size(); i++) {

		int arrIndexY = mapIndices[i].y * 9;
		int arrIndexX = mapIndices[i].x;

		//If another piece is in this spot, prevent rotation
		if (stageMap[arrIndexY + arrIndexX] != 0)
			return false;

		stageMap[arrIndexY + arrIndexX] = 1;
	}

	onStage = true;

	nxt::Matrix4x4 m;
	m.SetIdentity();

	world.SetIdentity();
	rasterWorld.SetIdentity();

	world = nxt::MatrixMultiply(rotY, m);
	rasterWorld = nxt::MatrixMultiply(rasterRotY, m);

	//world.SetTranslation(startPosX, startPosY, startPosZ);
	world._r[3] = startPosX;
	world._r[7] = startPosY;
	world._r[11] = startPosZ;

	rasterWorld.SetTranslation(startPosX, startPosY, startPosZ);

	return true;
}
