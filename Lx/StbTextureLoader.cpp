#include "StbTextureLoader.h"
#include <string>
#define STB_IMAGE_IMPLEMENTATION
#include "NxT/stb_image.h"

using namespace nxt;

void* StbTextureLoader::stbi_load_texture(std::string fileName, int& x, int& y, int& channels) {
	return stbi_load(fileName.c_str(), &x, &y, &channels, 4);
}