#pragma once
#include <unordered_map>
#include <functional>
#include <mutex>

#include "StbTextureLoader.h"
#include "NxT/IGraphics.h"

namespace nxt {

	typedef unsigned long long ResourceID;
	template<typename T, typename U>
	class ResourceManager {
	public:

		struct Resource {
			U object;
			uint32_t refCount;
		};

		ResourceID LoadResource(T* t, std::string name) {
			loadResource.lock();
			
			ResourceID resourceID = stringHash(name);
			if (resources.find(resourceID) == -1) {

				U object = t->Load(name);
				if (object) {
					Resource resource{};
					resource.object = object;
					resource.refCount = 1;

					resources[resourceID] = resource;
				}
				else {
					return 0;
				}
			}
			
			return resourceID;

			loadResource.unlock();
		}

		ResourceID GetResource(std::string name) {
			getResource.lock();

			ResourceID resourceID = stringHash(name);
			if (resources.find(resourceID) != -1) {
				resources[resourceID].refCount++;
				return resourceID;
			}

			return 0;

			getResource.unlock();
		}

		void ReleaseResource(ResourceID resource) {
			releaseResource.lock();

			if (resources.find(resourceID) != -1) {
				resources[resourceID].refCount--;
				if (resources[resourceID] == 0) {
					t->Remove(resources[resourceID].object);
					resources.erase(resourceID);
				}
			}

			releaseResource.unlock();
		}
	private:
		T t;
		std::unordered_map<ResourceID, Resource> resources;
		std::mutex loadResource, getResource, releaseResource;
		std::hash<std::string> stringHash;
	};
};

static nxt::ResourceManager<nxt::StbTextureLoader, nxt::ITexture*> textureManager;