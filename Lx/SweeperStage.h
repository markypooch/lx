#pragma once
#include "IStage.h"
#include "NxT/ConstantBufferHelper.h"
#include "NxT/Matrix.h"
#include "SweeperCube.h"
#include "Input.h"

namespace sweeper {

	struct wvp {
		nxt::Matrix4x4 world;
		nxt::Matrix4x4 worldRotate;
		nxt::Matrix4x4 viewProj;
		nxt::Matrix4x4 wvp;
	};

	struct Transforms {
		float position[4];
	};

	struct Crystals {
		float position[4];
		float color[4];
	};

	struct View {
		float cameraPosition[4];
	};

	struct PBRAppSettings {
		float modulateEmission;
		float iblCubeMapRotationY;
		float maxReflectanceLOD;
		float ambient;
		float specularContribution;
		int applyOcclusion;
		int pad[2];
	};

	struct RTXModelData;

	class SweeperStage : public IStage
	{
	public:
		SweeperStage(nxt::IWorldRenderer* iWorldRenderer) 
			: IStage(iWorldRenderer) {}

		virtual void Initialize();
		virtual void Update(float, int mouseButtons);
		virtual void Render();
	private:
		void GenerateStage();
		void CheckInput(int mouseButtons);
	private:

		std::vector<nxt::SweeperCube> cubes;

		POINT lastCursorPos{};

		RTXModelData* rtxModelData;
		float worldRotX, worldRotY, worldRotZ;
		nxt::Matrix4x4 worldX, worldY, worldZ;
		nxt::Matrix4x4 worldRot;

		float angleRot;
		bool rotateCubelets, pick, hover, setCursorOrigin = false;

		nxt::IInstanceBuffer* iInstanceMet;
		nxt::IInstanceBuffer* iInstanceBuffer;
		nxt::IInstanceBuffer* iInstanceCrystal;

		std::vector<Crystals> crystals;
		std::vector<Transforms> whiteCubeTransforms;
		std::vector<Transforms> metCubeTransforms;
		nxt::Matrix4x4 view, proj;
		nxt::Matrix4x4 rot, trans;

		nxt::Matrix4x4 world;
		nxt::Matrix4x4 viewProj;
		nxt::Matrix4x4 lightViewProj;
		nxt::Matrix4x4 lightView;
		nxt::Matrix4x4 lightProj;

		nxt::Matrix4x4 m, m2, m3, m4;

		nxt::IMesh* crystal;
		nxt::IMesh* whiteCube;
		nxt::IMesh* metCube;
		nxt::IMesh* mine;
		nxt::IMesh* t0, *t1;
		nxt::IMesh* shadowPlane;

		nxt::IConstantBufferHelper<wvp>* iConstantBufferHelper;
		nxt::IConstantBufferHelper<View>* iConstantBufferHelperView;
		nxt::IConstantBufferHelper<PBRAppSettings>* iConstantBufferHelperPBR;

		nxt::ITexture* ibl_diffuse, * ibl_specular, * brdf_lut;
		std::vector<nxt::SubResource> ibl_diffuse_subresource;
		std::vector<nxt::SubResource> ibl_specular_subresource;
	};
};