#pragma once
#include "Input.h"
#include "NxT/IGraphics.h"
#include "NxT/IWorldRenderer.h"

namespace sweeper {

	class IStage {
	public:
		IStage(nxt::IWorldRenderer* iWorldRenderer) {
			this->iWorldRenderer = iWorldRenderer;
		}

		virtual void Initialize() = 0;
		virtual void Update(float, const int) = 0;
		virtual void Render() = 0;
	protected:
		nxt::IWorldRenderer* iWorldRenderer;
	};
};