#pragma once

namespace nxt {
	struct Vec2 {
		float x, y;
	};

	struct Vec3 {
		float x, y, z;

		Vec3(float x, float y, float z) {
			this->x = x;
			this->y = y;
			this->z = z;
		}

		Vec3() {
			this->x = 0.0f;
			this->y = 0.0f;
			this->z = 0.0f;
		}

		void Normalize() {
			float length = sqrt(x * x + y * y + z * z);

			x /= length;
			y /= length;
			z /= length;
		}
	};

	static Vec3 Vec3Lerp(Vec3 start, Vec3 end, float t) {
		return Vec3(
			start.x + (end.x - start.x) * t,
			start.y + (end.y - start.y) * t,
			start.z + (end.z - start.z) * t
		);
	}
}
