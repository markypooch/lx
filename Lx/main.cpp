#include <Windows.h>
#include "NxT/IGraphics.h"
#include "NxT/Direct3D12/D3D12Graphics.h"
#include "NxT/IMesh.h"
#include "NxT/Direct3D12/D3D12Mesh.h"
#include <iostream>
#include "IStage.h"
#include "NxT/WorldRenderers/DeferredWorldRenderer.h"
#include "RTXetrisStage.h"

#include <condition_variable>
#include <chrono>

#include <thread>

using namespace nxt;
using namespace sweeper;

//globals
bool     g_Destroy = false;
int      mouseButtons;
WORD zDelta;
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	WORD fw = 0;
	//WORD zDelta = 0;
	switch (message) {
	case WM_DESTROY:
		PostQuitMessage(0);
		g_Destroy = true;
		break;
	case WM_RBUTTONUP:
		mouseButtons |= RMBU;
		break;
	case WM_RBUTTONDOWN:
		mouseButtons |= RMBD;
		break;
	case WM_LBUTTONUP:
		mouseButtons |= LMBU;
		break;
	case WM_LBUTTONDOWN:
		mouseButtons |= LMBD;
		break;
	case WM_KEYDOWN:
		if (wParam == VK_LEFT) {
			mouseButtons |= LKDN;
		}
		if (wParam == VK_RIGHT) {
			mouseButtons |= RKDN;
		}
		if (wParam == VK_UP) {
			mouseButtons |= KUP;
		}
		if (wParam == VK_DOWN) {
			mouseButtons |= DKDN;
		}
		break;
	case WM_MOUSEWHEEL:
		zDelta = GET_WHEEL_DELTA_WPARAM(wParam);
		if (zDelta == 120) {
			mouseButtons |= WHEELU;
		}
		else {
			mouseButtons |= WHEELD;
		}
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE pInstance, LPSTR lpCmdLine, int nCmdShow) {

	HWND hWnd;
	WNDCLASSEX wnd = {};
	wnd.cbSize = sizeof(WNDCLASSEX);
	wnd.lpszClassName = "logic";
	wnd.hbrBackground = (HBRUSH)COLOR_WINDOW;
	wnd.lpfnWndProc = WndProc;
	wnd.hInstance = hInstance;
	wnd.style = CS_HREDRAW | CS_VREDRAW;

	if (!RegisterClassEx(&wnd)) {
		return -1;
	}

	IGraphics* iGraphics = nullptr;
	IWorkingContext** contexts;

	hWnd = CreateWindowEx(0, "logic", "RTXetris", WS_OVERLAPPEDWINDOW, 0, 0, 3840, 2160, 0, 0, hInstance, NULL);
	ShowWindow(hWnd, nCmdShow);

	iGraphics = new D3D12Graphics(3840, 2160, 60, hWnd);
	iGraphics->Initialize();

	contexts = iGraphics->CreateWorkingContexts(4, 2);

	contexts[0]->BeginRecording();
	contexts[1]->BeginRecording();
	contexts[2]->BeginRecording();
	contexts[3]->BeginRecording();

	nxt::IWorldRenderer* iWorldRenderer = new nxt::DeferredWorldRenderer(iGraphics, contexts, 2);
	IStage* stage = new RTXetrisStage(iWorldRenderer);

	IWorkingContext* context[4] = { contexts[0], contexts[1], contexts[2], contexts[3] };

	stage->Initialize();

	contexts[0]->StopRecording();
	contexts[1]->StopRecording();
	contexts[2]->StopRecording();
	contexts[3]->StopRecording();
	
	iGraphics->SubmitWorkingContextsForExecution(context, 4);

	contexts[0]->currentWorkingBuffer = -1;
	contexts[1]->currentWorkingBuffer = -1;
	contexts[2]->currentWorkingBuffer = -1;
	contexts[3]->currentWorkingBuffer = -1;

	double dt = 0.0;

	MSG msg = {};
	while (true) {
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else {

			auto startTime = std::chrono::high_resolution_clock::now();

			if (g_Destroy) {
				break;
			}

			stage->Update(dt, mouseButtons);
			stage->Render();

			iGraphics->FlipBuffers();

			auto endTime = std::chrono::high_resolution_clock::now();
			dt = std::chrono::duration<double, std::milli>(endTime - startTime).count() / 1000.0f;

			mouseButtons = 0;
		}
	}

	return 0;
}