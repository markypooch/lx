#pragma once

namespace nxt {
	enum MouseButtons {
		LMBD = 1 << 1,
		LMBU = 1 << 2,
		RMBD = 1 << 3,
		RMBU = 1 << 4,
		WHEELU = 1 << 5,
		WHEELD = 1 << 6,
		RKDN = 1 << 7,
		LKDN = 1 << 8,
		KUP = 1 << 9,
		DKDN = 1 << 10
	};
};
