cbuffer wvp : register(b0) {
    matrix world;
    matrix worldRotate;
    matrix vp;
    matrix wvp;
}

cbuffer View : register(b1) {
	float4 cameraPosition;
}

struct GLTFModelVertex
{
	float4 Position : POSITION;
	float2 TexCoord : TEXCOORD;
	float3 Normal : NORMAL;
	float4 Tangent : TANGENT;
	float4 Bitangent : BITANGENT;
};

struct VertexOut
{
	float4 pos : SV_POSITION;
        float4 wPos : POSITION;
        float4 norm : NORMAL;
        float3 ViewDir : VIEWDIR;
        float2 tex  : TEXCOORD;
        float4 lPos : TEXCOORD2;
        int materialIndex : MATERIALINDEX;
};

VertexOut main(GLTFModelVertex vert)
{
	VertexOut oVert = (VertexOut)0;

        oVert.wPos         = mul(world, float4(vert.Position.xyz, 1.0f));
        oVert.pos         = mul(wvp, float4(vert.Position.xyz, 1.0f));
       
        matrix lvp = mul(worldRotate, world);
        oVert.lPos = mul(lvp, float4(vert.Position.xyz, 1.0f));
        //oVert.pos         = mul(lvp, float4(vert.Position.xyz, 1.0f));

        oVert.norm        = normalize(mul(world, float4(vert.Normal.xyz, 0.0f)));
        oVert.tex         = vert.TexCoord;
        oVert.materialIndex = (int)vert.Bitangent.w;

        oVert.ViewDir =  cameraPosition.xyz - oVert.wPos.xyz;
        return oVert;
}
