struct VertexIn {
    float4 pos : POSITION;
    float2 tex : TEXCOORD;
    float4 normal : NORMAL;
    float4 tang  : TANGENT;
    float4 bitang : BITANGENT;
};

struct VertexOut {
    float4 pos : SV_POSITION;
    float2 tex : TEXCOORD;
};

VertexOut main(VertexIn vIn) {
    VertexOut vOut;

    vOut.pos = vIn.pos;
    vOut.tex = vIn.tex;

    return vOut;
}