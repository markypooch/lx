#define bindlessTexture2DArr space1

#define M_PI_F ((float)3.141592653589793)
#define M_1_PI_F ((float)0.318309886183790)

ByteAddressBuffer materialIndices : register(t3);
ByteAddressBuffer positions : register(t4);
ByteAddressBuffer indices : register(t5);
ByteAddressBuffer normals : register(t6);
ByteAddressBuffer instanceOffsets : register(t7);
ByteAddressBuffer uvs : register(t8);
ByteAddressBuffer bitangents : register(t9);
ByteAddressBuffer tangents : register(t10);

TextureCube<float4> skyMap : register(t11);
TextureCube<float4> specularSkyMap : register(t12);
Texture2D<float4>   brdflut : register(t13);

Texture2D<float4> materialTextures[] : register(t14, bindlessTexture2DArr);

RWTexture2D< float4 > gOutput : register(u0);
RWTexture2D< float4 > gNormalsOutput : register(u1);
RaytracingAccelerationStructure gBVHScene : register(t2);

SamplerState LinearSampler : register(s0);

struct MetallicRoughness {
    float4 baseColor;
    float4 emissiveColor;
    float matMetallic;
    float matRoughness;
    float matOcclusion;
    int hasBaseColorTexture;
    int hasNormalTexture;
    int hasEmissiveTexture;
    int hasMetallicRoughnessTexture;
    int hasOcclusionTexture;
    //int baseTextureIndex;
    //int pad[3];
};

cbuffer Material : register(b1) {
    MetallicRoughness materials[128];
}

cbuffer Settings : register(b0) {
    float4x4 viewToWorld;
    float4x4 view;
    float2 zPlaneSize;
    float projNear;
    float randomSeed;
    float translate;
    float pad[3];
}

struct ShadowRayPayload
{
	bool miss;
};

struct HitInfo
{
  float3 radiance;
  uint recursionDepth;
  float4 direction;
  float4 normals;
  uint miss;
  float3 pad;
};

struct Attributes
{
  float2 bary;
};


// Uses the inversion method to map two uniformly random numbers to a three dimensional
// unit hemisphere where the probability of a given sample is proportional to the cosine
// of the angle between the sample direction and the "up" direction (0, 1, 0)
float3 sampleCosineWeightedHemisphere(float2 u) {
	float phi = 2.0f * M_PI_F * u.x;

	float sin_phi, cos_phi;
	sincos(phi, sin_phi, cos_phi);

	float cos_theta = sqrt(u.y);
	float sin_theta = sqrt(1.0f - cos_theta * cos_theta);

	return float3(sin_theta * cos_phi, cos_theta, sin_theta * sin_phi);
}

// Aligns a direction on the unit hemisphere such that the hemisphere's "up" direction
// (0, 1, 0) maps to the given surface normal direction
float3 alignHemisphereWithNormal(float3 sample, float3 normal) {
	// Set the "up" vector to the normal
	float3 up = normal;

	// Find an arbitrary direction perpendicular to the normal. This will become the
	// "right" vector.
	float3 right = normalize(cross(normal, float3(0.0072f, 1.0f, 0.0034f)));

	// Find a third vector perpendicular to the previous two. This will be the
	// "forward" vector.
	float3 forward = cross(right, up);

	// Map the direction on the unit hemisphere to the coordinate system aligned
	// with the normal.
	return sample.x * right + sample.y * up + sample.z * forward;
}

//====
// https://github.com/playdeadgames/temporal/blob/master/Assets/Shaders/IncNoise.cginc
// The MIT License (MIT)
//
// Copyright (c) [2015] [Playdead]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

//note: normalized random, float=[0, 1]
float PDnrand( float2 n ) {
	return frac( sin(dot(n.xy, float2(12.9898, 78.233f)))* 43758.5453 );
}
float2 PDnrand2( float2 n ) {
	return frac( sin(dot(n.xy, float2(12.9898, 78.233f)))* float2(43758.5453, 28001.8384) );
}
float3 PDnrand3( float2 n ) {
	return frac( sin(dot(n.xy, float2(12.9898, 78.233f)))* float3(43758.5453, 28001.8384, 50849.4141 ) );
}
float4 PDnrand4( float2 n ) {
	return frac( sin(dot(n.xy, float2(12.9898, 78.233f)))* float4(43758.5453, 28001.8384, 50849.4141, 12996.89) );
}

// Convert uniform distribution into triangle-shaped distribution.
// https://www.shadertoy.com/view/4t2SDh
// Input is in range [0, 1]
// Output is in range [-1, 1], which is useful for dithering.
float2 uniformNoiseToTriangular(float2 n) {
	float2 orig = n*2.0-1.0;
	n = orig*rsqrt(abs(orig));
	n = max(-1.0,n);
	n = n-float2(sign(orig));
	return n;
}

float3 irefract(float3 incidentVec, float3 normal, float eta)
{
  float N_dot_I = dot(normal, incidentVec);
  float k = 1.f - eta * eta * (1.f - N_dot_I * N_dot_I);
  if (k < 0.f)
    return float3(0.f, 0.f, 0.f);
  else
    return eta * incidentVec - (eta * N_dot_I + sqrt(k)) * normal;
}


float DistributionGGX(float NdotH, float roughness) {
    float a = roughness*roughness;
    float a2 = a*a;
    
    float NdotH2 = NdotH*NdotH;

    float denominator = (NdotH2 * (a2 - 1.0f) + 1.0f);
    denominator = (3.14) * denominator * denominator;

    return a2/denominator;
}

float GeometrySchlickGGX(float NdotV, float roughness) {
    float r = (roughness + 1.0f);
    float k = (r*r) / 8.0f;
    
    float denominator = NdotV * (1.0f - k) + k;

    return NdotV / denominator;
}

float GeometrySmith(float NdotL, float NdotV, float roughness) {
    float ggx2  = GeometrySchlickGGX(NdotV, roughness);
    float ggx1  = GeometrySchlickGGX(NdotL, roughness);
	
    return ggx1 * ggx2;
}

float luminance(float3 v) {
    return dot(v, float3(0.2126f, 0.7152f, 0.0722f));
}

float3 change_luminance(float3 v_in, float l_out) {
    float l_in = luminance(v_in);
    return v_in * (l_out / l_in);
}

float3 extended_reinhard(float3 v_in, float max_white_l) {
    float l_old = luminance(v_in);

    float numerator = l_old * (1.0f + (l_old / (max_white_l * max_white_l)));
    float l_new     = numerator / (1.0f + l_old);
    
    return change_luminance(v_in, l_new);
}

float3 fresnelSchlick(float cosTheta, float3 F0, float roughness)
{
    return F0 + (max(float3(1.0, 1.0, 1.0)-roughness, F0) - F0) * pow(max(1.0 - cosTheta, 0.0), 5.0);
    //return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
    //return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(max(1.0 - cosTheta, 0.0), 5.0);
}  

float4 CookTorrance_Schlick(float4 V, float4 N, float rot, float3 color, float specularIntensity, float metallic, float roughness, float occlusion, float ambientContribution, float maxSpecularLOD, float specularContribution) {

    float3 F0;
    F0 = lerp(0.04, color, metallic);
    
    float3 F    = fresnelSchlick(max(dot(N, V), 0.0), F0, roughness);   
    float3x3 m = {
        cos(rot), 0.0f, -sin(rot),
        0.0f,     1.0f, 0.0f,
        sin(rot), 0.0f, cos(rot)
    };

    float3 sN = N.xyz;

    sN = mul(m, N.xyz);    
    sN = normalize(sN);
    float3 R    = reflect(V.xyz, sN).rgb;
    float3 kS           = F;
    float3 kD           = (float3(1, 1, 1) - kS) * (1.0f - ((metallic)));
    float3 diffuseBRDF  = color * (skyMap.SampleLevel(LinearSampler, sN, 0).rgb*0.25f); // 3.14159265359;
    
    float3 prefilteredColor = specularSkyMap.SampleLevel(LinearSampler, R,  roughness * maxSpecularLOD).rgb*1.0f;    
    float2 brdf  = brdflut.SampleLevel(LinearSampler, float2(max(dot(sN, V.xyz), 0.0), roughness), 0).rg;
    float3 specular = prefilteredColor * (F * brdf.x + brdf.y);

   // vec3 ambient = (kD * diffuse + specular);// * ao;
    
   // vec3 color = ambient + Lo;

    // add to outgoing radiance Lo
    float3 ambient = (kD * diffuseBRDF + specular);
    float3 Lo = ambient;

    float4 outputColor;
    outputColor = float4(Lo, 1.0f) + float4(color*ambientContribution, 0.0f);
    
    return outputColor;
}

float3 mon2lin(float3 x)
{
	return float3(pow(abs(x[0]), 1.2), pow(abs(x[1]), 1.2), pow(abs(x[2]), 1.2));
}


[shader("closesthit")]
void ClosestHit(inout HitInfo payload : SV_RayPayload, Attributes attribs : SV_IntersectionAttributes)
{
   
  const uint maxRecursionDepth = 3;

  if (payload.recursionDepth > maxRecursionDepth)
  {
	//payload.radiance = float4(extended_reinhard(payload.radiance.xyz, 25.0f), 1.0f).xyz;
    	return;
  }

  float3 uvw;
  uvw.yz = attribs.bary;
  uvw.x = 1.0f - uvw.y - uvw.z;

  uint2 pixelPos = DispatchRaysIndex().xy;
  float2 pixelUV = (float2(pixelPos) + 0.5) / float2(DispatchRaysDimensions().xy);

  uint triangleIndex = PrimitiveIndex() + instanceOffsets.Load(InstanceID() << 2);
  uint materialIndex = instanceOffsets.Load(InstanceID() << 2);
  float4 baseColor = float4(0.0f, 0.0f, 0.0f, 0.0f);
  float baseMet   = 0.0f;
  float baseRough = 0.0f;

  uint4 i012 = indices.Load4((3 * triangleIndex) << 2);
  uint i0 = i012[0];
  uint i1 = i012[1];
  uint i2 = i012[2];

  float4 position012[3] = {
	asfloat(positions.Load4((i0 * 3) << 2)),
	asfloat(positions.Load4((i1 * 3) << 2)),
	asfloat(positions.Load4((i2 * 3) << 2))
  };

  float4 normal012[3] = {
	asfloat(normals.Load4((i0 * 3) << 2)),
	asfloat(normals.Load4((i1 * 3) << 2)),
	asfloat(normals.Load4((i2 * 3) << 2))
  };
  float2 uv012[3] = {
		asfloat(uvs.Load2((i0 * 1) << 3)),
		asfloat(uvs.Load2((i1 * 1) << 3)),
		asfloat(uvs.Load2((i2 * 1) << 3))
  };

  float4 bitang012[3] = {
	asfloat(bitangents.Load4((i0 * 3) << 2)),
	asfloat(bitangents.Load4((i1 * 3) << 2)),
	asfloat(bitangents.Load4((i2 * 3) << 2))
  };

  float4 tang012[3] = {
	asfloat(tangents.Load4((i0 * 3) << 2)),
	asfloat(tangents.Load4((i1 * 3) << 2)),
	asfloat(tangents.Load4((i2 * 3) << 2))
  };


  float2 uv = uvw.x * uv012[0] + uvw.y * uv012[1] + uvw.z * uv012[2];

  //payload.radiance = float3(colorMap.SampleLevel(LinearSampler, uv, 0).xyz);
  

  float3 pos = uvw.x * position012[0].xyz + uvw.y * position012[1].xyz + uvw.z * position012[2].xyz;
  float3 normal = normalize(uvw.x * normal012[0].xyz + uvw.y * normal012[1].xyz + uvw.z * normal012[2].xyz);
  float3 bitangent = normalize(uvw.x * bitang012[0].xyz + uvw.y * bitang012[1].xyz + uvw.z * bitang012[2].xyz);
  float3 tangent = normalize(uvw.x * tang012[0].xyz + uvw.y * tang012[1].xyz + uvw.z * tang012[2].xyz);

  //float3 translate = float3(ObjectToWorld4x3()[0].z, ObjectToWorld4x3()[1].z, ObjectToWorld4x3()[2].z);

  float3 position = pos;

  float3x3 world = ObjectToWorld3x4();
  
  float3 translation = float3(world[2][0], world[2][1], world[2][2]);
  world[0][2] = 0.0f;
  world[1][2] = 0.0f;
  world[2][2] = 0.0f;

  //position += translation;

  position = mul((float3x3)ObjectToWorld3x4(), position);
  normal   = mul((float3x3)ObjectToWorld3x4(), normal);

  //tangent  = mul((float3x3)ObjectToWorld3x4(), tangent);
  //bitangent = mul((float3x3)ObjectToWorld3x4(), bitangent);
  
  normal = normalize(normal);
  tangent = normalize(tangent);
  bitangent = normalize(bitangent);
    
  if (materials[materialIndices.Load(triangleIndex << 2)].hasNormalTexture) {
    float4 bumpMap = materialTextures[materials[materialIndices.Load(triangleIndex << 2)].baseColor.w + 1].SampleLevel(LinearSampler, -float2(uv.x + translate, uv.y + translate), 0); //materials[materialIndices.Load(materialIndex)].baseTextureIndex
    bumpMap = (bumpMap*2.0f)-1.0f;

    float3 bumpNormal = (tangent * bumpMap.x) + (bitangent * bumpMap.y) + (normal * bumpMap.z);
    bumpNormal = normalize(bumpNormal);

    normal = bumpNormal;
  }

    //if (payload.recursionDepth == 0) {
    //payload.normals = float4( bitangent.xyz, 0.0f);
    //}

    float roughness = 0.0f;
    float occlusion = 0.0f;
    float4 emission  = float4(0.0f, 0.0f, 0.0f, 0.0f);


    if (materials[materialIndices.Load(triangleIndex << 2)].hasBaseColorTexture) {
         baseColor = materialTextures[materials[materialIndices.Load(triangleIndex << 2)].baseColor.w].SampleLevel(LinearSampler, uv, 0);
    }
    else {
         baseColor = float4(materials[materialIndices.Load(triangleIndex << 2)].baseColor.rgb, 0.0f);
    }

    if (materials[materialIndices.Load(triangleIndex << 2)].hasMetallicRoughnessTexture) {
         float2 metallicRough = materialTextures[materials[materialIndices.Load(triangleIndex << 2)].baseColor.w + 2].SampleLevel(LinearSampler, uv, 0);//metallicRoughness.SampleLevel(LinearSampler, uv, 0).rg;
         baseMet = metallicRough.x;
         baseRough = metallicRough.y + 0.15f;
    }
    else {
         baseMet  = materials[materialIndices.Load(materialIndex)].matMetallic;
         baseRough = materials[materialIndices.Load(materialIndex)].matRoughness;
    }




    float4 cameraPosition = float4(0.0f, 0.0f, 0.0f, 0.0f);
    float4 V = normalize(float4(position.xyz, 1.0f) - float4(cameraPosition.xyz, 1.0f));

  //float4 L = float4(-5.0f, 0.0f, 0.0f, 1.0f);

  float4 toL2 = normalize(float4(position.xyz, 1.0f) - float4(float3(0.0f, 0.0f, 0.0f), 1.0f));
  float d2 = length(toL2);

  float4 toL = normalize(float4(position.xyz, 1.0f) - float4(float3(0.0f, 0.0f, -1.0f), 1.0f));

  float d = length(toL);
  float att = d*(0.01f*0.01f);
  float att2 = d2*(1.0f*1.0f);

  //float3 lightDir = float3(0.4f, 1.0f, 0.0f);
  //float3 viewDir = float3(0.0f, 0.0f, -1.0f);

  float3 rayOrigin = position + normal * 0.001;
  
  HitInfo reflectRayPayload;
  reflectRayPayload.radiance = 0.0;
  if (InstanceID() == 0 && payload.recursionDepth == 0) {
       //float3 rNormal = float3(normal.x, normal.y, -normal.z);
       float3 reflectDir = reflect(payload.direction.xyz, normal);
       float3 rayOrigin = position + normal * 0.1;
       RayDesc reflectRay;
          reflectRay.Origin = float3(rayOrigin.x, rayOrigin.y, rayOrigin.z);
          reflectRay.Direction = float3(reflectDir.x, reflectDir.y, reflectDir.z);
          reflectRay.TMin = 0.0;
          reflectRay.TMax = 1000;

          reflectRayPayload.recursionDepth = payload.recursionDepth + 1;
          reflectRayPayload.radiance = 0.0;
          reflectRayPayload.direction = float4(reflectRay.Direction.xyz, 1.0f);
          reflectRayPayload.normals = payload.normals;
          TraceRay(gBVHScene, 0, 0xFF, 0, 0, 0, reflectRay, reflectRayPayload);
          
          float specular = dot(float3(rayOrigin.x, rayOrigin.y, rayOrigin.z), normalize(V.xyz));
          float specularShininess = 200.0f;
          float3 reflectRadiance = reflectRayPayload.radiance.xyz;
          if(specular > 0.0f)
          {
              // Increase the specular light by the shininess value.
              specular = pow(specular, specularShininess);

              // Add the specular to the final color.
              //reflectRadiance += specular;
          }
          
          payload.radiance += float4(reflectRadiance.xyz, 1.0f) * 0.35f;
  }

  //payload.radiance += dot(lightDir, normal) * baseColor;
		//payload.radiance += indirectRayPayload.radiance.xyz * baseColor * 1.0f;

   //float4 V, float4 N, float rot, float3 color, float specularIntensity, float metallic, float roughness, float occlusion, float ambientContribution, float maxSpecularLOD, float specularContribution)
  if (payload.recursionDepth > 0) {

   	    //payload.radiance += 1.5 * saturate(dot(float3(-0.8f, 0.0f, 1.0f), normal)) * baseColor * M_1_PI_F;

            float3 ambient = float3(0.5f, 0.5f, 0.5f);
            if (InstanceID() == 0 && payload.direction.w < 1.0f || payload.direction.w > 0.0f && InstanceID() == 0) {
                baseColor = float4(0.0f, 0.0f, 0.0f, 0.0f);
            }

             payload.radiance += 5.0f * (dot(float3(0.0f, 0.0f, -1.0f), normal) * 0.5f + 0.5f) * mon2lin(baseColor) * M_1_PI_F;
             //payload.radiance = float3(baseColor.r, baseColor.g, baseColor.b);
            //payload.radiance = reflectRayPayload.radiance;
            //payload.radiance += baseColor * ambient;
            //payload.radiance += 2.5 * saturate(dot(float3(-0.8f, 0.0f, 0.0f), normal)) * baseColor * M_1_PI_F;
            //payload.radiance += 2.5 * saturate(dot(float3(0.0f, -0.8f, 0.0f), normal)) * baseColor * M_1_PI_F;
           // payload.radiance += 2.5 * saturate(dot(float3(0.0f,  0.8f, 0.0f), normal)) * baseColor * M_1_PI_F;
            

   	     //payload.radiance.xyz = float4(extended_reinhard(payload.radiance.xyz, 10.0f), 1.0f).xyz;
   
   }
   //payload.radiance += reflectRayPayload.radiance;
    //}
   //payload.radiance += 5.0 * saturate(dot(-normal, normal)) * baseColor * M_1_PI_F;

  //payload.radiance += CookTorrance_Schlick(toL/d, V, float4(normal.xyz, 0.0f), baseColor.xyz, 9.0f, baseMet, baseRough, float3(20.5f, 20.5f, 20.5f)/att).xyz;
 // payload.radiance += (baseColor.xyz * 0.4);
  //payload.radiance = float4(extended_reinhard(payload.radiance.xyz, 6.5f), 1.0f).xyz;

  //payload.radiance += CookTorrance_Schlick(toL2/d2, -V, float4(normal.xyz, 0.0f), baseColor.xyz, 7.0f, baseMet, baseRough, float3(2.5f, 8.5f, 6.5f)/att2).xyz;
  //payload.radiance += (baseColor.xyz * 0.4);
  //payload.radiance = float4(extended_reinhard(payload.radiance.xyz, 6.5f), 1.0f).xyz;

  
  float2 sample0 = uniformNoiseToTriangular(PDnrand2(pixelUV + randomSeed + payload.recursionDepth)) * 0.5 + 0.5;
  float3 sampleDirLocal = sampleCosineWeightedHemisphere(sample0);
  float3 sampleDir = alignHemisphereWithNormal(sampleDirLocal, normal);

  if (payload.recursionDepth < maxRecursionDepth)
	{
     
         
         

          RayDesc indirectRay;
          indirectRay.Origin = rayOrigin;
          indirectRay.Direction = sampleDir;
          indirectRay.TMin = 0.0;
          indirectRay.TMax = 1000;

          HitInfo indirectRayPayload;
          indirectRayPayload.recursionDepth = payload.recursionDepth + 1;
          indirectRayPayload.radiance = 0.0;
          indirectRayPayload.direction = float4(sampleDir, 0.0f);
          indirectRayPayload.normals = payload.normals;
          TraceRay(gBVHScene, 0, 0xFF, 0, 0, 0, indirectRay, indirectRayPayload);
          
         if (InstanceID() == 0)
          	payload.radiance += indirectRayPayload.radiance.xyz * (baseColor*1.25f);
          
	}
}
