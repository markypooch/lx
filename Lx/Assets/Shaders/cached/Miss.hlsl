#define M_PI_F ((float)3.141592653589793)
#define M_1_PI_F ((float)0.318309886183790)

ByteAddressBuffer materialIndices : register(t3);

ByteAddressBuffer positions : register(t4);
ByteAddressBuffer indices : register(t5);
ByteAddressBuffer normals : register(t6);
ByteAddressBuffer instanceOffsets : register(t7);
ByteAddressBuffer uvs : register(t8);

Texture2D<float4> colorMap : register(t9);
Texture2D<float4> metallicRoughness : register(t10);

RWTexture2D< float4 > gOutput : register(u0);
RWTexture2D< float4 > gNormalsOutput : register(u1);
RaytracingAccelerationStructure gBVHScene : register(t2);

SamplerState LinearSampler : register(s0);

struct MetallicRoughness {
    float4 baseColor;
    float4 emissiveColor;
    float matMetallic;
    float matRoughness;
    float matOcclusion;
    int hasBaseColorTexture;
    int hasNormalTexture;
    int hasEmissiveTexture;
    int hasMetallicRoughnessTexture;
    int hasOcclusionTexture;
};

cbuffer Material : register(b1) {
    MetallicRoughness materials[8];
}

cbuffer Settings : register(b0) {
    float4x4 viewToWorld;
    float4x4 view;
    float2 zPlaneSize;
    float projNear;
    float randomSeed;
    uint frameIndex;
    uint pad[3];
}

struct HitInfo
{
  float3 radiance;
  uint recursionDepth;
  float4 direction;
  float4 normals;
  uint miss;
  float3 pad;
};

[shader("miss")]
void Miss(inout HitInfo payload : SV_RayPayload)
{
    payload.radiance = float3(0.0f, 0.0f, 0.0f);
    payload.miss = 1;
}