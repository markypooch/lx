#define M_PI_F ((float)3.141592653589793)
#define M_1_PI_F ((float)0.318309886183790)

ByteAddressBuffer materialIndices : register(t3);

ByteAddressBuffer positions : register(t4);
ByteAddressBuffer indices : register(t5);
ByteAddressBuffer normals : register(t6);
ByteAddressBuffer instanceOffsets : register(t7);
ByteAddressBuffer uvs : register(t8);

Texture2D<float4> colorMap : register(t9);
Texture2D<float4> metallicRoughness : register(t10);

RWTexture2D< float4 > gOutput : register(u0);
RWTexture2D< float4 > gNormalsOutput : register(u1);
RaytracingAccelerationStructure gBVHScene : register(t2);

SamplerState LinearSampler : register(s0);

struct MetallicRoughness {
    float4 baseColor;
    float4 emissiveColor;
    float matMetallic;
    float matRoughness;
    float matOcclusion;
    int hasBaseColorTexture;
    int hasNormalTexture;
    int hasEmissiveTexture;
    int hasMetallicRoughnessTexture;
    int hasOcclusionTexture;
};

cbuffer Material : register(b1) {
    MetallicRoughness materials[8];
}

cbuffer Settings : register(b0) {
    float4x4 viewToWorld;
    float4x4 view;
    float2 zPlaneSize;
    float projNear;
    float randomSeed;
    uint frameIndex;
    uint pad[3];
}

struct HitInfo
{
  float3 radiance;
  uint recursionDepth;
  float4 direction;
  float4 normals;
  uint miss;
  float3 pad;
};

struct Attributes
{
  float2 bary;
};

[shader("raygeneration")]
void RayGen() {
    // Initialize the ray payload
    HitInfo payload;
    payload.radiance = float3(0.0, 0.0, 0.0);
    payload.normals = float4(0.0, 0.0, 0.0, 0.0);
    payload.recursionDepth = 0;
    payload.miss = 0;

    // Get the location within the dispatched 2D grid of work items
    // (often maps to pixels, so this could represent a pixel coordinate).
    uint2 launchIndex = DispatchRaysIndex().xy;
    float2 dims = float2(DispatchRaysDimensions().xy);
    float2 d = ((launchIndex.xy / dims.xy) * 2.0f - 1.0f);
    d.y *= -1;

    // Define a ray, consisting of origin, direction, and the min-max distance
    // values
    float3 direction = normalize(float3(d * zPlaneSize, 1.0));
	float3 directionWS = mul(float3x3(viewToWorld[0].xyz, viewToWorld[1].xyz, viewToWorld[2].xyz), direction);

	RayDesc ray;
	ray.Origin = float3(0.0f, 0.0f, 0.0f);//float3(viewToWorld[0].w, viewToWorld[1].w, viewToWorld[2].w) + directionWS * projNear;
	ray.Direction = float4(0.0f, 0.0f, 1.0f, 0.0f);//directionWS;
    ray.TMin = 0.0;
    ray.TMax = 1000;

    payload.direction = float4(0.0f, 0.0f, 0.0f, 0.0f);
    float4 accumulatedRadiance = 0.0;
    for (int i = 0; i < 1; i++) {

        // Trace the ray
        TraceRay(
          // Parameter name: AccelerationStructure
          // Acceleration structure
          gBVHScene,
          RAY_FLAG_NONE,

          // Parameter name: InstanceInclusionMask
          // Instance inclusion mask, which can be used to mask out some geometry to
          // this ray by and-ing the mask with a geometry mask. The 0xFF flag then
          // indicates no geometry will be masked
          0xFF,

          // Parameter name: RayContributionToHitGroupIndex
          // Depending on the type of ray, a given object can have several hit
          // groups attached (ie. what to do when hitting to compute regular
          // shading, and what to do when hitting to compute shadows). Those hit
          // groups are specified sequentially in the SBT, so the value below
          // indicates which offset (on 4 bits) to apply to the hit groups for this
          // ray. In this sample we only have one hit group per object, hence an
          // offset of 0.
          0,

          // Parameter name: MultiplierForGeometryContributionToHitGroupIndex
          // The offsets in the SBT can be computed from the object ID, its instance
          // ID, but also simply by the order the objects have been pushed in the
          // acceleration structure. This allows the application to group shaders in
          // the SBT in the same order as they are added in the AS, in which case
          // the value below represents the stride (4 bits representing the number
          // of hit groups) between two consecutive objects.
          0,

          // Parameter name: MissShaderIndex
          // Index of the miss shader to use in case several consecutive miss
          // shaders are present in the SBT. This allows to change the behavior of
          // the program when no geometry have been hit, for example one to return a
          // sky color for regular rendering, and another returning a full
          // visibility value for shadow rays. This sample has only one miss shader,
          // hence an index 0
          0,

          // Parameter name: Ray
          // Ray information to trace
          ray,

          // Parameter name: Payload
          // Payload associated to the ray, which will be used to communicate
          // between the hit/miss shaders and the raygen
          payload);

      }

    //float4 accumulatedRadiance = 0.0;
	//if (frameIndex == 0)
	//{
		accumulatedRadiance = float4(payload.radiance, 1.0);
	//}
	//else
	//{
		//accumulatedRadiance = gOutput[launchIndex.xy];
		//accumulatedRadiance.w += 1.0;
		//accumulatedRadiance.rgb += (payload.radiance - accumulatedRadiance.rgb) / accumulatedRadiance.w;
	//}

    gOutput[launchIndex] = accumulatedRadiance;
    gNormalsOutput[launchIndex] = float4(payload.normals.xyz, 1.0f);

}