#pragma once
#include "Pass.h"
#include <random>

namespace nxt {

	class SSAOPass : public Pass {
	public:
		SSAOPass(IWorkingContext* context, PassInput passInput) {
			this->context = context;

			std::uniform_real_distribution<float> randomFloats(0.0, 1.0); // random floats between [0.0, 1.0]
			std::default_random_engine generator;

			for (unsigned int i = 0; i < 64; ++i)
			{
				Vec3 sample(
					randomFloats(generator) * 2.0 - 1.0,
					randomFloats(generator) * 2.0 - 1.0,
					randomFloats(generator)
				);
				sample.Normalize();

				float scale = (float)i / 64.0;
				scale = lerp(0.1f, 1.0f, scale * scale);
				sample.x *= scale;
				sample.y *= scale;
				sample.z *= scale;

				kernel.push_back(sample);
			}

			std::vector<Vec3> ssaoNoise;
			for (unsigned int i = 0; i < 16; i++)
			{
				Vec3 noise(
					randomFloats(generator) * 2.0 - 1.0,
					randomFloats(generator) * 2.0 - 1.0,
					0.0f);
				ssaoNoise.push_back(noise);
			}

			ssaoNoiseTexture = context->CreateTexture("SSAO", ssaoNoise.data(), 3840, 2160, 1, nxt::Format::R32_FLOAT, UsageFlags::NONE, 1, 1);
		}

		virtual void Run() {
		}
	private:
		float lerp(float a, float b, float f)
		{
			return a + f * (b - a);
		}
		ITexture* ssaoNoiseTexture;

		ITexture* lightPassCompositeTexture;
		ITexture* brightnessTexture;
		ITexture* blurXTexture;
		IWorkingContext* context;

		std::vector<Vec3> kernel;
		std::vector<Vec3> noise;
		Matrix4x4 noise;

		std::vector<nxt::IRenderTarget*> rtvs;
		std::vector<IMesh*> ndcPassQuads;
	};
};