#pragma once
#include <cmath>
#include <memory>
#include "../Vector.h"

namespace nxt {

	struct Matrix4x4 {
		float _r[16];

		void SetIdentity() {
			_r[0] = 1.0f;
			_r[1] = 0.0f;
			_r[2] = 0.0f;
			_r[3] = 0.0f;
			_r[4] = 0.0f;
			_r[5] = 1.0f;
			_r[6] = 0.0f;
			_r[7] = 0.0f;
			_r[8] = 0.0f;
			_r[9] = 0.0f;
			_r[10] = 1.0f;
			_r[11] = 0.0f;
			_r[12] = 0.0f;
			_r[13] = 0.0f;
			_r[14] = 0.0f;
			_r[15] = 1.0f;
		}

		// x0 = cos(a)
		// y0 = sin(a)
		// x1 = cos(x0+b) = x0 * cosb - y0 * sinb
		// y1 = sin(y0+b) = y0 * cosb + x0 * sina
		//
		void SetRotationY(float angle) {

			_r[0]  = cos(angle);
			_r[1]  = 0.0f;
			_r[2]  = sin(angle);
			_r[3]  = 0.0f;
			
			_r[4]  = 0.0f;
			_r[5]  = 1.0f;
			_r[6]  = 0.0f;
			_r[7]  = 0.0f;

			_r[8]  = -sin(angle);
			_r[9]  = 0.0f;
			_r[10] = cos(angle);
			_r[11] = 0.0f;

			_r[12] = 0.0f;
			_r[13] = 0.0f;
			_r[14] = 0.0f;
			_r[15] = 1.0f;
		}

		void SetRotationZ(float angle) {

			_r[0] = cos(angle);
			_r[1] = -sin(angle);
			_r[2] = 0.0f;
			_r[3] = 0.0f;

			_r[4] = sin(angle);
			_r[5] = cos(angle);
			_r[6] = 0.0f;
			_r[7] = 0.0f;

			_r[8] = 0.0f;
			_r[9] = 0.0f;
			_r[10] = 1.0f;
			_r[11] = 0.0f;

			_r[12] = 0.0f;
			_r[13] = 0.0f;
			_r[14] = 0.0f;
			_r[15] = 1.0f;
		}

		void SetRotationX(float angle) {

			_r[0] = 1.0f;
			_r[1] = 0.0f;
			_r[2] = 0.0f;
			_r[3] = 0.0f;

			_r[4] = 0.0f;
			_r[5] = cos(angle);
			_r[6] = -sin(angle);
			_r[7] = 0.0f;

			_r[8] = 0.0f;
			_r[9] =  sin(angle);
			_r[10] = cos(angle);
			_r[11] = 0.0f;

			_r[12] = 0.0f;
			_r[13] = 0.0f;
			_r[14] = 0.0f;
			_r[15] = 1.0f;
		}

		void SetScale(float x, float y, float z) {
			_r[0] = x;
			_r[5] = y;
			_r[10] = z;
			_r[15] = 1.0f;
		}

		void SetTranslation(float x, float y, float z) {
			_r[12] = x;
			_r[13] = y;
			_r[14] = z;
			_r[15] = 1.0f;
		}

		Matrix4x4 operator*(const Matrix4x4& b) {

			float r[16];

			r[0] = _r[0] * b._r[0] + _r[1] * b._r[4] + _r[2] * b._r[8] + _r[3] * b._r[12];
			r[1] = _r[0] * b._r[1] + _r[1] * b._r[5] + _r[2] * b._r[9] + _r[3] * b._r[13];
			r[2] = _r[0] * b._r[2] + _r[1] * b._r[6] + _r[2] * b._r[10] + _r[3] * b._r[14];
			r[3] = _r[0] * b._r[3] + _r[1] * b._r[7] + _r[2] * b._r[11] + _r[3] * b._r[15];
			r[4] = _r[4] * b._r[0] + _r[5] * b._r[4] + _r[6] * b._r[8] + _r[7] * b._r[12];
			r[5] = _r[4] * b._r[1] + _r[5] * b._r[5] + _r[6] * b._r[9] + _r[7] * b._r[13];
			r[6] = _r[4] * b._r[2] + _r[5] * b._r[6] + _r[6] * b._r[10] + _r[7] * b._r[14];
			r[7] = _r[4] * b._r[3] + _r[5] * b._r[7] + _r[6] * b._r[11] + _r[7] * b._r[15];
			r[8] = _r[8] * b._r[0] + _r[9] * b._r[4] + _r[10] * b._r[8] + _r[11] * b._r[12];
			r[9] = _r[8] * b._r[1] + _r[9] * b._r[5] + _r[10] * b._r[9] + _r[11] * b._r[13];
			r[10] = _r[8] * b._r[2] + _r[9] * b._r[6] + _r[10] * b._r[10] + _r[11] * b._r[14];
			r[11] = _r[8] * b._r[3] + _r[9] * b._r[7] + _r[10] * b._r[11] + _r[11] * b._r[15];
			r[12] = _r[12] * b._r[0] + _r[13] * b._r[4] + _r[14] * b._r[8] + _r[15] * b._r[12];
			r[13] = _r[12] * b._r[1] + _r[13] * b._r[5] + _r[14] * b._r[9] + _r[15] * b._r[13];
			r[14] = _r[12] * b._r[2] + _r[13] * b._r[6] + _r[14] * b._r[10] + _r[15] * b._r[14];
			r[15] = _r[12] * b._r[3] + _r[13] * b._r[7] + _r[14] * b._r[11] + _r[15] * b._r[15];

			Matrix4x4 m;
			memcpy(m._r, r, sizeof(float) * 16);

			return m;
		}
	};

	static Matrix4x4 MatrixInverse(Matrix4x4 mIn) {

		Matrix4x4 inverse;

		float a0 = mIn._r[0] * mIn._r[5] - mIn._r[1] * mIn._r[4];
		float a1 = mIn._r[0] * mIn._r[6] - mIn._r[2] * mIn._r[4];
		float a2 = mIn._r[0] * mIn._r[7] - mIn._r[3] * mIn._r[4];
		float a3 = mIn._r[1] * mIn._r[6] - mIn._r[2] * mIn._r[5];
		float a4 = mIn._r[1] * mIn._r[7] - mIn._r[3] * mIn._r[5];
		float a5 = mIn._r[2] * mIn._r[7] - mIn._r[3] * mIn._r[6];
		float b0 = mIn._r[8] * mIn._r[13] - mIn._r[9] * mIn._r[12];
		float b1 = mIn._r[8] * mIn._r[14] - mIn._r[10] * mIn._r[12];
		float b2 = mIn._r[8] * mIn._r[15] - mIn._r[11] * mIn._r[12];
		float b3 = mIn._r[9] * mIn._r[14] - mIn._r[10] * mIn._r[13];
		float b4 = mIn._r[9] * mIn._r[15] - mIn._r[11] * mIn._r[13];
		float b5 = mIn._r[10] * mIn._r[15] - mIn._r[11] * mIn._r[14];

		float det = a0 * b5 - a1 * b4 + a2 * b3 + a3 * b2 - a4 * b1 + a5 * b0;
		if (fabsf(det) > 1.0e-15f)
		{
			//Matrix4x4 inverse;
			inverse._r[0] = +mIn._r[5] * b5 - mIn._r[6] * b4 + mIn._r[7] * b3;
			inverse._r[4] = -mIn._r[4] * b5 + mIn._r[6] * b2 - mIn._r[7] * b1;
			inverse._r[8] = +mIn._r[4] * b4 - mIn._r[5] * b2 + mIn._r[7] * b0;
			inverse._r[12] = -mIn._r[4] * b3 + mIn._r[5] * b1 - mIn._r[6] * b0;
			inverse._r[1] = -mIn._r[1] * b5 + mIn._r[2] * b4 - mIn._r[3] * b3;
			inverse._r[5] = +mIn._r[0] * b5 - mIn._r[2] * b2 + mIn._r[3] * b1;
			inverse._r[9] = -mIn._r[0] * b4 + mIn._r[1] * b2 - mIn._r[3] * b0;
			inverse._r[13] = +mIn._r[0] * b3 - mIn._r[1] * b1 + mIn._r[2] * b0;
			inverse._r[2] = +mIn._r[13] * a5 - mIn._r[14] * a4 + mIn._r[15] * a3;
			inverse._r[6] = -mIn._r[12] * a5 + mIn._r[14] * a2 - mIn._r[15] * a1;
			inverse._r[10] = +mIn._r[12] * a4 - mIn._r[13] * a2 + mIn._r[15] * a0;
			inverse._r[14] = -mIn._r[12] * a3 + mIn._r[13] * a1 - mIn._r[14] * a0;
			inverse._r[3] = -mIn._r[9] * a5 + mIn._r[10] * a4 - mIn._r[11] * a3;
			inverse._r[7] = +mIn._r[8] * a5 - mIn._r[10] * a2 + mIn._r[11] * a1;
			inverse._r[11] = -mIn._r[8] * a4 + mIn._r[9] * a2 - mIn._r[11] * a0;
			inverse._r[15] = +mIn._r[8] * a3 - mIn._r[9] * a1 + mIn._r[10] * a0;

			float invDet = 1.0f / det;
			inverse._r[0] *= invDet;
			inverse._r[1] *= invDet;
			inverse._r[2] *= invDet;
			inverse._r[3] *= invDet;
			inverse._r[4] *= invDet;
			inverse._r[5] *= invDet;
			inverse._r[6] *= invDet;
			inverse._r[7] *= invDet;
			inverse._r[8] *= invDet;
			inverse._r[9] *= invDet;
			inverse._r[10] *= invDet;
			inverse._r[11] *= invDet;
			inverse._r[12] *= invDet;
			inverse._r[13] *= invDet;
			inverse._r[14] *= invDet;
			inverse._r[15] *= invDet;

			return inverse;
		}

		inverse.SetIdentity();
		return inverse;
	}

	static Matrix4x4 MatrixMultiply(const Matrix4x4& a, const Matrix4x4& b) {


		Matrix4x4 m;
		float r[16];
		
		r[0] =  a._r[0]  * b._r[0] + a._r[1]  * b._r[4] + a._r[2]  * b._r[8]  + a._r[3]  * b._r[12];
		r[1] =  a._r[0]  * b._r[1] + a._r[1]  * b._r[5] + a._r[2]  * b._r[9]  + a._r[3]  * b._r[13];
		r[2] =  a._r[0]  * b._r[2] + a._r[1]  * b._r[6] + a._r[2]  * b._r[10] + a._r[3]  * b._r[14];
		r[3] =  a._r[0]  * b._r[3] + a._r[1]  * b._r[7] + a._r[2]  * b._r[11] + a._r[3]  * b._r[15];
		r[4] =  a._r[4]  * b._r[0] + a._r[5]  * b._r[4] + a._r[6]  * b._r[8]  + a._r[7]  * b._r[12];
		r[5] =  a._r[4]  * b._r[1] + a._r[5]  * b._r[5] + a._r[6]  * b._r[9]  + a._r[7]  * b._r[13];
		r[6] =  a._r[4]  * b._r[2] + a._r[5]  * b._r[6] + a._r[6]  * b._r[10] + a._r[7]  * b._r[14];
		r[7] =  a._r[4]  * b._r[3] + a._r[5]  * b._r[7] + a._r[6]  * b._r[11] + a._r[7]  * b._r[15];
		r[8] =  a._r[8]  * b._r[0] + a._r[9]  * b._r[4] + a._r[10] * b._r[8]  + a._r[11] * b._r[12];
		r[9] =  a._r[8]  * b._r[1] + a._r[9]  * b._r[5] + a._r[10] * b._r[9]  + a._r[11] * b._r[13];
		r[10] = a._r[8]  * b._r[2] + a._r[9]  * b._r[6] + a._r[10] * b._r[10] + a._r[11] * b._r[14];
		r[11] = a._r[8]  * b._r[3] + a._r[9]  * b._r[7] + a._r[10] * b._r[11] + a._r[11] * b._r[15];
		r[12] = a._r[12] * b._r[0] + a._r[13] * b._r[4] + a._r[14] * b._r[8]  + a._r[15] * b._r[12];
		r[13] = a._r[12] * b._r[1] + a._r[13] * b._r[5] + a._r[14] * b._r[9]  + a._r[15] * b._r[13];
		r[14] = a._r[12] * b._r[2] + a._r[13] * b._r[6] + a._r[14] * b._r[10] + a._r[15] * b._r[14];
		r[15] = a._r[12] * b._r[3] + a._r[13] * b._r[7] + a._r[14] * b._r[11] + a._r[15] * b._r[15];

		//Matrix4x4 m;
		memcpy(m._r, r, sizeof(float) * 16);

		return m;
	}

	static Matrix4x4 RotationY(float angle) {

		Matrix4x4 m;

		m._r[0] = cos(angle);
		m._r[1] = 0.0f;
		m._r[2] = sin(angle);
		m._r[3] = 0.0f;
		
		m._r[4] = 0.0f;
		m._r[5] = 1.0f;
		m._r[6] = 0.0f;
		m._r[7] = 0.0f;
		
		m._r[8] = -sin(angle);
		m._r[9] = 0.0f;
		m._r[10] = cos(angle);
		m._r[11] = 0.0f;
		
		m._r[12] = 0.0f;
		m._r[13] = 0.0f;
		m._r[14] = 0.0f;
		m._r[15] = 1.0f;

		return m;
	}

	
	static Matrix4x4 Translation(float x, float y, float z) {
		Matrix4x4 m;

		m._r[0] = 1.0f;
		m._r[1] = 0.0f;
		m._r[2] = 0.0f;
		m._r[3] = 0.0f;
		
		m._r[4] = 0.0f;
		m._r[5] = 1.0f;
		m._r[6] = 0.0f;
		m._r[7] = 0.0f;
		
		m._r[8] = 0.0f;
		m._r[9] = 0.0f;
		m._r[10] = 1.0f;
		m._r[11] = 0.0f;
		
		m._r[12] = x;
		m._r[13] = y;
		m._r[14] = z;
		m._r[15] = 1.0f;

		return m;
	}

	static Matrix4x4 ProjectionLH(float aspectRatio, float fov, float fnear, float ffar) {

		Matrix4x4 m;

		float f = 1 / tan(fov * 0.5f);

		m._r[0] = f / aspectRatio;
		m._r[1] = 0.0f;
		m._r[2] = 0.0f;
		m._r[3] = 0.0f;
		m._r[4] = 0.0f;
		m._r[5] = f;
		m._r[6] = 0.0f;
		m._r[7] = 0.0f;
		m._r[8] = 0.0f;
		m._r[9] = 0.0f;
		m._r[10] = ffar / (ffar-fnear);
		m._r[11] = 1.0f;
		m._r[12] = 0.0f;
		m._r[13] = 0.0f;
		m._r[14] = -ffar * fnear / (ffar - fnear);
		m._r[15] = 0.0f;

		return m;
	}

	static Matrix4x4 OrthoLH(int x, int y, float fnear, float ffar) {

		Matrix4x4 m;

		m._r[0] = 2.0f / x;
		m._r[1] = 0.0f;
		m._r[2] = 0.0f;
		m._r[3] = 0.0f;
		m._r[4] = 0.0f;
		m._r[5] = 2.0f / y;
		m._r[6] = 0.0f;
		m._r[7] = 0.0f;
		m._r[8] = 0.0f;
		m._r[9] = 0.0f;
		m._r[10] = 1.0f / (ffar - fnear);
		m._r[11] = fnear / (fnear - ffar);
		m._r[12] = 0.0f;
		m._r[13] = 0.0f;
		m._r[14] = 0.0f;
		m._r[15] = 1.0f;

		return m;
	}

	struct Matrix2x2 {
		float _r[4];

		void SetIdentity() {
			_r[0] = 1.0f;
			_r[1] = 0.0f;
			_r[2] = 0.0f;
			_r[3] = 1.0f;
		}

		// x0 = cos(a)
		// y0 = sin(a)
		// x1 = cos(x0+b) = x0 * cosb - y0 * sinb
		// y1 = sin(y0+b) = y0 * cosb + x0 * sina
		//
		void SetRotationY(float angle) {
			_r[0] = cosf(angle);
			_r[1] = sinf(angle);

			_r[2] = -sinf(angle);
			_r[3] = cosf(angle);
		}


		void SetRotationZ(float angle) {

			_r[0] = cosf(angle);
			_r[1] = -sinf(angle);

			_r[2] = sinf(angle);
			_r[3] = cosf(angle);
		}

		Vec2 Matrix2x2MultiplyVector(const Vec2& a) const {

			Vec2 out;

			out.x = _r[0] * a.x + _r[1] * a.y;
			out.y = _r[2] * a.x + _r[3] * a.y;

			return out;
		}
	};

}