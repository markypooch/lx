#include "ResourceManager.h"
#include "../IGraphics.h"

namespace nxt {
	class PipelineLoader : public IResourceLoader {
	public:
		virtual void Load();
		virtual void Unload();
	};
};