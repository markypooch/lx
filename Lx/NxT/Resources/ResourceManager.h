#pragma once
#include "../IGraphics.h"

namespace nxt {

	enum ResourceLoaderType {
		TEXTURE = 0,
		MODEL,
		PIPELINE,
		BUFFER
	};

	template<typename T, typename U>
	class IResourceLoader {
	public:
		virtual void Load(const U) {};
		virtual void Unload() {};
	private:
		struct RefRes {
			T* resource;
			unsigned int refCount;
		};

		std::unordered_map<uint32_t, RefRes> resources;
	};

	class ResourceManager
	{
	public:
		template<typename T>
		T* RetrieveLoader(ResourceLoaderType);
	private:

		template<typename T>
		bool RemoveLoader(uint32_t id);

		IResourceLoader<IPipeline*, PipelineDesc> *pipelineResourceLoader;
		//IResourceLoader<ITexture*, TextureDesc>   *textureResourceLoader;
		//IResourceLoader<IMesh*, ModelDesc>        *modelResourceLoader;
		//IResourceLoader<IShader*, ShaderDesc>     *shaderResourceLoader;
	};
};
