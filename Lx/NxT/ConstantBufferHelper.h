#pragma once
#include "IGraphics.h"

namespace nxt {

	template<typename T>
	class IConstantBufferHelper
	{
	public:
		virtual void Initialize(unsigned int numberOfConstantBuffers) = 0;
		virtual IConstantBuffer* Update(size_t s = 0) = 0;
	public:
		T type;
	protected:
		unsigned int currentBuffer;
	};
};