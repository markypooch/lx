#pragma once
#include <string>
#include <Windows.h>
#include <vector>
#include <unordered_set>
#include <unordered_map>
#include "Matrix.h"

#define BUFFER_COUNT 3

namespace nxt {

	enum Format {
		UNKNOWN,
		R32_FLOAT,
		RGBA_8,
		RGBA_F32,
		DEPTH_32_FLOAT,
		R32G32B32A32_FLOAT,
		R32G32_FLOAT
	};

	enum ResourceUsage {
		RENDER_TARGET,
		PRESENT,
		DEPTH_WRITE,
		DEPTH_READ,
		COPY_SOURCE,
		COPY_DEST,
		GEN_READ,
		PS_RESOURCE,
		RESOURCE_UNKNOWN,
	};

	enum UsageFlags {
		NONE,
		ALLOW_RENDER_TARGET,
		ALLOW_DEPTH_STENCIL
	};

	enum DescriptorType {
		CBV_SRV_UAV,
		RTV,
		DSV,
		SRV,
		CBV,
		UAV
	};

	struct Descriptor {
		DescriptorType descriptorType;
		unsigned int indexInHeap;
	};

	enum ArgumentType {
		ARG_SRV,
		ARG_CBV,
		ARG_UAV,
	};

	enum Visibility {
		PIXEL,
		VERTEX,
		ALL
	};

	enum ShaderType {
		PS,
		VS
	};

	enum Classification {
		PER_VERTEX,
		INSTANCED
	};

	struct Pointlight {
		float position[4];
		float diffuse[4];
		float ambient[4];
	};

	struct ArgumentRange {
		unsigned int index;
		unsigned int space;
		ArgumentType type;
		unsigned int count;
		unsigned int start;
	};

	struct ResourceTransisition {
		ResourceUsage beforeResourceUsage;
		ResourceUsage afterResourceUsage;
	};

	struct ArgumentTable {
		std::vector<ArgumentRange> ranges;
	};

	struct ArgumentParameters {
		std::vector<ArgumentTable> tables;
		Visibility visibility;
	};

	struct VertexLayout {
		std::string semanticName;
		unsigned int semanticIndex;
		Format format;
		Classification classification;
		unsigned int byteOffset;
		unsigned int instanceDataStepRate;
		unsigned int inputSlot;
	};

	enum Blend {
		BLEND_ZERO = 1,
		BLEND_ONE = 2,
		BLEND_SRC_COLOR = 3,
		BLEND_SRC_ALPHA = 5,
		BLEND_DEST_ALPHA = 7,
		BLEND_DEST_COLOR = 9,
	};

	enum BlendOp {
		ADD,
		MUL
	};

	struct BlendDesc {
		bool blendEnable;
		Blend SrcBlend;
		Blend SrcBlendAlpha;
		Blend DestBlend;
		Blend DestBlendAlpha;
		BlendOp BlendOp;
		//BlendOp BlendOpAlpha;
	};

	class Resource {
	protected:
		size_t id;
	};

	class IArgumentLayout {
	public:
		std::vector<ArgumentParameters> params;
	};

	class IArgumentLayoutObject : public Resource {
	public:
		IArgumentLayout iArgumentLayout;
	};

	class ITexture : public Resource {
	public:
		bool rtResource = false;
		bool dsvResource = false;
		ResourceUsage currentUsage[BUFFER_COUNT];
		Descriptor descriptor[BUFFER_COUNT];
	};

	class IRenderTarget : public Resource {
	public:
		ResourceUsage currentUsage;
		Descriptor descriptor[BUFFER_COUNT];
	private:
		ITexture* iTexture;
	};

	class IVertexBuffer : public Resource {
	public:
		std::string name;
	};

	class IIndexBuffer : public Resource {
	public:
	};

	struct InstanceDesc {
		uint32_t instanceID;
		nxt::Matrix4x4 transform;
	};

	class IAccelerationStructure : public Resource {
	public:
		std::vector<InstanceDesc> instances;
		uint32_t asDescriptorIndex;
		bool dirty;
	};

	class ISRVBuffer : public Resource {
	public:
	};

	class IInstanceBuffer : public Resource {
	public:
	};

	class IConstantBuffer : public Resource {
	public:
		Descriptor descriptor;
		ResourceUsage currentUsage;
	};

	class IShader : public Resource {
	public:
		ShaderType shaderType;
	};

	class IVertexLayout : public Resource {
	public:
		VertexLayout** vertexLayout;
	};

	class IPipeline : public Resource {
	public:
	};

	class IBlend : public Resource {
	public:
	};

	class IRasterizer : public Resource {
	public:
	};

	struct TextureTransisition : public ResourceTransisition {
		ITexture* texture;
	};

	struct RenderTargetTransisition : public ResourceTransisition {
		IRenderTarget* rtv;
	};

	class IGraphics;

	struct SubResource {
		void* pData;
		size_t rowPitch;
		size_t slicePitch;
	};

	class IMesh;

	struct RTXModelData {
		nxt::IMesh* scene;
		uint32_t numberOfModels;
		std::vector< IVertexBuffer*> vertexBuffers;
		std::vector<IIndexBuffer*> indexBuffers;
		std::vector<nxt::Matrix4x4> matrices;
		std::vector<uint32_t> verticeCount;
		std::vector<uint32_t> indiceCount;
		nxt::ISRVBuffer* positions, * indices, * uvs, * materialIndices, * normals, * instanceOffsets, * bitangs, * tangs;
		IConstantBuffer* iCB;
		std::vector<ITexture*> textures;
	};

	struct PipelineDesc {
		IShader** shaders;
		IVertexLayout* iLayout;
		Format format;
		Format dsvFormat;
		unsigned int shaderCount;
		unsigned int rtvCount;
	};

	class IWorkingContext {
	public:
		virtual void                   Initialize(int rtxContext) = 0;
		virtual void                   BeginRecording() = 0;
		virtual void                   TransisitionTexture(TextureTransisition textureTransisition) = 0;
		virtual void				   TransisitionRenderTarget(RenderTargetTransisition renderTargetTransisition) = 0;
		virtual IRenderTarget*         CreateRenderTarget(std::string renderTargetName, ITexture*) = 0;
		virtual IRenderTarget*         CreateDepthBuffer(std::string renderTargetName, ITexture*) = 0;
		virtual IArgumentLayoutObject* CreateArgumentLayout(std::string name, IArgumentLayout) = 0;
		virtual ITexture*              CreateTexture(std::string name, const void* pData, unsigned int width, unsigned int height, unsigned int channels, Format format, UsageFlags flags, unsigned int mipCount = 1, bool srv = true, bool rtx = false, ResourceUsage resourceUsage = ResourceUsage::RESOURCE_UNKNOWN) = 0;
		virtual ITexture*              CreateTexture(std::string name, const void* pData, unsigned int width, unsigned int height, size_t rowPitch, size_t slicePitch, Format format, UsageFlags flags, unsigned int mipCount=1, unsigned int arrSize=1) = 0;
		virtual ITexture*              CreateUAV(std::string name, const void* pData, unsigned int width, unsigned int height, unsigned int channels, Format format, UsageFlags flags, unsigned int mipCount = 1) = 0;
		virtual IVertexBuffer*         CreateVertexBuffer(std::string name, void* pData, unsigned int stride, unsigned int numberOfVertices, bool rtx = false) = 0;
		virtual IInstanceBuffer*       CreateInstanceBuffer(std::string name, void* pData, unsigned int stride, unsigned int numberOfElements) = 0;
		virtual ISRVBuffer*            CreateSrvBuffer(std::string name, void* pData, unsigned int stride, unsigned int numberOfElements) = 0;
		virtual void                   SetSRVBuffer(ISRVBuffer*, unsigned int bindSlot) = 0;
		virtual IIndexBuffer*          CreateIndexBuffer(void* pData, unsigned int stride, unsigned int numOfIndices, bool rtx = false) = 0;
		virtual IShader*               CreateShader(LPCWSTR filename, ShaderType shaderType) = 0;
		virtual IConstantBuffer*       CreateConstantBuffer(std::string name, size_t size, size_t stride, IConstantBuffer* cb = nullptr, uint32_t offset = 0, bool rtx = false) = 0;
		virtual IAccelerationStructure* CreateAccelerationStructure(RTXModelData& rtxModelData) = 0;
		virtual void                   SetViewport(unsigned int width, unsigned int height) = 0;
		virtual void                   RebuildTLAS(IAccelerationStructure* iAS) = 0;
		virtual void				    SetRTXBuffer(unsigned int bindIndex, IConstantBuffer* iTexture) = 0;
		virtual void				    SetRTXTexture(unsigned int bindIndex, ITexture* iTexture, bool rtx=true) = 0;
		virtual void				    SetRTXTextureArray(unsigned int bindIndex, ITexture** iTextures, unsigned int size, bool rtx = true) = 0;
		virtual void                   SetTexture(unsigned int bindIndex, ITexture* iTexture, bool rtx=false) = 0;
		virtual void                   UpdateTextureMip(ITexture*, void* pData, unsigned int mipIndex, size_t rowSlice, size_t slicePitch, size_t arrIndex, size_t mipCount, size_t arrCount, size_t totalSlice) = 0;
		virtual void                   SetBuffer(unsigned int bindIndex, IConstantBuffer* iCB) = 0;
		virtual IVertexLayout*         CreateVertexLayout(std::string name, VertexLayout* layout, unsigned int numElements) = 0;
		virtual void                   SetBackBufferPresentState(IGraphics*) = 0;
		virtual IPipeline*             CreatePipeline(std::string name, std::vector<IShader*> shaders, IArgumentLayoutObject*, IVertexLayout*, unsigned int, Format rtvFormat, Format dsvFormat) = 0;
		virtual void                   SetPipeline(IPipeline* pipeline) = 0;
		virtual void                   SetVertexBuffer(unsigned int bindIndex, IVertexBuffer*, IInstanceBuffer* iInstanceBuffer = nullptr) = 0;
		virtual void                   SetIndexBuffer(unsigned int bindIndex, IIndexBuffer*) = 0;
		virtual void                   BindArgumentLayout() = 0;
		virtual void                   SetRenderTargets(unsigned int count, std::vector<IRenderTarget*>, IRenderTarget*, bool bindDepthOnly=false) = 0;
		virtual void                   Clear(IRenderTarget*, IRenderTarget*, const float* clearColor) = 0;
		virtual void                   StopRecording() = 0;
		virtual void                   DispatchRays(IAccelerationStructure*, ITexture* iTexture) = 0;
		virtual void                   DrawInstanced(unsigned int instanceCount, unsigned int vertexCount, unsigned int start) = 0;
		virtual void                   DrawIndexedInstanced(unsigned int instances, unsigned int startIndex, unsigned int indexCount) = 0;
		virtual void                   SetPresent(unsigned int index) = 0;
	protected:
		static std::unordered_map<size_t, IPipeline*>             pipelines;
		static std::unordered_map<size_t, IRenderTarget*>         renderTargets;
		static std::unordered_map<size_t, ITexture*>              textures;
		static std::unordered_map<size_t, IArgumentLayoutObject*> argumentLayoutObjects;
		static std::unordered_map<size_t, IShader*>			   shaders;
		static std::unordered_map<size_t, IVertexLayout*>         vertexLayouts;
		static std::unordered_map<size_t, IVertexBuffer*>         vertexBuffers;
	public:
		int currentWorkingBuffer = -1;
	};

	class IGraphics {
	public:
		virtual void Initialize() = 0;
		virtual IWorkingContext** CreateWorkingContexts(unsigned int numberOfContexts, int rtxContext=-1) = 0;
		virtual void SubmitWorkingContextsForExecution(IWorkingContext**, unsigned int) = 0;
		virtual void FlipBuffers() = 0;

		bool rtSupport = true;
	};
};