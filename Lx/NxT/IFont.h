#pragma once
#include "IGraphics.h"
#include "ConstantBufferHelper.h"

namespace nxt {

	struct FontCode {
		Matrix4x4 wvp;
		int asciiFontCode;
		float pad[3];
	};

	class IFont
	{
	public:
		virtual void RenderText(IWorkingContext* iWorkContext, Vec2 position, const std::string text) = 0;
	protected:
		ITexture* fontmap;
		IMesh* fontQuad;
		IConstantBufferHelper<FontCode> *fontCodeCB;

		Matrix4x4 proj;
	};
};