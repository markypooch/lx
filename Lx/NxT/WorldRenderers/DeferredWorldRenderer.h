#pragma once
#include "../IWorldRenderer.h"

#include "../IGraphics.h"
#include "../IMesh.h"
#include "../Direct3D12/D3D12ConstantBuffer.h"

#include "Passes/ShadowPass.h"
#include "Passes/GBufferPass.h"
#include "Passes/DXRPass.h"
#include "Passes/LightCompositePass.h"
#include "Passes/LightPass.h"
#include "Passes/BloomPass.h"
#include "Passes/RendererCompositePass.h"

#include <condition_variable>
#include <thread>

namespace nxt {
	
	class DeferredWorldRenderer : public nxt::IWorldRenderer
	{
	public:
		DeferredWorldRenderer(IGraphics* context, IWorkingContext** contexts, int rtxContext = -1);

		virtual void   Initialize(RTXModelData* rtxModelData);
		virtual IMesh* LoadModel(const char* pFilepath, nxt::MeshType meshType, bool genNormals = false);
		virtual void   SubmitMesh(IMesh* imesh, std::vector<ObjectBinding<IConstantBuffer*>> bufferBindings, std::vector<ObjectBinding<ITexture*>> textureBindings, std::vector<bool> subsetRender, unsigned int instanceCount = 1, IInstanceBuffer* iInstanceBuffer = nullptr, bool shadowed = true);
		virtual void   SubmitRTXDispatchRequest(IAccelerationStructure* iAs, RTXModelData* rtxModelData);
		virtual IAccelerationStructure* GetIAS() {
			return iAS;
		}
		virtual void   SubmitForwardMesh(IMesh* imesh, std::vector<ObjectBinding<IConstantBuffer*>> bufferBindings, std::vector<ObjectBinding<ITexture*>> textureBindings, unsigned int instanceCount = 1, IInstanceBuffer* iInstanceBuffer = nullptr, ShadowMesh shadowMesh = ShadowMesh());
		virtual void   Draw(float dt, IConstantBuffer* view, Matrix4x4 worldRotate);

	public:
		virtual IMesh* CreateUtilityNDCQuad(std::string name, std::string vertexShaderPath, std::string pixelShaderPath, nxt::Format format);
		static bool start;
		ITexture* ibl_diffuse;
		ITexture* ibl_specular;
		ITexture* brdfLut;
	protected:
		virtual IAccelerationStructure* InitializeRTX(RTXModelData* rtxModelData);
	private:
		int rtxContext;


		std::condition_variable cv;

		IRenderTarget* shadowRTV;
		ITexture* shadowMap;
		ITexture* brightnessTexture;
		ITexture* blurXTexture;
		ITexture* blurYTexture;
		ITexture* bloomTexture;

		ITexture* compositeTexture;

		ITexture* forwardTexture;
		IRenderTarget* compositeRT;
		IRenderTarget* forwardRT;
		IRenderTarget* gBufferDepth;
		IRenderTarget* materialRT;
		IRenderTarget* materialRT2;
		IRenderTarget* normalRT;
		IRenderTarget* worldPosRT;
		IRenderTarget* lightRT;
		IRenderTarget* ssgiRT;

		IRenderTarget* brightnessRT;
		IRenderTarget* blurXRT;
		IRenderTarget* blurYRT;
		IRenderTarget* bloomCompositeRT;

		ITexture* ssgiTexture;

		ITexture* directionalTexture;
		IRenderTarget* directionalRT;

		ITexture* pointLightTexture;
		IRenderTarget* pointLightRT;

		std::vector<Pass*> jobsListOne, jobsListTwo, jobsListThree, jobsListFour;
		std::vector<Pass*> allJobs;

		RTXModelData* rtxModelData;
		Matrix4x4 view, inverseView;

		nxt::IAccelerationStructure* iAS;
		nxt::ITexture* dxrOutput[3];
		nxt::ITexture* dxrNormalOutput;
		nxt::IMesh* rtxTestQuad;
		nxt::IMesh* rtxScene;

		nxt::IMesh* lightPassQuad, *lightCompositePassQuad, * brightnessQuad, * blurXQuad, * blurYQuad, *rendererCompositeQuad;

		ShadowPass* shadowPass;
		//ForwardPass* forwardPass;
		GBufferPass* gbufferPass;
		LightPass* lightPass;
		DXRPass* dxrPass;
		LightCompositePass* lightCompositePass;
		BloomPass* bloomPass;
		RendererCompositePass* rendererCompositePass;

		std::thread threadZero;
		std::thread threadOne;
		std::thread threadTwo;
		std::thread threadThree;
	};
};