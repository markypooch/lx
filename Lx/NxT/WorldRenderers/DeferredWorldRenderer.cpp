#include <condition_variable>
#include "DeferredWorldRenderer.h"
#include "../Direct3D12/D3D12Mesh.h"
using namespace nxt;

//std::condition_variable cv;

bool g_start = false;
bool DeferredWorldRenderer::start = false;

void Thread(nxt::Pass** jobs, int numberOfJobs) {

	int jobsCompleted = 0;
	while (true) {
		if (g_start) {

			if (jobsCompleted < numberOfJobs)
			{
				for (int i = 0; i < numberOfJobs; i++) {
					jobs[i]->Run();
					jobs[i]->ran = true;
					jobsCompleted++;
				}
			}
		}
		else {
			jobsCompleted = 0;
		}
	}
}

DeferredWorldRenderer::DeferredWorldRenderer(IGraphics* context, IWorkingContext** contexts, int rtxContext) 
	: IWorldRenderer(context) 
 {
	this->rtxContext = rtxContext;
	this->iWorkContexts = { contexts[0], contexts[1], contexts[2], contexts[3] };

	IWorkingContext* workContext = this->iWorkContexts[0];
	isDXRSupported = context->rtSupport;

	//Create our backing textures
	/////////////////////////////////
	forwardTexture      = workContext->CreateTexture("Forward", nullptr, 3840, 2160, 4, nxt::Format::RGBA_F32, nxt::UsageFlags::ALLOW_RENDER_TARGET, 1, true, false, ResourceUsage::PS_RESOURCE);
	shadowMap           = workContext->CreateTexture("Shadow", nullptr, 8192, 8192, 4, nxt::Format::DEPTH_32_FLOAT, nxt::UsageFlags::ALLOW_DEPTH_STENCIL, 1, true, false, ResourceUsage::PS_RESOURCE);
	worldPosTexture     = workContext->CreateTexture("World", nullptr, 3840, 2160, 4, nxt::Format::RGBA_F32, nxt::UsageFlags::ALLOW_RENDER_TARGET, 1, true, false, ResourceUsage::PS_RESOURCE);
	normalTexture       = workContext->CreateTexture("Normal", nullptr, 3840, 2160, 4, nxt::Format::RGBA_F32, nxt::UsageFlags::ALLOW_RENDER_TARGET, 1, true, false, ResourceUsage::PS_RESOURCE);
	materialTexture     = workContext->CreateTexture("Material", nullptr, 3840, 2160, 4, nxt::Format::RGBA_F32, nxt::UsageFlags::ALLOW_RENDER_TARGET, 1, true, false, ResourceUsage::PS_RESOURCE);
	materialTexture2    = workContext->CreateTexture("Material", nullptr, 3840, 2160, 4, nxt::Format::RGBA_F32, nxt::UsageFlags::ALLOW_RENDER_TARGET, 1, true, false, ResourceUsage::PS_RESOURCE);
	gbufferDepthTexture = workContext->CreateTexture("GBufferDepth", nullptr, 3840, 2160, 4, nxt::Format::DEPTH_32_FLOAT, nxt::UsageFlags::ALLOW_DEPTH_STENCIL, 1, true, false, ResourceUsage::DEPTH_WRITE);
	lightTexture        = workContext->CreateTexture("World", nullptr, 3840, 2160, 4, nxt::Format::RGBA_8, nxt::UsageFlags::ALLOW_RENDER_TARGET, 1, true, false, ResourceUsage::PS_RESOURCE);
	directionalTexture  = workContext->CreateTexture("Directional", nullptr, 3840, 2160, 4, nxt::Format::RGBA_8, nxt::UsageFlags::ALLOW_RENDER_TARGET, 1, true, false, ResourceUsage::PS_RESOURCE);
	ssgiTexture         = workContext->CreateTexture("SSGI", nullptr, 3840, 2160, 4, nxt::Format::RGBA_F32, nxt::UsageFlags::ALLOW_RENDER_TARGET, 1, true, false, ResourceUsage::PS_RESOURCE);
	compositeTexture    = workContext->CreateTexture("GBufferDepth", nullptr, 3840, 2160, 4, nxt::Format::RGBA_8, nxt::UsageFlags::ALLOW_RENDER_TARGET, 1, true, false, ResourceUsage::PS_RESOURCE);
	brightnessTexture   = workContext->CreateTexture("GBufferDepth", nullptr, 3840, 2160, 4, nxt::Format::RGBA_8, nxt::UsageFlags::ALLOW_RENDER_TARGET, 1, true, false, ResourceUsage::PS_RESOURCE);
	blurXTexture        = workContext->CreateTexture("World", nullptr, 3840, 2160, 4, nxt::Format::RGBA_8, nxt::UsageFlags::ALLOW_RENDER_TARGET, 1, true, false, ResourceUsage::PS_RESOURCE);
	blurYTexture        = workContext->CreateTexture("Directional", nullptr, 3840, 2160, 4, nxt::Format::RGBA_8, nxt::UsageFlags::ALLOW_RENDER_TARGET, 1, true, false, ResourceUsage::PS_RESOURCE);
	bloomTexture        = workContext->CreateTexture("Bloom", nullptr, 3840, 2160, 4, nxt::Format::RGBA_8, nxt::UsageFlags::ALLOW_RENDER_TARGET, 1, true, false, ResourceUsage::PS_RESOURCE);
	compositeRT         = workContext->CreateRenderTarget("SSGIRT", compositeTexture);

	//Create our render targets
	/////////////////////////////////
	ssgiRT              = workContext->CreateRenderTarget("SSGIRT", ssgiTexture);
	forwardRT           = workContext->CreateRenderTarget("forwardRT", forwardTexture);
	shadowRTV           = workContext->CreateDepthBuffer("ShadowRTV", shadowMap);
	worldPosRT          = workContext->CreateRenderTarget("WorldRT", worldPosTexture);
	normalRT            = workContext->CreateRenderTarget("NormalRT", normalTexture);
	materialRT          = workContext->CreateRenderTarget("MaterialRT", materialTexture);
	materialRT2         = workContext->CreateRenderTarget("MaterialRT", materialTexture2);
	gBufferDepth        = workContext->CreateDepthBuffer("gD", gbufferDepthTexture);
	lightRT             = workContext->CreateRenderTarget("ComposeRT", lightTexture);
	directionalRT       = workContext->CreateRenderTarget("DirectionalRT", directionalTexture);
	bloomCompositeRT    = workContext->CreateRenderTarget("SSGIRT", bloomTexture);
	blurXRT             = workContext->CreateRenderTarget("SSGIRT", blurXTexture);
	blurYRT             = workContext->CreateRenderTarget("SSGIRT", blurYTexture);
	brightnessRT        = workContext->CreateRenderTarget("SSGIRT", brightnessTexture);

	lightPassQuad = CreateUtilityNDCQuad("LightQuad", "Assets/Shaders/directional.vs", "Assets/Shaders/directional.ps", Format::RGBA_8);
	lightCompositePassQuad = CreateUtilityNDCQuad("LightCompositeQuad", "Assets/Shaders/present.vs", "Assets/Shaders/present.ps", Format::RGBA_8);
	brightnessQuad = CreateUtilityNDCQuad("BrightnessQuad", "Assets/Shaders/BrightnessExtract.vs", "Assets/Shaders/BrightnessExtract.ps", Format::RGBA_8);
	blurXQuad = CreateUtilityNDCQuad("BlurXQuad", "Assets/Shaders/BlurX.vs", "Assets/Shaders/BlurX.ps", Format::RGBA_8);
	blurYQuad = CreateUtilityNDCQuad("BlurYQuad", "Assets/Shaders/BlurY.vs", "Assets/Shaders/BlurY.ps", Format::RGBA_8);
	rendererCompositeQuad = CreateUtilityNDCQuad("RendererCompositeQuad", "Assets/Shaders/BloomComposite.vs", "Assets/Shaders/BloomComposite.ps", Format::RGBA_8);
}

void DeferredWorldRenderer::Initialize(RTXModelData* rtxModelData) {

	//If we have raytracing support, configure the pass, and add it to our jobs list
	///////////////////////////////////////////////////////////////////////////////////
	if (isDXRSupported) {

		iAS = this->InitializeRTX(rtxModelData);

		RTXPassInput rtxPassInput{};
		rtxPassInput.iTextures[0] = dxrOutput[0];
		rtxPassInput.iTextures[1] = dxrNormalOutput;
		rtxPassInput.rtxModelData = rtxModelData;

		dxrPass = new DXRPass(iGraphics, iWorkContexts[rtxContext], rtxPassInput);
		jobsListTwo = { dxrPass };
		allJobs.insert(allJobs.end(), jobsListTwo.begin(), jobsListTwo.end());

		threadOne = std::thread(Thread, jobsListTwo.data(), jobsListTwo.size());
	}

	//Describe the inputs to each pass
	////////////////////////////////////////////////////
	PassInput shadowPassInput{};
	shadowPassInput.renderTargets[0] = shadowRTV;

	PassInput gbufferPassInput{};
	gbufferPassInput.renderTargets[0] = worldPosRT;
	gbufferPassInput.renderTargets[1] = normalRT;
	gbufferPassInput.renderTargets[2] = materialRT;
	gbufferPassInput.renderTargets[3] = materialRT2;
	gbufferPassInput.dsv = gBufferDepth;
	gbufferPassInput.iTextures[0] = shadowMap;

	PassInput lightPassInput{};
	lightPassInput.renderTargets[0] = lightRT;
	lightPassInput.ndcPassQuad[0] = lightPassQuad;
	lightPassInput.iTextures[0] = worldPosTexture;
	lightPassInput.iTextures[1] = normalTexture;
	lightPassInput.iTextures[2] = materialTexture;

	PassInput lightCompositePassInput{};
	lightCompositePassInput.renderTargets[0] = compositeRT;
	lightCompositePassInput.iTextures[0] = lightTexture;
	lightCompositePassInput.ndcPassQuad[0] = lightCompositePassQuad;

	PassInput bloomPassInput{};
	bloomPassInput.renderTargets[0] = brightnessRT;
	bloomPassInput.renderTargets[1] = blurXRT;
	bloomPassInput.renderTargets[2] = blurYRT;
	bloomPassInput.iTextures[0] = compositeTexture;
	bloomPassInput.iTextures[1] = brightnessTexture;
	bloomPassInput.iTextures[2] = blurXTexture;
	bloomPassInput.ndcPassQuad[0] = brightnessQuad;
	bloomPassInput.ndcPassQuad[1] = blurXQuad;
	bloomPassInput.ndcPassQuad[2] = blurYQuad;

	PassInput rendererCompositePassInput{};
	rendererCompositePassInput.iTextures[0] = blurYTexture;
	rendererCompositePassInput.iTextures[1] = compositeTexture;
	rendererCompositePassInput.iTextures[2] = dxrOutput[0];
	rendererCompositePassInput.iTextures[3] = materialTexture2;
	rendererCompositePassInput.ndcPassQuad[0] = rendererCompositeQuad;

	//Create our rendering passes
	//////////////////////////////////////////////
	shadowPass            = new ShadowPass(iWorkContexts[0], shadowPassInput);
	gbufferPass           = new GBufferPass(iWorkContexts[1], gbufferPassInput);
	lightPass             = new LightPass(iWorkContexts[1], lightPassInput);
	lightCompositePass    = new LightCompositePass(iWorkContexts[1], lightCompositePassInput);
	bloomPass             = new BloomPass(iWorkContexts[1], bloomPassInput);
	rendererCompositePass = new RendererCompositePass(iWorkContexts[3], rendererCompositePassInput);

	jobsListOne   = { shadowPass };
	jobsListThree = { gbufferPass, lightPass, lightCompositePass, bloomPass };
	jobsListFour  = { rendererCompositePass };

	//Used for tracking completion of all jobs before submitting to the GPU
	////////////////////////////////////////////////////////////////////////////
	allJobs.insert(allJobs.end(), jobsListOne.begin(), jobsListOne.end());
	allJobs.insert(allJobs.end(), jobsListThree.begin(), jobsListThree.end());
	allJobs.insert(allJobs.end(), jobsListFour.begin(), jobsListFour.end());

	//Start our worker threads
	threadZero = std::thread(Thread, jobsListOne.data(), jobsListOne.size());
	threadTwo  = std::thread(Thread, jobsListThree.data(), jobsListThree.size());
	threadThree = std::thread(Thread, jobsListFour.data(), jobsListFour.size());
}

IMesh* DeferredWorldRenderer::CreateUtilityNDCQuad(std::string name, std::string vertexShaderPath, std::string pixelShaderPath, nxt::Format format) {
	
	IMesh* mesh = nullptr;
	mesh = new D3D12Mesh(iWorkContexts[0], iGraphics, NDC_QUAD);
	mesh->meshType = NDC_QUAD;
	mesh->CreateNDCPassQuad(name, vertexShaderPath, pixelShaderPath, format);

	return mesh;
}

IMesh* DeferredWorldRenderer::LoadModel(const char* pFilepath, nxt::MeshType meshType, bool genNormals) {

	IMesh* mesh = nullptr;
	mesh = new D3D12Mesh(iWorkContexts[0], iGraphics, meshType);
	mesh->meshType = meshType;
	mesh->Load(pFilepath, 1, genNormals);

	return mesh;
}

void DeferredWorldRenderer::SubmitRTXDispatchRequest(IAccelerationStructure* iAs, RTXModelData* rtxModelData) {
	this->iAS = iAs;
	this->rtxModelData = rtxModelData;
}

void DeferredWorldRenderer::SubmitMesh(IMesh* imesh, std::vector<ObjectBinding<IConstantBuffer*>> bufferBindings, std::vector<ObjectBinding<ITexture*>> textureBindings, std::vector<bool> subset, unsigned int instanceCount, IInstanceBuffer* iInstanceBuffer, bool shadowed) {
	meshes.push_back({
		imesh,
		bufferBindings,
		textureBindings,
		instanceCount,
		iInstanceBuffer,
		subset
	});

	if (shadowed) {
		shadowMeshes.push_back({
			imesh,
			bufferBindings,
			textureBindings,
			instanceCount,
			iInstanceBuffer,
		});
			
	}
}


void DeferredWorldRenderer::SubmitForwardMesh(IMesh* imesh, std::vector<ObjectBinding<IConstantBuffer*>> bufferBindings, std::vector<ObjectBinding<ITexture*>> textureBindings, unsigned int instanceCount, IInstanceBuffer* iInstanceBuffer, ShadowMesh sm) {
	forwardmeshes.push_back({
		imesh,
		bufferBindings,
		textureBindings,
		instanceCount,
		iInstanceBuffer,
		
	});
}

IAccelerationStructure* DeferredWorldRenderer::InitializeRTX(RTXModelData* rtxModelData) {

	if (rtxContext != -1) {
		iAS = iWorkContexts[rtxContext]->CreateAccelerationStructure(*rtxModelData);
		for (int i = 0; i < 3; i++) dxrOutput[i] = iWorkContexts[rtxContext]->CreateUAV("dxrOutput", nullptr, 1920, 1080, 4, nxt::Format::RGBA_8, UsageFlags::NONE);
		dxrNormalOutput = iWorkContexts[rtxContext]->CreateUAV("dxrOutput", nullptr, 1920, 1080, 4, nxt::Format::RGBA_8, UsageFlags::NONE);
	}
	this->rtxModelData = nullptr;

	return iAS;
}

void DeferredWorldRenderer::Draw(float dt, IConstantBuffer* v, Matrix4x4 worldRotate) {
	
	//Prepare world renderer data for submission to passes
	////////////////////////////////////////////////////////////
	WorldRendererData shadowWorldRendererData;
	shadowWorldRendererData.worldRendererMeshes = &shadowMeshes;
	
	WorldRendererData gbufferWorldRendererData;
	gbufferWorldRendererData.worldRendererMeshes = &meshes;

	WorldRendererData lightPassWorldRendererData;
	lightPassWorldRendererData.viewCB = v;

	WorldRendererData dxrWorldRendererData;
	dxrWorldRendererData.iAS = iAS;
	dxrWorldRendererData.dt = dt;

	shadowPass->SetSceneData(shadowWorldRendererData);
	gbufferPass->SetSceneData(gbufferWorldRendererData);
	lightPass->SetSceneData(lightPassWorldRendererData);

	if (isDXRSupported) {
		dxrWorldRendererData.iAS = iAS;

		dxrPass->SetSceneData(dxrWorldRendererData);
	}

	for (int i = 0; i < allJobs.size(); i++) 
		allJobs[i]->ran = false;

	//Launch our worker threads on the jobs we've submitted, and wait until all recording is completed
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	iWorkContexts[0]->BeginRecording();
	iWorkContexts[1]->BeginRecording();
	iWorkContexts[2]->BeginRecording();
	iWorkContexts[3]->BeginRecording();

	g_start = true;

	int numberCompletedJobs = 0;
	while (numberCompletedJobs < allJobs.size()) {
		numberCompletedJobs = 0;
		for (int i = 0; i < allJobs.size(); i++) {
			if (allJobs[i]->ran) {
				numberCompletedJobs++;
			}
		}
	}

	g_start = false;

	iWorkContexts[3]->SetBackBufferPresentState(iGraphics);

	iWorkContexts[0]->StopRecording();
	iWorkContexts[1]->StopRecording();
	iWorkContexts[2]->StopRecording();
	iWorkContexts[3]->StopRecording();


	//Submit our recorded command lists to the gpu queue
	////////////////////////////////////////////////////////////////
	nxt::IWorkingContext* workContexts[4] = { iWorkContexts[0],  iWorkContexts[1], iWorkContexts[2] , iWorkContexts[3] };
	iGraphics->SubmitWorkingContextsForExecution(workContexts, 4);

	meshes.clear();
	shadowMeshes.clear();
	forwardmeshes.clear();
}