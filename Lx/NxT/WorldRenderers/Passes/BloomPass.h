#include "Pass.h"

namespace nxt {

	class BloomPass : public Pass {
	public:
		BloomPass(IWorkingContext* context, PassInput passInput) {
			this->context = context;
			
			this->ndcPassQuads.resize(3);
			this->ndcPassQuads[0] = passInput.ndcPassQuad[0];
			this->ndcPassQuads[1] = passInput.ndcPassQuad[1];
			this->ndcPassQuads[2] = passInput.ndcPassQuad[2];

			this->lightPassCompositeTexture = passInput.iTextures[0];
			this->brightnessTexture = passInput.iTextures[1];
			this->blurXTexture = passInput.iTextures[2];

			this->rtvs.resize(3);
			this->rtvs[0] = passInput.renderTargets[0];
			this->rtvs[1] = passInput.renderTargets[1];
			this->rtvs[2] = passInput.renderTargets[2];
		}

		virtual void Run() {

			//Extract brightness from LightPassComposite Output
			//////////////////////////////
			RenderTargetTransisition renderTargetTransisition{};
			renderTargetTransisition.beforeResourceUsage = ResourceUsage::PS_RESOURCE;
			renderTargetTransisition.afterResourceUsage = ResourceUsage::RENDER_TARGET;
			renderTargetTransisition.rtv = rtvs[0];

			context->TransisitionRenderTarget(renderTargetTransisition);

			const float clearColor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
			context->SetRenderTargets(1, { rtvs[0] }, nullptr);

			context->Clear(rtvs[0], nullptr, clearColor);
			context->BindArgumentLayout();

			ndcPassQuads[0]->Bind(context);
			ndcPassQuads[0]->SetPipeline(context, ndcPassQuads[0]->iPipeline);

			TextureTransisition textureTransisition{};
			textureTransisition.beforeResourceUsage = ResourceUsage::RENDER_TARGET;
			textureTransisition.afterResourceUsage = ResourceUsage::PS_RESOURCE;
			textureTransisition.texture = lightPassCompositeTexture;

			context->TransisitionTexture(textureTransisition);

			context->SetTexture(0, lightPassCompositeTexture); //true);
			ndcPassQuads[0]->Draw(context, 1, 6, 0);
			/////////////////////////////
			//

			//Blur along the X
			/////////////////////////////
			renderTargetTransisition.beforeResourceUsage = ResourceUsage::PS_RESOURCE;
			renderTargetTransisition.afterResourceUsage = ResourceUsage::RENDER_TARGET;
			renderTargetTransisition.rtv = rtvs[1];

			context->TransisitionRenderTarget(renderTargetTransisition);
			context->SetRenderTargets(1, { rtvs[1] }, nullptr);

			context->Clear(rtvs[1], nullptr, clearColor);
			context->BindArgumentLayout();

			ndcPassQuads[1]->Bind(context);
			ndcPassQuads[1]->SetPipeline(context, ndcPassQuads[1]->iPipeline);

			textureTransisition.beforeResourceUsage = ResourceUsage::RENDER_TARGET;
			textureTransisition.afterResourceUsage = ResourceUsage::PS_RESOURCE;
			textureTransisition.texture = brightnessTexture;

			context->TransisitionTexture(textureTransisition);

			context->SetTexture(0, brightnessTexture); //true)
			ndcPassQuads[1]->Draw(context, 1, 6, 0);
			/////////////////////////////
			//
			
			//Blur along the Y
			/////////////////////////////
			renderTargetTransisition.beforeResourceUsage = ResourceUsage::PS_RESOURCE;
			renderTargetTransisition.afterResourceUsage = ResourceUsage::RENDER_TARGET;
			renderTargetTransisition.rtv = rtvs[2];

			context->TransisitionRenderTarget(renderTargetTransisition);

			context->SetRenderTargets(1, { rtvs[2] }, nullptr);

			context->Clear(rtvs[2], nullptr, clearColor);
			context->BindArgumentLayout();

			ndcPassQuads[2]->Bind(context);
			ndcPassQuads[2]->SetPipeline(context, ndcPassQuads[2]->iPipeline);

			textureTransisition.beforeResourceUsage = ResourceUsage::RENDER_TARGET;
			textureTransisition.afterResourceUsage = ResourceUsage::PS_RESOURCE;
			textureTransisition.texture = blurXTexture;

			context->TransisitionTexture(textureTransisition);

			context->SetTexture(0, blurXTexture); //true);
			ndcPassQuads[2]->Draw(context, 1, 6, 0);
			//////////////////////////////
			//
		}
	private:
		ITexture* lightPassCompositeTexture;
		ITexture* brightnessTexture;
		ITexture* blurXTexture;
		IWorkingContext* context;

		std::vector<nxt::IRenderTarget*> rtvs;
		std::vector<IMesh*> ndcPassQuads;
	};
};