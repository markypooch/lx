#include "Pass.h"

namespace nxt {
	class RendererCompositePass : public Pass {
	public:
		RendererCompositePass(IWorkingContext* context, PassInput passInput) {
			this->context = context;
			this->ndcPassQuad = passInput.ndcPassQuad[0];
			this->bloomBlurYPassTexture = passInput.iTextures[0];
			this->lightPassCompositeTexture = passInput.iTextures[1];
			this->dxrOutputTexture = passInput.iTextures[2];
			this->materialBackboard = passInput.iTextures[3];
		}
		
		virtual void Run() {

			const float clearColor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
			context->SetRenderTargets(0, {}, nullptr);

			context->Clear(nullptr, nullptr, clearColor);
			context->BindArgumentLayout();

			ndcPassQuad->Bind(context);
			ndcPassQuad->SetPipeline(context, ndcPassQuad->iPipeline);

			TextureTransisition textureTransisition{};
			textureTransisition.beforeResourceUsage = ResourceUsage::RENDER_TARGET;
			textureTransisition.afterResourceUsage = ResourceUsage::PS_RESOURCE;
			textureTransisition.texture = bloomBlurYPassTexture;

			context->TransisitionTexture(textureTransisition);

			textureTransisition.texture = materialBackboard;
			context->TransisitionTexture(textureTransisition);

			context->SetTexture(0, lightPassCompositeTexture); //true);
			context->SetTexture(1, bloomBlurYPassTexture);
			if (dxrOutputTexture != nullptr)
				context->SetTexture(2, dxrOutputTexture, true);
			context->SetTexture(3, materialBackboard);
			ndcPassQuad->Draw(context, 1, 6, 0);
		}

	private:
		ITexture* bloomBlurYPassTexture;
		ITexture* lightPassCompositeTexture;
		ITexture* dxrOutputTexture;
		ITexture* materialBackboard;
		IWorkingContext* context;
		nxt::IMesh* ndcPassQuad;
	};

};