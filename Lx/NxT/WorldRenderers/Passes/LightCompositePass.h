#include "Pass.h"

namespace nxt {
	class LightCompositePass : public Pass {
	public:
		LightCompositePass(IWorkingContext* context, PassInput passInput) {
			this->context = context;
			this->ndcPassQuad = passInput.ndcPassQuad[0];
			this->lightPassTexture = passInput.iTextures[0];
			this->forwardPassTexture = passInput.iTextures[1];
			this->rtv = passInput.renderTargets[0];
		}

		virtual void Run() {

			const float clearColor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };

			RenderTargetTransisition renderTargetTransisition{};
			renderTargetTransisition.beforeResourceUsage = ResourceUsage::PS_RESOURCE;
			renderTargetTransisition.afterResourceUsage = ResourceUsage::RENDER_TARGET;
			renderTargetTransisition.rtv = rtv;

			context->TransisitionRenderTarget(renderTargetTransisition);

			context->SetRenderTargets(1, { rtv }, nullptr);

			context->Clear(rtv, nullptr, clearColor);
			context->BindArgumentLayout();

			ndcPassQuad->Bind(context);
			ndcPassQuad->SetPipeline(context, ndcPassQuad->iPipeline);

			TextureTransisition textureTransisition{};
			textureTransisition.beforeResourceUsage = ResourceUsage::RENDER_TARGET;
			textureTransisition.afterResourceUsage = ResourceUsage::PS_RESOURCE;
			textureTransisition.texture = lightPassTexture;

			context->TransisitionTexture(textureTransisition);

			context->SetTexture(0, lightPassTexture);
			//context->SetTexture(1, forwardTexture);
			ndcPassQuad->Draw(context, 1, 6, 0);
		}
	private:
		ITexture* lightPassTexture;
		ITexture* forwardPassTexture;
		IWorkingContext* context;
		nxt::IRenderTarget* rtv;
		nxt::IMesh* ndcPassQuad;
	};
};