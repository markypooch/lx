#include "Pass.h"

namespace nxt {
	class LightPass : public Pass {
	public:
		LightPass(IWorkingContext* context, PassInput passInput) {
			this->context = context;
			this->rtv = passInput.renderTargets[0];
			this->ndcPassQuad = passInput.ndcPassQuad[0];
			
			this->gBuffer.resize(3);
			this->gBuffer[0] = passInput.iTextures[0];
			this->gBuffer[1] = passInput.iTextures[1];
			this->gBuffer[2] = passInput.iTextures[2];
		}

		virtual void SetSceneData(WorldRendererData worldRendererData) {
			this->data = &worldRendererData;
		}

		virtual void Run() {

			RenderTargetTransisition renderTargetTransisition{};
			renderTargetTransisition.beforeResourceUsage = ResourceUsage::PS_RESOURCE;
			renderTargetTransisition.afterResourceUsage = ResourceUsage::RENDER_TARGET;
			renderTargetTransisition.rtv = rtv;

			context->TransisitionRenderTarget(renderTargetTransisition);

			const float clearColor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
			context->SetRenderTargets(1, { rtv }, nullptr);

			context->Clear(rtv, nullptr, clearColor);
			context->BindArgumentLayout();

			ndcPassQuad->Bind(context);
			ndcPassQuad->SetPipeline(context, ndcPassQuad->iPipeline);

			context->SetBuffer(1, data->viewCB);

			TextureTransisition textureTransisition{};
			textureTransisition.beforeResourceUsage = ResourceUsage::RENDER_TARGET;
			textureTransisition.afterResourceUsage = ResourceUsage::PS_RESOURCE;

			for (int i = 0; i < 3; i++) {
				textureTransisition.texture = gBuffer[i];
				context->TransisitionTexture(textureTransisition);

				context->SetTexture(i, gBuffer[i]);
			}

			ndcPassQuad->Draw(context, 1, 6, 0);
		}
	private:
		IWorkingContext* context;
		nxt::IRenderTarget* rtv;
		nxt::IMesh* ndcPassQuad;
		std::vector<nxt::ITexture*> gBuffer;
		WorldRendererData* data;
	};
};