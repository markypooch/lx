#pragma once
#include "Pass.h"
#include "../../Direct3D12/D3D12ConstantBuffer.h"

#include <chrono>

namespace nxt {


	struct RTXSettings {
		float viewToWorld[16];
		float view[16];
		float zPlaneSize[2];
		float projNear;
		float randomSeed;
		float translate;
		float pad[3];

	};

	class DXRPass : public Pass {
	public:
		DXRPass(IGraphics* iGraphics, IWorkingContext* context, RTXPassInput passInput) {
			this->context = context;
			this->dxrOutput = passInput.iTextures[0];
			this->dxrNormalOutput = passInput.iTextures[1];
			this->rtxModelData = passInput.rtxModelData;

			Matrix4x4 proj = ProjectionLH(1920.0f / 1080.0f, 60.0f * 3.14f / 180.0f, 0.1f, 200.0f);

			view.SetIdentity();
			view.SetTranslation(0.0f, -1.0f, 20.0f);
			inverseView = MatrixInverse(view);

			_rtsettings_cb = new D3D12ConstantBufferHelper<RTXSettings>(iGraphics, context, true);
			_rtsettings_cb->Initialize(6);

			memcpy(&_rtsettings_cb->type.view[0], &view._r[0], sizeof(float) * 16);
			memcpy(&_rtsettings_cb->type.viewToWorld[0], &inverseView._r[0], sizeof(float) * 16);

			//_rtsettings_cb->type.viewToWorld = inverseView
			_rtsettings_cb->type.zPlaneSize[0] = 1.0f / proj._r[0];
			_rtsettings_cb->type.zPlaneSize[1] = 1.0f / proj._r[5];
			_rtsettings_cb->type.translate = 0;
			_rtsettings_cb->type.randomSeed = (float)sin((double)std::chrono::high_resolution_clock::now().time_since_epoch().count());
		}

		virtual void SetSceneData(WorldRendererData data) {
			this->data = &data;
		}

		virtual void Run() {

			if (data->iAS->dirty) {
				context->RebuildTLAS(data->iAS);
				_rtsettings_cb->type.projNear = 0.0f;

				data->iAS->dirty = false;
			}
			else {
				_rtsettings_cb->type.projNear++;
			}

			context->SetRTXTexture(0, dxrOutput);
			context->SetRTXTexture(1, dxrNormalOutput);
			context->SetSRVBuffer(this->rtxModelData->materialIndices, 3);
			context->SetSRVBuffer(this->rtxModelData->positions, 4);
			context->SetSRVBuffer(this->rtxModelData->indices, 5);
			context->SetSRVBuffer(this->rtxModelData->normals, 6);
			context->SetSRVBuffer(this->rtxModelData->instanceOffsets, 7);
			context->SetSRVBuffer(this->rtxModelData->uvs, 8);
			context->SetSRVBuffer(this->rtxModelData->bitangs, 9);
			context->SetSRVBuffer(this->rtxModelData->tangs, 10);
		
			context->SetRTXTextureArray(0, this->rtxModelData->textures.data(), this->rtxModelData->textures.size());
			
			context->SetRTXBuffer(0, _rtsettings_cb->Update());
			context->SetRTXBuffer(1, this->rtxModelData->iCB);
			
			context->DispatchRays(data->iAS, dxrOutput);

			time += data->dt;
			if (time > 64)
				time = 0.0f;

			_rtsettings_cb->type.translate = time / 64.0f;

			_rtsettings_cb->type.randomSeed = (float)sin((double)std::chrono::high_resolution_clock::now().time_since_epoch().count());
		}

	private:
		RTXModelData* rtxModelData;
		ITexture* dxrOutput;
		ITexture* dxrNormalOutput;
		IWorkingContext* context;

		Matrix4x4 view, inverseView, proj;
		float time = 0.0f;
		float translateAmount;
		IConstantBufferHelper<RTXSettings>* _rtsettings_cb;
		WorldRendererData* data;
	};

};