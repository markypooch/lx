#include "Pass.h"

namespace nxt {
	class ShadowPass : public Pass {
	public:
		ShadowPass(IWorkingContext* context, PassInput passInput) {
			this->context = context;
			this->rtv = passInput.renderTargets[0];


			//We build a special purpose pso for the shadow map pass
			/////////////////////////////////////////////////////////////////
			VertexLayout vertexLayout[] = {
				{"POSITION", 0, Format::R32G32B32A32_FLOAT, Classification::PER_VERTEX, 0, 0, 0},
				{"TEXCOORD", 0, Format::R32G32_FLOAT, Classification::PER_VERTEX, 16, 0, 0},
				{"NORMAL", 0, Format::R32G32B32A32_FLOAT, Classification::PER_VERTEX, 24, 0, 0},
				{"TANGENT", 0, Format::R32G32B32A32_FLOAT, Classification::PER_VERTEX, 40, 0, 0},
				{"BITANGENT", 0, Format::R32G32B32A32_FLOAT, Classification::PER_VERTEX, 56, 0, 0}
			};

			unsigned int rtvCount = 1;
			IShader* vs = nullptr, * ps = nullptr;

			//Compile shadow map shaders
			auto vsShaderName = L"Assets/Shaders/" + s2ws("ShadowMap.vs");
			LPCWSTR vsName = vsShaderName.c_str();

			vs = context->CreateShader(vsName, ShaderType::VS);
			auto psShaderName = L"Assets/Shaders/" + s2ws("ShadowMap.ps");

			LPCWSTR psName = psShaderName.c_str();
			ps = context->CreateShader(psName, ShaderType::PS);

			IVertexLayout* vlo = context->CreateVertexLayout("GLTFLayout", vertexLayout, 5);
			pso = context->CreatePipeline
			(
				"ShadowMap",
				{ vs, ps },
				nullptr,
				vlo,
				0,
				nxt::Format::UNKNOWN,
				nxt::Format::DEPTH_32_FLOAT
			);
			/////////////////////////////////////////////////////////////////////
			//

		}

		std::wstring s2ws(const std::string& s)
		{
			int len;
			int slength = (int)s.length() + 1;
			len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
			wchar_t* buf = new wchar_t[len];
			MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
			std::wstring r(buf);
			delete[] buf;
			return r;
		}

		virtual void SetSceneData(WorldRendererData data) {
			this->data = &data;
		}

		virtual void Run() {

			const float clearColor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };

			RenderTargetTransisition renderTargetTrans{};
			renderTargetTrans.rtv = rtv;
			renderTargetTrans.beforeResourceUsage = ResourceUsage::PS_RESOURCE;
			renderTargetTrans.afterResourceUsage = ResourceUsage::DEPTH_WRITE;

			context->TransisitionRenderTarget(renderTargetTrans);

			context->SetRenderTargets(0, {}, rtv, true);
			context->SetViewport(8192, 8192);

			context->Clear(nullptr, rtv, clearColor);

			std::vector<WorldRendererMesh> meshes = *data->worldRendererMeshes;
			for (int i = 0; i < meshes.size(); i++) {

				IMesh* mesh = meshes[i].mesh;

				for (auto const& subset : mesh->modelData->subsets) {
					context->BindArgumentLayout();
					for (auto const& buffer : meshes[i].bufferBindings)
						context->SetBuffer(buffer.bindIndex, buffer.object);

					mesh->Bind(context, meshes[i].iInstantBuffer);
					mesh->SetPipeline(context, pso);

					mesh->DrawIndexed(context, subset.indexOffset, subset.indexSize, meshes[i].instanceCount);
				}
			}

			context->SetViewport(3840, 2160);
		}
	private:
		IPipeline* pso;
		IWorkingContext* context;
		nxt::IRenderTarget* rtv;
		WorldRendererData* data;
	};
};