#include "Pass.h"

namespace nxt {
	class GBufferPass : public Pass {
	public:
		GBufferPass(IWorkingContext* context, PassInput passInput) {
			this->context = context;
			
			this->rtvs.resize(4);
			this->rtvs[0] = passInput.renderTargets[0];
			this->rtvs[1] = passInput.renderTargets[1];
			this->rtvs[2] = passInput.renderTargets[2];
			this->rtvs[3] = passInput.renderTargets[3];
			
			this->dsv = passInput.dsv;
			this->shadowTexture = passInput.iTextures[0];
		}

		virtual void SetSceneData(WorldRendererData worldRendererData) {
			this->data = &worldRendererData;
		}

		virtual void Run() {

			RenderTargetTransisition renderTargetTransisition{};
			renderTargetTransisition.beforeResourceUsage = ResourceUsage::PS_RESOURCE;
			renderTargetTransisition.afterResourceUsage = ResourceUsage::RENDER_TARGET;

			for (int i = 0; i < 4; i++) {
				renderTargetTransisition.rtv = rtvs[i];
				context->TransisitionRenderTarget(renderTargetTransisition);
			}

			const float clearColor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
			context->SetRenderTargets(rtvs.size(), rtvs, dsv);

			for (int i = 0; i < rtvs.size(); i++)
				context->Clear(rtvs[i], nullptr, clearColor);
			context->Clear(nullptr, dsv, clearColor);

			TextureTransisition textureTrans{};
			textureTrans.texture = shadowTexture;
			textureTrans.beforeResourceUsage = ResourceUsage::DEPTH_WRITE;
			textureTrans.afterResourceUsage = ResourceUsage::PS_RESOURCE;

			context->TransisitionTexture(textureTrans);

			std::vector<WorldRendererMesh> meshes = *data->worldRendererMeshes;
			for (int i = 0; i < meshes.size(); i++) {
				IMesh* mesh = meshes[i].mesh;

				int index = 0;
				for (auto const& subsetVector : mesh->modelData->orderedSubsets) {
					for (auto const& subset : subsetVector.second) {
						if (meshes[i].subsetRender[index]) {
							//Subset subset = mesh->modelData->subsets[j];

							context->BindArgumentLayout();
							for (auto const& buffer : meshes[i].bufferBindings)
								context->SetBuffer(buffer.bindIndex, buffer.object);

							for (auto const& texture : meshes[i].textureBindings)
								context->SetTexture(texture.bindIndex, texture.object);

							context->SetTexture(0, shadowTexture);
							mesh->Bind(context, meshes[i].iInstantBuffer);
							mesh->SetPipeline(context, subset->pipeline);

							context->SetBuffer(4, mesh->modelData->materialCB);

							for (auto const& texture : subset->textures)
								context->SetTexture(texture.first, texture.second);

							mesh->DrawIndexed(context, subset->indexOffset, subset->indexSize, meshes[i].instanceCount);
						}
						index++;
					}
				}
			}
		}
	private:
		nxt::ITexture* shadowTexture;
		IWorkingContext* context;
		std::vector<nxt::IRenderTarget*> rtvs;
		nxt::IRenderTarget* dsv;
		WorldRendererData* data;
	};
};