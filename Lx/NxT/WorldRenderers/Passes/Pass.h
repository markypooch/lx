#pragma once

#include "../../IGraphics.h"
#include "../../IMesh.h"
#include "../../IWorldRenderer.h"

namespace nxt {

	struct PassInput {
		IRenderTarget* renderTargets[8];
		ITexture* iTextures[8];
		IMesh* ndcPassQuad[8];
		IRenderTarget* dsv;

		unsigned int numberOfRenderTargets;
		unsigned int numberOfTextures;
		unsigned int numberOfMeshes;
	};

	struct RTXPassInput {
		ITexture* iTextures[8];
		RTXModelData* rtxModelData;
	};


	class Pass {
	public:
		virtual void Run() = 0;
		virtual void SetSceneData(WorldRendererData worldRendererData) {};
	public:
		bool ran = false;
	};
};