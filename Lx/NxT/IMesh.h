#pragma once
#include "IGraphics.h"
#include "ConstantBufferHelper.h"
#include <map>

namespace nxt {

	struct Positions {
		float pos[3];
	};

	struct UV {
		float texcoord[2];
	};

	struct Normal {
		float normal[3];
	};

	struct Bitangent {
		float bitangent[3];
	};

	struct Tangent {
		float tangent[3];
	};

	enum MeshType {
		GLTF,
		OBJ,
		NDC_QUAD
	};

	struct MetallicRoughness {
		float baseColor[4];
		float emissiveColor[4];
		float matMetallic;
		float matRoughness;
		float matOcclusion;
		int hasBaseColorTexture;
		int hasNormalTexture;
		int hasEmissiveTexture;
		int hasMetallicRoughnessTexture;
		int hasOcclusionTexture;
		//int baseTextureIdx;
		//int pad[4];
	};

	struct Material : public MetallicRoughness {
		
		ITexture* normalMap         = nullptr;
		ITexture* metallicRoughness = nullptr;
		ITexture* baseColor         = nullptr;
	};

	struct Subset {

		std::string name;
		unsigned int indexOffset;
		unsigned int indexSize;
		std::map<unsigned int, ITexture*> textures;
		std::map<unsigned int, ITexture*> rtxTextures;
		std::map<unsigned int, IConstantBuffer*> buffers;

		IArgumentLayout* rootSignature;
		IPipeline* pipeline;
	};


	struct ModelData {
		IVertexBuffer* vertexBuffer;
		IIndexBuffer* indexBuffer;
		std::vector<Subset> subsets;
		std::map<int, std::vector<Subset*>> orderedSubsets;

		std::vector<Pointlight> pointLights;

		IConstantBuffer* materialCB;
		IConstantBuffer* materialCBRTX;
		IPipeline* shadowPipeline;
		IConstantBufferHelper<MetallicRoughness[128]>* cbMaterial;
	};

	class IMesh
	{
	public:
		virtual void Load(const char* pFilename = "", unsigned int instances=1, bool genNormals=false) = 0;
		virtual void Bind(IWorkingContext*, IInstanceBuffer* iInstantBuffer = nullptr) = 0;
		virtual void SetPipeline(IWorkingContext*, IPipeline*) = 0;
		virtual void SetArgumentLayout(IArgumentLayoutObject* argumentLayout) = 0;
		virtual void SetPipelineMap(std::unordered_map<std::string, IPipeline*> pipelineMap) = 0;
		virtual void Draw(IWorkingContext*, unsigned int instanceCount, unsigned int vertexCount, unsigned int start) = 0;
		virtual void DrawIndexed(IWorkingContext*, unsigned int startIndex, unsigned int indexCount, unsigned int instanceCount) = 0;
		virtual void CreateNDCPassQuad(std::string name, std::string vertexShaderPath, std::string pixelShaderPath, nxt::Format format, float sizeX = 1.0f, float sizeY = 1.0f) = 0;
		ModelData* LoadAI(IWorkingContext*, IGraphics*, std::string magmaModelName, unsigned int screenWidth, unsigned int screenHeight, bool genNormals);
		void DrawMesh();
	protected:
		std::wstring s2ws(const std::string& s)
		{
			int len;
			int slength = (int)s.length() + 1;
			len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
			wchar_t* buf = new wchar_t[len];
			MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
			std::wstring r(buf);
			delete[] buf;
			return r;
		}

	public:
		IPipeline* iPipeline;
		IPipeline* shadowPipeline;
		ModelData* modelData;

		ITexture* diffuseMap;
		ITexture* normalMap;

		IVertexBuffer* iVertexBuffer;
		IIndexBuffer* iIndexBuffer;

		//RTX data
		std::vector<Bitangent> bitang;
		std::vector<Tangent> tang;
		std::vector<unsigned int> offsets;
		std::vector<UV> uv;
		std::vector<Normal> normal;
		std::vector<unsigned int> materialIndice;
		std::vector<unsigned int> indice;
		std::vector<Positions> pos;

		std::map<std::string, uint8_t> textureNameMap;
		std::vector<ITexture*> textures;

		std::vector<unsigned> rtxVertexSizes;
		std::vector<unsigned> rtxIndexSizes;
		std::vector<IVertexBuffer*> iRTXVertexBuffers;
		std::vector<IIndexBuffer*> iRTXIndexBuffers;
		ISRVBuffer* positions, * indices, * uvs, * materialIndices, * normals, * instanceOffsets, *bitangs, *tangs;

		MeshType meshType;
		uint8_t matIndex;

		std::map<uint8_t, MetallicRoughness> mats;
		IArgumentLayoutObject* argumentLayoutObject;
		std::unordered_map<std::string, IPipeline*> pipelineMap;

		static int rtxMatIndex;
		static int rtxIndiceOffset;
		static int vbVerticesRtxOffsetSize;
		static int ibIndicesRtxOffsetSize;
		static int rtxOffset;
		static std::map<std::string, uint8_t> rtxNameMap;
		static std::map<uint8_t, MetallicRoughness> rtxMats;
		static std::map<std::string, uint8_t> rtxTextureNameMap;
		static std::vector<ITexture*> rtxTextures;
		static int rtxSubsetIndex;
		static std::map<std::string, uint8_t> nameMap;
	};


};