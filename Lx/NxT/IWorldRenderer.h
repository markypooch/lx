#pragma once
#include <unordered_map>
#include "../NxT/Matrix.h"

#include "IMesh.h"

namespace nxt {

	template<typename T>
	struct ObjectBinding {
		unsigned int bindIndex;
		T object;
	};

	struct ShadowMesh {
		bool shadowEnabled;
		std::vector<ObjectBinding<IConstantBuffer*>> bufferBindings;
	};

	struct WorldRendererMesh {
		IMesh* mesh;
		std::vector<ObjectBinding<IConstantBuffer*>> bufferBindings;
		std::vector<ObjectBinding<ITexture*>> textureBindings;
		unsigned int instanceCount = 1;
		IInstanceBuffer* iInstantBuffer;
		std::vector<bool> subsetRender;
	};

	struct WorldRendererData {
	
		std::vector<WorldRendererMesh>* worldRendererMeshes;
		IConstantBuffer* viewCB;
		IAccelerationStructure* iAS;
		float dt;
	};

	class IWorldRenderer
	{
	public:

		IWorldRenderer(IGraphics* iGraphics) {
			this->iGraphics = iGraphics;
			//this->iWorkContexts = contexts;
		}

		void RegisterPipeline(std::string name, IPipeline* iPipeline) {
			pipelines[name] = std::move(iPipeline);
		}

		virtual void   Initialize(RTXModelData* rtxModelData) = 0;
		virtual IMesh* LoadModel(const char* pFilepath, nxt::MeshType meshType, bool genNormals = false) = 0;
		virtual void   SubmitForwardMesh(IMesh* imesh, std::vector<ObjectBinding<IConstantBuffer*>> bufferBindings, std::vector<ObjectBinding<ITexture*>> textureBindings, unsigned int instanceCount = 1, IInstanceBuffer* iInstanceBuffer = nullptr, ShadowMesh shadowMesh = ShadowMesh()) = 0;
		virtual void   SubmitMesh(IMesh* imesh, std::vector<ObjectBinding<IConstantBuffer*>> bufferBindings, std::vector<ObjectBinding<ITexture*>> textureBindings, std::vector<bool> subsetRender, unsigned int instanceCount=1, IInstanceBuffer* iInstanceBuffer=nullptr, bool shadowed = true) = 0;
		virtual IAccelerationStructure* GetIAS() = 0;
		virtual void   SubmitRTXDispatchRequest(IAccelerationStructure* iAS, RTXModelData* rtxModelData) = 0;
		virtual void   Draw(float, IConstantBuffer* view, Matrix4x4 worldRotate) = 0;

	public:

		ITexture* gbufferDepthTexture;
		ITexture* materialTexture;
		ITexture* materialTexture2;
		ITexture* normalTexture;
		ITexture* worldPosTexture;
		ITexture* lightTexture;

		IGraphics* iGraphics;
		std::vector<IWorkingContext*> iWorkContexts;

		bool isDXRSupported = false;

	protected:

		virtual IAccelerationStructure* InitializeRTX(RTXModelData* rtxModelData) = 0;

		IAccelerationStructure* iAS;
		IArgumentLayoutObject* al;
		std::unordered_map<std::string, IPipeline*> pipelines;
		std::vector<WorldRendererMesh> meshes;
		std::vector<WorldRendererMesh> shadowMeshes;
		std::vector<WorldRendererMesh> forwardmeshes;
	};
};