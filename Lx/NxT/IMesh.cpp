#include "IMesh.h"
using namespace nxt;

#include "ConstantBufferHelper.h"
#include "Direct3D12/D3D12ConstantBuffer.h"
#include "../StbTextureLoader.h"

#include <d3d12shader.h>
#include <unordered_map>
#include <fstream>
#include <iostream>
#include <string>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/pbrmaterial.h>

static std::unordered_map<std::string, ModelData*> models;

struct Vertex {
	float position[4];
	float texcoord[2];
	float normal[4];
	float tangent[4];
	float bitangent[4];
};

namespace {

	static std::unordered_map<std::string, IPipeline*> pipelines;
	std::wstring s2ws(const std::string& s)
	{
		int len;
		int slength = (int)s.length() + 1;
		len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
		wchar_t* buf = new wchar_t[len];
		MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
		std::wstring r(buf);
		delete[] buf;
		return r;
	}


	std::vector<std::string> stringSplit(std::string delimiter, std::string s) {

		std::vector<std::string> tokens;

		size_t pos = 0;
		std::string token;
		while ((pos = s.find(delimiter)) != std::string::npos) {
			token = s.substr(0, pos);
			tokens.push_back(token);
			s.erase(0, pos + delimiter.length());

			if ((pos = s.find(delimiter)) == std::string::npos) {
				token = s.substr(0, pos);
				tokens.push_back(token);
			}
		}

		return tokens;
	}

	IPipeline* ReadPipelineDefinitions(IWorkingContext* iWorkingContext, std::string materialAttributes) {
		std::string pipelineName = "pipeline_" + materialAttributes;

		if (pipelines.count(pipelineName) > 0) {
			return pipelines[pipelineName];
		}
		else {
			std::ifstream fIn;
			fIn.open("Assets/pipeline.txt");

			std::vector<VertexLayout> vertexLayout;
			vertexLayout.resize(5);

			vertexLayout[0].semanticName = "POSITION";
			vertexLayout[0].format = Format::R32G32B32A32_FLOAT;
			vertexLayout[0].classification = Classification::PER_VERTEX;
			vertexLayout[0].byteOffset = 0;
			vertexLayout[0].semanticIndex = 0;
			vertexLayout[0].inputSlot = 0;
			vertexLayout[0].instanceDataStepRate = 0;

			vertexLayout[1].semanticName = "TEXCOORD";
			vertexLayout[1].format = Format::R32G32_FLOAT;
			vertexLayout[1].classification = Classification::PER_VERTEX;
			vertexLayout[1].byteOffset = 16;
			vertexLayout[1].semanticIndex = 0;
			vertexLayout[1].inputSlot = 0;
			vertexLayout[1].instanceDataStepRate = 0;

			vertexLayout[2].semanticName = "NORMAL";
			vertexLayout[2].format = Format::R32G32B32A32_FLOAT;
			vertexLayout[2].classification = Classification::PER_VERTEX;
			vertexLayout[2].byteOffset = 24;
			vertexLayout[2].semanticIndex = 0;
			vertexLayout[2].inputSlot = 0;
			vertexLayout[2].instanceDataStepRate = 0;

			vertexLayout[3].semanticName = "TANGENT";
			vertexLayout[3].format = Format::R32G32B32A32_FLOAT;
			vertexLayout[3].classification = Classification::PER_VERTEX;
			vertexLayout[3].byteOffset = 40;
			vertexLayout[3].semanticIndex = 0;
			vertexLayout[3].inputSlot = 0;
			vertexLayout[3].instanceDataStepRate = 0;

			vertexLayout[4].semanticName = "BITANGENT";
			vertexLayout[4].format = Format::R32G32B32A32_FLOAT;
			vertexLayout[4].classification = Classification::PER_VERTEX;
			vertexLayout[4].byteOffset = 56;
			vertexLayout[4].semanticIndex = 0;
			vertexLayout[4].inputSlot = 0;
			vertexLayout[4].instanceDataStepRate = 0;

			CD3DX12_RASTERIZER_DESC    rasterizerStateDesc(D3D12_DEFAULT);
			CD3DX12_BLEND_DESC         blendDesc(D3D12_DEFAULT);
			CD3DX12_DEPTH_STENCIL_DESC depthStencilDesc(D3D12_DEFAULT);
			unsigned int rtvCount = 1;
			IShader* vs = nullptr, * ps = nullptr;
			nxt::Format rtvFormat = nxt::Format::UNKNOWN;
			nxt::Format dsvFormat = nxt::Format::UNKNOWN;

			std::string line;
			bool pipelineDescriptionFound = false;
			while (std::getline(fIn, line)) {
				size_t separator = line.find_first_of(":");
				if (separator == std::string::npos)
					continue;

				std::string descriptor = line.substr(0, separator);
				if (descriptor == "name") {
					if (pipelineDescriptionFound)
						break;

					std::string value = line.substr(separator+1, line.size() - 1);
					if (value.find(pipelineName) == std::string::npos)
						continue;

					pipelineDescriptionFound = true;
				}
				if (pipelineDescriptionFound) {
					if (descriptor == "blend") {
						std::string value = line.substr(separator+1, line.size() - 1);
						if (value == "additive") {
							blendDesc.RenderTarget[0].BlendEnable = true;
							blendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
							blendDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_SRC_ALPHA;
							blendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_ONE;
							blendDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ONE;
							blendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
							blendDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
						}
					}
					else if (descriptor == "rasterizer") {
						std::string value = line.substr(separator+1, line.size() - 1);
						if (value == "cff") {
							rasterizerStateDesc.CullMode = D3D12_CULL_MODE_FRONT;
							rasterizerStateDesc.FillMode = D3D12_FILL_MODE_SOLID;
						}
					}
					else if (descriptor == "depthstate") {
						std::string value = line.substr(separator+1, line.size() - 1);
						if (value == "dd") {
							depthStencilDesc.DepthEnable = false;
							depthStencilDesc.DepthFunc = D3D12_COMPARISON_FUNC_NEVER;
							depthStencilDesc.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
						}
						else if (value == "df") {
							depthStencilDesc.DepthEnable = true;
							depthStencilDesc.DepthFunc = D3D12_COMPARISON_FUNC_LESS;
							depthStencilDesc.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
						}
					}
					else if (descriptor == "vertexshader") {
						std::string value = line.substr(separator+1, line.size() - 1);
						auto vsShaderName = L"Assets/Shaders/" + s2ws(value);

						LPCWSTR vsName = vsShaderName.c_str();

						vs = iWorkingContext->CreateShader(vsName, ShaderType::VS);
					}
					else if (descriptor == "pixelshader") {

						std::string value = line.substr(separator+1, line.size() - 1);
						auto psShaderName = L"Assets/Shaders/" + s2ws(value);

						LPCWSTR psName = psShaderName.c_str();

						ps = iWorkingContext->CreateShader(psName, ShaderType::PS);
					}
					else if (descriptor == "rtvcount") {
						std::string value = line.substr(separator+1, line.size() - 1);
						rtvCount = std::stoi(value);
					}
					else if (descriptor == "rtvformat") {
						std::string value = line.substr(separator+1, line.size() - 1);
						if (value == "RGB_F32") {
							rtvFormat = nxt::Format::R32G32B32A32_FLOAT;
						}
						else if (value == "RGB_8") {
							rtvFormat = nxt::Format::RGBA_8;
						}
					}
					else if (descriptor == "dsvformat") {
						std::string value = line.substr(separator+1, line.size() - 1);
						if (value == "DEPTH_32") {
							dsvFormat = nxt::Format::DEPTH_32_FLOAT;
						}
					}
					else if (descriptor == "instancing") {
						std::string value = line.substr(separator + 1, line.size() - 1);
						if (value == "true") {
							nxt::VertexLayout layout{};
							layout.classification = Classification::INSTANCED;
							layout.byteOffset = 0;
							layout.semanticIndex = 1;
							layout.format = nxt::Format::R32G32B32A32_FLOAT;
							layout.semanticName = "TEXCOORD";
							layout.inputSlot = 1;
							layout.instanceDataStepRate = 1;

							vertexLayout.push_back(layout);
						}
					}
					else if (descriptor == "instancingcolor") {
						std::string value = line.substr(separator + 1, line.size() - 1);
						if (value == "true") {
							nxt::VertexLayout layout{};
							layout.classification = Classification::INSTANCED;
							layout.byteOffset = 16;
							layout.semanticIndex = 0;
							layout.format = nxt::Format::R32G32B32A32_FLOAT;
							layout.semanticName = "COLOR";
							layout.inputSlot = 1;
							layout.instanceDataStepRate = 1;

							vertexLayout.push_back(layout);
						}
					}
				}
			}
			IVertexLayout* vlo = iWorkingContext->CreateVertexLayout("GLTFLayout", vertexLayout.data(), vertexLayout.size());
			IPipeline* pso = iWorkingContext->CreatePipeline
			(
				pipelineName,
				{ vs, ps },
				nullptr,
				vlo,
				rtvCount,
				rtvFormat,
				dsvFormat
			);

			pipelines[pipelineName] = pso;
			return pipelines[pipelineName];
		}
	}
}

int IMesh::rtxMatIndex;
int IMesh::rtxIndiceOffset;
int IMesh::vbVerticesRtxOffsetSize;
int IMesh::ibIndicesRtxOffsetSize;
int IMesh::rtxOffset;
int IMesh::rtxSubsetIndex;
std::map<std::string, uint8_t> IMesh::rtxNameMap;
std::map<uint8_t, MetallicRoughness> IMesh::rtxMats;
std::map<std::string, uint8_t> IMesh::rtxTextureNameMap;
std::vector<ITexture*> IMesh::rtxTextures;
std::map<std::string, uint8_t> IMesh::nameMap;

ModelData* IMesh::LoadAI(nxt::IWorkingContext* iWorkingContext, nxt::IGraphics* iGraphics, std::string magmaModelName, unsigned int screenWidth, unsigned int screenHeight, bool genNormals) {

	//if (models.count(magmaModelName) == 5) {
		Assimp::Importer importer;
		
		unsigned int flags = aiProcess_ConvertToLeftHanded | aiProcess_CalcTangentSpace  | aiProcess_PreTransformVertices;
		if (genNormals)
			flags |= aiProcess_GenNormals;

		const aiScene* scene = importer.ReadFile(magmaModelName, flags);

		ModelData* magmaModel = new ModelData();
		magmaModel->subsets.resize(scene->mNumMeshes);
		std::vector<Subset> subset;

		std::string diffuseTextureName, diffuseTexture;
		std::string normalTextureName, normalTexture;
		unsigned int indicesOffset = 0;

		std::vector<Vertex> vbVertices;
		std::vector<unsigned int> ibIndices;

		//ReadPointLights(magmaModelName, *magmaModel, 800, 600);
		bool createdShadowPipeline = false;
		for (int i = 0; i < scene->mNumMeshes; i++) {

			//offsets.push_back(ibIndices.size() / 3);

			aiMesh* mesh = scene->mMeshes[i];
			auto material = scene->mMaterials[mesh->mMaterialIndex];

			aiString name;
			aiGetMaterialString(material, AI_MATKEY_NAME, &name);
			magmaModel->subsets[i].name = std::string(name.C_Str());

			aiColor4D diffuse, ambient, emissive, specular;
			aiGetMaterialColor(material, AI_MATKEY_GLTF_PBRMETALLICROUGHNESS_BASE_COLOR_FACTOR, &diffuse);

			float metallic, roughness;

			aiGetMaterialFloat(material, AI_MATKEY_GLTF_PBRMETALLICROUGHNESS_METALLIC_FACTOR, &metallic);
			aiGetMaterialFloat(material, AI_MATKEY_GLTF_PBRMETALLICROUGHNESS_ROUGHNESS_FACTOR, &roughness);

			std::vector<Positions> lPositions;
			std::vector<unsigned int> lIndices;

			std::string f = std::string(name.C_Str());
			int redact = 0;
			if (std::isdigit(f[f.size() - 1])) {
				if (std::isdigit(f[f.size() - 2])) {
					redact = 2;
				}
				else {
					redact = 1;
				}
			}

			name = f.substr(0, f.size() - redact);

			if (nameMap.count(std::string(name.C_Str())) == 0) {

				MetallicRoughness metRough;

				metRough.baseColor[0] = diffuse.r;
				metRough.baseColor[1] = diffuse.g;
				metRough.baseColor[2] = diffuse.b;
				metRough.baseColor[3] = diffuse.a;

				metRough.matMetallic = metallic;
				metRough.matRoughness = roughness;

				metRough.emissiveColor[0] = 0.0f;
				metRough.emissiveColor[1] = 0.0f;
				metRough.emissiveColor[2] = 0.0f;
				metRough.emissiveColor[3] = 0.0f;
				if (std::string(name.C_Str()) != "stone") {
					metRough.emissiveColor[0] = 1.0f;
					metRough.emissiveColor[1] = 0.3f;
				}
							
				metRough.matOcclusion = 1.0f;
				metRough.hasBaseColorTexture = 0;
				metRough.hasEmissiveTexture = 0;
				metRough.hasMetallicRoughnessTexture = 0;
				metRough.hasNormalTexture = 0;
				metRough.hasOcclusionTexture = 0;
				//CreatePipeline(iWorkingContext, &magmaModel->subsets[i], materialAttributes);

				aiString Path;
				aiTextureMapping aiTextureMapping;
				material->GetTexture(AI_MATKEY_GLTF_PBRMETALLICROUGHNESS_BASE_COLOR_TEXTURE, &Path, &aiTextureMapping, 0, 0, 0, 0);
				if (Path.length > 0) {
					metRough.hasBaseColorTexture = 1;

					int x, y;
					int ch = 4;

					std::string fp = "Assets/Textures/" + std::string(Path.C_Str());
					void* pData;
					pData = StbTextureLoader::stbi_load_texture(fp.c_str(), x, y, ch);
					magmaModel->subsets[i].textures[0] = iWorkingContext->CreateTexture(Path.C_Str(), pData, x, y, ch, nxt::Format::RGBA_8, UsageFlags::NONE);
					magmaModel->subsets[i].rtxTextures[0] = iWorkingContext->CreateTexture(Path.C_Str(), pData, x, y, ch, nxt::Format::RGBA_8, UsageFlags::NONE, 1, true, true);

					textureNameMap[std::string(name.C_Str())] = textures.size();
					rtxTextureNameMap[std::string(name.C_Str())] = rtxTextures.size();

					textures.push_back(magmaModel->subsets[i].rtxTextures[0]);
					rtxTextures.push_back(magmaModel->subsets[i].rtxTextures[0]);
				}

				material->GetTexture(aiTextureType_NORMALS, 0, &Path, NULL, NULL, NULL, NULL, NULL);
				if (Path.length > 0) {
					metRough.hasNormalTexture = 1;

					int x, y;
					int ch = 4;

					std::string fp = "Assets/Textures/" + std::string(Path.C_Str());
					void* pData;
					pData = StbTextureLoader::stbi_load_texture(fp.c_str(), x, y, ch);
					magmaModel->subsets[i].textures[1] = iWorkingContext->CreateTexture(Path.C_Str(), pData, x, y, ch, nxt::Format::RGBA_8, UsageFlags::NONE);
					magmaModel->subsets[i].rtxTextures[1] = iWorkingContext->CreateTexture(Path.C_Str(), pData, x, y, ch, nxt::Format::RGBA_8, UsageFlags::NONE, 1, true, true);

					//nameMap[std::string(name.C_Str())] = textures.size();
					textures.push_back(magmaModel->subsets[i].rtxTextures[1]);
					rtxTextures.push_back(magmaModel->subsets[i].rtxTextures[1]);
				}

				material->GetTexture(AI_MATKEY_GLTF_PBRMETALLICROUGHNESS_METALLICROUGHNESS_TEXTURE, &Path);
				if (Path.length > 0) {
					metRough.hasMetallicRoughnessTexture = 1;

					int x, y;
					int ch = 4;

					std::string fp = "Assets/Textures/" + std::string(Path.C_Str());
					void* pData;
					pData = StbTextureLoader::stbi_load_texture(fp.c_str(), x, y, ch);
					magmaModel->subsets[i].textures[2] = iWorkingContext->CreateTexture(Path.C_Str(), pData, x, y, ch, nxt::Format::RGBA_8, UsageFlags::NONE);
					magmaModel->subsets[i].rtxTextures[2] = iWorkingContext->CreateTexture(Path.C_Str(), pData, x, y, ch, nxt::Format::RGBA_8, UsageFlags::NONE, 1, true, true);

					//nameMap[std::string(name.C_Str())] = textures.size();
					textures.push_back(magmaModel->subsets[i].rtxTextures[2]);
					rtxTextures.push_back(magmaModel->subsets[i].rtxTextures[2]);
				}

				//magmaModel->subsets[i].materialCB = magmaModel->subsets[i].cbMaterial->Update();

				nameMap[std::string(name.C_Str())] = rtxMatIndex;
				mats[i] = metRough;
				mats[i].baseColor[3] = textureNameMap[std::string(name.C_Str())];

				nameMap[std::string(name.C_Str())] = rtxMatIndex;
				rtxMats[rtxMatIndex] = metRough;
				rtxMats[rtxMatIndex].baseColor[3] = rtxTextureNameMap[std::string(name.C_Str())];

				matIndex++;
				rtxMatIndex++;
			}
			else {
				MetallicRoughness metRough;

				metRough.baseColor[0] = diffuse.r;
				metRough.baseColor[1] = diffuse.g;
				metRough.baseColor[2] = diffuse.b;
				metRough.baseColor[3] = diffuse.a;

				metRough.matMetallic = metallic;
				metRough.matRoughness = roughness;

				metRough.emissiveColor[0] = 0.0f;
				metRough.emissiveColor[1] = 0.0f;
				metRough.emissiveColor[2] = 0.0f;
				metRough.emissiveColor[3] = 0.0f;
				if (std::string(name.C_Str()) != "stone") {
					metRough.emissiveColor[0] = 1.0f;
					metRough.emissiveColor[1] = 0.3f;
				}

				metRough.matOcclusion = 1.0f;
				metRough.hasBaseColorTexture = 0;
				metRough.hasEmissiveTexture = 0;
				metRough.hasMetallicRoughnessTexture = 0;
				metRough.hasNormalTexture = 0;
				metRough.hasOcclusionTexture = 0;

				mats[i] = metRough;
				mats[i].baseColor[3] = 0;
			}
			magmaModel->subsets[i].pipeline = ReadPipelineDefinitions(iWorkingContext, name.C_Str());
			//std::vector<std::string> materialAttributes = stringSplit(":", name.C_Str());

			std::vector<Vertex> vertices;
			vertices.resize(mesh->mNumVertices);
			for (int j = 0; j < mesh->mNumVertices; j++) {
				vertices[j].position[0] = mesh->mVertices[j].x;
				vertices[j].position[1] = mesh->mVertices[j].y;
				vertices[j].position[2] = mesh->mVertices[j].z;
				vertices[j].position[3] = 1.0;

				pos.push_back(Positions{ mesh->mVertices[j].x , mesh->mVertices[j].y, mesh->mVertices[j].z });
				lPositions.push_back(Positions{ mesh->mVertices[j].x , mesh->mVertices[j].y, mesh->mVertices[j].z });

				vertices[j].texcoord[0] = (mesh->mTextureCoords[0]) ? mesh->mTextureCoords[0][j].x : 0.0f;
				vertices[j].texcoord[1] = (mesh->mTextureCoords[0]) ? mesh->mTextureCoords[0][j].y : 0.0f;

				uv.push_back(UV{ vertices[j].texcoord[0], vertices[j].texcoord[1] });

				vertices[j].normal[0] = mesh->mNormals[j].x;
				vertices[j].normal[1] = mesh->mNormals[j].y;
				vertices[j].normal[2] = mesh->mNormals[j].z;
				vertices[j].normal[3] = 0.0;

				normal.push_back(Normal{ vertices[j].normal[0] , vertices[j].normal[1] , vertices[j].normal[2] });

				vertices[j].bitangent[0] = (mesh->mBitangents) ? mesh->mBitangents[j].x : 0.0f;
				vertices[j].bitangent[1] = (mesh->mBitangents) ? mesh->mBitangents[j].y : 0.0f;
				vertices[j].bitangent[2] = (mesh->mBitangents) ? mesh->mBitangents[j].z : 0.0f;
				vertices[j].bitangent[3] = nameMap[std::string(name.C_Str())];

				bitang.push_back({ vertices[j].bitangent[0], vertices[j].bitangent[1], vertices[j].bitangent[2] });

				vertices[j].tangent[0] = (mesh->mTangents) ? mesh->mTangents[j].x : 0.0f;
				vertices[j].tangent[1] = (mesh->mTangents) ? mesh->mTangents[j].y : 0.0f;
				vertices[j].tangent[2] = (mesh->mTangents) ? mesh->mTangents[j].z : 0.0f;
				vertices[j].tangent[3] = 0.0;

				tang.push_back({ vertices[j].tangent[0] , vertices[j].tangent[1] , vertices[j].tangent[2] });
			}

			std::vector<unsigned int> indices;
			unsigned int endIndex = 0;
			unsigned int rasterEndIndex = 0;
			if (i > 0) {
				indicesOffset = vbVertices.size();
				rasterEndIndex = ibIndices.size();
			}


			if (rtxSubsetIndex > 0) {
				rtxIndiceOffset = vbVerticesRtxOffsetSize + vbVertices.size();

				endIndex = ibIndicesRtxOffsetSize + ibIndices.size();
				offsets.push_back(endIndex / 3);
			}
			else {
				offsets.push_back(0);
			}

			for (int j = 0; j < mesh->mNumFaces; j++) {
				const aiFace& face = mesh->mFaces[j];
				if (face.mNumIndices == 3) {
					indices.push_back(indicesOffset + face.mIndices[0]);
					indices.push_back(indicesOffset + face.mIndices[1]);
					indices.push_back(indicesOffset + face.mIndices[2]);

					indice.push_back(rtxIndiceOffset + face.mIndices[0]);
					indice.push_back(rtxIndiceOffset + face.mIndices[1]);
					indice.push_back(rtxIndiceOffset + face.mIndices[2]);

					lIndices.push_back(face.mIndices[0]);
					lIndices.push_back(face.mIndices[1]);
					lIndices.push_back(face.mIndices[2]);

				}

				materialIndice.push_back(nameMap[name.C_Str()]);
			}

			//subset[i].vertices = vertices;
			//subset[i].indices = indices;
			//aiGetMaterialColor(material, AI_MATKEY_COLOR_AMBIENT, &ambient);
			//aiGetMaterialColor(material, AI_MATKEY_COLOR_SPECULAR, &specular);
			//aiGetMaterialColor(material, AI_MATKEY_GLTF_, &emissive);

			if (magmaModel->cbMaterial == nullptr) {
				magmaModel->cbMaterial = new D3D12ConstantBufferHelper<MetallicRoughness[128]>(iGraphics, iWorkingContext);
				magmaModel->cbMaterial->Initialize(6);

				size_t t = sizeof(MetallicRoughness) * 128;

				//magmaModel->cbMaterialRTX = new D3D12ConstantBufferHelper<MetallicRoughness[128]>(iGraphics, iWorkingContext, true);
				//magmaModel->cbMaterialRTX->Initialize(6);
			}

			if (!createdShadowPipeline) {
				//createdShadowPipeline = CreateShadowPipeline(&context, magmaModel, materialAttributes);
			}
			
			magmaModel->subsets[i].indexSize = indices.size();
			magmaModel->subsets[i].indexOffset = rasterEndIndex;

			vbVertices.insert(vbVertices.end(), vertices.begin(), vertices.end());
			ibIndices.insert(ibIndices.end(), indices.begin(), indices.end());

			iRTXVertexBuffers.push_back(iWorkingContext->CreateVertexBuffer(name.C_Str(), lPositions.data(), sizeof(Positions), lPositions.size(), true));
			iRTXIndexBuffers.push_back(iWorkingContext->CreateIndexBuffer(lIndices.data(), sizeof(unsigned int), lIndices.size(), true));

			rtxVertexSizes.push_back(lPositions.size());
			rtxIndexSizes.push_back(lIndices.size());

			rtxSubsetIndex++;
		}

		uint8_t idx = 0;
		for (const auto& entry : mats) {

			magmaModel->cbMaterial->type[idx]    = mats[idx];
			//magmaModel->cbMaterialRTX->type[idx] = mats[idx];
			idx++;
		}

		magmaModel->materialCB    = magmaModel->cbMaterial->Update(sizeof(MetallicRoughness)*128);
		//magmaModel->materialCBRTX = magmaModel->cbMaterialRTX->Update(sizeof(MetallicRoughness) * 128);


		vbVerticesRtxOffsetSize += vbVertices.size();
		ibIndicesRtxOffsetSize += ibIndices.size();

		iVertexBuffer = iWorkingContext->CreateVertexBuffer(magmaModelName, vbVertices.data(), sizeof(Vertex), vbVertices.size());
		iIndexBuffer = iWorkingContext->CreateIndexBuffer(ibIndices.data(), sizeof(unsigned int), ibIndices.size());

		/*
		tangs           = iWorkingContext->CreateSrvBuffer("", tang.data(), sizeof(Tangent), tang.size());
		bitangs         = iWorkingContext->CreateSrvBuffer("", bitang.data(), sizeof(Bitangent), bitang.size());
		positions       = iWorkingContext->CreateSrvBuffer("", pos.data(), sizeof(Positions), pos.size());
		indices         = iWorkingContext->CreateSrvBuffer("", indice.data(), sizeof(unsigned int), indice.size());
		materialIndices = iWorkingContext->CreateSrvBuffer("", materialIndice.data(), sizeof(unsigned), materialIndice.size());
		normals         = iWorkingContext->CreateSrvBuffer("", normal.data(), sizeof(Normal), normal.size());
		uvs             = iWorkingContext->CreateSrvBuffer("", uv.data(), sizeof(UV), uv.size());
		instanceOffsets = iWorkingContext->CreateSrvBuffer("", offsets.data(), sizeof(unsigned), offsets.size());
		*/
		models[magmaModelName] = magmaModel;

		return models[magmaModelName];
	//}
	//else {
		//return models[magmaModelName];
	//}
}

void IMesh::DrawMesh() {

}

/*
aiString normalMap;
//aiGetMaterialTexture(material, aiTextureType::aiTextureType_NORMALS, 0, &normalMap);

aiString Path;
material->GetTexture(aiTextureType_DIFFUSE, 0, &Path, NULL, NULL, NULL, NULL, NULL);

if (std::string(Path.C_Str()).find("bc") != std::string::npos) {
}

material->GetTexture(aiTextureType_HEIGHT, 0, &Path, NULL, NULL, NULL, NULL, NULL);
if (std::string(Path.C_Str()).find("normal") != std::string::npos) {
}*/
/*
magmaModel->subsets[i].cbMaterial->type.ambient[0] = ambient.r;
magmaModel->subsets[i].cbMaterial->type.ambient[1] = ambient.g;
magmaModel->subsets[i].cbMaterial->type.ambient[2] = ambient.b;
magmaModel->subsets[i].cbMaterial->type.ambient[3] = ambient.a;

magmaModel->subsets[i].cbMaterial->type.emissive[0] = emissive.r;
magmaModel->subsets[i].cbMaterial->type.emissive[1] = emissive.g;
magmaModel->subsets[i].cbMaterial->type.emissive[2] = emissive.b;
magmaModel->subsets[i].cbMaterial->type.emissive[3] = emissive.a;

magmaModel->subsets[i].cbMaterial->type.specular[0] = specular.r;
magmaModel->subsets[i].cbMaterial->type.specular[1] = specular.g;
magmaModel->subsets[i].cbMaterial->type.specular[2] = specular.b;
magmaModel->subsets[i].cbMaterial->type.specular[3] = specular.a;

magmaModel->subsets[i].buffers[1].buffer = magmaModel->subsets[i].cbMaterial->GetBuffer();
magmaModel->subsets[i].buffers[1].stage = PS;
magmaModel->subsets[i].cbMaterial->Update(PS, 1);

aiString Path;
material->GetTexture(aiTextureType_DIFFUSE, 0, &Path, NULL, NULL, NULL, NULL, NULL);
if (std::string(Path.C_Str()).find("diffuse") != std::string::npos) {
	diffuseTexture = std::string(Path.C_Str());
	size_t textureNameIndex = diffuseTexture.find_last_of('\\') + 1;

	diffuseTextureName = diffuseTexture.substr(textureNameIndex, diffuseTexture.size() - textureNameIndex);
	diffuseTextureName = "Assets/Textures/" + diffuseTextureName;

	nengine::TextureDesc textureDesc = {};
	textureDesc.bufferUsage = nengine::DEFAULT;
	textureDesc.bindFlags = nengine::SRV;
	textureDesc.format = nengine::R8G8B8A8_UNORM;
	textureDesc.pitch = 4;

	magmaModel->subsets[i].textures[0].textureName = "Diffuse";
	magmaModel->subsets[i].textures[0].texture = nengine::TextureManager::LoadTexture(context, diffuseTextureName, textureDesc);
	magmaModel->subsets[i].textures[0].stage = PS;
}

material->GetTexture(aiTextureType_HEIGHT, 0, &Path, NULL, NULL, NULL, NULL, NULL);
if (std::string(Path.C_Str()).find("norm") != std::string::npos || std::string(Path.C_Str()).find("NORM") != std::string::npos) {
	normalTexture = std::string(Path.C_Str());
	size_t textureNameIndex = normalTexture.find_last_of('\\') + 1;

	normalTextureName = normalTexture.substr(textureNameIndex, normalTexture.size() - textureNameIndex);
	normalTextureName = "Assets/Textures/" + normalTextureName;

	nengine::TextureDesc textureDesc = {};
	textureDesc.bufferUsage = nengine::DEFAULT;
	textureDesc.bindFlags = nengine::SRV;
	textureDesc.format = nengine::R8G8B8A8_UNORM;
	textureDesc.pitch = 4;

	magmaModel->subsets[i].textures[1].textureName = "Normal";
	magmaModel->subsets[i].textures[1].texture = nengine::TextureManager::LoadTexture(context, normalTextureName, textureDesc);
	magmaModel->subsets[i].textures[1].stage = PS;
}
*/