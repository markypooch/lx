#pragma once
#include <string>
#include <vector>

namespace nxt {
	enum LoggingLevel {
		INFO,
		ERROR
	};

	class Logger
	{
	public:
		static void Write(std::string message, LoggingLevel loggingLevel) {
			messages.push_back(message);
		};
	public:
		static std::vector<std::string> messages;
	};
};
