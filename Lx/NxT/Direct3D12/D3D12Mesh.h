#pragma once
#include "../IMesh.h"
#include "../../StbTextureLoader.h"
#include "D3D12Graphics.h"
#include <d3d12.h>

namespace nxt {
	class D3D12Mesh : public IMesh
	{
	public:
		D3D12Mesh(IWorkingContext*, IGraphics*, MeshType meshType);

		virtual void Load(const char* pFilename = "", unsigned int instances = 1, bool genNormals = false);
		virtual void SetArgumentLayout(IArgumentLayoutObject* argumentLayout) {};
		virtual void SetPipelineMap(std::unordered_map<std::string, IPipeline*> pipelineMap) {};
		virtual void SetPipeline(IWorkingContext*, IPipeline*);
		virtual void Bind(IWorkingContext*, IInstanceBuffer* iInstantBuffer = nullptr);
		virtual void CreateNDCPassQuad(std::string name, std::string vertexShaderPath, std::string pixelShaderPath, nxt::Format format, float sizeX=1.0f, float sizeY=1.0f);
		virtual void Draw(IWorkingContext*, unsigned int instanceCount, unsigned int vertexCount, unsigned int start);
		virtual void DrawIndexed(IWorkingContext*, unsigned int startIndex, unsigned int indexCount, unsigned int instanceCount);
	protected:
		template <typename T>
		void CreateVertexBuffer(T* vertices, unsigned int numOfVertices);
		void CreateIndexBuffer(unsigned int*);
	protected:

		unsigned int instances;

		//ModelData* modelData;
		IShader* vsShader, *psShader;
		IVertexLayout* vertexLayoutObject;
		IArgumentLayoutObject* argumentLayoutObject;

		IWorkingContext* iWorkingContext;
		IGraphics* iGraphics;
		ID3D12Device* pDX12Device;

		ID3D12Resource* vertexBuffer, * vertexBufferUpload;
		ID3D12Resource* indexBuffer, * indexBufferUpload;

		D3D12_VERTEX_BUFFER_VIEW vbView;
		D3D12_INDEX_BUFFER_VIEW ibView;

		MeshType meshType;
	};
};
