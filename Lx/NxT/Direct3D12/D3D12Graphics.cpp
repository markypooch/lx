#include "D3D12Graphics.h"
#include <dxcapi.h>
#include "d3dx12.h"
#include <functional>
using namespace nxt;
#define ALIGN(_alignment, _val) (((_val + _alignment - 1) / _alignment) * _alignment)

struct D3D12ShaderCompilerInfo
{
	dxc::DxcDllSupport		DxcDllHelper;
	IDxcCompiler* compiler = nullptr;
	IDxcLibrary* library = nullptr;
};

std::wstring s2ws(const std::string& s)
{
	int len;
	int slength = (int)s.length() + 1;
	len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
	wchar_t* buf = new wchar_t[len];
	MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
	std::wstring r(buf);
	delete[] buf;
	return r;
}

D3D12_INPUT_CLASSIFICATION ConvertClassification(Classification classification) {
	switch (classification) {
	case PER_VERTEX:
		return D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA;
	case INSTANCED:
		return D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA;
	}
}

D3D12_RESOURCE_FLAGS ConvertUsageFlags(UsageFlags format) {

	switch (format) {
	case ALLOW_RENDER_TARGET:
		return D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET;

	case ALLOW_DEPTH_STENCIL:
		return D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;
	default:
		return D3D12_RESOURCE_FLAG_NONE;
	}
}

D3D12_RESOURCE_STATES ConvertResourceUsageState(ResourceUsage resourceUsage) {

	switch (resourceUsage) {
	case RENDER_TARGET:
		return D3D12_RESOURCE_STATE_RENDER_TARGET;
	case ResourceUsage::PS_RESOURCE:
		return D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE;
	case ResourceUsage::COPY_DEST:
		return D3D12_RESOURCE_STATE_COPY_DEST;
	case ResourceUsage::PRESENT:
		return D3D12_RESOURCE_STATE_PRESENT;
	case ResourceUsage::DEPTH_READ:
		return D3D12_RESOURCE_STATE_DEPTH_READ;
	case ResourceUsage::DEPTH_WRITE:
		return D3D12_RESOURCE_STATE_DEPTH_WRITE;
	case ResourceUsage::GEN_READ:
	default:
		return D3D12_RESOURCE_STATE_GENERIC_READ;
	}
}

D3D12_DESCRIPTOR_RANGE_TYPE ConvertDescriptorType(ArgumentType argumentType) {
	switch (argumentType) {
	case ArgumentType::ARG_SRV:
		return D3D12_DESCRIPTOR_RANGE_TYPE::D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	case ArgumentType::ARG_CBV:
		return D3D12_DESCRIPTOR_RANGE_TYPE::D3D12_DESCRIPTOR_RANGE_TYPE_CBV;
	default:
		return D3D12_DESCRIPTOR_RANGE_TYPE::D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	}
}

D3D12_SHADER_VISIBILITY ConvertVisibility(Visibility visibility) {
	switch (visibility) {
	case PIXEL:
		return D3D12_SHADER_VISIBILITY_PIXEL;
	case VERTEX:
		return D3D12_SHADER_VISIBILITY_VERTEX;
	case ALL:
		return D3D12_SHADER_VISIBILITY_ALL;
	default:
		return D3D12_SHADER_VISIBILITY_ALL;
	}
}

D3D12WorkingContext::D3D12WorkingContext(D3D12Graphics* graphics, ID3D12Device* pDX12Device) {
	this->graphics = graphics;
	this->pDX12Device = pDX12Device;

	view = {};
	view.Height = 2160;
	view.Width = 3840;
	view.MinDepth = 0.1f;
	view.MaxDepth = 1.0f;
	view.TopLeftX = 0.0f;
	view.TopLeftY = 0.0f;

	rect = {};
	rect.bottom = 2160;
	rect.right = 3840;
}

IArgumentLayoutObject* D3D12WorkingContext::CreateArgumentLayout(std::string name, IArgumentLayout argumentLayout) {

	D3D12ArgumentLayout* argumentLayoutObject = new D3D12ArgumentLayout();

	std::vector<D3D12_ROOT_PARAMETER> rootParams;
	std::vector<D3D12_DESCRIPTOR_RANGE> ranges;

	for (int i = 0; i < argumentLayout.params.size(); i++) {

		bool noArgument = false;
		for (int j = 0; j < argumentLayout.params[i].tables.size(); j++) {

			unsigned int offset = 0;
			for (int k = 0; k < argumentLayout.params[i].tables[j].ranges.size(); k++) {

				if (argumentLayout.params[i].tables.size() == 0) {
					noArgument = true;
					break;
				}

				D3D12_DESCRIPTOR_RANGE range = {};
				range.BaseShaderRegister = argumentLayout.params[i].tables[j].ranges[k].start;
				range.NumDescriptors = argumentLayout.params[i].tables[j].ranges[k].count;
				range.OffsetInDescriptorsFromTableStart = offset;
				range.RangeType = ConvertDescriptorType(argumentLayout.params[i].tables[j].ranges[k].type);
				range.RegisterSpace = 0;

				ranges.push_back(range);

				offset += argumentLayout.params[i].tables[j].ranges[k].count;
			}

			if (noArgument)
				break;

			D3D12_ROOT_DESCRIPTOR_TABLE table = {};
			table.pDescriptorRanges = ranges.data();
			table.NumDescriptorRanges = ranges.size();

			D3D12_ROOT_PARAMETER rootParameter = {};
			rootParameter.DescriptorTable = table;
			rootParameter.ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
			rootParameter.ShaderVisibility = ConvertVisibility(argumentLayout.params[i].visibility);

			rootParams.push_back(rootParameter);
			ranges.clear();
		}
	}

	D3D12_STATIC_SAMPLER_DESC samplerDesc = {};
	samplerDesc.AddressU = samplerDesc.AddressV = samplerDesc.AddressW = D3D12_TEXTURE_ADDRESS_MODE_MIRROR;
	samplerDesc.ComparisonFunc = D3D12_COMPARISON_FUNC_NEVER;
	samplerDesc.MaxAnisotropy = 0;
	samplerDesc.MinLOD = 0.0f;
	samplerDesc.MaxLOD = D3D12_FLOAT32_MAX;
	samplerDesc.MipLODBias = 0;
	samplerDesc.RegisterSpace = 0;
	samplerDesc.ShaderRegister = 0;
	samplerDesc.ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	noargument:
	CD3DX12_ROOT_SIGNATURE_DESC rootSignatureDesc = CD3DX12_ROOT_SIGNATURE_DESC(rootParams.size(), rootParams.data(), 1, &samplerDesc, D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT);
	
	HRESULT hr;

	ID3DBlob* rootSignatureBlob;
	hr = D3D12SerializeRootSignature(&rootSignatureDesc, D3D_ROOT_SIGNATURE_VERSION_1, &rootSignatureBlob, nullptr);
	if (FAILED(hr)) {

	}

	hr = pDX12Device->CreateRootSignature(0, rootSignatureBlob->GetBufferPointer(), rootSignatureBlob->GetBufferSize(), IID_PPV_ARGS(&argumentLayoutObject->rootSignature));
	if (FAILED(hr)) {

	}
	argumentLayoutObject->iArgumentLayout = argumentLayout;

	return argumentLayoutObject;
}

void D3D12WorkingContext::BindArgumentLayout() {
	
	ID3D12DescriptorHeap* heaps[1] = { cbvSrvUavHeapShaderVisible->heap };
	
	cmdList->SetGraphicsRootSignature(graphics->rootSignature);
	cmdList->SetDescriptorHeaps(1, heaps);

	CD3DX12_GPU_DESCRIPTOR_HANDLE gpuHandle = CD3DX12_GPU_DESCRIPTOR_HANDLE(
		cbvSrvUavHeapShaderVisible->heap->GetGPUDescriptorHandleForHeapStart(),
		offsetForDraw,
		cbvSrvUavDescriptorHandleIncrementSize
	);

	cmdList->SetGraphicsRootDescriptorTable(0, gpuHandle);
}

void D3D12WorkingContext::Initialize(int rtxContext) {

	HRESULT hr;

	for (int i = 0; i < BUFFER_COUNT; i++) {
		hr = pDX12Device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&cmdAlloc[i]));
		if (FAILED(hr)) {

		}

		fenceValue[i] = 0;
		hr = pDX12Device->CreateFence(fenceValue[i], D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&fence[i]));
		if (FAILED(hr)) {

		}
	}
	hr = pDX12Device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, cmdAlloc[0], nullptr, IID_PPV_ARGS(&cmdList));
	if (FAILED(hr)) {

	}
	hr = cmdList->Close();
	if (FAILED(hr)) {

	}

	rtvDescriptorHandleIncrementSize = pDX12Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
	cbvSrvUavDescriptorHandleIncrementSize = pDX12Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
	dsvDescriptorHandleIncrementSize = pDX12Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_DSV);
	//pDX12Device->CreateDescriptorHeap(&dsvDescriptorHeapDesc, IID_PPV_ARGS(&dsvDescriptorHeapNonShaderVisible->heap));

	cbvSrvUavHeapShaderVisible    = new DescriptorHeap();

	D3D12_DESCRIPTOR_HEAP_DESC cbvSrvUavDescriptorHeapDesc = {};
	cbvSrvUavDescriptorHeapDesc.NumDescriptors = 32768;
	cbvSrvUavDescriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	
	cbvSrvUavDescriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	pDX12Device->CreateDescriptorHeap(&cbvSrvUavDescriptorHeapDesc, IID_PPV_ARGS(&cbvSrvUavHeapShaderVisible->heap));

	cmdList->QueryInterface(IID_PPV_ARGS(&dxrCmdList));

	this->rtxContext = rtxContext;
}

IVertexBuffer* D3D12WorkingContext::CreateVertexBuffer(std::string name, void* pData, unsigned int stride, unsigned int numOfVertices, bool rtx) {
	
	D3D12VertexBuffer* vertexBuffer = new D3D12VertexBuffer();
	
	HRESULT hr;
	hr = pDX12Device->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(stride * numOfVertices),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&vertexBuffer->vertexBufferUpload));

	hr = pDX12Device->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
		D3D12_HEAP_FLAG_NONE,
		&(CD3DX12_RESOURCE_DESC::Buffer(stride * numOfVertices)),
		D3D12_RESOURCE_STATE_COPY_DEST,
		nullptr,
		IID_PPV_ARGS(&vertexBuffer->vertexBuffer));

	if (FAILED(hr)) {

	}

	D3D12_SUBRESOURCE_DATA resource = {};
	resource.pData = pData;
	resource.RowPitch = stride * numOfVertices;
	resource.SlicePitch = stride * numOfVertices;

	UpdateSubresources(cmdList, vertexBuffer->vertexBuffer, vertexBuffer->vertexBufferUpload, 0, 0, 1, &resource);
	
	
	if (!rtx) {
		cmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(vertexBuffer->vertexBuffer, D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER));
	}
	else {
		cmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(vertexBuffer->vertexBuffer, D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE));
	}

	synchNeeded = true;

	vertexBuffer->vbView = {};
	vertexBuffer->vbView.BufferLocation = vertexBuffer->vertexBuffer->GetGPUVirtualAddress();
	vertexBuffer->vbView.SizeInBytes = stride * numOfVertices;
	vertexBuffer->vbView.StrideInBytes = stride;
	vertexBuffer->name = name;


	return vertexBuffer;
}

IInstanceBuffer* D3D12WorkingContext::CreateInstanceBuffer(std::string name, void* pData, unsigned int stride, unsigned int numberOfElements) {
	D3D12InstanceBuffer* instanceBuffer = new D3D12InstanceBuffer();

	HRESULT hr;
	hr = pDX12Device->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(stride * numberOfElements),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&instanceBuffer->instanceBufferUpload));

	hr = pDX12Device->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
		D3D12_HEAP_FLAG_NONE,
		&(CD3DX12_RESOURCE_DESC::Buffer(stride * numberOfElements)),
		D3D12_RESOURCE_STATE_COPY_DEST,
		nullptr,
		IID_PPV_ARGS(&instanceBuffer->instanceBuffer));

	if (FAILED(hr)) {

	}

	D3D12_SUBRESOURCE_DATA resource = {};
	resource.pData = pData;
	resource.RowPitch = stride * numberOfElements;
	resource.SlicePitch = stride * numberOfElements;

	UpdateSubresources(cmdList, instanceBuffer->instanceBuffer, instanceBuffer->instanceBufferUpload, 0, 0, 1, &resource);
	cmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(instanceBuffer->instanceBuffer, D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER));

	synchNeeded = true;

	instanceBuffer->instanceBufferView = {};
	instanceBuffer->instanceBufferView.BufferLocation = instanceBuffer->instanceBuffer->GetGPUVirtualAddress();
	instanceBuffer->instanceBufferView.SizeInBytes = stride * numberOfElements;
	instanceBuffer->instanceBufferView.StrideInBytes = stride;

	return instanceBuffer;
}

IIndexBuffer* D3D12WorkingContext::CreateIndexBuffer(void* pData, unsigned int stride, unsigned int numOfIndices, bool rtx) {
	D3D12IndexBuffer* indexBuffer = new D3D12IndexBuffer();

	HRESULT hr;
	hr = pDX12Device->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(stride * numOfIndices),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&indexBuffer->indexBufferUpload));

	hr = pDX12Device->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(stride * numOfIndices),
		D3D12_RESOURCE_STATE_COPY_DEST,
		nullptr,
		IID_PPV_ARGS(&indexBuffer->indexBuffer));

	if (FAILED(hr)) {

	}

	D3D12_SUBRESOURCE_DATA resource = {};
	resource.pData = pData;
	resource.RowPitch = stride * numOfIndices;
	resource.SlicePitch = stride * numOfIndices;

	UpdateSubresources(cmdList, indexBuffer->indexBuffer, indexBuffer->indexBufferUpload, 0, 0, 1, &resource);

	if (!rtx) {
		cmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(indexBuffer->indexBuffer, D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_INDEX_BUFFER));
	}
	else {
		cmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(indexBuffer->indexBuffer, D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE));
	}
	synchNeeded = true;

	indexBuffer->ibView = {};
	indexBuffer->ibView.BufferLocation = indexBuffer->indexBuffer->GetGPUVirtualAddress();
	indexBuffer->ibView.SizeInBytes = stride * numOfIndices;
	indexBuffer->ibView.Format = DXGI_FORMAT_R32_UINT;

	return indexBuffer;
}

IShader* D3D12WorkingContext::CreateShader(LPCWSTR filename, ShaderType shaderType) {

	D3D12Shader* shader = new D3D12Shader();
	shader->shaderType = shaderType;

	LPCSTR target = "vs_5_0";
	switch (shaderType) {
	case PS:
		target = "ps_5_0";
		break;
	case VS:
		target = "vs_5_0";
		break;
	}


	ID3DBlob* error;
	HRESULT hr = D3DCompileFromFile(filename, nullptr, nullptr, "main", target, D3DCOMPILE_DEBUG, D3DCOMPILE_SKIP_OPTIMIZATION, &shader->compiledCode, &error);
	if (FAILED(hr)) {

	}

	if (error) {
		const char* message = (char*)error->GetBufferPointer();
		int x = 12;
	}


	shader->byteCode.BytecodeLength = shader->compiledCode->GetBufferSize();
	shader->byteCode.pShaderBytecode = shader->compiledCode->GetBufferPointer();

	return shader;
}

void D3D12WorkingContext::SetTexture(unsigned int bindIndex, ITexture* iTexture, bool rtx) {
	
	//Is this a RenderTarget? If so it's triple buffered, and we need to grab the appropriate texture
	unsigned int descriptorIndex = (iTexture->rtResource) ? iTexture->descriptor[currentWorkingBuffer].indexInHeap : iTexture->descriptor[0].indexInHeap;

	CD3DX12_CPU_DESCRIPTOR_HANDLE sourceDescriptor;
	CD3DX12_CPU_DESCRIPTOR_HANDLE destDescriptor;

	if (!rtx) {

		//Get handles to our two heaps
		sourceDescriptor = CD3DX12_CPU_DESCRIPTOR_HANDLE(
			graphics->cbvSrvUavHeapNonShaderVisible->heap->GetCPUDescriptorHandleForHeapStart(),
			descriptorIndex,
			cbvSrvUavDescriptorHandleIncrementSize);

		destDescriptor = CD3DX12_CPU_DESCRIPTOR_HANDLE(
			cbvSrvUavHeapShaderVisible->heap->GetCPUDescriptorHandleForHeapStart(),
			offsetForDraw + bindIndex,
			cbvSrvUavDescriptorHandleIncrementSize
		);
	}
	else {
		//Get handles to our two heaps
		sourceDescriptor = CD3DX12_CPU_DESCRIPTOR_HANDLE(
			graphics->pRTXDescriptorHeap->heap->GetCPUDescriptorHandleForHeapStart(),
			descriptorIndex,
			cbvSrvUavDescriptorHandleIncrementSize);

		destDescriptor = CD3DX12_CPU_DESCRIPTOR_HANDLE(
			cbvSrvUavHeapShaderVisible->heap->GetCPUDescriptorHandleForHeapStart(),
			offsetForDraw + bindIndex,
			cbvSrvUavDescriptorHandleIncrementSize
		);
	}
	cbvSrvUavOffset++;

	//Copy the descriptor to a shader visible heap
	pDX12Device->CopyDescriptorsSimple(1, destDescriptor, sourceDescriptor, D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
}

void D3D12WorkingContext::SetBuffer(unsigned int bindIndex, IConstantBuffer* iCB) {

	unsigned int descriptorIndex = iCB->descriptor.indexInHeap;
	const unsigned int buffersOffset = 9;

	//Get handles to our two heaps
	CD3DX12_CPU_DESCRIPTOR_HANDLE sourceDescriptor = CD3DX12_CPU_DESCRIPTOR_HANDLE(
		graphics->cbvSrvUavHeapNonShaderVisible->heap->GetCPUDescriptorHandleForHeapStart(),
		descriptorIndex,
		cbvSrvUavDescriptorHandleIncrementSize);

	CD3DX12_CPU_DESCRIPTOR_HANDLE destDescriptor = CD3DX12_CPU_DESCRIPTOR_HANDLE(
		cbvSrvUavHeapShaderVisible->heap->GetCPUDescriptorHandleForHeapStart(),
		offsetForDraw + bindIndex + buffersOffset,
		cbvSrvUavDescriptorHandleIncrementSize
	);

	cbvSrvUavOffset++;

	//Copy the descriptor to a shader visible heap
	pDX12Device->CopyDescriptorsSimple(1, destDescriptor, sourceDescriptor, D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
}


void D3D12WorkingContext::SetRTXTexture(unsigned int bindIndex, ITexture* iTexture, bool rtx) {

	//Is this a RenderTarget? If so it's triple buffered, and we need to grab the appropriate texture
	unsigned int descriptorIndex = (iTexture->rtResource) ? iTexture->descriptor[currentWorkingBuffer].indexInHeap : iTexture->descriptor[0].indexInHeap;
	unsigned buffersOffset = 0;
	if (iTexture->rtResource) {

		//Peform the transition to SRV
		if (iTexture->currentUsage[currentWorkingBuffer] != ResourceUsage::PS_RESOURCE) {
			D3D12Texture* texture = static_cast<D3D12Texture*>(iTexture);
			cmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(texture->texture[currentWorkingBuffer], ConvertResourceUsageState(texture->currentUsage[currentWorkingBuffer]), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE));
			iTexture->currentUsage[currentWorkingBuffer] = ResourceUsage::PS_RESOURCE;
		}
	}
	else if (iTexture->dsvResource) {
		//Peform the transition to depth read
		if (iTexture->currentUsage[currentWorkingBuffer] != ResourceUsage::DEPTH_READ) {
			D3D12Texture* texture = static_cast<D3D12Texture*>(iTexture);
			cmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(texture->texture[currentWorkingBuffer], ConvertResourceUsageState(texture->currentUsage[currentWorkingBuffer]), D3D12_RESOURCE_STATE_DEPTH_READ));
			iTexture->currentUsage[currentWorkingBuffer] = ResourceUsage::DEPTH_READ;
		}
	}
	CD3DX12_CPU_DESCRIPTOR_HANDLE sourceDescriptor;
	CD3DX12_CPU_DESCRIPTOR_HANDLE destDescriptor;

	if (rtx)
	{	//Get handles to our two heaps
		sourceDescriptor = CD3DX12_CPU_DESCRIPTOR_HANDLE(
			graphics->pRTXDescriptorHeap->heap->GetCPUDescriptorHandleForHeapStart(),
			descriptorIndex,
			cbvSrvUavDescriptorHandleIncrementSize);

		destDescriptor = CD3DX12_CPU_DESCRIPTOR_HANDLE(
			graphics->pRTXDstDescriptorHeap->heap->GetCPUDescriptorHandleForHeapStart(),
			rtxOffsetForDraw + buffersOffset + bindIndex,
			cbvSrvUavDescriptorHandleIncrementSize
		);
	}
	else {
		sourceDescriptor = CD3DX12_CPU_DESCRIPTOR_HANDLE(
			graphics->cbvSrvUavHeapNonShaderVisible->heap->GetCPUDescriptorHandleForHeapStart(),
			descriptorIndex,
			cbvSrvUavDescriptorHandleIncrementSize);

		destDescriptor = CD3DX12_CPU_DESCRIPTOR_HANDLE(
			graphics->pRTXDstDescriptorHeap->heap->GetCPUDescriptorHandleForHeapStart(),
			rtxOffsetForDraw + buffersOffset + bindIndex,
			cbvSrvUavDescriptorHandleIncrementSize
		);
	}
	cbvSrvUavOffset++;

	//Copy the descriptor to a shader visible heap
	graphics->dxrDevice->CopyDescriptorsSimple(1, destDescriptor, sourceDescriptor, D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
}

void D3D12WorkingContext::SetRTXTextureArray(unsigned int bindIndex, ITexture** iTextures, unsigned int size, bool rtx) {
	
	int buffersOffset = 16;
	for (int i = 0; i < size; i++) {
		int descriptorIndex = (iTextures[i]->rtResource) ? iTextures[i]->descriptor[currentWorkingBuffer].indexInHeap : iTextures[i]->descriptor[0].indexInHeap;

		CD3DX12_CPU_DESCRIPTOR_HANDLE sourceDescriptor = CD3DX12_CPU_DESCRIPTOR_HANDLE{
			graphics->pRTXDescriptorHeap->heap->GetCPUDescriptorHandleForHeapStart(),
			descriptorIndex,
			cbvSrvUavDescriptorHandleIncrementSize
		};

		CD3DX12_CPU_DESCRIPTOR_HANDLE destDescriptor = CD3DX12_CPU_DESCRIPTOR_HANDLE{
			graphics->pRTXDstDescriptorHeap->heap->GetCPUDescriptorHandleForHeapStart(),
			buffersOffset + i + (int)rtxOffsetForDraw,
			cbvSrvUavDescriptorHandleIncrementSize
		};

		graphics->dxrDevice->CopyDescriptorsSimple(1, destDescriptor, sourceDescriptor, D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
	}
}

void D3D12WorkingContext::SetRTXBuffer(unsigned int bindIndex, IConstantBuffer* iCB) {

	unsigned int descriptorIndex = iCB->descriptor.indexInHeap;
	const unsigned int buffersOffset = 14;

	//Get handles to our two heaps
	CD3DX12_CPU_DESCRIPTOR_HANDLE sourceDescriptor = CD3DX12_CPU_DESCRIPTOR_HANDLE(
		graphics->pRTXDescriptorHeap->heap->GetCPUDescriptorHandleForHeapStart(),
		descriptorIndex,
		cbvSrvUavDescriptorHandleIncrementSize);

	CD3DX12_CPU_DESCRIPTOR_HANDLE destDescriptor = CD3DX12_CPU_DESCRIPTOR_HANDLE(
		graphics->pRTXDstDescriptorHeap->heap->GetCPUDescriptorHandleForHeapStart(),
		rtxOffsetForDraw + bindIndex + buffersOffset,
		cbvSrvUavDescriptorHandleIncrementSize
	);

	cbvSrvUavOffset++;

	//Copy the descriptor to a shader visible heap
	pDX12Device->CopyDescriptorsSimple(1, destDescriptor, sourceDescriptor, D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
}

IConstantBuffer* D3D12WorkingContext::CreateConstantBuffer(std::string name, size_t size, size_t stride, IConstantBuffer* cb, uint32_t offset, bool rtx) {

	D3D12ConstantBuffer* d3d12ConstantBuffer = new D3D12ConstantBuffer();

	graphics->pDX12Device->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(64 * 1024),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&d3d12ConstantBuffer->pResource));

	int sub = (int)size - 256;
	int mod = 256 - ((int)size % 256);

	size_t res = size + mod;

	D3D12_CONSTANT_BUFFER_VIEW_DESC cbViewDesc = {};
	cbViewDesc.BufferLocation = d3d12ConstantBuffer->pResource->GetGPUVirtualAddress();
	cbViewDesc.SizeInBytes = res;

	if (!rtx) {

		CD3DX12_CPU_DESCRIPTOR_HANDLE cbvHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
			graphics->cbvSrvUavHeapNonShaderVisible->heap->GetCPUDescriptorHandleForHeapStart(),
			graphics->cbvSrvUavHeapNonShaderVisible->numberOfDescriptorsAllocated,
			cbvSrvUavDescriptorHandleIncrementSize
		);
		graphics->pDX12Device->CreateConstantBufferView(&cbViewDesc, cbvHandle);

		CD3DX12_RANGE read(0, 0);
		d3d12ConstantBuffer->pResource->Map(0, &read, &d3d12ConstantBuffer->pGPUAddress);
		d3d12ConstantBuffer->descriptor.indexInHeap = graphics->cbvSrvUavHeapNonShaderVisible->numberOfDescriptorsAllocated;

		graphics->cbvSrvUavHeapNonShaderVisible->numberOfDescriptorsAllocated++;
	}
	else {
		CD3DX12_CPU_DESCRIPTOR_HANDLE cbvHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
			graphics->pRTXDescriptorHeap->heap->GetCPUDescriptorHandleForHeapStart(),
			graphics->pRTXDescriptorHeap->numberOfDescriptorsAllocated,
			cbvSrvUavDescriptorHandleIncrementSize
		);
		graphics->pDX12Device->CreateConstantBufferView(&cbViewDesc, cbvHandle);

		CD3DX12_RANGE read(0, 0);
		d3d12ConstantBuffer->pResource->Map(0, &read, &d3d12ConstantBuffer->pGPUAddress);
		d3d12ConstantBuffer->descriptor.indexInHeap = graphics->pRTXDescriptorHeap->numberOfDescriptorsAllocated++;
		//graphics->cbvSrvUavHeapNonShaderVisible->numberOfDescriptorsAllocated++;
	}

	return d3d12ConstantBuffer;
}

IVertexLayout* D3D12WorkingContext::CreateVertexLayout(std::string name, VertexLayout* vertexLayout, unsigned int numElements) {

	D3D12VertexLayout* iVertexLayout = new D3D12VertexLayout();
	iVertexLayout->inputElement.resize(numElements);
	for (int i = 0; i < numElements; i++) {
		std::string semanticName = vertexLayout[i].semanticName;

		if (semanticName == "POSITION") {
			iVertexLayout->inputElement[i].SemanticName = "POSITION";
		}
		if (semanticName == "NORMAL") {
			iVertexLayout->inputElement[i].SemanticName = "NORMAL";
		}
		if (semanticName == "BITANGENT") {
			iVertexLayout->inputElement[i].SemanticName = "BITANGENT";
		}
		if (semanticName == "TANGENT") {
			iVertexLayout->inputElement[i].SemanticName = "TANGENT";
		}
		if (semanticName == "TEXCOORD") {
			iVertexLayout->inputElement[i].SemanticName = "TEXCOORD";
		}
		if (semanticName == "COLOR") {
			iVertexLayout->inputElement[i].SemanticName = "COLOR";
		}
		iVertexLayout->inputElement[i].SemanticIndex = vertexLayout[i].semanticIndex;
		iVertexLayout->inputElement[i].Format = ConvertFormat(vertexLayout[i].format);
		iVertexLayout->inputElement[i].AlignedByteOffset = vertexLayout[i].byteOffset;
		iVertexLayout->inputElement[i].InputSlot = vertexLayout[i].inputSlot;
		iVertexLayout->inputElement[i].InputSlotClass = ConvertClassification(vertexLayout[i].classification);
		iVertexLayout->inputElement[i].InstanceDataStepRate = vertexLayout[i].instanceDataStepRate;
	}

	iVertexLayout->inputLayout.NumElements = numElements;
	iVertexLayout->inputLayout.pInputElementDescs = iVertexLayout->inputElement.data();

	return iVertexLayout;
}

IPipeline* D3D12WorkingContext::CreatePipeline(std::string name, std::vector<IShader*> shaders, IArgumentLayoutObject* argumentLayoutObject, IVertexLayout* vertexLayout, unsigned int rtvCount, nxt::Format rtvFormat, nxt::Format dsvFormat) {

	D3D12Pipeline* pipeline = new D3D12Pipeline();
	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
	D3D12Shader* shader = nullptr;
	
	for (int i = 0; i < shaders.size(); i++) {
		switch (shaders[i]->shaderType) {
		case PS:
			shader = static_cast<D3D12Shader*>(shaders[i]);
			psoDesc.PS = shader->byteCode;
			break;
		case VS:
			shader = static_cast<D3D12Shader*>(shaders[i]);
			psoDesc.VS = shader->byteCode;
			break;
		}
	}

	D3D12ArgumentLayout* argumentLayout = static_cast<D3D12ArgumentLayout*>(argumentLayoutObject);
	D3D12VertexLayout* inputL = static_cast<D3D12VertexLayout*>(vertexLayout);
	psoDesc.InputLayout = inputL->inputLayout;
	psoDesc.pRootSignature = graphics->rootSignature;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	psoDesc.NumRenderTargets = rtvCount;

	for (int i = 0; i < rtvCount; i++) {
		psoDesc.RTVFormats[i] = ConvertFormat(rtvFormat);
	}
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
	psoDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);

	if (dsvFormat != UNKNOWN)
		psoDesc.DSVFormat = DXGI_FORMAT_D32_FLOAT;
	psoDesc.SampleDesc.Count = 1;
	psoDesc.SampleDesc.Quality = 0;
	psoDesc.SampleMask = 0xffffffff;

	HRESULT hr = pDX12Device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&pipeline->pso));
	if (FAILED(hr)) {
		
	}

	
	return pipeline;
}

void D3D12WorkingContext::SetBackBufferPresentState(IGraphics* iGraphics) {
	D3D12Graphics* d3d12Graphics = static_cast<D3D12Graphics*>(iGraphics);
	cmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(d3d12Graphics->pRtvBackBufferTextures[currentWorkingBuffer], D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT));
	d3d12Graphics->backBufferUsageState[currentWorkingBuffer] = ResourceUsage::PRESENT;
}

void D3D12WorkingContext::SetPipeline(IPipeline* iPipeline) {
	D3D12Pipeline* d3d12Pipeline = static_cast<D3D12Pipeline*>(iPipeline);
	cmdList->SetPipelineState(d3d12Pipeline->pso);
}

void D3D12WorkingContext::SetVertexBuffer(unsigned int bindIndex, IVertexBuffer* iVertexBuffer, IInstanceBuffer* iInstanceBuffer) {

	D3D12VertexBuffer* vertexBuffer = static_cast<D3D12VertexBuffer*>(iVertexBuffer);
	cmdList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	if (iInstanceBuffer) {
		D3D12InstanceBuffer* instanceBuffer = static_cast<D3D12InstanceBuffer*>(iInstanceBuffer);

		D3D12_VERTEX_BUFFER_VIEW vbViews[2];
		vbViews[0] = vertexBuffer->vbView;
		vbViews[1] = instanceBuffer->instanceBufferView;

		cmdList->IASetVertexBuffers(0, 2, vbViews);
	}
	else {
		cmdList->IASetVertexBuffers(0, 1, &vertexBuffer->vbView);
	}
}

void D3D12WorkingContext::SetIndexBuffer(unsigned int bindIndex, IIndexBuffer* iIndexBuffer) {
	D3D12IndexBuffer* indexBuffer = static_cast<D3D12IndexBuffer*>(iIndexBuffer);
	cmdList->IASetIndexBuffer(&indexBuffer->ibView);
}

ISRVBuffer* D3D12WorkingContext::CreateSrvBuffer(std::string name, void* pData, unsigned int stride, unsigned int numberOfElements) {

	D3D12SRVBuffer* d3d12Texture = new D3D12SRVBuffer();

	pDX12Device->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(stride * numberOfElements),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&d3d12Texture->pResourceUpload)
	);
	
	CD3DX12_RANGE read(0, 0);
	d3d12Texture->pResourceUpload->Map(0, &read, &d3d12Texture->pGPUAddress);

	memcpy(d3d12Texture->pGPUAddress, pData, stride * numberOfElements);

	d3d12Texture->pResourceUpload->Unmap(0, 0);

	D3D12_BUFFER_SRV srv{};
	srv.FirstElement = 0;
	srv.NumElements = (static_cast<UINT>(numberOfElements) * stride) / sizeof(float);
	srv.StructureByteStride = 0;
	srv.Flags = D3D12_BUFFER_SRV_FLAG_RAW;

	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc{};
	srvDesc.Buffer = srv;
	srvDesc.Format = DXGI_FORMAT_R32_TYPELESS;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_BUFFER;
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;

	CD3DX12_CPU_DESCRIPTOR_HANDLE srvDescriptorHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
		graphics->pRTXDescriptorHeap->heap->GetCPUDescriptorHandleForHeapStart(),
		graphics->pRTXDescriptorHeap->numberOfDescriptorsAllocated,
		cbvSrvUavDescriptorHandleIncrementSize
	);

	pDX12Device->CreateShaderResourceView(d3d12Texture->pResourceUpload, &srvDesc, srvDescriptorHandle);

	d3d12Texture->srvDesriptor.descriptorType = DescriptorType::CBV_SRV_UAV;
	d3d12Texture->srvDesriptor.indexInHeap = graphics->pRTXDescriptorHeap->numberOfDescriptorsAllocated++;

	this->synchNeeded = true;

	return d3d12Texture;
}

void D3D12WorkingContext::SetSRVBuffer(ISRVBuffer* iSRVBuffer, unsigned int bindSlot) {

	D3D12SRVBuffer* d3d12SrvBuffer = static_cast<D3D12SRVBuffer*>(iSRVBuffer);

	unsigned int descriptorIndex = d3d12SrvBuffer->srvDesriptor.indexInHeap;
	const unsigned int buffersOffset = 0;

	//Get handles to our two heaps
	CD3DX12_CPU_DESCRIPTOR_HANDLE sourceDescriptor = CD3DX12_CPU_DESCRIPTOR_HANDLE(
		graphics->pRTXDescriptorHeap->heap->GetCPUDescriptorHandleForHeapStart(),
		descriptorIndex,
		cbvSrvUavDescriptorHandleIncrementSize);

	CD3DX12_CPU_DESCRIPTOR_HANDLE destDescriptor = CD3DX12_CPU_DESCRIPTOR_HANDLE(
		graphics->pRTXDstDescriptorHeap->heap->GetCPUDescriptorHandleForHeapStart(),
		rtxOffsetForDraw + bindSlot + buffersOffset,
		cbvSrvUavDescriptorHandleIncrementSize
	);

	cbvSrvUavOffset++;

	//Copy the descriptor to a shader visible heap
	pDX12Device->CopyDescriptorsSimple(1, destDescriptor, sourceDescriptor, D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
}

ITexture* D3D12WorkingContext::CreateTexture(std::string name, const void* pData, unsigned int width, unsigned int height, unsigned int channels, Format format, UsageFlags flags, unsigned int mip, bool srv, bool rtx, ResourceUsage resourceUsage) {

	D3D12Texture* d3d12Texture = new D3D12Texture();

	for (int i = 0; i < BUFFER_COUNT; i++) {

		D3D12_RESOURCE_DESC resourceDesc = {};
		resourceDesc.Width = width;
		resourceDesc.Height = height;
		resourceDesc.DepthOrArraySize = 1;
		resourceDesc.Alignment = 0;
		resourceDesc.Flags = ConvertUsageFlags(flags);
		resourceDesc.Format = (format == DEPTH_32_FLOAT) ? DXGI_FORMAT_R32_FLOAT : ConvertFormat(format);
		resourceDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
		resourceDesc.MipLevels = mip;
		resourceDesc.SampleDesc.Count = 1;
		resourceDesc.SampleDesc.Quality = 0;

		D3D12_CLEAR_VALUE* value = nullptr;
		
		if (flags == ALLOW_RENDER_TARGET) {
			value = new D3D12_CLEAR_VALUE();
			value->Color[0] = 0.0f;
			value->Color[1] = 0.0f;
			value->Color[2] = 0.0f;
			value->Color[3] = 0.0f;
			value->Format = (format == DEPTH_32_FLOAT) ? DXGI_FORMAT_R32_FLOAT : ConvertFormat(format);
		}

		pDX12Device->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
			D3D12_HEAP_FLAG_NONE,
			&resourceDesc,
			D3D12_RESOURCE_STATE_COPY_DEST,
			value,
			IID_PPV_ARGS(&d3d12Texture->texture[i]));

		d3d12Texture->texture[i]->SetName(LPCWSTR(s2ws(name).c_str()));

		if (flags != ALLOW_RENDER_TARGET && flags != ALLOW_DEPTH_STENCIL) {
			unsigned long long textureUploadHeap;
			pDX12Device->GetCopyableFootprints(&resourceDesc, 0, 1, 0, nullptr, 0, 0, &textureUploadHeap);

			pDX12Device->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
				D3D12_HEAP_FLAG_NONE,
				&CD3DX12_RESOURCE_DESC::Buffer(textureUploadHeap),
				D3D12_RESOURCE_STATE_GENERIC_READ,
				nullptr,
				IID_PPV_ARGS(&d3d12Texture->textureUploadHeap[i]));

			D3D12_SUBRESOURCE_DATA resource = {};
			resource.pData = pData;
			resource.RowPitch = (width * channels);
			resource.SlicePitch = (width * channels) * height;

			if (!rtx)
				UpdateSubresources(cmdList, d3d12Texture->texture[i], d3d12Texture->textureUploadHeap[i], 0, 0, 1, &resource);
			else
				UpdateSubresources(dxrCmdList, d3d12Texture->texture[i], d3d12Texture->textureUploadHeap[i], 0, 0, 1, &resource);
		}
		
		D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
		srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
		srvDesc.Texture2D.MipLevels = mip;
		srvDesc.Format = (format == DEPTH_32_FLOAT) ? DXGI_FORMAT_R32_FLOAT : ConvertFormat(format);
		srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;

		if (!rtx) {

			CD3DX12_CPU_DESCRIPTOR_HANDLE srvDescriptorHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
				graphics->cbvSrvUavHeapNonShaderVisible->heap->GetCPUDescriptorHandleForHeapStart(),
				graphics->cbvSrvUavHeapNonShaderVisible->numberOfDescriptorsAllocated,
				cbvSrvUavDescriptorHandleIncrementSize
			);

			pDX12Device->CreateShaderResourceView(d3d12Texture->texture[i], &srvDesc, srvDescriptorHandle);

			d3d12Texture->descriptor[i].descriptorType = DescriptorType::CBV_SRV_UAV;
			d3d12Texture->descriptor[i].indexInHeap = graphics->cbvSrvUavHeapNonShaderVisible->numberOfDescriptorsAllocated++;
		}
		else {
			CD3DX12_CPU_DESCRIPTOR_HANDLE srvDescriptorHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
				graphics->pRTXDescriptorHeap->heap->GetCPUDescriptorHandleForHeapStart(),
				graphics->pRTXDescriptorHeap->numberOfDescriptorsAllocated,
				cbvSrvUavDescriptorHandleIncrementSize
			);

			pDX12Device->CreateShaderResourceView(d3d12Texture->texture[i], &srvDesc, srvDescriptorHandle);

			d3d12Texture->descriptor[i].descriptorType = DescriptorType::CBV_SRV_UAV;
			d3d12Texture->descriptor[i].indexInHeap = graphics->pRTXDescriptorHeap->numberOfDescriptorsAllocated++;
		}

		this->synchNeeded = true;

		if (flags != ALLOW_RENDER_TARGET && flags != ALLOW_DEPTH_STENCIL) {
			if (!rtx) {
				cmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(d3d12Texture->texture[i], D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE));
				d3d12Texture->currentUsage[i] = ResourceUsage::PS_RESOURCE;
			}
			else {
				dxrCmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(d3d12Texture->texture[i], D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE));
				d3d12Texture->currentUsage[i] = ResourceUsage::PS_RESOURCE;
			}
			break;
		}
		else {

			if (format == DEPTH_32_FLOAT) {
				cmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(d3d12Texture->texture[i], D3D12_RESOURCE_STATE_COPY_DEST, ConvertResourceUsageState(resourceUsage)));
				d3d12Texture->currentUsage[i] = ResourceUsage::DEPTH_WRITE;
			}
			else {
				cmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(d3d12Texture->texture[i], D3D12_RESOURCE_STATE_COPY_DEST, ConvertResourceUsageState(resourceUsage)));
				d3d12Texture->currentUsage[i] = ResourceUsage::RENDER_TARGET;
			}
		}
	}

	return d3d12Texture;
}

ITexture* D3D12WorkingContext::CreateTexture(std::string name, const void* pData, unsigned int width, unsigned int height, size_t rowPitch, size_t slicePitch, Format format, UsageFlags flags, unsigned int mip, unsigned int arrSize) {

	D3D12Texture* d3d12Texture = new D3D12Texture();

	for (int i = 0; i < BUFFER_COUNT; i++) {

		D3D12_RESOURCE_DESC resourceDesc = {};
		resourceDesc.Width = width;
		resourceDesc.Height = height;
		resourceDesc.DepthOrArraySize = arrSize;
		resourceDesc.Alignment = 0;
		resourceDesc.Flags = ConvertUsageFlags(flags);
		resourceDesc.Format = (format == DEPTH_32_FLOAT) ? DXGI_FORMAT_R32_FLOAT : ConvertFormat(format);
		resourceDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
		resourceDesc.MipLevels = mip;
		resourceDesc.SampleDesc.Count = 1;
		resourceDesc.SampleDesc.Quality = 0;

		D3D12_CLEAR_VALUE* value = nullptr;

		if (flags == ALLOW_RENDER_TARGET) {
			value = new D3D12_CLEAR_VALUE();
			value->Color[0] = 0.0f;
			value->Color[1] = 0.0f;
			value->Color[2] = 0.0f;
			value->Color[3] = 0.0f;
			value->Format = (format == DEPTH_32_FLOAT) ? DXGI_FORMAT_R32_FLOAT : ConvertFormat(format);
		}

		pDX12Device->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
			D3D12_HEAP_FLAG_NONE,
			&resourceDesc,
			D3D12_RESOURCE_STATE_COPY_DEST,
			value,
			IID_PPV_ARGS(&d3d12Texture->texture[i]));

		if (flags != ALLOW_RENDER_TARGET && flags != ALLOW_DEPTH_STENCIL) {
			unsigned long long textureUploadHeap;
			pDX12Device->GetCopyableFootprints(&resourceDesc, 0, 1, 0, nullptr, 0, 0, &textureUploadHeap);

			for (int j = 0; j < 32*3; j++) {
				pDX12Device->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
					D3D12_HEAP_FLAG_NONE,
					&CD3DX12_RESOURCE_DESC::Buffer(textureUploadHeap),
					D3D12_RESOURCE_STATE_GENERIC_READ,
					nullptr,
					IID_PPV_ARGS(&d3d12Texture->textureUploadHeap[j]));
			}
			/*
			D3D12_SUBRESOURCE_DATA resource = {};
			resource.pData = pData;
			resource.RowPitch = rowPitch;
			resource.SlicePitch = slicePitch;

			UpdateSubresources(cmdList, d3d12Texture->texture[i], d3d12Texture->textureUploadHeap[i], 0, 0, 1, &resource);*/
		}

		D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
		srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
		srvDesc.Texture2D.MipLevels = mip;
		srvDesc.Format = (format == DEPTH_32_FLOAT) ? DXGI_FORMAT_R32_FLOAT : ConvertFormat(format);
		srvDesc.ViewDimension = (arrSize==6) ? D3D12_SRV_DIMENSION_TEXTURECUBE : D3D12_SRV_DIMENSION_TEXTURE2D;

		CD3DX12_CPU_DESCRIPTOR_HANDLE srvDescriptorHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
			graphics->cbvSrvUavHeapNonShaderVisible->heap->GetCPUDescriptorHandleForHeapStart(),
			graphics->cbvSrvUavHeapNonShaderVisible->numberOfDescriptorsAllocated,
			cbvSrvUavDescriptorHandleIncrementSize
		);

		pDX12Device->CreateShaderResourceView(d3d12Texture->texture[i], &srvDesc, srvDescriptorHandle);

		d3d12Texture->descriptor[i].descriptorType = DescriptorType::CBV_SRV_UAV;
		d3d12Texture->descriptor[i].indexInHeap = graphics->cbvSrvUavHeapNonShaderVisible->numberOfDescriptorsAllocated++;

		this->synchNeeded = true;

		if (flags != ALLOW_RENDER_TARGET && flags != ALLOW_DEPTH_STENCIL) {
			cmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(d3d12Texture->texture[i], D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE));
			d3d12Texture->currentUsage[i] = ResourceUsage::PS_RESOURCE;
			break;
		}
		else {

			if (format == DEPTH_32_FLOAT) {
				cmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(d3d12Texture->texture[i], D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_DEPTH_WRITE));
				d3d12Texture->currentUsage[i] = ResourceUsage::DEPTH_WRITE;
			}
			else {
				cmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(d3d12Texture->texture[i], D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_RENDER_TARGET));
				d3d12Texture->currentUsage[i] = ResourceUsage::RENDER_TARGET;
			}
		}
	}

	return d3d12Texture;
}

ITexture* D3D12WorkingContext::CreateUAV(std::string name, const void* pData, unsigned int width, unsigned int height, unsigned int channels, Format format, UsageFlags flags, unsigned int mipCount) {
	D3D12Texture* iTexture = new D3D12Texture();
	
	D3D12_RESOURCE_DESC desc = {};
	desc.DepthOrArraySize = 1;
	desc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	desc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	desc.Flags = D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS;
	desc.Width = width;
	desc.Height = height;
	desc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	desc.MipLevels = 1;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;

	// Create the buffer resource
	graphics->dxrDevice->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT), 
		D3D12_HEAP_FLAG_NONE, 
		&desc, 
		D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, 
		nullptr,
		IID_PPV_ARGS(&iTexture->texture[0]));

	// Create the DXR output buffer UAV
	D3D12_UNORDERED_ACCESS_VIEW_DESC uavDesc = {};
	uavDesc.ViewDimension = D3D12_UAV_DIMENSION_TEXTURE2D;

	CD3DX12_CPU_DESCRIPTOR_HANDLE uavDescriptorHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
		graphics->pRTXDescriptorHeap->heap->GetCPUDescriptorHandleForHeapStart(),
		graphics->pRTXDescriptorHeap->numberOfDescriptorsAllocated,
		cbvSrvUavDescriptorHandleIncrementSize
	);

	graphics->dxrDevice->CreateUnorderedAccessView(iTexture->texture[0], nullptr, &uavDesc, uavDescriptorHandle);

	iTexture->descriptor->indexInHeap = graphics->pRTXDescriptorHeap->numberOfDescriptorsAllocated++;
	iTexture->currentUsage[0] = ResourceUsage::COPY_SOURCE;

	return iTexture;
}

void D3D12WorkingContext::UpdateTextureMip(ITexture* texture, void* pData, unsigned int mipIndex, size_t rowSlice, size_t slicePitch, size_t arrIndex, size_t mipCount, size_t arrCount, size_t totalSlice) {
	
	D3D12Texture* d3d12Texture = static_cast<D3D12Texture*>(texture);
	
	cmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(d3d12Texture->texture[0], D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_COPY_DEST));

	resources.push_back({});
	resources.at(resources.size() - 1).pData = pData;
	resources.at(resources.size() - 1).RowPitch = rowSlice;
	resources.at(resources.size() - 1).SlicePitch = slicePitch;

	unsigned n = D3D12CalcSubresource(mipIndex, arrIndex, 0, mipCount, arrCount);
	UpdateSubresources(cmdList, d3d12Texture->texture[0], d3d12Texture->textureUploadHeap[D3D12CalcSubresource(mipIndex, arrIndex, 0, mipCount, arrCount)], totalSlice, n, 1, &resources[resources.size() - 1]);
	cmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(d3d12Texture->texture[0], D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE));
}

void D3D12WorkingContext::SetRenderTargets(unsigned int rtvCount, std::vector<IRenderTarget*> renderTargets, IRenderTarget* dsv, bool bindDepthOnly) {

	if (rtvCount == 0 && !bindDepthOnly) {
		unsigned int frameIndex = graphics->pSwapChain->GetCurrentBackBufferIndex();
		CD3DX12_CPU_DESCRIPTOR_HANDLE rtvCpuHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
			graphics->pRtvDescriptorHeap->GetCPUDescriptorHandleForHeapStart(),
			frameIndex,
			rtvDescriptorHandleIncrementSize
		);

		if (graphics->backBufferUsageState[frameIndex] != ResourceUsage::RENDER_TARGET) {
			cmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(graphics->pRtvBackBufferTextures[frameIndex], ConvertResourceUsageState(graphics->backBufferUsageState[frameIndex]), D3D12_RESOURCE_STATE_RENDER_TARGET));
			graphics->backBufferUsageState[frameIndex] = ResourceUsage::RENDER_TARGET;
		}

		cmdList->OMSetRenderTargets(1, &rtvCpuHandle, true, nullptr);
	}
	else {

		for (int i = 0; i < rtvCount; i++) {

			D3D12RenderTarget* rtv = static_cast<D3D12RenderTarget*>(renderTargets[i]);

			CD3DX12_CPU_DESCRIPTOR_HANDLE srcRtvCpuHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
				graphics->rtvHeapNonShaderVisible->heap->GetCPUDescriptorHandleForHeapStart(),
				rtv->descriptor[currentWorkingBuffer].indexInHeap,
				graphics->rtvDescriptorHandleIncrementSize
			);

			CD3DX12_CPU_DESCRIPTOR_HANDLE dstRtvCpuHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
				graphics->rtvHeapNonShaderVisible->heap->GetCPUDescriptorHandleForHeapStart(),
				2048+i,
				graphics->rtvDescriptorHandleIncrementSize
			);

			pDX12Device->CopyDescriptorsSimple(1, dstRtvCpuHandle, srcRtvCpuHandle, D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
		}

		CD3DX12_CPU_DESCRIPTOR_HANDLE pRtvHandles = CD3DX12_CPU_DESCRIPTOR_HANDLE(
			graphics->rtvHeapNonShaderVisible->heap->GetCPUDescriptorHandleForHeapStart(),
			2048,
			graphics->rtvDescriptorHandleIncrementSize
		);

		if (dsv) {
			CD3DX12_CPU_DESCRIPTOR_HANDLE dsvCpuHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
				graphics->dsvDescriptorHeapNonShaderVisible->heap->GetCPUDescriptorHandleForHeapStart(),
				dsv->descriptor[currentWorkingBuffer].indexInHeap,
				graphics->dsvDescriptorHandleIncrementSize
			);

			cmdList->OMSetRenderTargets(rtvCount, &pRtvHandles, (rtvCount > 1) ? true : false, &dsvCpuHandle);
		}
		else {
			cmdList->OMSetRenderTargets(rtvCount, &pRtvHandles, (rtvCount > 1) ? true : false, nullptr);
		}
	}
}

IRenderTarget* D3D12WorkingContext::CreateRenderTarget(std::string name, ITexture* iTexture) {

	D3D12RenderTarget* d3d12Rtv = new D3D12RenderTarget();
	D3D12Texture* texture = static_cast<D3D12Texture*>(iTexture);
	texture->rtResource = true;

	for (int i = 0; i < BUFFER_COUNT; i++) {
	

		D3D12_RENDER_TARGET_VIEW_DESC rtvDesc = {};
		rtvDesc.Format = texture->format;
		rtvDesc.Texture2D.MipSlice = 0;
		rtvDesc.ViewDimension = D3D12_RTV_DIMENSION_TEXTURE2D;

		CD3DX12_CPU_DESCRIPTOR_HANDLE rtvCpuDescriptorHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
			graphics->rtvHeapNonShaderVisible->heap->GetCPUDescriptorHandleForHeapStart(),
			graphics->rtvHeapNonShaderVisible->numberOfDescriptorsAllocated,
			rtvDescriptorHandleIncrementSize
		);

		d3d12Rtv->descriptor[i].indexInHeap = graphics->rtvHeapNonShaderVisible->numberOfDescriptorsAllocated++;
		d3d12Rtv->descriptor[i].descriptorType = DescriptorType::RTV;
		d3d12Rtv->texture = texture;

		pDX12Device->CreateRenderTargetView(texture->texture[i], &rtvDesc, rtvCpuDescriptorHandle);
	}

	return d3d12Rtv;
}

IRenderTarget* D3D12WorkingContext::CreateDepthBuffer(std::string name, ITexture* iTexture) {
	D3D12RenderTarget* d3d12Rtv = new D3D12RenderTarget();
	D3D12Texture* texture = static_cast<D3D12Texture*>(iTexture);
	texture->rtResource = true;

	for (int i = 0; i < BUFFER_COUNT; i++) {
		D3D12_DEPTH_STENCIL_VIEW_DESC rtvDesc = {};
		rtvDesc.Format = DXGI_FORMAT_D32_FLOAT;
		rtvDesc.Texture2D.MipSlice = 0;
		rtvDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
		//rtvDesc.Flags = 

		CD3DX12_CPU_DESCRIPTOR_HANDLE rtvCpuDescriptorHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
			graphics->dsvDescriptorHeapNonShaderVisible->heap->GetCPUDescriptorHandleForHeapStart(),
			graphics->dsvDescriptorHeapNonShaderVisible->numberOfDescriptorsAllocated,
			graphics->dsvDescriptorHandleIncrementSize
		);

		texture->dsvResource = true;
		d3d12Rtv->descriptor[i].indexInHeap = graphics->dsvDescriptorHeapNonShaderVisible->numberOfDescriptorsAllocated++;
		d3d12Rtv->descriptor[i].descriptorType = DescriptorType::DSV;
		d3d12Rtv->texture = texture;

		pDX12Device->CreateDepthStencilView(texture->texture[i], &rtvDesc, rtvCpuDescriptorHandle);
	}

	return d3d12Rtv;
}

void D3D12WorkingContext::DrawIndexedInstanced(unsigned int instances, unsigned int startIndex, unsigned int indexCount) {
	cmdList->DrawIndexedInstanced(indexCount, instances, startIndex, 0, 0);
	offsetForDraw += TABLE_WIDTH;
}

void D3D12WorkingContext::DrawInstanced(unsigned int instanceCount, unsigned int vertexCount, unsigned int start) {
	cmdList->DrawInstanced(vertexCount, instanceCount, start, 0);
	offsetForDraw += TABLE_WIDTH;
}

void D3D12WorkingContext::BeginRecording() {

	currentWorkingBuffer++;
	if (currentWorkingBuffer > 2) {
		offsetForDraw = 0;
		rtxOffsetForDraw = 0;
		currentWorkingBuffer = 0;
	}

	if (fence[currentWorkingBuffer]->GetCompletedValue() < fenceValue[currentWorkingBuffer]) {
		fence[currentWorkingBuffer]->SetEventOnCompletion(fenceValue[currentWorkingBuffer], fenceEvent);
		WaitForSingleObject(fenceEvent, INFINITE);
	}

	cmdAlloc[currentWorkingBuffer]->Reset();
	cmdList->Reset(cmdAlloc[currentWorkingBuffer], nullptr);

	view = {};
	view.Height = 2160;
	view.Width = 3840;
	view.MinDepth = 0.1f;
	view.MaxDepth = 1.0f;
	view.TopLeftX = 0.0f;
	view.TopLeftY = 0.0f;

	rect.left = 0.0f;
	rect.right = 3840;
	rect.top = 0.0f;
	rect.bottom = 2160;

	cmdList->RSSetViewports(1, &view);
	cmdList->RSSetScissorRects(1, &rect);
}

void D3D12WorkingContext::SetViewport(unsigned int width, unsigned int height) {

	view.Width = width;
	view.Height = height;
	view.TopLeftY = 0.0f;
	view.TopLeftX = 0.0f;
	view.MinDepth = 0.01f;
	view.MaxDepth = 1.0f;

	rect.left = 0.0f;
	rect.right = width;
	rect.top = 0.0f;
	rect.bottom = height;

	cmdList->RSSetViewports(1, &view);
	cmdList->RSSetScissorRects(1, &rect);
}

void D3D12WorkingContext::StopRecording() {
	HRESULT hr = cmdList->Close();
	if (FAILED(hr)) {
		int i = 12;
	}
}

void D3D12WorkingContext::DispatchRays(IAccelerationStructure* iAS, ITexture* iTexture) {
	
	D3D12AccelerationStructure* as = static_cast<D3D12AccelerationStructure*>(iAS);
	D3D12Texture*          texture = static_cast<D3D12Texture*>(iTexture);
	
	//Get handles to our two heaps
	CD3DX12_CPU_DESCRIPTOR_HANDLE sourceDescriptor = CD3DX12_CPU_DESCRIPTOR_HANDLE(
		graphics->pRTXDescriptorHeap->heap->GetCPUDescriptorHandleForHeapStart(),
		as->asDescriptorIndex,
		cbvSrvUavDescriptorHandleIncrementSize);

	CD3DX12_CPU_DESCRIPTOR_HANDLE destDescriptor = CD3DX12_CPU_DESCRIPTOR_HANDLE(
		graphics->pRTXDstDescriptorHeap->heap->GetCPUDescriptorHandleForHeapStart(),
		rtxOffsetForDraw + 2,
		cbvSrvUavDescriptorHandleIncrementSize
	);

	graphics->dxrDevice->CopyDescriptorsSimple(1, destDescriptor, sourceDescriptor, D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

	BindDXRTable();

	// Set the UAV/SRV/CBV and sampler heaps
	ID3D12DescriptorHeap* ppHeaps[] = { graphics->pRTXDstDescriptorHeap->heap };
	dxrCmdList->SetDescriptorHeaps(_countof(ppHeaps), ppHeaps);
	dxrCmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(texture->texture[0], D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_UNORDERED_ACCESS));


	// Dispatch rays
	D3D12_DISPATCH_RAYS_DESC desc = {};
	desc.RayGenerationShaderRecord.StartAddress = graphics->dxr.shaderTable->GetGPUVirtualAddress();
	desc.RayGenerationShaderRecord.SizeInBytes  = graphics->dxr.shaderTableRecordSize;

	desc.MissShaderTable.StartAddress  = graphics->dxr.shaderTable->GetGPUVirtualAddress() + graphics->dxr.shaderTableRecordSize;
	desc.MissShaderTable.SizeInBytes   = graphics->dxr.shaderTableRecordSize;		// Only a single Miss program entry
	desc.MissShaderTable.StrideInBytes = graphics->dxr.shaderTableRecordSize;

	desc.HitGroupTable.StartAddress  = graphics->dxr.shaderTable->GetGPUVirtualAddress() + (graphics->dxr.shaderTableRecordSize * 2);
	desc.HitGroupTable.SizeInBytes   = graphics->dxr.shaderTableRecordSize;			// Only a single Hit program entry
	desc.HitGroupTable.StrideInBytes = graphics->dxr.shaderTableRecordSize;

	desc.Width = 1920;
	desc.Height = 1080;
	desc.Depth = 3;

	dxrCmdList->SetPipelineState1(graphics->dxr.rtpso);
	dxrCmdList->DispatchRays(&desc);

	dxrCmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(texture->texture[0], D3D12_RESOURCE_STATE_UNORDERED_ACCESS, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE));
	rtxOffsetForDraw += RTX_TABLE_WIDTH;
}

void D3D12WorkingContext::SetPresent(unsigned int index) {
	unsigned int frameIndex = graphics->pSwapChain->GetCurrentBackBufferIndex();

	if (graphics->backBufferUsageState[frameIndex] != ResourceUsage::PRESENT) {
		cmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(graphics->pRtvBackBufferTextures[frameIndex], D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT));
		graphics->backBufferUsageState[frameIndex] = ResourceUsage::PRESENT;
	}
}

void D3D12WorkingContext::TransisitionRenderTarget(RenderTargetTransisition rtvTransisition) {
	D3D12RenderTarget* d3d12RenderTarget = static_cast<D3D12RenderTarget*>(rtvTransisition.rtv);
	cmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(d3d12RenderTarget->texture->texture[currentWorkingBuffer], ConvertResourceUsageState(rtvTransisition.beforeResourceUsage), ConvertResourceUsageState(rtvTransisition.afterResourceUsage)));
}


void D3D12WorkingContext::TransisitionTexture(TextureTransisition textureTransisition) {

	D3D12Texture* d3d12Texture = static_cast<D3D12Texture*>(textureTransisition.texture);
	cmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(d3d12Texture->texture[currentWorkingBuffer], ConvertResourceUsageState(textureTransisition.beforeResourceUsage), ConvertResourceUsageState(textureTransisition.afterResourceUsage)));
}

void D3D12WorkingContext::Clear(IRenderTarget* iRenderTarget, IRenderTarget* iDepthBuffer, const float* clearColor) {
	CD3DX12_CPU_DESCRIPTOR_HANDLE *cpuRtvDescriptorHandle = nullptr;
	CD3DX12_CPU_DESCRIPTOR_HANDLE *cpuDsvDescriptorHandle = nullptr;

	if (iRenderTarget == nullptr) {
		if (iDepthBuffer == nullptr) {
			unsigned int frameIndex = graphics->pSwapChain->GetCurrentBackBufferIndex();

			cpuRtvDescriptorHandle = &CD3DX12_CPU_DESCRIPTOR_HANDLE(
				graphics->pRtvDescriptorHeap->GetCPUDescriptorHandleForHeapStart(),
				frameIndex,
				graphics->rtvDescriptorHandleIncrementSize
			);
		}
		else {
			cpuDsvDescriptorHandle = &CD3DX12_CPU_DESCRIPTOR_HANDLE(
				graphics->dsvDescriptorHeapNonShaderVisible->heap->GetCPUDescriptorHandleForHeapStart(),
				iDepthBuffer->descriptor[currentWorkingBuffer].indexInHeap,
				dsvDescriptorHandleIncrementSize
			);
		}
	}
	else {
		cpuRtvDescriptorHandle = &CD3DX12_CPU_DESCRIPTOR_HANDLE(
			graphics->rtvHeapNonShaderVisible->heap->GetCPUDescriptorHandleForHeapStart(),
			iRenderTarget->descriptor[currentWorkingBuffer].indexInHeap,
			graphics->rtvDescriptorHandleIncrementSize
		);

		if (iDepthBuffer != nullptr) {
			cpuDsvDescriptorHandle = &CD3DX12_CPU_DESCRIPTOR_HANDLE(
				graphics->dsvDescriptorHeapNonShaderVisible->heap->GetCPUDescriptorHandleForHeapStart(),
				iDepthBuffer->descriptor[currentWorkingBuffer].indexInHeap,
				graphics->dsvDescriptorHandleIncrementSize
			);
		}
	}

	if (cpuRtvDescriptorHandle)
		cmdList->ClearRenderTargetView(*cpuRtvDescriptorHandle, clearColor, 0, nullptr);
	if (cpuDsvDescriptorHandle) 
		cmdList->ClearDepthStencilView(*cpuDsvDescriptorHandle, D3D12_CLEAR_FLAG_DEPTH, 1.0f, 0, 0, nullptr);
	
}

IAccelerationStructure* D3D12WorkingContext::CreateAccelerationStructure(RTXModelData& rtxModelData) {


	HRESULT hr;

	D3D12_RESOURCE_DESC scratchDesc{};
	graphics->dxr.BLAS.resize(2048);

	IAccelerationStructure* as = new D3D12AccelerationStructure();
	std::vector<D3D12_RAYTRACING_GEOMETRY_DESC> geometryDescs;
	for (int i = 0; i < rtxModelData.vertexBuffers.size(); i++) {

		D3D12VertexBuffer* vb = static_cast<D3D12VertexBuffer*>(rtxModelData.vertexBuffers[i]);
		D3D12IndexBuffer* ib = static_cast<D3D12IndexBuffer*>(rtxModelData.indexBuffers[i]);

		// Describe the geometry that goes in the bottom acceleration structure(s)
		D3D12_RAYTRACING_GEOMETRY_DESC geometryDesc;
		geometryDesc.Type = D3D12_RAYTRACING_GEOMETRY_TYPE_TRIANGLES;
		geometryDesc.Triangles.VertexBuffer.StartAddress = vb->vertexBuffer->GetGPUVirtualAddress();
		geometryDesc.Triangles.VertexBuffer.StrideInBytes = vb->vbView.StrideInBytes;
		geometryDesc.Triangles.VertexCount = static_cast<UINT>(rtxModelData.verticeCount[i]);
		geometryDesc.Triangles.VertexFormat = DXGI_FORMAT_R32G32B32_FLOAT;
		geometryDesc.Triangles.IndexBuffer = ib->indexBuffer->GetGPUVirtualAddress();
		geometryDesc.Triangles.IndexFormat = ib->ibView.Format;
		geometryDesc.Triangles.IndexCount = static_cast<UINT>(rtxModelData.indiceCount[i]);
		geometryDesc.Triangles.Transform3x4 = 0;
		geometryDesc.Flags = D3D12_RAYTRACING_GEOMETRY_FLAG_OPAQUE;

		//geometryDescs.push_back(geometryDesc);


		D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS buildFlags = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_PREFER_FAST_TRACE;

		// Get the size requirements for the BLAS buffers
		D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS ASInputs = {};
		ASInputs.Type = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL;
		ASInputs.DescsLayout = D3D12_ELEMENTS_LAYOUT_ARRAY;
		ASInputs.pGeometryDescs = &geometryDesc;
		ASInputs.NumDescs = 1;
		ASInputs.Flags = buildFlags;

		D3D12_RAYTRACING_ACCELERATION_STRUCTURE_PREBUILD_INFO ASPreBuildInfo = {};
		graphics->dxrDevice->GetRaytracingAccelerationStructurePrebuildInfo(&ASInputs, &ASPreBuildInfo);

		ASPreBuildInfo.ScratchDataSizeInBytes = ALIGN(D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BYTE_ALIGNMENT, ASPreBuildInfo.ScratchDataSizeInBytes);
		ASPreBuildInfo.ResultDataMaxSizeInBytes = ALIGN(D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BYTE_ALIGNMENT, ASPreBuildInfo.ResultDataMaxSizeInBytes);

		scratchDesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
		scratchDesc.Alignment = max(D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BYTE_ALIGNMENT, D3D12_DEFAULT_RESOURCE_PLACEMENT_ALIGNMENT);
		scratchDesc.DepthOrArraySize = 1;
		scratchDesc.MipLevels = 1;
		scratchDesc.SampleDesc.Count = 1;
		scratchDesc.SampleDesc.Quality = 0;
		scratchDesc.Height = 1;
		scratchDesc.Width = ASPreBuildInfo.ScratchDataSizeInBytes;
		scratchDesc.Format = DXGI_FORMAT_UNKNOWN;
		scratchDesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
		scratchDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS;

		hr = graphics->dxrDevice->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
			D3D12_HEAP_FLAG_NONE,
			&scratchDesc,
			D3D12_RESOURCE_STATE_UNORDERED_ACCESS,
			nullptr,
			IID_PPV_ARGS(&graphics->dxr.BLAS[i].pScratch));

		if (FAILED(hr)) {
			return nullptr;
		}

		// Create the BLAS buffer

		scratchDesc.Width = ASPreBuildInfo.ResultDataMaxSizeInBytes;

		hr = graphics->dxrDevice->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
			D3D12_HEAP_FLAG_NONE,
			&scratchDesc,
			D3D12_RESOURCE_STATE_RAYTRACING_ACCELERATION_STRUCTURE,
			nullptr,
			IID_PPV_ARGS(&graphics->dxr.BLAS[i].pResult));

		if (FAILED(hr)) {
			return nullptr;
		}

#if NAME_D3D_RESOURCES
		dxr.BLAS.pResult->SetName(L"DXR BLAS");
#endif

		// Describe and build the bottom level acceleration structure
		D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_DESC buildDesc = {};
		buildDesc.Inputs = ASInputs;
		buildDesc.ScratchAccelerationStructureData = graphics->dxr.BLAS[i].pScratch->GetGPUVirtualAddress();
		buildDesc.DestAccelerationStructureData = graphics->dxr.BLAS[i].pResult->GetGPUVirtualAddress();

		dxrCmdList->BuildRaytracingAccelerationStructure(&buildDesc, 0, nullptr);

		// Wait for the BLAS build to complete
		D3D12_RESOURCE_BARRIER uavBarrier;
		uavBarrier.Type = D3D12_RESOURCE_BARRIER_TYPE_UAV;
		uavBarrier.UAV.pResource = graphics->dxr.BLAS[i].pResult;
		uavBarrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
		dxrCmdList->ResourceBarrier(1, &uavBarrier);
	}

	for (uint32_t i = 0; i < rtxModelData.vertexBuffers.size(); i++) {
		D3D12_RAYTRACING_INSTANCE_DESC instanceDesc = {};
		instanceDesc.InstanceID = i%rtxModelData.numberOfModels;
		instanceDesc.InstanceContributionToHitGroupIndex = 0;
		instanceDesc.InstanceMask = 0xFF;
		memcpy(instanceDesc.Transform, rtxModelData.matrices[i]._r, sizeof(float) * 12);

		as->instances.push_back({ i });

		memcpy(as->instances[as->instances.size() - 1].transform._r, rtxModelData.matrices[i]._r, sizeof(float) * 16);


		instanceDesc.AccelerationStructure = graphics->dxr.BLAS[i].pResult->GetGPUVirtualAddress();
		instanceDesc.Flags = D3D12_RAYTRACING_INSTANCE_FLAG_TRIANGLE_FRONT_COUNTERCLOCKWISE;

		instanceDescs.push_back(instanceDesc);
	}




	hr = graphics->dxrDevice->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(sizeof(instanceDescs[0])* rtxModelData.vertexBuffers.size()),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&graphics->dxr.TLAS.pInstanceDesc));

	if (FAILED(hr)) {
		return nullptr;
	}

	graphics->dxr.TLAS.pInstanceDesc->Map(0, nullptr, (void**)&pInstanceMappedData);

	for (int i = 0; i < rtxModelData.vertexBuffers.size(); i++) {
		memcpy(pInstanceMappedData, &instanceDescs[i], sizeof(instanceDescs[0]));
		pInstanceMappedData += sizeof(instanceDescs[0]);
	}

	graphics->dxr.TLAS.pInstanceDesc->Unmap(0, nullptr);

	D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS buildFlags;
	buildFlags = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_ALLOW_UPDATE;

	// Get the size requirements for the TLAS buffers
	D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_INPUTS ASInputs;
	ASInputs = {};
	ASInputs.Type = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL;
	ASInputs.DescsLayout = D3D12_ELEMENTS_LAYOUT_ARRAY;
	ASInputs.InstanceDescs = graphics->dxr.TLAS.pInstanceDesc->GetGPUVirtualAddress();
	ASInputs.NumDescs = instanceDescs.size();
	ASInputs.Flags = buildFlags;

	D3D12_RAYTRACING_ACCELERATION_STRUCTURE_PREBUILD_INFO ASPreBuildInfo = {};
	ASPreBuildInfo = {};
	graphics->dxrDevice->GetRaytracingAccelerationStructurePrebuildInfo(&ASInputs, &ASPreBuildInfo);

	ASPreBuildInfo.ResultDataMaxSizeInBytes = ALIGN(D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BYTE_ALIGNMENT, ASPreBuildInfo.ResultDataMaxSizeInBytes);
	ASPreBuildInfo.ScratchDataSizeInBytes = ALIGN(D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BYTE_ALIGNMENT, ASPreBuildInfo.ScratchDataSizeInBytes);

	// Set TLAS size
	graphics->dxr.tlasSize = ASPreBuildInfo.ResultDataMaxSizeInBytes;

	//D3D12_RESOURCE_DESC scratchDesc{};
	scratchDesc.Width = ASPreBuildInfo.ScratchDataSizeInBytes;
	scratchDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS;
	scratchDesc.Alignment = max(D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BYTE_ALIGNMENT, D3D12_DEFAULT_RESOURCE_PLACEMENT_ALIGNMENT);
	// Create TLAS scratch buffer
	//D3D12BufferCreateInfo bufferInfo(ASPreBuildInfo.ScratchDataSizeInBytes, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS, D3D12_RESOURCE_STATE_UNORDERED_ACCESS);
	//bufferInfo.alignment = max(D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BYTE_ALIGNMENT, D3D12_DEFAULT_RESOURCE_PLACEMENT_ALIGNMENT);
	//D3DResources::Create_Buffer(d3d, bufferInfo, &dxr.TLAS.pScratch);

	hr = pDX12Device->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
		D3D12_HEAP_FLAG_NONE,
		&scratchDesc,
		D3D12_RESOURCE_STATE_UNORDERED_ACCESS,
		nullptr,
		IID_PPV_ARGS(&graphics->dxr.TLAS.pScratch));

	if (FAILED(hr)) {
		return nullptr;
	}
#if NAME_D3D_RESOURCES
	dxr.TLAS.pScratch->SetName(L"DXR TLAS Scratch");
#endif

	// Create the TLAS buffer
	scratchDesc.Width = ASPreBuildInfo.ResultDataMaxSizeInBytes;

	hr = pDX12Device->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
		D3D12_HEAP_FLAG_NONE,
		&scratchDesc,
		D3D12_RESOURCE_STATE_RAYTRACING_ACCELERATION_STRUCTURE,
		nullptr,
		IID_PPV_ARGS(&graphics->dxr.TLAS.pResult));

	if (FAILED(hr)) {
		return nullptr;
	}
	//D3DResources::Create_Buffer(d3d, bufferInfo, &dxr.TLAS.pResult);
#if NAME_D3D_RESOURCES
	dxr.TLAS.pResult->SetName(L"DXR TLAS");
#endif

	// Describe and build the TLAS
	D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_DESC buildDesc = {};
	buildDesc = {};
	buildDesc.Inputs = ASInputs;
	buildDesc.ScratchAccelerationStructureData = graphics->dxr.TLAS.pScratch->GetGPUVirtualAddress();
	buildDesc.DestAccelerationStructureData    = graphics->dxr.TLAS.pResult->GetGPUVirtualAddress();

	dxrCmdList->BuildRaytracingAccelerationStructure(&buildDesc, 0, nullptr);

	// Wait for the TLAS build to complete
	D3D12_RESOURCE_BARRIER uavBarrier;
	uavBarrier = {};
	uavBarrier.Type = D3D12_RESOURCE_BARRIER_TYPE_UAV;
	uavBarrier.UAV.pResource = graphics->dxr.TLAS.pResult;
	uavBarrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
	dxrCmdList->ResourceBarrier(1, &uavBarrier);

	// Create the DXR Top Level Acceleration Structure SRV
	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc;
	srvDesc.Format = DXGI_FORMAT_UNKNOWN;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_RAYTRACING_ACCELERATION_STRUCTURE;
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc.RaytracingAccelerationStructure.Location = graphics->dxr.TLAS.pResult->GetGPUVirtualAddress();

	CD3DX12_CPU_DESCRIPTOR_HANDLE pRTXTlasHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
		graphics->pRTXDescriptorHeap->heap->GetCPUDescriptorHandleForHeapStart(),
		graphics->pRTXDescriptorHeap->numberOfDescriptorsAllocated,
		cbvSrvUavDescriptorHandleIncrementSize
	);
	graphics->dxrDevice->CreateShaderResourceView(nullptr, &srvDesc, pRTXTlasHandle);
  
	as->asDescriptorIndex = graphics->pRTXDescriptorHeap->numberOfDescriptorsAllocated++;

	return as;
}

void D3D12WorkingContext::RebuildTLAS(IAccelerationStructure* iAS) {

	D3D12_RESOURCE_BARRIER uavBarrier = {};
	uavBarrier.Type = D3D12_RESOURCE_BARRIER_TYPE_UAV;
	uavBarrier.UAV.pResource = graphics->dxr.TLAS.pResult;
	dxrCmdList->ResourceBarrier(1, &uavBarrier);

	D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_DESC buildDesc = {};
	buildDesc = {};
	buildDesc.Inputs.DescsLayout = D3D12_ELEMENTS_LAYOUT_ARRAY;
	buildDesc.Inputs.NumDescs = iAS->instances.size();
	buildDesc.Inputs.Flags |= D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_PERFORM_UPDATE;
	buildDesc.Inputs.Type = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL;
	buildDesc.ScratchAccelerationStructureData = graphics->dxr.TLAS.pScratch->GetGPUVirtualAddress();
	buildDesc.SourceAccelerationStructureData = graphics->dxr.TLAS.pResult->GetGPUVirtualAddress();
	buildDesc.DestAccelerationStructureData = graphics->dxr.TLAS.pResult->GetGPUVirtualAddress();

	instanceDescs.clear();
	for (uint32_t i = 0; i < iAS->instances.size(); i++) {
		D3D12_RAYTRACING_INSTANCE_DESC instanceDesc = {};
		instanceDesc.InstanceID = i % iAS->instances.size();
		instanceDesc.InstanceContributionToHitGroupIndex = 0;
		instanceDesc.InstanceMask = 0xFF;
		memcpy(instanceDesc.Transform, iAS->instances[i].transform._r, sizeof(float) * 12);

		instanceDesc.AccelerationStructure = graphics->dxr.BLAS[i].pResult->GetGPUVirtualAddress();
		instanceDesc.Flags = D3D12_RAYTRACING_INSTANCE_FLAG_TRIANGLE_FRONT_COUNTERCLOCKWISE;

		instanceDescs.push_back(instanceDesc);
	}

	buildDesc.Inputs.InstanceDescs = graphics->dxr.TLAS.pInstanceDesc->GetGPUVirtualAddress();

	graphics->dxr.TLAS.pInstanceDesc->Map(0, nullptr, (void**)&pInstanceMappedData);

	for (int i = 0; i < iAS->instances.size(); i++) {
		memcpy(pInstanceMappedData, &instanceDescs[i], sizeof(instanceDescs[0]));
		pInstanceMappedData += sizeof(instanceDescs[0]);
	}

	graphics->dxr.TLAS.pInstanceDesc->Unmap(0, nullptr);

	dxrCmdList->BuildRaytracingAccelerationStructure(&buildDesc, 0, nullptr);

	// Wait for the TLAS build to complete
	uavBarrier = {};
	uavBarrier.Type = D3D12_RESOURCE_BARRIER_TYPE_UAV;
	uavBarrier.UAV.pResource = graphics->dxr.TLAS.pResult;
	uavBarrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
	dxrCmdList->ResourceBarrier(1, &uavBarrier);
}

D3D12Graphics::D3D12Graphics(unsigned int width, unsigned int height, unsigned int rr, HWND hWnd) {

	this->width = width;
	this->height = height;
	this->rr = rr;

	this->hWnd = hWnd;
}

void D3D12Graphics::Initialize() {

	HRESULT hr;

	CreateDXGIFactory1(IID_PPV_ARGS(&factory));
	IDXGIAdapter* pAdapter = nullptr;
	IDXGIAdapter* targetAdapter = nullptr;

	uint8_t adapterIdx = 0;
	size_t videoMem = 0;

	for (uint8_t i = 0; i < 8; i++) {
		HRESULT hr = factory->EnumAdapters(i, &pAdapter);
		if (FAILED(hr)) {
			break;
		}

		DXGI_ADAPTER_DESC adapterDesc{};
		pAdapter->GetDesc(&adapterDesc);

		if (videoMem < adapterDesc.DedicatedVideoMemory) {
			videoMem = adapterDesc.DedicatedVideoMemory;
			targetAdapter = pAdapter;
		}
	}
	

	D3D12CreateDevice(targetAdapter, D3D_FEATURE_LEVEL_12_0, IID_PPV_ARGS(&pDX12Device));

	D3D12_COMMAND_QUEUE_DESC cmdQueueDesc = {};
	cmdQueueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
	cmdQueueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;

	pDX12Device->CreateCommandQueue(&cmdQueueDesc, IID_PPV_ARGS(&pDX12CommandQueue));

	DXGI_SWAP_CHAIN_DESC swapDesc = {};
	swapDesc.BufferCount = BUFFER_COUNT;
	swapDesc.BufferDesc.Height = height;
	swapDesc.BufferDesc.Width = width;
	swapDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapDesc.BufferDesc.RefreshRate.Numerator = 60;
	swapDesc.BufferDesc.RefreshRate.Denominator = 1;
	swapDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapDesc.OutputWindow = hWnd;
	swapDesc.SampleDesc.Count = 1;
	swapDesc.SampleDesc.Quality = 0;
	swapDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	swapDesc.Windowed = true;

	IDXGISwapChain* swapChain1;
	factory->CreateSwapChain(pDX12CommandQueue, &swapDesc, &swapChain1);

	pSwapChain = static_cast<IDXGISwapChain3*>(swapChain1);

	D3D12_DESCRIPTOR_HEAP_DESC rtvDescriptorHeapDesc = {};
	rtvDescriptorHeapDesc.NumDescriptors = 256;
	rtvDescriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;

	pDX12Device->CreateDescriptorHeap(&rtvDescriptorHeapDesc, IID_PPV_ARGS(&pRtvDescriptorHeap));
	rtvDescriptorHandleIncrementSize = pDX12Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);

	D3D12_DESCRIPTOR_HEAP_DESC cbvSrvUavDescriptorHeapDesc = {};
	cbvSrvUavDescriptorHeapDesc.NumDescriptors = 32768;
	cbvSrvUavDescriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;

	cbvSrvUavHeapNonShaderVisible = new DescriptorHeap();
	pDX12Device->CreateDescriptorHeap(&cbvSrvUavDescriptorHeapDesc, IID_PPV_ARGS(&cbvSrvUavHeapNonShaderVisible->heap));
	rtvDescriptorHandleIncrementSize = pDX12Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);

	rtvDescriptorHeapDesc = {};
	rtvDescriptorHeapDesc.NumDescriptors = 4096;
	rtvDescriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;

	rtvDescriptorHandleIncrementSize = pDX12Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
	cbvSrvUavDescriptorHandleIncrementSize = pDX12Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

	D3D12_DESCRIPTOR_HEAP_DESC dsvDescriptorHeapDesc = {};
	dsvDescriptorHeapDesc.NumDescriptors = 4096;
	dsvDescriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;

	dsvDescriptorHandleIncrementSize = pDX12Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_DSV);
	//pDX12Device->CreateDescriptorHeap(&dsvDescriptorHeapDesc, IID_PPV_ARGS(&dsvDescriptorHeapNonShaderVisible->heap));

	rtvHeapShaderVisible = new DescriptorHeap();
	rtvHeapNonShaderVisible = new DescriptorHeap();

	dsvDescriptorHeapNonShaderVisible = new DescriptorHeap();
	hr = pDX12Device->CreateDescriptorHeap(&rtvDescriptorHeapDesc, IID_PPV_ARGS(&rtvHeapNonShaderVisible->heap));
	if (FAILED(hr)) {

	}

	hr = pDX12Device->CreateDescriptorHeap(&dsvDescriptorHeapDesc, IID_PPV_ARGS(&dsvDescriptorHeapNonShaderVisible->heap));
	if (FAILED(hr)) {

	}

	//rtvDescriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	pDX12Device->CreateDescriptorHeap(&rtvDescriptorHeapDesc, IID_PPV_ARGS(&rtvHeapShaderVisible->heap));
	if (FAILED(hr)) {

	}


	for (unsigned int i = 0; i < BUFFER_COUNT; i++) {

		HRESULT hr;
		pSwapChain->GetBuffer(i, IID_PPV_ARGS(&pRtvBackBufferTextures[i]));
		CD3DX12_CPU_DESCRIPTOR_HANDLE rtvCpuDescriptorHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
			pRtvDescriptorHeap->GetCPUDescriptorHandleForHeapStart(),
			i,
			rtvDescriptorHandleIncrementSize
		);
		pDX12Device->CreateRenderTargetView(pRtvBackBufferTextures[i], nullptr, rtvCpuDescriptorHandle);
		backBufferUsageState[i] = ResourceUsage::PRESENT;
	}


	view = {};
	view.Height = height;
	view.Width = width;
	view.MinDepth = 0.1f;
	view.MaxDepth = 1.0f;
	view.TopLeftX = 0.0f;
	view.TopLeftY = 0.0f;

	scissorRect = {};
	scissorRect.bottom = height;
	scissorRect.right = width;

	D3D12_STATIC_SAMPLER_DESC samplerDesc[2] = {};
	samplerDesc[0].AddressU = samplerDesc[0].AddressV = samplerDesc[0].AddressW = D3D12_TEXTURE_ADDRESS_MODE_MIRROR;
	samplerDesc[0].ComparisonFunc = D3D12_COMPARISON_FUNC_NEVER;
	samplerDesc[0].Filter = D3D12_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR;
	samplerDesc[0].MaxAnisotropy = 0;
	samplerDesc[0].MinLOD = 0.0f;
	samplerDesc[0].MaxLOD = D3D12_FLOAT32_MAX;
	samplerDesc[0].MipLODBias = 0;
	samplerDesc[0].RegisterSpace = 0;
	samplerDesc[0].ShaderRegister = 0;
	samplerDesc[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	samplerDesc[1].AddressU = samplerDesc[1].AddressV = samplerDesc[1].AddressW = D3D12_TEXTURE_ADDRESS_MODE_MIRROR;
	samplerDesc[1].ComparisonFunc = D3D12_COMPARISON_FUNC_NEVER;
	samplerDesc[1].Filter = D3D12_FILTER_COMPARISON_MIN_MAG_MIP_POINT;
	samplerDesc[1].MaxAnisotropy = 0;
	samplerDesc[1].MinLOD = 0.0f;
	samplerDesc[1].MaxLOD = D3D12_FLOAT32_MAX;
	samplerDesc[1].MipLODBias = 0;
	samplerDesc[1].RegisterSpace = 0;
	samplerDesc[1].ShaderRegister = 1;
	samplerDesc[1].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;


	D3D12_DESCRIPTOR_RANGE range[2];
	range[0].BaseShaderRegister = 0;
	range[0].NumDescriptors = 9;
	range[0].OffsetInDescriptorsFromTableStart = 0;
	range[0].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	range[0].RegisterSpace = 0;

	range[1].BaseShaderRegister = 0;
	range[1].NumDescriptors = 6;
	range[1].OffsetInDescriptorsFromTableStart = 9;
	range[1].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_CBV;
	range[1].RegisterSpace = 0;

	D3D12_ROOT_DESCRIPTOR_TABLE table{};
	table.NumDescriptorRanges = 2;
	table.pDescriptorRanges = range;

	D3D12_ROOT_PARAMETER param{};
	param.DescriptorTable = table;
	param.ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;
	param.ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;

	IArgumentLayoutObject* al;

	ID3DBlob* rootSigCode;
	CD3DX12_ROOT_SIGNATURE_DESC rootSigDesc = CD3DX12_ROOT_SIGNATURE_DESC(1, &param, 2, samplerDesc, D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT);
	D3D12SerializeRootSignature(&rootSigDesc, D3D_ROOT_SIGNATURE_VERSION_1, &rootSigCode, nullptr);

	pDX12Device->CreateRootSignature(0, rootSigCode->GetBufferPointer(), rootSigCode->GetBufferSize(), IID_PPV_ARGS(&rootSignature));

	//Check if the device supports at least raytracing tier 1
	///////////////////////////////////////////////////////////////////
	D3D12_FEATURE_DATA_D3D12_OPTIONS5 options5 = {}; 
	pDX12Device->CheckFeatureSupport(D3D12_FEATURE_D3D12_OPTIONS5, &options5, sizeof(options5)); 
	if (options5.RaytracingTier < D3D12_RAYTRACING_TIER_1_0) {
		rtSupport = false;
		return;
	}

	pDX12Device->QueryInterface(IID_PPV_ARGS(&dxrDevice));


	pRTXDescriptorHeap = new DescriptorHeap();

	D3D12_DESCRIPTOR_HEAP_DESC rtxHeapDesc{};
	rtxHeapDesc.NumDescriptors = 1200;
	rtxHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	rtxHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;

	pDX12Device->CreateDescriptorHeap(&rtxHeapDesc, IID_PPV_ARGS(&pRTXDescriptorHeap->heap));

	pRTXDstDescriptorHeap = new DescriptorHeap();

	D3D12_DESCRIPTOR_HEAP_DESC rtxHeapDesc2{};
	rtxHeapDesc2.NumDescriptors = 1200;
	rtxHeapDesc2.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	rtxHeapDesc2.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;

	pDX12Device->CreateDescriptorHeap(&rtxHeapDesc2, IID_PPV_ARGS(&pRTXDstDescriptorHeap->heap));

	D3D12_DESCRIPTOR_RANGE rtxrange[4];
	rtxrange[0].BaseShaderRegister = 0;
	rtxrange[0].NumDescriptors = 2;
	rtxrange[0].OffsetInDescriptorsFromTableStart = 0;
	rtxrange[0].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_UAV;
	rtxrange[0].RegisterSpace = 0;
	
	rtxrange[1].BaseShaderRegister = 2;
	rtxrange[1].NumDescriptors = 12;
	rtxrange[1].OffsetInDescriptorsFromTableStart = 2;
	rtxrange[1].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	rtxrange[1].RegisterSpace = 0;

	rtxrange[3].BaseShaderRegister = 0;
	rtxrange[3].NumDescriptors = 2;
	rtxrange[3].OffsetInDescriptorsFromTableStart = 14;
	rtxrange[3].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_CBV;
	rtxrange[3].RegisterSpace = 0;

	rtxrange[2].BaseShaderRegister = 14;
	rtxrange[2].NumDescriptors = 64; //max srvs
	rtxrange[2].OffsetInDescriptorsFromTableStart = 16;
	rtxrange[2].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	rtxrange[2].RegisterSpace = 1;


	D3D12_ROOT_DESCRIPTOR_TABLE rttable{};
	rttable.NumDescriptorRanges = 4;
	rttable.pDescriptorRanges = rtxrange;

	D3D12_ROOT_PARAMETER params{};
	params.DescriptorTable = rttable;
	params.ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	params.ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	D3D12_STATIC_SAMPLER_DESC rtxSamplerDesc = {};
	rtxSamplerDesc.AddressU = rtxSamplerDesc.AddressV = rtxSamplerDesc.AddressW = D3D12_TEXTURE_ADDRESS_MODE_MIRROR;
	rtxSamplerDesc.ComparisonFunc = D3D12_COMPARISON_FUNC_NEVER;
	rtxSamplerDesc.Filter = D3D12_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR;
	rtxSamplerDesc.MaxAnisotropy = 0;
	rtxSamplerDesc.MinLOD = 0.0f;
	rtxSamplerDesc.MaxLOD = D3D12_FLOAT32_MAX;
	rtxSamplerDesc.MipLODBias = 0;
	rtxSamplerDesc.RegisterSpace = 0;
	rtxSamplerDesc.ShaderRegister = 0;
	rtxSamplerDesc.ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	CD3DX12_ROOT_SIGNATURE_DESC rrootSigDesc{};
	rrootSigDesc.Init(0, nullptr, 0, nullptr, D3D12_ROOT_SIGNATURE_FLAG_NONE);

	ID3DBlob* rtxrsblob;
	D3D12SerializeRootSignature(&rrootSigDesc, D3D_ROOT_SIGNATURE_VERSION_1_0, &rtxrsblob, nullptr);

	pDX12Device->CreateRootSignature(0, rtxrsblob->GetBufferPointer(), rtxrsblob->GetBufferSize(), IID_PPV_ARGS(&rtGlobal));


	ID3DBlob* rootSigCode2, *error;
	CD3DX12_ROOT_SIGNATURE_DESC rootSigDesc2 = CD3DX12_ROOT_SIGNATURE_DESC(1, &params, 1, &rtxSamplerDesc, D3D12_ROOT_SIGNATURE_FLAG_LOCAL_ROOT_SIGNATURE);
	D3D12SerializeRootSignature(&rootSigDesc2, D3D_ROOT_SIGNATURE_VERSION_1, &rootSigCode2,&error);


	if (error) {
		const char* msg = (const char*)error->GetBufferPointer();
		int x = 12;
	}

	pDX12Device->CreateRootSignature(0, rootSigCode2->GetBufferPointer(), rootSigCode2->GetBufferSize(), IID_PPV_ARGS(&rtLocal));



	CD3DX12_STATE_OBJECT_DESC rtPipeline{ D3D12_STATE_OBJECT_TYPE_RAYTRACING_PIPELINE };

	auto lib = rtPipeline.CreateSubobject<CD3DX12_DXIL_LIBRARY_SUBOBJECT>();
	
	Init_Shader_Compiler(shaderCompiler);
	
	dxr.rgs = RtProgram(D3D12ShaderInfo(L"Assets\\Shaders\\RayGen.hlsl", L"", L"lib_6_3"));
	Compile_Shader(shaderCompiler, dxr.rgs);

	dxr.miss = RtProgram(D3D12ShaderInfo(L"Assets\\Shaders\\Miss.hlsl", L"", L"lib_6_3"));
	Compile_Shader(shaderCompiler, dxr.miss);

	dxr.hit = HitProgram(L"Hit");
	dxr.hit.chs = RtProgram(D3D12ShaderInfo(L"Assets\\Shaders\\ClosestHit.hlsl", L"", L"lib_6_3"));
	Compile_Shader(shaderCompiler, dxr.hit.chs);

	UINT index = 0;
	std::vector<D3D12_STATE_SUBOBJECT> subobjects;
	subobjects.resize(10);

	// Add state subobject for the RGS
	D3D12_EXPORT_DESC rgsExportDesc = {};
	rgsExportDesc.Name = L"RayGen_12";
	rgsExportDesc.ExportToRename = L"RayGen";
	rgsExportDesc.Flags = D3D12_EXPORT_FLAG_NONE;

	D3D12_DXIL_LIBRARY_DESC	rgsLibDesc = {};
	rgsLibDesc.DXILLibrary.BytecodeLength = dxr.rgs.blob->GetBufferSize();
	rgsLibDesc.DXILLibrary.pShaderBytecode = dxr.rgs.blob->GetBufferPointer();
	rgsLibDesc.NumExports = 1;
	rgsLibDesc.pExports = &rgsExportDesc;

	D3D12_STATE_SUBOBJECT rgs = {};
	rgs.Type = D3D12_STATE_SUBOBJECT_TYPE_DXIL_LIBRARY;
	rgs.pDesc = &rgsLibDesc;

	subobjects[index++] = rgs;

	// Add state subobject for the Miss shader
	D3D12_EXPORT_DESC msExportDesc = {};
	msExportDesc.Name = L"Miss_5";
	msExportDesc.ExportToRename = L"Miss";
	msExportDesc.Flags = D3D12_EXPORT_FLAG_NONE;

	D3D12_DXIL_LIBRARY_DESC	msLibDesc = {};
	msLibDesc.DXILLibrary.BytecodeLength = dxr.miss.blob->GetBufferSize();
	msLibDesc.DXILLibrary.pShaderBytecode = dxr.miss.blob->GetBufferPointer();
	msLibDesc.NumExports = 1;
	msLibDesc.pExports = &msExportDesc;

	D3D12_STATE_SUBOBJECT ms = {};
	ms.Type = D3D12_STATE_SUBOBJECT_TYPE_DXIL_LIBRARY;
	ms.pDesc = &msLibDesc;

	subobjects[index++] = ms;

	// Add state subobject for the Closest Hit shader
	D3D12_EXPORT_DESC chsExportDesc = {};
	chsExportDesc.Name = L"ClosestHit_76";
	chsExportDesc.ExportToRename = L"ClosestHit";
	chsExportDesc.Flags = D3D12_EXPORT_FLAG_NONE;

	D3D12_DXIL_LIBRARY_DESC	chsLibDesc = {};
	chsLibDesc.DXILLibrary.BytecodeLength = dxr.hit.chs.blob->GetBufferSize();
	chsLibDesc.DXILLibrary.pShaderBytecode = dxr.hit.chs.blob->GetBufferPointer();
	chsLibDesc.NumExports = 1;
	chsLibDesc.pExports = &chsExportDesc;

	D3D12_STATE_SUBOBJECT chs = {};
	chs.Type = D3D12_STATE_SUBOBJECT_TYPE_DXIL_LIBRARY;
	chs.pDesc = &chsLibDesc;

	subobjects[index++] = chs;

	// Add a state subobject for the hit group
	D3D12_HIT_GROUP_DESC hitGroupDesc = {};
	hitGroupDesc.ClosestHitShaderImport = L"ClosestHit_76";
	hitGroupDesc.HitGroupExport = L"HitGroup";

	D3D12_STATE_SUBOBJECT hitGroup = {};
	hitGroup.Type = D3D12_STATE_SUBOBJECT_TYPE_HIT_GROUP;
	hitGroup.pDesc = &hitGroupDesc;

	subobjects[index++] = hitGroup;

	// Add a state subobject for the shader payload configuration
	D3D12_RAYTRACING_SHADER_CONFIG shaderDesc = {};
	shaderDesc.MaxPayloadSizeInBytes = sizeof(float)*16;	// RGB and HitT
	shaderDesc.MaxAttributeSizeInBytes = D3D12_RAYTRACING_MAX_ATTRIBUTE_SIZE_IN_BYTES;

	D3D12_STATE_SUBOBJECT shaderConfigObject = {};
	shaderConfigObject.Type = D3D12_STATE_SUBOBJECT_TYPE_RAYTRACING_SHADER_CONFIG;
	shaderConfigObject.pDesc = &shaderDesc;

	subobjects[index++] = shaderConfigObject;

	// Create a list of the shader export names that use the payload
	const WCHAR* shaderExports[] = { L"RayGen_12", L"Miss_5", L"HitGroup" };

	// Add a state subobject for the association between shaders and the payload
	D3D12_SUBOBJECT_TO_EXPORTS_ASSOCIATION shaderPayloadAssociation = {};
	shaderPayloadAssociation.NumExports = _countof(shaderExports);
	shaderPayloadAssociation.pExports = shaderExports;
	shaderPayloadAssociation.pSubobjectToAssociate = &subobjects[(index - 1)];

	D3D12_STATE_SUBOBJECT shaderPayloadAssociationObject = {};
	shaderPayloadAssociationObject.Type = D3D12_STATE_SUBOBJECT_TYPE_SUBOBJECT_TO_EXPORTS_ASSOCIATION;
	shaderPayloadAssociationObject.pDesc = &shaderPayloadAssociation;

	subobjects[index++] = shaderPayloadAssociationObject;

	// Add a state subobject for the shared root signature 
	D3D12_STATE_SUBOBJECT rayGenRootSigObject = {};
	rayGenRootSigObject.Type = D3D12_STATE_SUBOBJECT_TYPE_LOCAL_ROOT_SIGNATURE;
	rayGenRootSigObject.pDesc = &rtLocal;

	subobjects[index++] = rayGenRootSigObject;

	//Create a list of the shader export names that use the root signature
	const WCHAR* rootSigExports[] = { L"RayGen_12", L"HitGroup", L"Miss_5" };

	//Add a state subobject for the association between the RayGen shader and the local root signature
	D3D12_SUBOBJECT_TO_EXPORTS_ASSOCIATION rayGenShaderRootSigAssociation = {};
	rayGenShaderRootSigAssociation.NumExports = _countof(rootSigExports);
	rayGenShaderRootSigAssociation.pExports = rootSigExports;
	rayGenShaderRootSigAssociation.pSubobjectToAssociate = &subobjects[(index - 1)];

	D3D12_STATE_SUBOBJECT rayGenShaderRootSigAssociationObject = {};
	rayGenShaderRootSigAssociationObject.Type = D3D12_STATE_SUBOBJECT_TYPE_SUBOBJECT_TO_EXPORTS_ASSOCIATION;
	rayGenShaderRootSigAssociationObject.pDesc = &rayGenShaderRootSigAssociation;

	subobjects[index++] = rayGenShaderRootSigAssociationObject;

	D3D12_STATE_SUBOBJECT globalRootSig;
	globalRootSig.Type = D3D12_STATE_SUBOBJECT_TYPE_GLOBAL_ROOT_SIGNATURE;
	globalRootSig.pDesc = &rtGlobal;

	subobjects[index++] = globalRootSig;

	// Add a state subobject for the ray tracing pipeline config
	D3D12_RAYTRACING_PIPELINE_CONFIG pipelineConfig = {};
	pipelineConfig.MaxTraceRecursionDepth = 8;

	D3D12_STATE_SUBOBJECT pipelineConfigObject = {};
	pipelineConfigObject.Type = D3D12_STATE_SUBOBJECT_TYPE_RAYTRACING_PIPELINE_CONFIG;
	pipelineConfigObject.pDesc = &pipelineConfig;

	subobjects[index++] = pipelineConfigObject;

	// Describe the Ray Tracing Pipeline State Object
	D3D12_STATE_OBJECT_DESC pipelineDesc = {};
	pipelineDesc.Type = D3D12_STATE_OBJECT_TYPE_RAYTRACING_PIPELINE;
	pipelineDesc.NumSubobjects = static_cast<UINT>(subobjects.size());
	pipelineDesc.pSubobjects = subobjects.data();

	// Create the RT Pipeline State Object (RTPSO)
	hr = dxrDevice->CreateStateObject(&pipelineDesc, IID_PPV_ARGS(&dxr.rtpso));
	//Utils::Validate(hr, L"Error: failed to create state object!");
#if NAME_D3D_RESOURCES
	dxr.rtpso->SetName(L"DXR Pipeline State Object");
#endif

	// Get the RTPSO properties
	hr = dxr.rtpso->QueryInterface(IID_PPV_ARGS(&dxr.rtpsoInfo));
	//Utils::Validate(hr, L"Error: failed to get RTPSO info object!");

	/*
	The Shader Table layout is as follows:
		Entry 0 - Ray Generation shader
		Entry 1 - Miss shader
		Entry 2 - Closest Hit shader
	All shader records in the Shader Table must have the same size, so shader record size will be based on the largest required entry.
	The ray generation program requires the largest entry:
		32 bytes - D3D12_SHADER_IDENTIFIER_SIZE_IN_BYTES
	  +  8 bytes - a CBV/SRV/UAV descriptor table pointer (64-bits)
	  = 40 bytes ->> aligns to 64 bytes
	The entry size must be aligned up to D3D12_RAYTRACING_SHADER_BINDING_TABLE_RECORD_BYTE_ALIGNMENT
	*/

	uint32_t shaderIdSize = D3D12_SHADER_IDENTIFIER_SIZE_IN_BYTES;
	uint32_t shaderTableSize = 0;

	dxr.shaderTableRecordSize = shaderIdSize;
	dxr.shaderTableRecordSize += 8;							// CBV/SRV/UAV descriptor table
	dxr.shaderTableRecordSize = ALIGN(D3D12_RAYTRACING_SHADER_RECORD_BYTE_ALIGNMENT, dxr.shaderTableRecordSize);

	shaderTableSize = (dxr.shaderTableRecordSize * 3);		// 3 shader records in the table
	shaderTableSize = ALIGN(D3D12_RAYTRACING_SHADER_TABLE_BYTE_ALIGNMENT, shaderTableSize);

	pDX12Device->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(shaderTableSize),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&dxr.shaderTable));

	// Create the shader table buffer
#if NAME_D3D_RESOURCES
	dxr.shaderTable->SetName(L"DXR Shader Table");
#endif

}

void D3D12WorkingContext::BindDXRTable() {

	uint32_t shaderIdSize = D3D12_SHADER_IDENTIFIER_SIZE_IN_BYTES;
	uint32_t shaderTableSize = 0;

	graphics->dxr.shaderTableRecordSize = shaderIdSize;
	graphics->dxr.shaderTableRecordSize += 8;							// CBV/SRV/UAV descriptor table
	graphics->dxr.shaderTableRecordSize = ALIGN(D3D12_RAYTRACING_SHADER_RECORD_BYTE_ALIGNMENT, graphics->dxr.shaderTableRecordSize);

	shaderTableSize = (graphics->dxr.shaderTableRecordSize * 3);		// 3 shader records in the table
	shaderTableSize = ALIGN(D3D12_RAYTRACING_SHADER_TABLE_BYTE_ALIGNMENT, shaderTableSize);

	// Map the buffer
	uint8_t* pData;
	graphics->dxr.shaderTable->Map(0, nullptr, (void**)&pData);

	// Shader Record 0 - Ray Generation program and local root parameter data (descriptor table with constant buffer and IB/VB pointers)
	memcpy(pData, graphics->dxr.rtpsoInfo->GetShaderIdentifier(L"RayGen_12"), shaderIdSize);

	CD3DX12_GPU_DESCRIPTOR_HANDLE rtxGPUHeapHandle = CD3DX12_GPU_DESCRIPTOR_HANDLE(
		graphics->pRTXDstDescriptorHeap->heap->GetGPUDescriptorHandleForHeapStart(),
		rtxOffsetForDraw,
		graphics->cbvSrvUavDescriptorHandleIncrementSize
	);

	// Set the root parameter data. Point to start of descriptor heap.
	*reinterpret_cast<D3D12_GPU_DESCRIPTOR_HANDLE*>(pData + shaderIdSize) = rtxGPUHeapHandle;

	// Shader Record 1 - Miss program (no local root arguments to set)
	pData += graphics->dxr.shaderTableRecordSize;
	memcpy(pData, graphics->dxr.rtpsoInfo->GetShaderIdentifier(L"Miss_5"), shaderIdSize);

	// Shader Record 2 - Closest Hit program and local root parameter data (descriptor table with constant buffer and IB/VB pointers)
	pData += graphics->dxr.shaderTableRecordSize;
	memcpy(pData, graphics->dxr.rtpsoInfo->GetShaderIdentifier(L"HitGroup"), shaderIdSize);

	// Set the root parameter data. Point to start of descriptor heap.
	*reinterpret_cast<D3D12_GPU_DESCRIPTOR_HANDLE*>(pData + shaderIdSize) = rtxGPUHeapHandle;

	// Unmap
	graphics->dxr.shaderTable->Unmap(0, nullptr);
}

IWorkingContext** D3D12Graphics::CreateWorkingContexts(unsigned int numberOfContexts, int rtxContext) {

	for (int i = 0; i < numberOfContexts; i++) {
		iWorkingContexts.push_back(
			new D3D12WorkingContext(this, pDX12Device)
		);

		iWorkingContexts[i]->Initialize((i==rtxContext) ? rtxContext : -1);
	}

	return iWorkingContexts.data();
}

void D3D12Graphics::SubmitWorkingContextsForExecution(IWorkingContext** workingContexts, unsigned int numberOfContexts) {

	std::vector<ID3D12CommandList*> cmdLists;
	cmdLists.resize(numberOfContexts);

	for (int i = 0; i < numberOfContexts; i++) {
		D3D12WorkingContext* workingContext = static_cast<D3D12WorkingContext *>(workingContexts[i]);
		cmdLists[i] = workingContext->cmdList;
	}

	pDX12CommandQueue->ExecuteCommandLists(numberOfContexts, cmdLists.data());

	for (int i = 0; i < numberOfContexts; i++) {
		D3D12WorkingContext* workingContext = static_cast<D3D12WorkingContext*>(workingContexts[i]);

		workingContext->fenceValue[workingContexts[i]->currentWorkingBuffer]++;
		pDX12CommandQueue->Signal(workingContext->fence[workingContexts[i]->currentWorkingBuffer], workingContext->fenceValue[workingContexts[i]->currentWorkingBuffer]);
	}
}

void D3D12Graphics::FlipBuffers() {

	auto hr = pSwapChain->Present(0, 0);
	auto hr2 = pDX12Device->GetDeviceRemovedReason();

	if (FAILED(hr)) {
		if (FAILED(hr2)) {
			int x = 30;
		}
	}
}