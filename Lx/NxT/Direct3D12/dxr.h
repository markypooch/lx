#pragma once
#include <d3d12.h>
#include <d3dcompiler.h>
#include <dxgi1_4.h>
#include <string>
#include <vector>
#include "dxc/dxcapi.h"
#include "dxc/dxcapi.use.h"

namespace nxt {

	struct D3D12ShaderCompilerInfo
	{
		dxc::DxcDllSupport		DxcDllHelper;
		IDxcCompiler* compiler = nullptr;
		IDxcLibrary* library = nullptr;
	};

	struct D3D12ShaderInfo
	{
		LPCWSTR		filename = nullptr;
		LPCWSTR		entryPoint = nullptr;
		LPCWSTR		targetProfile = nullptr;
		LPCWSTR*    arguments = nullptr;
		DxcDefine*  defines = nullptr;
		UINT32		argCount = 0;
		UINT32		defineCount = 0;

		D3D12ShaderInfo() {}
		D3D12ShaderInfo(LPCWSTR inFilename, LPCWSTR inEntryPoint, LPCWSTR inProfile)
		{
			filename = inFilename;
			entryPoint = inEntryPoint;
			targetProfile = inProfile;
		}
	};


	struct AccelerationStructureBuffer
	{
		ID3D12Resource* pScratch = nullptr;
		ID3D12Resource* pResult = nullptr;
		ID3D12Resource* pInstanceDesc = nullptr;	// only used in top-level AS
	};

	struct RtProgram
	{
		D3D12ShaderInfo			info = {};
		IDxcBlob* blob = nullptr;
		ID3D12RootSignature* pRootSignature = nullptr;

		D3D12_DXIL_LIBRARY_DESC	dxilLibDesc;
		D3D12_EXPORT_DESC		exportDesc;
		D3D12_STATE_SUBOBJECT	subobject;
		std::wstring			exportName;

		RtProgram()
		{
			exportDesc.ExportToRename = nullptr;
		}

		RtProgram(D3D12ShaderInfo shaderInfo)
		{
			info = shaderInfo;
			subobject.Type = D3D12_STATE_SUBOBJECT_TYPE_DXIL_LIBRARY;
			exportName = shaderInfo.entryPoint;
			exportDesc.ExportToRename = nullptr;
			exportDesc.Flags = D3D12_EXPORT_FLAG_NONE;
		}

		void SetBytecode()
		{
			exportDesc.Name = exportName.c_str();

			dxilLibDesc.NumExports = 1;
			dxilLibDesc.pExports = &exportDesc;
			dxilLibDesc.DXILLibrary.BytecodeLength = blob->GetBufferSize();
			dxilLibDesc.DXILLibrary.pShaderBytecode = blob->GetBufferPointer();

			subobject.pDesc = &dxilLibDesc;
		}

	};

	struct HitProgram
	{
		RtProgram ahs;
		RtProgram chs;

		std::wstring exportName;
		D3D12_HIT_GROUP_DESC desc = {};
		D3D12_STATE_SUBOBJECT subobject = {};

		HitProgram() {}
		HitProgram(LPCWSTR name) : exportName(name)
		{
			desc = {};
			desc.HitGroupExport = exportName.c_str();
			subobject.Type = D3D12_STATE_SUBOBJECT_TYPE_HIT_GROUP;
			subobject.pDesc = &desc;
		}

		void SetExports(bool anyHit)
		{
			desc.HitGroupExport = exportName.c_str();
			if (anyHit) desc.AnyHitShaderImport = ahs.exportDesc.Name;
			desc.ClosestHitShaderImport = chs.exportDesc.Name;
		}

	};

	struct DXRGlobal
	{
		AccelerationStructureBuffer                     TLAS;
		std::vector<AccelerationStructureBuffer>        BLAS;
		uint64_t										tlasSize;

		ID3D12Resource* shaderTable = nullptr;
		uint32_t										shaderTableRecordSize = 0;

		RtProgram										rgs;
		RtProgram										miss;
		HitProgram										hit;

		ID3D12StateObject* rtpso = nullptr;
		ID3D12StateObjectProperties* rtpsoInfo = nullptr;
	};

	void Compile_Shader(D3D12ShaderCompilerInfo& compilerInfo, D3D12ShaderInfo& info, IDxcBlob** blob);
	void Compile_Shader(D3D12ShaderCompilerInfo& compilerInfo, RtProgram& program);
	void Init_Shader_Compiler(D3D12ShaderCompilerInfo& shaderCompiler);
	
}