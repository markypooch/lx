#include "D3D12Mesh.h"
using namespace nxt;

struct NDCVertex {
	float position[4];
	float uv[2];
	float normal[4];
	float tangent[4];
	float bitangent[4];
};

struct RTXVertex {
	float position[3];
};

struct NDCTexturedVertex {
	float position[4];
	float uv[2];
};

D3D12Mesh::D3D12Mesh(IWorkingContext* iWorkingContext, IGraphics* iGraphics, MeshType meshType) {
	this->meshType = meshType;
	this->iGraphics = iGraphics;
	this->pDX12Device = static_cast<D3D12WorkingContext*>(iWorkingContext)->pDX12Device;
	this->iWorkingContext = iWorkingContext;
}

void D3D12Mesh::CreateNDCPassQuad(std::string name, std::string vertexShaderPath, std::string pixelShaderPath, nxt::Format format, float sizeX, float sizeY) {
	NDCVertex vertices[6] = {
			{-sizeX,-sizeY, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f},
			{-sizeX, sizeY, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f},
			{ sizeX, sizeY, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f},
			{ sizeX, sizeY, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f},
			{ sizeX,-sizeY, 0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f},
			{-sizeX,-sizeY, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f},
	};
	this->iVertexBuffer = iWorkingContext->CreateVertexBuffer(name, vertices, sizeof(NDCVertex), 6);

	VertexLayout vertexLayout[] = {
		{"POSITION", 0, Format::R32G32B32A32_FLOAT, Classification::PER_VERTEX, 0, 0, 0},
		{"TEXCOORD", 0, Format::R32G32_FLOAT, Classification::PER_VERTEX, 16, 0, 0},
		{"NORMAL", 0, Format::R32G32B32A32_FLOAT, Classification::PER_VERTEX, 24, 0, 0},
		{"TANGENT", 0, Format::R32G32B32A32_FLOAT, Classification::PER_VERTEX, 40, 0, 0},
		{"BITANGENT", 0, Format::R32G32B32A32_FLOAT, Classification::PER_VERTEX, 56, 0, 0}
	};

	this->vertexLayoutObject = iWorkingContext->CreateVertexLayout(name, vertexLayout, 5);

	this->vsShader = iWorkingContext->CreateShader(s2ws(vertexShaderPath).c_str(), ShaderType::VS);
	this->psShader = iWorkingContext->CreateShader(s2ws(pixelShaderPath).c_str(), ShaderType::PS);

	std::vector<IShader*> shaders = { this->vsShader, this->psShader };
	this->iPipeline = iWorkingContext->CreatePipeline(name, shaders, argumentLayoutObject, vertexLayoutObject, 1, format, nxt::Format::UNKNOWN);
}

void D3D12Mesh::Load(const char* pFileName, unsigned int instances, bool genNormals) {
	this->instances = instances;
	if (meshType == MeshType::GLTF) {
		modelData = LoadAI(iWorkingContext, iGraphics, pFileName, 800, 600, genNormals);
	}
}

void D3D12Mesh::SetPipeline(IWorkingContext* context, IPipeline* pipeline) {
	context->SetPipeline(pipeline);
}

void D3D12Mesh::Bind(IWorkingContext* context, IInstanceBuffer* iInstanceBuffer) {
	context->SetVertexBuffer(0, iVertexBuffer, iInstanceBuffer);

	if (iIndexBuffer)
		context->SetIndexBuffer(0, iIndexBuffer);
}

void D3D12Mesh::Draw(IWorkingContext* context, unsigned int instanceCount, unsigned int vertexCount, unsigned int start) {
	context->DrawInstanced(instanceCount, vertexCount, start);
}

void D3D12Mesh::DrawIndexed(IWorkingContext* context, unsigned int startIndex, unsigned int indexCount, unsigned int instances) {
	context->DrawIndexedInstanced(instances, startIndex, indexCount);
}