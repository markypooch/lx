#pragma once
#include "../IFont.h"

namespace nxt {
	class D3D12Font : public IFont
	{
	public:
		D3D12Font(IWorkingContext* iWorkingContext, IGraphics* iGraphics, std::string fontMapAssetPath);
		~D3D12Font();

		virtual void RenderText(IWorkingContext* iWorkContext, Vec2 position, const std::string text);
	};
};