#include "D3D12Font.h"
#include "D3D12ConstantBuffer.h"
#include "D3D12Mesh.h"
using namespace nxt;

D3D12Font::D3D12Font(IWorkingContext* iWorkingContext, IGraphics* iGraphics, std::string fontMapAssetPath) {

	fontCodeCB = new D3D12ConstantBufferHelper<FontCode>(iGraphics, iWorkingContext);
	fontCodeCB->Initialize(1024);

	fontQuad = new D3D12Mesh(iWorkingContext, iGraphics, MeshType::NDC_QUAD);
	fontQuad->CreateNDCPassQuad("FontQuad", "Assets/Shaders/font.vs", "Assets/Shaders/font.ps", Format::R32G32B32A32_FLOAT, 100, 100);

	int x, y;
	int ch = 4;

	std::string fp = "Assets/Textures/FontMap.png";
	void* pData;
	pData = StbTextureLoader::stbi_load_texture(fp.c_str(), x, y, ch);
	fontmap = iWorkingContext->CreateTexture(fp.c_str(), pData, x, y, ch, nxt::Format::RGBA_8, UsageFlags::NONE);

	proj = OrthoLH(3840, 2160, 0.1f, 100.0f);
}

D3D12Font::~D3D12Font() {

}

void D3D12Font::RenderText(IWorkingContext* iWorkContext, Vec2 position, const std::string text) {

	Matrix4x4 world;
	world.SetIdentity();

	float offsetX = 0.0f;
	for (int i = 0; i < text.size(); i++) {

		world.SetTranslation(position.x + offsetX, position.y, 0.0f);

		fontCodeCB->type.asciiFontCode = (int)text[i];
		fontCodeCB->type.wvp = world * proj;

		iWorkContext->BindArgumentLayout();
		iWorkContext->SetBuffer(1, fontCodeCB->Update());
		iWorkContext->SetTexture(0, fontmap, false);

		fontQuad->Bind(iWorkContext);
		fontQuad->SetPipeline(iWorkContext, fontQuad->iPipeline);

		fontQuad->Draw(iWorkContext, 1, 6, 0);
		offsetX += 110.0f;
	}
}