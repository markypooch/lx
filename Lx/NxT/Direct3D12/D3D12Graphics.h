#pragma once
#include "../IGraphics.h"

#include <d3d12.h>
#include <d3dcompiler.h>
#include <dxgi1_4.h>
#include <vector>
#include "dxr.h"

namespace nxt {

#define TABLE_WIDTH 15
#define RTX_TABLE_WIDTH 80

	class DescriptorHeap {
	public:
		ID3D12DescriptorHeap* heap;
		unsigned int numberOfDescriptorsAllocated;
	};

	class D3D12Texture : public ITexture {
	private:
		DXGI_FORMAT format;
		ID3D12Resource* texture[BUFFER_COUNT];
		ID3D12Resource* textureUploadHeap[BUFFER_COUNT*32];
		friend class D3D12WorkingContext;
	};

	class D3D12RenderTarget : public IRenderTarget {
	private:
		D3D12Texture* texture;
		friend class D3D12WorkingContext;
	};

	class D3D12ArgumentLayout : public IArgumentLayoutObject {
	private:
		friend class D3D12WorkingContext;
		ID3D12RootSignature* rootSignature;
	};

	class D3D12VertexBuffer : public IVertexBuffer {
	private:

		friend class D3D12WorkingContext;
		ID3D12Resource* vertexBuffer;
		ID3D12Resource* vertexBufferUpload;

		D3D12_VERTEX_BUFFER_VIEW vbView;
	};

	class D3D12IndexBuffer : public IIndexBuffer {
	private:
		friend class D3D12WorkingContext;

		ID3D12Resource* indexBuffer;
		ID3D12Resource* indexBufferUpload;

		D3D12_INDEX_BUFFER_VIEW ibView;
	};

	class D3D12InstanceBuffer : public IInstanceBuffer {
	private:
		friend class D3D12WorkingContext;

		ID3D12Resource* instanceBuffer;
		ID3D12Resource* instanceBufferUpload;

		D3D12_VERTEX_BUFFER_VIEW instanceBufferView;
	};

	class D3D12Pipeline : public IPipeline {
	private:
		friend class D3D12WorkingContext;
		ID3D12PipelineState* pso;
	};

	class D3D12Shader : public IShader {
	private:
		friend class D3D12WorkingContext;

		D3D12_SHADER_BYTECODE byteCode;
		ID3DBlob* compiledCode;
	};

	class D3D12VertexLayout : public IVertexLayout {
	private:
		friend class D3D12WorkingContext;

		std::vector<D3D12_INPUT_ELEMENT_DESC> inputElement;
		D3D12_INPUT_LAYOUT_DESC inputLayout;
	};

	class D3D12ConstantBuffer : public IConstantBuffer {
	private:
		friend class D3D12WorkingContext;

		ID3D12Resource* pResource;
		Descriptor cbDesriptor;
	public:
		void* pGPUAddress;
	};

	class D3D12SRVBuffer : public ISRVBuffer {
	private:
		friend class D3D12WorkingContext;

		ID3D12Resource* pResource;
		ID3D12Resource* pResourceUpload;
		Descriptor srvDesriptor;
	public:
		void* pGPUAddress;
	};

	class D3D12AccelerationStructure : public IAccelerationStructure {
	private:
		friend class D3D12WorkingContext;


	};
	/*
	class D3D12Blend : public Resource {
	private:
		CD3DX12_BLEND_DESC blend;
	};

	class D3D12Rasterizer : public Resource {
	private:
		CD3DX12_RASTERIZER_DESC rasterizer;
	};*/

	class D3D12Graphics : public IGraphics
	{
	public:
		D3D12Graphics(unsigned int width, unsigned int height, unsigned int RR, HWND pWindowHandle);

		virtual void Initialize();
		virtual IWorkingContext** CreateWorkingContexts(unsigned int numberOfContexts, int rtxContext=-1);
		virtual void SubmitWorkingContextsForExecution(IWorkingContext**, unsigned int numberOfContexts);
		virtual void FlipBuffers();
	private:
		DescriptorHeap* cbvSrvUavHeapNonShaderVisible;
		ResourceUsage backBufferUsageState[3];
		friend class D3D12WorkingContext;

		unsigned int rtvDescriptorHandleIncrementSize;
		unsigned int dsvDescriptorHandleIncrementSize;
		unsigned int cbvSrvUavDescriptorHandleIncrementSize;

		DescriptorHeap* rtvHeapNonShaderVisible;
		DescriptorHeap* dsvDescriptorHeapNonShaderVisible;

		DescriptorHeap* rtvHeapShaderVisible;
		DescriptorHeap* dsvDescriptorHeapShaderVisible;

		std::vector<IWorkingContext*> iWorkingContexts;

		unsigned int width;
		unsigned int height;
		unsigned int rr;

		HWND hWnd;

		ID3D12RootSignature* rootSignature;
		ID3D12Device* pDX12Device;
		ID3D12CommandQueue* pDX12CommandQueue;
		IDXGIFactory1* factory;
		IDXGISwapChain3* pSwapChain;

		ID3D12DescriptorHeap* pRtvDescriptorHeap;
		ID3D12DescriptorHeap* pDsvDescriptorHeap;
		DescriptorHeap* pRTXDescriptorHeap;
		DescriptorHeap* pRTXDstDescriptorHeap;

		ID3D12Resource* pRtvBackBufferTextures[BUFFER_COUNT];

		D3D12_VIEWPORT view;
		D3D12_RECT scissorRect;

		ID3D12Device5* dxrDevice;
		ID3D12StateObject* dxrStateObject;

		ID3D12RootSignature* rtGlobal;
		ID3D12RootSignature* rtLocal;
		DXRGlobal dxr;

		D3D12ShaderCompilerInfo shaderCompiler;
	};

	//DXGI_FORMAT ConvertFormat(Format format);
	//nxt::Format DXGIToNXTFormat(DXGI_FORMAT format);


	static DXGI_FORMAT ConvertFormat(Format format) {
		switch (format) {
		case R32_FLOAT:
			return DXGI_FORMAT_R32_FLOAT;
		case RGBA_8:
			return DXGI_FORMAT_R8G8B8A8_UNORM;
		case R32G32_FLOAT:
			return DXGI_FORMAT_R32G32_FLOAT;
		case R32G32B32A32_FLOAT:
		case RGBA_F32:
			return DXGI_FORMAT_R32G32B32A32_FLOAT;
		case DEPTH_32_FLOAT:
			return DXGI_FORMAT_D32_FLOAT;
		default:
			return DXGI_FORMAT(0);
		}
	}

	static nxt::Format DXGIToNXTFormat(DXGI_FORMAT format) {
		switch (format) {
		case DXGI_FORMAT_R8G8B8A8_UNORM:
			return nxt::Format::RGBA_8;
		case DXGI_FORMAT_R32G32B32A32_FLOAT:
			return nxt::Format::RGBA_F32;
		}
	}


	class D3D12WorkingContext : public IWorkingContext {
	public:
		D3D12WorkingContext(D3D12Graphics* graphics, ID3D12Device* pDX12Device);
	public:
		virtual void                    Initialize(int rtxContext);
		virtual void                    BeginRecording();
		virtual void                    TransisitionTexture(TextureTransisition textureTransisition);
		virtual void                    TransisitionRenderTarget(RenderTargetTransisition renderTargetTransisition);
		virtual void                    Clear(IRenderTarget*, IRenderTarget*, const float* clearColor);
		virtual IRenderTarget*          CreateRenderTarget(std::string renderTargetName, ITexture*);
		virtual IRenderTarget*          CreateDepthBuffer(std::string renderTargetName, ITexture*);
		virtual void                    SetRenderTargets(unsigned int count, std::vector<IRenderTarget*>, IRenderTarget* dsv, bool bindDepthOnly = false);
		virtual IShader*                CreateShader(LPCWSTR filename, ShaderType shaderType);
		virtual void                    SetBuffer(unsigned int bindIndex, IConstantBuffer* iCB);
		virtual void				    SetTexture(unsigned int bindIndex, ITexture* iTexture, bool rtx = false);
		virtual IAccelerationStructure* CreateAccelerationStructure(RTXModelData& rtxModelData);
		virtual void				    SetRTXBuffer(unsigned int bindIndex, IConstantBuffer* iTexture);
		virtual void				    SetRTXTextureArray(unsigned int bindIndex, ITexture** iTextures, unsigned int size, bool rtx = true);
		virtual void				    SetRTXTexture(unsigned int bindIndex, ITexture* iTexture, bool rtx=true);
		virtual ISRVBuffer*             CreateSrvBuffer(std::string name, void* pData, unsigned int stride, unsigned int numberOfElements);
		virtual void                    SetSRVBuffer(ISRVBuffer*, unsigned int bindSlot);
		virtual IPipeline*              CreatePipeline(std::string pipelineName, std::vector<IShader*>, IArgumentLayoutObject*, IVertexLayout* vertexLayout, unsigned int rtvCount, Format rtvFormat, Format dsvFormat);
		virtual IVertexLayout*          CreateVertexLayout(std::string vertexLayoutName, VertexLayout* layout, unsigned int numElements);
		virtual IConstantBuffer*        CreateConstantBuffer(std::string name, size_t size, size_t stride, IConstantBuffer* cb = nullptr, uint32_t offset = 0, bool rtx = false);
		virtual void                    RebuildTLAS(IAccelerationStructure* iAS);
		virtual void				    SetPipeline(IPipeline* pipeline);
		virtual void                    SetBackBufferPresentState(IGraphics* iGraphics);
		virtual void                    SetViewport(unsigned int width, unsigned int height);
		virtual void                    SetIndexBuffer(unsigned int bindIndex, IIndexBuffer*);
		virtual void				    SetVertexBuffer(unsigned int bindIndex, IVertexBuffer*, IInstanceBuffer* iInstanceBuffer = nullptr);
		virtual IArgumentLayoutObject*  CreateArgumentLayout(std::string argumentLayoutName, IArgumentLayout);
		virtual void                    BindArgumentLayout();
		virtual ITexture*               CreateTexture(std::string name, const void* pData, unsigned int width, unsigned int height, unsigned int channels, Format format, UsageFlags flags, unsigned int mip=1, bool srv=true, bool rtx=false, nxt::ResourceUsage resourceUsage = nxt::ResourceUsage::RESOURCE_UNKNOWN);
		virtual ITexture*               CreateTexture(std::string name, const void* pData, unsigned int width, unsigned int height, size_t rowPitch, size_t slicePitch, Format format, UsageFlags flags, unsigned int mip=1, unsigned int arrSize=1);
		virtual ITexture*               CreateUAV(std::string name, const void* pData, unsigned int width, unsigned int height, unsigned int channels, Format format, UsageFlags flags, unsigned int mipCount = 1);
		virtual void                    UpdateTextureMip(ITexture*, void* pData, unsigned int mipIndex, size_t rowSlice, size_t slicePitch, size_t arrIndex, size_t mipCount, size_t arrCount, size_t totalSlice);
		virtual IVertexBuffer*          CreateVertexBuffer(std::string name, void* pData, unsigned int stride, unsigned int numberOfVertices, bool rtx = false);
		virtual IInstanceBuffer*        CreateInstanceBuffer(std::string name, void* pData, unsigned int stride, unsigned int numberOfElements);
		virtual IIndexBuffer*           CreateIndexBuffer(void* pData, unsigned int stride, unsigned int numOfIndices, bool rtx = false);
		virtual void                    DispatchRays(IAccelerationStructure*, ITexture* iTexture);
		virtual void                    StopRecording();
		virtual void                    DrawIndexedInstanced(unsigned int instances, unsigned int startIndex, unsigned int indexCount);
		virtual void                    DrawInstanced(unsigned int instanceCount, unsigned int vertexCount, unsigned int start);
		virtual void                    SetPresent(unsigned int index);
	public:
		unsigned int rtvDescriptorHandleIncrementSize;
		unsigned int dsvDescriptorHandleIncrementSize;
		unsigned int cbvSrvUavDescriptorHandleIncrementSize;

	private:

		void BindDXRTable();

		UINT8* pInstanceMappedData;
		std::vector<D3D12_RAYTRACING_INSTANCE_DESC> instanceDescs;
		DescriptorHeap* cbvSrvUavHeapShaderVisible;
		std::vector<D3D12_SUBRESOURCE_DATA> resources;
		friend class D3D12Graphics;
		friend class D3D12Mesh;

		D3D12_VIEWPORT view;
		D3D12_RECT rect;

		bool synchNeeded = false;

		unsigned int cbvSrvUavOffset = 0;
		unsigned int offsetForDraw = 0;
		unsigned int rtxOffsetForDraw = 0;

		ID3D12Device* pDX12Device;
		D3D12Graphics* graphics;

		ID3D12CommandAllocator* cmdAlloc[BUFFER_COUNT];
		ID3D12GraphicsCommandList* cmdList;

		ID3D12GraphicsCommandList4* dxrCmdList;

		unsigned long long fenceValue[BUFFER_COUNT];
		ID3D12Fence* fence[BUFFER_COUNT];
		HANDLE fenceEvent;

		int rtxContext;
	};
};