#pragma once
#include "d3dx12.h"
#include "D3D12Graphics.h"
#include "../ConstantBufferHelper.h"

namespace nxt {

	template<typename T>
	class D3D12ConstantBufferHelper : public IConstantBufferHelper<T>
	{
	public:
		D3D12ConstantBufferHelper(IGraphics* d3d12Graphics, IWorkingContext* context, bool rtx = false) {
			this->graphics = static_cast<D3D12Graphics*>(d3d12Graphics);
			this->context = context;
			this->rtx = rtx;
			currentBuffer = 0;
		}

	public:
		virtual void Initialize(unsigned int numberOfConstantBuffers) {
			
			for (int i = 0; i < numberOfConstantBuffers; i++) {

				auto iCB = context->CreateConstantBuffer("ffff", sizeof(T), 0, nullptr, 0, rtx);
				D3D12ConstantBuffer* d3d12CB = static_cast<D3D12ConstantBuffer*>(iCB);

				buffers.push_back(d3d12CB);
			}
			this->numberOfConstantBuffers = numberOfConstantBuffers;
		}

		virtual IConstantBuffer* Update(size_t size = 0) {

			IConstantBuffer* iCB;

			size_t optionalSize = 0;
			if (size == 0) {
				optionalSize = sizeof(T);
			}
			else {
				optionalSize = size;
			}

			memcpy(buffers[currentBuffer]->pGPUAddress, &this->type, optionalSize);
			iCB = buffers[currentBuffer];

			currentBuffer++;
			if (currentBuffer >= numberOfConstantBuffers) {
				currentBuffer = 0;
			}

			return iCB;
		}

		D3D12ConstantBuffer* GetLatestBuffer() {
			return buffers[currentBuffer];
		}

	private:

		friend class D3D12WorkingContext;
		unsigned numberOfConstantBuffers;
		unsigned currentBuffer;
		bool rtx;

		std::vector<D3D12ConstantBuffer*> buffers;
		D3D12Graphics* graphics;
		IWorkingContext* context;
	};

};