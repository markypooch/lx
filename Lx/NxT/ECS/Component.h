#pragma once
#include "../IMesh.h"

namespace nxt {

	template<typename T>
	struct ObjectBinding {
		unsigned int bindIndex;
		T object;
	};

	struct ShadowMesh {
		bool shadowEnabled;
		std::vector<ObjectBinding<IConstantBuffer*>> bufferBindings;
	};

	struct RendererComponent {
		IMesh* mesh;
		std::vector<ObjectBinding<IConstantBuffer*>> bufferBindings;
		std::vector<ObjectBinding<ITexture*>> textureBindings;
		unsigned int instanceCount = 1;
		IInstanceBuffer* iInstantBuffer;
		ShadowMesh shadowMesh;
		bool visible = true;
	};
};
