#pragma once
#include "NxT/Matrix.h"
#include "Vector.h"
#include "NxT/IMesh.h"

namespace sweeper {

	enum PieceType {
		ELBOW_LEFT = 0,
		ELBOW_RIGHT = 1,
		NOODLE = 2,
		ZIG_LEFT = 3,
		ZIG_RIGHT = 4,
		SQUARE = 5,
		TEE = 6,
		NO_PIECE = 7
	};

	class RTXetrisPiece
	{
	public:
		RTXetrisPiece(float posX, float posY, float posZ, nxt::IMesh* rtxetrisPiece, uint8_t pieceType);

		float getX() const { return posX; }
		float getY() const { return posY; }
		float getZ() const { return posZ; }

		void  SetX(float posX) {

		}

		void  SetY(float posX) {

		}

		void  SetZ(float posZ) {

		}
		std::vector<uint32_t> instanceIndices;
		std::map<uint32_t, std::vector<uint32_t>> instanceIndicesMap;

		bool PlacePiece(uint8_t* stageMap);
		bool MoveLeft(uint8_t* stageMap);
		bool MoveRight(uint8_t* stageMap);
		bool MoveDown(uint8_t* stageMap);
		bool RotateLeft(uint8_t* stageMap);
		bool RotateRight(uint8_t* stageMap);

		const nxt::IMesh* getIMesh()     const { return this->rtxetrisPiece; }
		const PieceType   getPieceType() const { return static_cast<PieceType>(this->pieceType); }
		void  CheckInLine(uint8_t* line, uint8_t lineCount);
		bool onStage = false;

		void BuildInstanceMap();

	private:
		void ClearPiecePositionOnMap(uint8_t* stageMap);
		void RestorePiecePositionOnMap(uint8_t* stageMap);

		bool MovePiece(nxt::Vec2& direction, uint8_t* stageMap);
		void UpdateCoordinatesAndMapIndices(uint8_t* stageMap, const std::vector<nxt::Vec2>& tempCoordinates, const std::vector<nxt::Vec2>& tempMapIndices);
		bool RotatePiece(const nxt::Matrix2x2& a, uint8_t* stageMap);

		std::map<int, std::vector<bool>> subsetBoolMask;
		std::vector<nxt::Vec2> coordinates;
		std::vector<nxt::Vec2> renderOrder;
		std::vector<nxt::Vec2> mapIndices;
		std::vector<bool> componentsOnMap;

		float startPosX, startPosY, startPosZ;
		float posX, posY, posZ;
		float currentRotation;
		float currentRasterRotation;
		int   pieceComponentsLineified;

		uint8_t mapIndexX, mapIndexY;
		nxt::Matrix4x4 world;
		nxt::Matrix4x4 rasterWorld;
		nxt::Matrix4x4 rotY, rasterRotY;
		uint8_t pieceType;
	public:
		bool active = false;
		nxt::IMesh* rtxetrisPiece;
		const nxt::Matrix4x4& GetRTXWorldMatrix() const { return world; }
		nxt::Matrix4x4& GetWorldMatrix() { return rasterWorld; }

		std::vector<bool> GetComponentsOnMap() const {

			std::vector<bool> bools;

			for (auto& mapItem : subsetBoolMask) {
				bools.insert(bools.end(), mapItem.second.begin(), mapItem.second.end());
			}

			return bools;
		}
	};
};
