#pragma once
#include <string>

namespace nxt {
	class StbTextureLoader {
	public:
		static void* stbi_load_texture(std::string fileName, int& x, int& y, int& channels);
	};
};