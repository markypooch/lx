#pragma once
#include "IStage.h"
#include "NxT/ConstantBufferHelper.h"
#include "NxT/Matrix.h"
#include "RTXetrisPiece.h"
#include "SweeperCube.h"
#include "Input.h"
#include "NxT/IMesh.h"

namespace sweeper {

	struct wvp {
		nxt::Matrix4x4 world;
		nxt::Matrix4x4 worldRotate;
		nxt::Matrix4x4 viewProj;
		nxt::Matrix4x4 wvp;
	};

	struct Transforms {
		float position[4];
	};

	struct Crystals {
		float position[4];
		float color[4];
	};

	struct View {
		float cameraPosition[4];
	};

	struct PBRAppSettings {
		float modulateEmission;
		float iblCubeMapRotationY;
		float maxReflectanceLOD;
		float ambient;
		float specularContribution;
		int applyOcclusion;
		int pad[2];
	};

	struct BorderColorModulation {
		float borderColor[4];
		int sine;
		float time;
		float pad[2];
	};

	struct nxt::RTXModelData;

	class RTXetrisStage : public IStage
	{
	public:
		RTXetrisStage(nxt::IWorldRenderer* iWorldRenderer)
			: IStage(iWorldRenderer) {}

		virtual void Initialize();
		virtual void Update(float, int mouseButtons);
		virtual void Render();
		
	private:
		void GenerateStage();
		void CheckInput(int mouseButtons);
	private:
		void LoadModels(std::vector<RTXetrisPiece*>& pieceVec, PieceType pieceType, std::string modelPath);
		void BuildRTXData(std::vector<unsigned int>& vertexSizes, std::vector<unsigned int>& rtxIndiceSizes, std::vector<nxt::IIndexBuffer*>& rtxIndexBuffers, std::vector<nxt::IVertexBuffer*>& rtxVertexBuffers, std::vector<RTXetrisPiece*>& pieceVec);
		RTXetrisPiece* currentPiece;

		float pieceMoveDownDelta;
		float pieceDisableTimer;
		bool  pieceDisableTimerStart;

		static const uint8_t STAGE_SIZE = 126;
		uint8_t stage[STAGE_SIZE] = { 0 };

		//std::vector<nxt::SweeperCube> cubes;
		float viewPosZ;

		nxt::RTXModelData* rtxModelData;
		nxt::IAccelerationStructure* iAS;

		std::vector<int> piecesAtPlay;

		std::vector<RTXetrisPiece*> elbow_left;
		std::vector<RTXetrisPiece*> elbow_right;
		std::vector<RTXetrisPiece*> noodle;
		std::vector<RTXetrisPiece*> square;
		std::vector<RTXetrisPiece*> tee;
		std::vector<RTXetrisPiece*> zig_left;
		std::vector<RTXetrisPiece*> zig_right;

		POINT lastCursorPos{};

		float worldRotX, worldRotY, worldRotZ;
		nxt::Matrix4x4 worldX, worldY, worldZ;
		nxt::Matrix4x4 worldRot;

		float angleRot;
		bool rotateCubelets, pick, hover, setCursorOrigin = false;

		nxt::Matrix4x4 view, proj;
		nxt::Matrix4x4 rot, trans;

		nxt::Matrix4x4 lightView, lightProj;
		nxt::Matrix4x4 lightViewRotY, lightViewRotZ;
		nxt::Matrix4x4 lightViewProj;

		nxt::Matrix4x4 world;
		nxt::Matrix4x4 viewProj;
		//nxt::Matrix4x4 lightViewProj;

		std::vector<nxt::Vec3> colorValues;
		uint8_t currentColorIndex;
		float colorTransisitionDT;

		std::vector<nxt::Vec3> colorValuesSineWave1;
		uint8_t currentColorIndexSW1;
		float colorTransisitionDTSW1;

		std::vector<nxt::Vec3> colorValuesSineWave2;
		uint8_t currentColorIndexSW2;
		float colorTransisitionDTSW2;


		nxt::Matrix4x4 m, m2, m3, m4;

		nxt::IMesh* rtxetrisStage;
		nxt::IMesh* rtxetrisReflectPlane;
		nxt::IMesh* rtxRtxetrisStage;
		nxt::IMesh* rtxBorder, *rtxPieceBorder, *sinModel;

		nxt::IMesh* pieceUIQuad;

		nxt::IConstantBufferHelper<wvp>* iConstantBufferHelper;
		nxt::IConstantBufferHelper<View>* iConstantBufferHelperView;
		nxt::IConstantBufferHelper<PBRAppSettings>* iConstantBufferHelperPBR;
		nxt::IConstantBufferHelper<BorderColorModulation>* iConstantBufferHelperBorderColor;

		nxt::ITexture* ibl_diffuse, * ibl_specular, * brdf_lut;
		std::vector<nxt::SubResource> ibl_diffuse_subresource;
		std::vector<nxt::SubResource> ibl_specular_subresource;
		
		std::vector<PieceType> pieceRotation;
		int pieceRotationCount = 0;
		bool firstRotation = true;

		//RTX data
		std::vector<nxt::Bitangent> bitang;
		std::vector<nxt::Tangent> tang;
		std::vector<unsigned int> offsets;
		std::vector<nxt::UV> uv;
		std::vector<nxt::Normal> normal;
		std::vector<unsigned int> materialIndice;
		std::vector<unsigned int> indice;
		std::vector<nxt::Positions> pos;

		nxt::ISRVBuffer* positions, * indices, * uvs, * materialIndices, * normals, * instanceOffsets, * bitangs, * tangs;
		nxt::IConstantBufferHelper<nxt::MetallicRoughness[128]>* cbMaterialRTX;
		std::vector<nxt::ITexture*> textures;

		//REMOVE LATER 
		float lightViewZ = 12.0f;
		float lightOrthoX = 30.0f;
		float lightOrthoY = 30.0f;
		float lightOrthoZ = 30.0f;

		float dt;
	};
};