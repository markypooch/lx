#define M_PI_F ((float)3.141592653589793)
#define M_1_PI_F ((float)0.318309886183790)

ByteAddressBuffer materialIndices : register(t3);

ByteAddressBuffer positions : register(t4);
ByteAddressBuffer indices : register(t5);
ByteAddressBuffer normals : register(t6);
ByteAddressBuffer instanceOffsets : register(t7);
ByteAddressBuffer uvs : register(t8);

Texture2D<float4> colorMap : register(t9);
Texture2D<float4> metallicRoughness : register(t10);

RWTexture2D< float4 > gOutput : register(u0);
RWTexture2D< float4 > gNormalsOutput : register(u1);
RaytracingAccelerationStructure gBVHScene : register(t2);

SamplerState LinearSampler : register(s0);

struct MetallicRoughness {
    float4 baseColor;
    float4 emissiveColor;
    float matMetallic;
    float matRoughness;
    float matOcclusion;
    int hasBaseColorTexture;
    int hasNormalTexture;
    int hasEmissiveTexture;
    int hasMetallicRoughnessTexture;
    int hasOcclusionTexture;
};

cbuffer Material : register(b1) {
    MetallicRoughness materials[8];
}

cbuffer Settings : register(b0) {
    float4x4 viewToWorld;
    float4x4 view;
    float2 zPlaneSize;
    float projNear;
    float randomSeed;
    float4 L;
    uint frameIndex;
    uint pad[3];
}

struct ShadowRayPayload
{
	bool miss;
};

struct HitInfo
{
  float3 radiance;
  uint recursionDepth;
  float4 direction;
  float4 normals;
  uint miss;
  float3 pad;
};

struct Attributes
{
  float2 bary;
};


// Uses the inversion method to map two uniformly random numbers to a three dimensional
// unit hemisphere where the probability of a given sample is proportional to the cosine
// of the angle between the sample direction and the "up" direction (0, 1, 0)
float3 sampleCosineWeightedHemisphere(float2 u) {
	float phi = 2.0f * M_PI_F * u.x;

	float sin_phi, cos_phi;
	sincos(phi, sin_phi, cos_phi);

	float cos_theta = sqrt(u.y);
	float sin_theta = sqrt(1.0f - cos_theta * cos_theta);

	return float3(sin_theta * cos_phi, cos_theta, sin_theta * sin_phi);
}

// Aligns a direction on the unit hemisphere such that the hemisphere's "up" direction
// (0, 1, 0) maps to the given surface normal direction
float3 alignHemisphereWithNormal(float3 sample, float3 normal) {
	// Set the "up" vector to the normal
	float3 up = normal;

	// Find an arbitrary direction perpendicular to the normal. This will become the
	// "right" vector.
	float3 right = normalize(cross(normal, float3(0.0072f, 1.0f, 0.0034f)));

	// Find a third vector perpendicular to the previous two. This will be the
	// "forward" vector.
	float3 forward = cross(right, up);

	// Map the direction on the unit hemisphere to the coordinate system aligned
	// with the normal.
	return sample.x * right + sample.y * up + sample.z * forward;
}

//====
// https://github.com/playdeadgames/temporal/blob/master/Assets/Shaders/IncNoise.cginc
// The MIT License (MIT)
//
// Copyright (c) [2015] [Playdead]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

//note: normalized random, float=[0, 1]
float PDnrand( float2 n ) {
	return frac( sin(dot(n.xy, float2(12.9898, 78.233f)))* 43758.5453 );
}
float2 PDnrand2( float2 n ) {
	return frac( sin(dot(n.xy, float2(12.9898, 78.233f)))* float2(43758.5453, 28001.8384) );
}
float3 PDnrand3( float2 n ) {
	return frac( sin(dot(n.xy, float2(12.9898, 78.233f)))* float3(43758.5453, 28001.8384, 50849.4141 ) );
}
float4 PDnrand4( float2 n ) {
	return frac( sin(dot(n.xy, float2(12.9898, 78.233f)))* float4(43758.5453, 28001.8384, 50849.4141, 12996.89) );
}

// Convert uniform distribution into triangle-shaped distribution.
// https://www.shadertoy.com/view/4t2SDh
// Input is in range [0, 1]
// Output is in range [-1, 1], which is useful for dithering.
float2 uniformNoiseToTriangular(float2 n) {
	float2 orig = n*2.0-1.0;
	n = orig*rsqrt(abs(orig));
	n = max(-1.0,n);
	n = n-float2(sign(orig));
	return n;
}

float3 irefract(float3 incidentVec, float3 normal, float eta)
{
  float N_dot_I = dot(normal, incidentVec);
  float k = 1.f - eta * eta * (1.f - N_dot_I * N_dot_I);
  if (k < 0.f)
    return float3(0.f, 0.f, 0.f);
  else
    return eta * incidentVec - (eta * N_dot_I + sqrt(k)) * normal;
}


float DistributionGGX(float NdotH, float roughness) {
    float a = roughness*roughness;
    float a2 = a*a;

    float NdotH2 = NdotH*NdotH;

    float denominator = (NdotH2 * (a2 - 1.0f) + 1.0f);
    denominator = (3.14) * denominator * denominator;

    return a2/denominator;
}

float GeometrySchlickGGX(float NdotV, float roughness) {
    float r = (roughness + 1.0f);
    float k = (r*r) / 8.0f;

    float denominator = NdotV * (1.0f - k) + k;

    return NdotV / denominator;
}

float GeometrySmith(float NdotL, float NdotV, float roughness) {
    float ggx2  = GeometrySchlickGGX(NdotV, roughness);
    float ggx1  = GeometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}

float3 fresnelSchlick(float cosTheta, float3 F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

float4 CookTorrance_Schlick(float4 L, float4 V, float4 N, float3 color, float specularIntensity, float metallic, float roughness, float3 radiance) {
    float4 H;
    H = normalize((L+V));

    float NdotV = saturate(max(dot(N, V), 0.0f));
    float NdotL = saturate(max(dot(N, L), 0.0f));
    float HdotV = saturate(max(dot(H, V), 0.0f));
    float NdotH = saturate(max(dot(N, H), 0.0f));

    float NDF   = DistributionGGX(NdotH, roughness);
    float  G    = GeometrySmith(NdotL, NdotV, roughness);

    float3 F0;
    F0 = lerp(0.0, color, metallic);

    float3 F    = fresnelSchlick(max(dot(H, V), 0.0), F0);

    float3 kS           = F;
    //float4 kD = lerp(float4(1, 1, 1, 1) - F, float4(0, 0, 0, 0), metallic);
    float3 kD           = (float3(1, 1, 1) - kS) * (1.0 - metallic);
    float3 diffuseBRDF  = kD * color / 3.14159265359;

    float3 specularBRDF = (NDF*G*F)/specularIntensity*max(NdotV, 0.0) * max(NdotL, 0.0);

    // add to outgoing radiance Lo
    float3 Lo = (diffuseBRDF+(specularBRDF)) * radiance * NdotL;

    float4 outputColor;
    outputColor = float4(Lo, 1.0f);

    return outputColor;
}

float luminance(float3 v) {
    return dot(v, float3(0.2126f, 0.7152f, 0.0722f));
}

float3 change_luminance(float3 v_in, float l_out) {
    float l_in = luminance(v_in);
    return v_in * (l_out / l_in);
}

float3 extended_reinhard(float3 v_in, float max_white_l) {
    float l_old = luminance(v_in);

    float numerator = l_old * (1.0f + (l_old / (max_white_l * max_white_l)));
    float l_new     = numerator / (1.0f + l_old);

    return change_luminance(v_in, l_new);
}

[shader("closesthit")]
void ClosestHit(inout HitInfo payload : SV_RayPayload, Attributes attribs : SV_IntersectionAttributes)
{
  

  return float4(1.0f, 1.0f, 1.0f, 1.0f);
  /*
  const uint maxRecursionDepth = 8;

  if (payload.recursionDepth > maxRecursionDepth)
  {
    	return;
  }

  float3 uvw;
  uvw.yz = attribs.bary;
  uvw.x = 1.0f - uvw.y - uvw.z;

  uint2 pixelPos = DispatchRaysIndex().xy;
  float2 pixelUV = (float2(pixelPos) + 0.5) / float2(DispatchRaysDimensions().xy);

  uint triangleIndex = PrimitiveIndex() + instanceOffsets.Load(InstanceID() << 2);
  float4 baseColor = float4(0.0f, 0.0f, 0.0f, 0.0f);
  float baseMet   = 0.0f;
  float baseRough = 0.0f;

  uint4 i012 = indices.Load4((3 * triangleIndex) << 2);
  uint i0 = i012[0];
  uint i1 = i012[1];
  uint i2 = i012[2];

  float4 position012[3] = {
	asfloat(positions.Load4((i0 * 3) << 2)),
	asfloat(positions.Load4((i1 * 3) << 2)),
	asfloat(positions.Load4((i2 * 3) << 2))
  };

  float4 normal012[3] = {
	asfloat(normals.Load4((i0 * 3) << 2)),
	asfloat(normals.Load4((i1 * 3) << 2)),
	asfloat(normals.Load4((i2 * 3) << 2))
  };
  float2 uv012[3] = {
		asfloat(uvs.Load2((i0 * 1) << 3)),
		asfloat(uvs.Load2((i1 * 1) << 3)),
		asfloat(uvs.Load2((i2 * 1) << 3))
  };

  float2 uv = uvw.x * uv012[0] + uvw.y * uv012[1] + uvw.z * uv012[2];

  float3 position = uvw.x * position012[0].xyz + uvw.y * position012[1].xyz + uvw.z * position012[2].xyz;
  float3 normal = normalize(uvw.x * normal012[0].xyz + uvw.y * normal012[1].xyz + uvw.z * normal012[2].xyz);
  position = mul((float3x3)ObjectToWorld3x4(), position);
  normal   = mul((float3x3)ObjectToWorld3x4(), normal);

  if (payload.recursionDepth == 0) {
    payload.normals = float4(normalize(mul((float3x3)view, normal)), 0.0f);
  }

    float roughness = 0.0f;
    float occlusion = 0.0f;
    float4 emission  = float4(0.0f, 0.0f, 0.0f, 0.0f);


    if (materials[materialIndices.Load(triangleIndex << 2)].hasBaseColorTexture) {
         baseColor = colorMap.SampleLevel(LinearSampler, uv, 0);
    }
    else {
         baseColor = materials[materialIndices.Load(triangleIndex << 2)].baseColor;
    }

    if (materials[materialIndices.Load(triangleIndex << 2)].hasMetallicRoughnessTexture) {
         float2 metallicRough = metallicRoughness.SampleLevel(LinearSampler, uv, 0).rg;
         baseMet = metallicRough.x;
         baseRough = metallicRough.y;
    }
    else {
         baseMet  = materials[materialIndices.Load(triangleIndex << 2)].matMetallic;
         baseRough = materials[materialIndices.Load(triangleIndex << 2)].matRoughness;
    }




    float4 cameraPosition = float4(0.0f, 3.0f, -8.0f, 0.0f);
    float4 V = normalize(float4(position.xyz, 1.0f) - float4(cameraPosition.xyz, 1.0f));

  //float4 L = float4(-5.0f, 0.0f, 0.0f, 1.0f);

  float4 toL2 = normalize(float4(position.xyz, 1.0f) - float4(float3(0.0f, -3.0f, 0.0f), 1.0f));
  float d2 = length(toL2);

  float4 toL = normalize(float4(position.xyz, 1.0f) - float4(L.xyz, 1.0f));

  float d = length(toL);
  float att = d*(1.0f*1.0f);
  float att2 = d2*(1.0f*1.0f);

  //float3 lightDir = float3(0.4f, 1.0f, 0.0f);
  //float3 viewDir = float3(0.0f, 0.0f, -1.0f);

  float3 rayOrigin = position + normal * 0.01;

  //payload.radiance += dot(lightDir, normal) * baseColor;

  if (baseMet > 0.0f) {


     // if (payload.recursionDepth == 0) {
      //  payload.radiance += (2.5f * saturate(dot(L.xyz, normal)) * baseColor.xyz * 0.31830f);
     // }
     // else {
     //   payload.radiance += (1.35f * saturate(dot(L.xyz, normal)) * baseColor.xyz * 0.31830f);
    //  }


      payload.radiance += (baseColor.xyz * 0.4);


      float3 refRayOrigin;
      refRayOrigin = position + payload.direction.xyz * 0.001;
      float idx = 1.125f;
      float3 n = normal;
      if (dot(normalize(payload.direction.xyz), normal.xyz) < 0.0f) {
          idx = 1.25f;
      }
      else {
          idx = 1.0f/1.25f;
          n = -normal;
      }

       double cos_theta = min(dot(normalize(-payload.direction.xyz), n.xyz), 1.0);
       double sin_theta = sqrt(1.0 - cos_theta*cos_theta);

       bool cannot_refract = idx * sin_theta > 1.0;
       //float3 direction;

       if (cannot_refract) {
            //direction = reflect(unit_direction, rec.normal);
            RayDesc reflectionRay;
              reflectionRay.Origin = rayOrigin;
              reflectionRay.Direction = reflect(payload.direction.xyz, normal);
              reflectionRay.TMin = 0.0;
              reflectionRay.TMax = 1000;

              HitInfo rindirectRayPayload;
              rindirectRayPayload.recursionDepth = payload.recursionDepth + 1;
              rindirectRayPayload.radiance = 0.0;
              rindirectRayPayload.normals = payload.normals;
              rindirectRayPayload.direction = float4(payload.direction.xyz, 0.0f);
              TraceRay(gBVHScene, 0, 0xFF, 0, 0, 0, reflectionRay, rindirectRayPayload);

              payload.radiance += (rindirectRayPayload.radiance*baseRough)*0.3f;
       }
       else {
            RayDesc refractiveRay;
          refractiveRay.Origin = refRayOrigin;
          refractiveRay.Direction = refract(normalize(payload.direction.xyz), normalize(n.xyz), idx);
          refractiveRay.TMin = 0.0;
          refractiveRay.TMax = 1000;

          HitInfo tindirectRayPayload;
          tindirectRayPayload.recursionDepth = payload.recursionDepth + 1;
          tindirectRayPayload.radiance = 0.0f;
          tindirectRayPayload.direction = float4(payload.direction.xyz, 0.0f);
          TraceRay(gBVHScene, 0, 0xFF, 0, 0, 0, refractiveRay, tindirectRayPayload);

          payload.radiance += (tindirectRayPayload.radiance*baseRough)*0.8f;
       }
  }

  payload.radiance += CookTorrance_Schlick(toL/d, -V, float4(normal.xyz, 0.0f), baseColor.xyz, 9.0f, baseMet, baseRough, float3(20.5f, 20.5f, 20.5f)/att).xyz;
  payload.radiance += (baseColor.xyz * 0.4);
  payload.radiance = float4(extended_reinhard(payload.radiance.xyz, 6.5f), 1.0f).xyz;*/

  //payload.radiance += CookTorrance_Schlick(toL2/d2, -V, float4(normal.xyz, 0.0f), baseColor.xyz, 7.0f, baseMet, baseRough, float3(2.5f, 8.5f, 6.5f)/att2).xyz;
  //payload.radiance += (baseColor.xyz * 0.4);
  //payload.radiance = float4(extended_reinhard(payload.radiance.xyz, 6.5f), 1.0f).xyz;

  /*
  float2 sample = uniformNoiseToTriangular(PDnrand2(pixelUV + randomSeed + payload.recursionDepth)) * 0.5 + 0.5;
  float3 sampleDirLocal = sampleCosineWeightedHemisphere(sample);
  float3 sampleDir = alignHemisphereWithNormal(sampleDirLocal, normal);

  if (payload.recursionDepth < maxRecursionDepth)
	{
		RayDesc indirectRay;
		indirectRay.Origin = rayOrigin;
		indirectRay.Direction = sampleDir;
		indirectRay.TMin = 0.0;
		indirectRay.TMax = 1000;

		HitInfo indirectRayPayload;
		indirectRayPayload.recursionDepth = payload.recursionDepth + 1;
		indirectRayPayload.radiance = 0.0;
		indirectRayPayload.direction = float4(sampleDir, 0.0f);
		indirectRayPayload.normals = payload.normals;
		TraceRay(gBVHScene, 0, 0xFF, 0, 0, 0, indirectRay, indirectRayPayload);

		payload.radiance += indirectRayPayload.radiance * baseColor;
	}*/
}
