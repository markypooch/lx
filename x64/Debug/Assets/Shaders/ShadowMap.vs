cbuffer wvp : register(b0) {
    matrix world;
    matrix worldRotate;
    matrix vp;
    matrix wvp;
}

struct GLTFModelVertex
{
	float4 Position : POSITION;
	float2 TexCoord : TEXCOORD;
	float3 Normal : NORMAL;
	float4 Tangent : TANGENT;
	float4 Bitangent : BITANGENT;
};

struct VertexOut
{
	float4 pos : SV_POSITION;
};


VertexOut main(GLTFModelVertex vert)
{
	VertexOut oVert = (VertexOut)0;
        matrix lvp = mul(worldRotate, world);
        oVert.pos         = mul(lvp, float4(vert.Position.xyz, 1.0f));
        return oVert;
}
