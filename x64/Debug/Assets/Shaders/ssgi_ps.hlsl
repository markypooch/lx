Texture2D diffuseMap : register(t0);
Texture2D depthBuffer : register(t1);

SamplerState LinearSampler : register(s0);
struct VertexOut {
    float4 pos : SV_POSITION;
    float2 tex : TEXCOORD;
};

float3 normal_from_depth(float2 texcoords) {
    // Delta coordinate of 1 pixel: 0.03125 = 1 (pixel) / 32 (pixels)
    const float2 offset1 = float2(0.0, 0.03125);
    const float2 offset2 = float2(0.03125, 0.0);
    
    // Fetch depth from depth buffer
    float depth  = depthBuffer.Sample(LinearSampler, texcoords).r;
    float depth1 = depthBuffer.Sample(LinearSampler, texcoords + offset1).r;
    float depth2 = depthBuffer.Sample(LinearSampler, texcoords + offset2).r;
    
    float3 p1 = float3(offset1.x, offset1.y, depth1 - depth);
    float3 p2 = float3(offset2.x, offset2.y, depth2 - depth);
    
    // Calculate normal
    float3 normal = cross(p1, p2);
    //normal.z = -normal.z;
    
    return normalize(normal);
}

float3 normal_from_pixels(float2 texcoords1, float2 texcoords2) {
    // Fetch depth from depth buffer
    float depth1 = depthBuffer.Sample(LinearSampler, texcoords1).r;
    float depth2 = depthBuffer.Sample(LinearSampler, texcoords2).r;
    
    // Calculate normal
    float3 normal = float3(texcoords2.x - texcoords1.x, texcoords2.y - texcoords1.y, depth2 - depth1);
    //normal.z = -normal.z;
    
    // Calculate distance between texcoords
    //dist = length(normal);
    
    return normal;
}

float3 Calculate_GI(float3 pixel_normal, float2 coord, float2 vScreenPos)
{
    float3 light_color;
    float3 pixel_to_light_normal;
    float3 light_normal, light_to_pixel_normal;
    float dist;
    float3 gi = float3(0.0f, 0.0f, 0.0f);
    
    // Calculate normal from the pixel to current pixel
    light_to_pixel_normal = normal_from_pixels(coord, vScreenPos);

    dist = length(light_to_pixel_normal);
    light_to_pixel_normal = normalize(light_to_pixel_normal);


    // Calculate normal from current pixel to the pixel
    pixel_to_light_normal = -light_to_pixel_normal;
    
    // Get the pixel color
    light_color = diffuseMap.Sample(LinearSampler, coord).rgb;
    // Calculate normal for the pixel
    light_normal = normal_from_depth(coord);
    // Calculate GI
    gi += light_color * max(0.0, dot(light_normal, light_to_pixel_normal)) * max(0.0, dot(pixel_normal, pixel_to_light_normal)) / dist;
    
/*
    // Calculate normal from the cull pixel to current pixel
    light_to_pixel_normal = normal_from_pixels(sNormalMap, coord, vScreenPos, dist);
    // Calculate normal from current pixel to the cull pixel
    pixel_to_light_normal = -light_to_pixel_normal;
    
    // Get the cull pixel color, base color need to be lighten to simulate direct light effect.
    light_color = texture2D(sEnvMap, coord).rgb * 5.0;
    // Calculate normal for the cull pixel
    light_normal = normal_from_depth(sNormalMap, coord);
    // Flip the normal
    light_normal = -light_normal;
    
    // Calculate GI
    gi += light_color * max(0.0, dot(light_normal, light_to_pixel_normal)) * max(0.0, dot(pixel_normal, pixel_to_light_normal)) / dist;*/
    
    return gi;
}

float4 main(VertexOut vOut) : SV_TARGET
{
    const int GRID_COUNT = 16;
    float3 pixel_normal;
    float3 gi;
    
    float2 vScreenPos = float2(vOut.pos.x / 3840.0f, vOut.pos.y / 2160.0f);

    // Calculate normal for current pixel
    pixel_normal = normal_from_depth(vScreenPos);
    // Prepare to accumulate GI
    gi = float3(0.0f, 0.0f, 0.0f);
    
    // Accumulate GI from some uniform samples
    for (int y = 0; y < GRID_COUNT; ++y) {
        for (int x = 0; x < GRID_COUNT; ++x) {
            float2 coord = vOut.tex + float2((float(x) + 0.5) / float(GRID_COUNT), (float(y) + 0.5) / float(GRID_COUNT));
            gi += Calculate_GI(pixel_normal, coord, vScreenPos);
        }
    }
    
    // Make GI not too strong
    gi /= float(GRID_COUNT * GRID_COUNT / 3);
    
    return float4(gi, 1.0);
}