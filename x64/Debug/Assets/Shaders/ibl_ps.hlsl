Texture2D diffuseMap : register(t0);
Texture2D normalMap : register(t1);
Texture2D specularMap : register(t2);
Texture2D reflectionMap : register(t3);
Texture2D causticMap : register(t4);
TextureCube skyMap : register(t5);
TextureCube specularSkyMap : register(t6);
Texture2D brdflut : register(t7);

SamplerState LinearSampler : register(s0);

float3x3 calculateTBN(float4 pos, float2 tex, float4 norm) {
     // get edge vectors of the pixel triangle
	float3 dp1 = ddx(float3(pos.xyz));
	float3 dp2 = ddy(float3(pos.xyz));
	float2 duv1 = ddx(tex);
	float2 duv2 = ddy(tex);

	// solve the linear system
	float3 dp2perp = cross(dp2, float3(norm.xyz));
	float3 dp1perp = cross(float3(norm.xyz), dp1);
	float3 T = dp2perp * duv1.x + dp1perp * duv2.x;
	float3 B = dp2perp * duv1.y + dp1perp * duv2.y;
        
        float invmax = rsqrt(max(dot(T, T), dot(B, B)));
	return transpose(float3x3(T * invmax, B * invmax, float3(norm.xyz)));
}

float DistributionGGX(float NdotH, float roughness) {
    float a = roughness*roughness;
    float a2 = a*a;
    
    float NdotH2 = NdotH*NdotH;

    float denominator = (NdotH2 * (a2 - 1.0f) + 1.0f);
    denominator = (3.14) * denominator * denominator;

    return a2/denominator;
}

float GeometrySchlickGGX(float NdotV, float roughness) {
    float r = (roughness + 1.0f);
    float k = (r*r) / 8.0f;
    
    float denominator = NdotV * (1.0f - k) + k;

    return NdotV / denominator;
}

float GeometrySmith(float NdotL, float NdotV, float roughness) {
    float ggx2  = GeometrySchlickGGX(NdotV, roughness);
    float ggx1  = GeometrySchlickGGX(NdotL, roughness);
	
    return ggx1 * ggx2;
}

float luminance(float3 v) {
    return dot(v, float3(0.2126f, 0.7152f, 0.0722f));
}

float3 change_luminance(float3 v_in, float l_out) {
    float l_in = luminance(v_in);
    return v_in * (l_out / l_in);
}

float3 extended_reinhard(float3 v_in, float max_white_l) {
    float l_old = luminance(v_in);

    float numerator = l_old * (1.0f + (l_old / (max_white_l * max_white_l)));
    float l_new     = numerator / (1.0f + l_old);
    
    return change_luminance(v_in, l_new);
}

float3 fresnelSchlick(float cosTheta, float3 F0, float roughness)
{
    return F0 + (max(float3(1.0, 1.0, 1.0)-roughness, F0) - F0) * pow(max(1.0 - cosTheta, 0.0), 5.0);
    //return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
    //return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(max(1.0 - cosTheta, 0.0), 5.0);
}  

float4 CookTorrance_Schlick(float4 V, float4 N, float rot, float3 color, float specularIntensity, float metallic, float roughness, float occlusion, float ambientContribution, float maxSpecularLOD, float specularContribution) {

    float3 F0;
    F0 = lerp(0.04, color, metallic);
    
    float3 F    = fresnelSchlick(max(dot(N, V), 0.0), F0, roughness);   
    float3x3 m = {
        cos(rot), 0.0f, -sin(rot),
        0.0f,     1.0f, 0.0f,
        sin(rot), 0.0f, cos(rot)
    };

    float3 sN = N.xyz;

    sN = mul(m, N.xyz);    
    sN = normalize(sN);
    float3 R    = reflect(V.xyz, sN).rgb;
    float3 kS           = F;
    float3 kD           = (float3(1, 1, 1) - kS) * (1.0f - (metallic*1.0));
    float3 diffuseBRDF  = color * (skyMap.Sample(LinearSampler, sN).rgb*1.0f); // 3.14159265359;
    
    float3 prefilteredColor = specularSkyMap.SampleLevel(LinearSampler, R,  roughness * maxSpecularLOD).rgb*specularContribution;    
    float2 brdf  = brdflut.Sample(LinearSampler, float2(max(dot(sN, V.xyz), 0.0), roughness)).rg;
    float3 specular = prefilteredColor * (F * brdf.x + brdf.y);

   // vec3 ambient = (kD * diffuse + specular);// * ao;
    
   // vec3 color = ambient + Lo;

    if (occlusion < 0.8f)
        occlusion = occlusion / 1.1f;
            
    // add to outgoing radiance Lo
    float3 ambient = (kD * diffuseBRDF + specular) * occlusion;
    float3 Lo = ambient;

    float4 outputColor;
    outputColor = float4(Lo, 1.0f) + float4(color*ambientContribution, 0.0f);
    
    return outputColor;
}

struct MetallicRoughness {
    float4 baseColor;
    float4 emissiveColor;
    float matMetallic;
    float matRoughness;
    float matOcclusion;
    int hasBaseColorTexture;
    int hasNormalTexture;
    int hasEmissiveTexture;
    int hasMetallicRoughnessTexture;
    int hasOcclusionTexture;
};

cbuffer WorldViewProj : register(b0) {
    matrix world;
    matrix worldRotate;
    matrix viewproj;
    matrix worldviewproj;
}

cbuffer View : register(b1) {
   float4 cameraPosition;
}

cbuffer PBRAppSettings : register(b2) {
   float modulateEmission;
   float iblCubeMapRotationY;
   float maxReflectanceLOD;
   float ambient;
   float specularContribution;
   int applyOcclusion;
}

cbuffer Material : register(b4) {
    MetallicRoughness materials[8];
}

struct VertexOut
{
	float4 pos : SV_POSITION;
        float4 wPos : POSITION;
        float4 norm : NORMAL;
        float2 tex  : TEXCOORD;
        int materialIndex : MATERIALINDEX;
};

struct GBuffer 
{
	float4 pos  : SV_TARGET0;
	float4 norm : SV_TARGET1;
	float4 mat  : SV_TARGET2;
};

GBuffer main(VertexOut input) {

    GBuffer gbuffer = (GBuffer)0;
    float2 texCoord = input.tex;
        
    float metallic  = 0.0f;
    float roughness = 0.0f;
    float occlusion = 0.0f;
    float4 emission  = float4(0.0f, 0.0f, 0.0f, 0.0f);
    float4 color     = float4(0.0f, 0.0f, 0.0f, 0.0f);
    
    if (materials[input.materialIndex].hasMetallicRoughnessTexture) {
         float2 metallicRoughness = specularMap.Sample(LinearSampler, input.tex).rg;
         metallic  = metallicRoughness.x;
         roughness = metallicRoughness.y;
    }
    else {
         metallic  = materials[input.materialIndex].matMetallic;
         roughness = materials[input.materialIndex].matRoughness;
    }

    if (materials[input.materialIndex].hasOcclusionTexture) {
        occlusion = saturate(causticMap.Sample(LinearSampler, texCoord).r);
    }
    else {
     	occlusion = materials[input.materialIndex.x].matOcclusion;
    }

    if (materials[input.materialIndex].hasEmissiveTexture) {
        emission = reflectionMap.Sample(LinearSampler, input.tex)*modulateEmission;
    }

    if (materials[input.materialIndex].hasBaseColorTexture) {
       color = diffuseMap.Sample(LinearSampler, texCoord);
    }
    else {
       color = materials[input.materialIndex].baseColor;
    }

    float4 normal = input.norm;
    if (materials[input.materialIndex].hasNormalTexture) {
        normal = float4(normalMap.Sample(LinearSampler, input.tex));
        float3x3 tbn  = calculateTBN(input.pos, input.tex, input.norm); 

        normal = (normal * 2.0f) - 1.0;
        normal = float4(normalize(mul(tbn, float3(normal.xyz))), 0.0f);
    }

    gbuffer.pos   =  float4(mul(world, input.wPos).xyz, metallic);
    gbuffer.norm  = normal;

    float4 N                      = normal;

    float4 P                      = input.wPos;
    float4 L, V, H;

    V = normalize(float4(P.xyz, 1.0f) - float4(cameraPosition.xyz, 1.0f));
    float4 outputColor;
   
    outputColor = CookTorrance_Schlick(V, N, iblCubeMapRotationY, color.xyz, 7.0f, metallic, roughness, occlusion, ambient, maxReflectanceLOD, specularContribution);
    outputColor = float4(extended_reinhard(outputColor.xyz, 5.0f), 1.0f);

    gbuffer.mat   = (outputColor) + emission;
    return gbuffer;
}