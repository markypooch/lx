struct VertexIn {
    float4 pos : POSITION;
    float2 tex : TEXCOORD;
    float4 norm : NORMAL;
    float4 bitangent : BITANGENT;
    float4 tangent : TANGENT; 
};

struct VertexOut {
    float4 pos : SV_POSITION;
    float2 tex : TEXCOORD;
    float4 norm : NORMAL;
    float4 bitangent : BITANGENT;
    float4 tangent : TANGENT; 
};

VertexOut main(VertexIn vIn) {
    VertexOut vOut;

    vOut.pos = vIn.pos;
    vOut.tex = vIn.tex;
    vOut.norm = vIn.norm;
    vOut.bitangent = vIn.bitangent;
    vOut.tangent = vIn.tangent;

   return vOut;
}