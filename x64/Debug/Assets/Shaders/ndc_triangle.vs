cbuffer wvp : register(b0) {
    matrix vp;
    matrix world;
}

struct VertexIn {
    float4 pos : POSITION;
    float2 tex : TEXCOORD;
    float4 norm : NORMAL;
     float4 tangent : TANGENT;
     float4 bitangent : BITANGENT;
};

struct VertexOut {
    float4 pos : SV_POSITION;
    float4 wPos : POSITION;
    float2 tex : TEXCOORD;
    float4 norm : NORMAL;
 float4 tangent : TANGENT;
     float4 bitangent : BITANGENT;
};

VertexOut main(VertexIn vIn) {
    VertexOut vOut;
    float4 wPos = mul(world, vIn.pos);
    vOut.pos = mul(vp, wPos);

    vOut.tex = vIn.tex;

    vOut.wPos = mul(world, vIn.pos);
    vOut.norm = mul(world, float4(vIn.norm.xyz, 0.0f));
    vOut.norm = normalize(vOut.norm);

    vOut.tangent = vIn.tangent;
    vOut.bitangent = vIn.bitangent;
    return vOut;
}