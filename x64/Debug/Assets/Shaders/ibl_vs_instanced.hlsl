cbuffer wvp : register(b0) {
    matrix world;
    matrix worldRotate;
    matrix vp;
    matrix wvp;
}

struct GLTFModelVertex
{
	float4 Position : POSITION;
	float2 TexCoord : TEXCOORD;
	float3 Normal : NORMAL;
	float4 Tangent : TANGENT;
	float4 Bitangent : BITANGENT;
        float4 instancePosition : TEXCOORD1;
};

struct VertexOut
{
	float4 pos : SV_POSITION;
        float4 wPos : POSITION;
        float4 norm : NORMAL;
        float2 tex  : TEXCOORD;
        int materialIndex : MATERIALINDEX;
};

VertexOut main(GLTFModelVertex vert)
{
	VertexOut oVert = (VertexOut)0;

        //oVert.pos         = mul(world, float4(vert.Position.xyz, 1.0f));
	//oVert.pos         = mul(world, float4(oVert.pos.xyz, 1.0f));

        float3 localPos   = vert.Position.xyz + vert.instancePosition.xyz;
        float4 rLocalPos  = mul(worldRotate, float4(localPos.xyz, 1.0f));
        oVert.pos         = mul(vp, float4(rLocalPos.xyz, 1.0f));

        oVert.wPos        = rLocalPos;
        oVert.norm        = normalize(mul(worldRotate, float4(vert.Normal.xyz, 0.0f)));

        //matrix lWorldViewProj = mul(viewProj, world);

        //oVert.lightPosition = mul(lWorldViewProj, vert.Position);

        oVert.tex         = vert.TexCoord;
        oVert.materialIndex = (int)vert.Bitangent.w;
        return oVert;
}