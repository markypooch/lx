#pragma warning( disable : 3570 )

Texture2D shadowMap : register(t0);
SamplerState linearSampler : register(s0);
SamplerState pointSampler : register(s1);

bool InPenumbraRegion(float3 lightClipPos, float bias, float2 texelSize) {

     float penumbraTest = 0.0f;
     float pcfDepth = shadowMap.Sample(pointSampler, lightClipPos.xy + float2(4, 0) * texelSize).r; 
     penumbraTest += lightClipPos.z - bias > pcfDepth ? 1.0 : 0.0;    

     pcfDepth = shadowMap.Sample(pointSampler, lightClipPos.xy + float2(0, 4) * texelSize).r; 
     penumbraTest += lightClipPos.z - bias > pcfDepth ? 1.0 : 0.0;    

     pcfDepth = shadowMap.Sample(pointSampler, lightClipPos.xy - float2(3, 0) * texelSize).r; 
     penumbraTest += lightClipPos.z - bias > pcfDepth ? 1.0 : 0.0;    

     pcfDepth = shadowMap.Sample(pointSampler, lightClipPos.xy - float2(0, 3) * texelSize).r; 
     penumbraTest += lightClipPos.z - bias > pcfDepth ? 1.0 : 0.0;        

     if ((penumbraTest/4) == 0.0f || (penumbraTest/4) == 1.0f) {
         return false;         
     }    

     return true;
}

float ComputeShadowTerm(float3 lightClipPos, float bias, float2 resolution) {
     float shadow = 0.0;     
     float2 texelSize = 8.0 / float2(resolution.x, resolution.y);

     //first check if even doing pcf will net us any visual difference
     if (!InPenumbraRegion(lightClipPos, bias, texelSize)) {
         float depth = shadowMap.Sample(pointSampler, lightClipPos.xy).r; 
         shadow = lightClipPos.z - bias > depth ? 1.0 : 0.0;
     }    
     else {
         //If the result isn't 1, or 0 we are on an edge, and should perform a more intensive pcf
         shadow = 0.0f;
         for(int x = -3; x <= 4; x += 1.0f)
         {
             for(int y = -3; y <= 4; y += 1.0f)
             {
                 float pcfDepth = shadowMap.Sample(pointSampler, lightClipPos.xy + float2(x, y) * texelSize).r; 
                 shadow += lightClipPos.z - bias > pcfDepth ? 1.0 : 0.0;        
             }
         }
         shadow /= 64.0;    
     }

     return shadow;
}

struct IncidenceVectors {
    float3 N;
    float3 V;
    float3 W; //WorldPosition
    float3 X; //tangent
    float3 Y; //bitangent
    float2 UV;
};

struct MetallicRoughness {
	float4 baseColor;
	float4 emissiveColor;
	float matMetallic;
	float matRoughness;
	float matOcclusion;
	int hasBaseColorTexture;
	int hasNormalTexture;
	int hasEmissiveTexture;
	int hasMetallicRoughnessTexture;
	int hasOcclusionTexture;
};

cbuffer WorldViewProj : register(b0) {
	matrix world;
	matrix worldRotate;
	matrix viewproj;
	matrix worldviewproj;
}

cbuffer View : register(b1) {
	float4 cameraPosition;
}

cbuffer PBRAppSettings : register(b2) {
	float modulateEmission;
	float iblCubeMapRotationY;
	float maxReflectanceLOD;
	float ambient;
	float specularContribution;
	int rtvmaterial;
}

cbuffer Material : register(b4) {
	MetallicRoughness materials[8];
}

float3x3 cotangent_frame(float3 N, float3 p, float2 uv)
{
	// get edge vectors of the pixel triangle
	float3 dp1 = ddx(p);
	float3 dp2 = ddy(p);
	float2 duv1 = ddx(uv);
	float2 duv2 = ddy(uv);

	// solve the linear system
	float3 dp2perp = cross(dp2, N);
	float3 dp1perp = cross(N, dp1);
	float3 T = dp2perp * duv1.x + dp1perp * duv2.x;
	float3 B = dp2perp * duv1.y + dp1perp * duv2.y;

	// construct a scale-invariant frame 
	float invmax = rsqrt(max(dot(T, T), dot(B, B)));
	return transpose(float3x3(T * invmax, B * invmax, N));
}

float sqr(float x)
{
	return x * x;
}

float SchlickFresnel(float u)
{
	float m = clamp(1 - u, 0, 1);
	float m2 = m * m;
	return m2 * m2 * m; // pow(m,5)
}

float GTR1(float NdotH, float a)
{
	if (a >= 1)
		return 1 / 3.1415;
	float a2 = a * a;
	float t = 1 + (a2 - 1) * NdotH * NdotH;
	return (a2 - 1) / (3.1415 * log(a2) * t);
}

float GTR2(float NdotH, float a)
{
	float a2 = a * a;
	float t = 1 + (a2 - 1) * NdotH * NdotH;
	return a2 / (3.1415 * t * t);
}

float GTR2_aniso(float NdotH, float HdotX, float HdotY, float ax, float ay)
{
	return 1 / (3.1415 * ax * ay * sqr(sqr(HdotX / ax) + sqr(HdotY / ay) + NdotH * NdotH));
}

float smithG_GGX(float NdotV, float alphaG)
{
	float a = alphaG * alphaG;
	float b = NdotV * NdotV;
	return 1 / (NdotV + sqrt(a + b - a * b));
}

float smithG_GGX_aniso(float NdotV, float VdotX, float VdotY, float ax, float ay)
{
	//return 1.0f / (NdotV + sqrt( pow(VdotX*ax, 2) + pow(VdotY*ay, 2) + pow(NdotV, 2) ));
	return 1.0f / (NdotV + sqrt(sqr(VdotX * ax) + sqr(VdotY * ay) + sqr(NdotV)));
}

float3 mon2lin(float3 x)
{
	return float3(pow(abs(x[0]), 2.2), pow(abs(x[1]), 2.2), pow(abs(x[2]), 2.2));
}

float3 lin2mon(float3 x)
{
	return float3(pow(abs(x[0]), 0.55), pow(abs(x[1]), 0.55), pow(abs(x[2]), 0.55));
}

void directionOfAnisotropicity(float3 normal, out float3 tangent, out float3 binormal)
{
	tangent = cross(normal, float3(1., 0., 1.));
	binormal = normalize(cross(normal, tangent));
	tangent = normalize(cross(normal, binormal));
}

class brdf
{
        float3 ComputeBrdf(float3 L, IncidenceVectors iVec, float diffuseContribution, float3 baseColor0, float metallic0, float rough0, float clearCoat0, float clearCoatRough0)
	{
                float3 outColor = float3(0.0f, 0.0f, 0.0f);
		float subsurface = 0.0f;
		float specular = 0.0f;
		float specularTint = 0.0f;
		float anisotropic = 0.0f;
		float sheen = 0.0f;
		float sheenTint = 0.5f;
                float transmission = 0.0f;
                float3 baseColor = baseColor0;
		
		float clearCoat = clearCoat0;
                float clearCoatRough = clearCoatRough0;

                float metallic = metallic0;
		float roughness = rough0;

                float diffuse_weight = (1.0 - saturate(metallic)) * (1.0 - saturate(transmission));
                float final_transmission = saturate(transmission) * (1.0 - saturate(metallic));
                float specular_weight = (1.0 - final_transmission);

		float NdotL = saturate(dot(iVec.N, L)) * 0.5f + 0.5f;
		float NdotV = saturate(dot(iVec.N, iVec.V));
		
		float3 H = normalize(L + iVec.V);
		float NdotH = saturate(dot(iVec.N, H));
		float LdotH = saturate(dot(L, H));

		float Cdlum = .2126 * baseColor[0] + .7152 * baseColor[1] + .0722 * baseColor[2]; // luminance approx.
		float3 Ctint = Cdlum > 0 ? baseColor / Cdlum : float3(1.0f, 1.0f, 1.0f); // normalize lum. to isolate hue+sat       
 
                
                //BSDF = BaseColor * principled_diffuse(Normal, Roughness);
                    
                // Diffuse fresnel - go from 1 at normal incidence to .5 at grazing
		// and mix in diffuse retro-reflection based on roughness
		float FL = SchlickFresnel(NdotL), FV = SchlickFresnel(NdotV);
		float Fd90 = LdotH * LdotH * roughness;
                float Fd = lerp(1.0, Fd90, FL) * lerp(1.0, Fd90, FV);
		    
                outColor = baseColor * ((diffuseContribution/3.14) * NdotL * Fd);  

                if (sheen > 1e-5) {
                    float FH = SchlickFresnel(LdotH);
                    float3 sheen_color = float3(1.0, 1.0, 1.0) * (1.0 - sheenTint) + Ctint * sheenTint;

                    outColor += sheen_color * sheen * (FH * NdotL);
                }

                //outColor *= diffuse_weight;

                if (specular_weight > 1e-5) {
                    float a = max(0.001f, roughness * roughness);

                    float aspect = sqrt(1.0f - anisotropic * 0.9f);
		    float ax = max(0.001f, sqr(a) / aspect);
		    float ay = max(0.001f, sqr(a) * aspect);

                    //float3 tmp_col = float3(1.0, 1.0, 1.0) * (1.0 - specularTint) + Ctint * specularTint;
                    float3 Cspec0 = lerp(specular * .08 * lerp(float3(1.0f, 1.0f, 1.0f), Ctint, specularTint), baseColor, metallic);
                    //float3 Cspec0 = (specular * 0.08 * tmp_col) * (1.0 - metallic) + baseColor * metallic;

                    float Ds = GTR2_aniso(NdotH, dot(H, iVec.X), dot(H, iVec.Y), ax, ay);
		    float FH = SchlickFresnel(LdotH);
		    float3 Fs = lerp(Cspec0, float3(1.0f, 1.0f, 1.0f), FH);
		    //float roughg = sqr(a * 0.5f + 0.5f);
		    float Gs;
		     Gs  = smithG_GGX_aniso(NdotL, dot(L, iVec.X), dot(L, iVec.Y), ax, ay);
                     Gs *= smithG_GGX_aniso(NdotV, dot(iVec.V, iVec.X), dot(iVec.V, iVec.Y), ax, ay);

                    //outColor += specular_weight * (Gs*Ds*Fs) * (NdotL);
                }

                if (clearCoat > 1e-5) {
                     float FH = SchlickFresnel(LdotH);
                     float Dr = GTR1(NdotH, lerp(.1, .001, 1.0f - 0.3f));
		     float Fr = lerp(.04, 1.0, FH);
		     float Gr = smithG_GGX(NdotL, .25) * smithG_GGX(NdotV, .25);
                
                     outColor += clamp(2.5f * clearCoat * Gr * Fr * Dr, 0.0f, 0.025f);
                     
                       
      
                }

                return lin2mon(outColor);
	}
};

struct VSOutput {
    float4 Position : SV_POSITION;
    float4 WorldPosition : POSITION;
    float3 WorldNormal : NORMAL;
    float3 ViewDir : VIEWDIR;
    float2 TexCoord : TEXCOORD0;
    float4 lPos : TEXCOORD2;
    int materialIndex : MATERIALINDEX;
};

struct GBuffer 
{
	float4 pos  : SV_TARGET0;
	float4 norm : SV_TARGET1;
	float4 mat  : SV_TARGET2;
        float4 mat2 : SV_TARGET3;
};

GBuffer main(VSOutput vsOut)
{
    float4 baseColor = materials[0].baseColor;

    float3 N = normalize(vsOut.WorldNormal);
    float3 worldPos = vsOut.WorldPosition.xyz / vsOut.WorldPosition.w;

    float3 outColor = float3(0.0f, 0.0f, 0.0f);
    float3 tangent, binormal;

    directionOfAnisotropicity(N, tangent, binormal);

    IncidenceVectors iVectors;
    iVectors.N = N;
    iVectors.V = -normalize(vsOut.ViewDir);
    iVectors.W = vsOut.WorldPosition.xyz;
    iVectors.X = normalize(tangent);
    iVectors.Y = normalize(binormal);
    iVectors.UV = vsOut.TexCoord;
    float3 lightClipPos = vsOut.lPos.xyz / vsOut.lPos.w;

    lightClipPos.x = (vsOut.lPos.x / 2.0f) + 0.5f;
    lightClipPos.y = (vsOut.lPos.y / -2.0f) + 0.5f;

    float metallic = materials[0].matMetallic;
    float roughness = materials[0].matRoughness;
    
    //if (materials[vsOut.materialIndex].hasMetallicRoughnessTexture) { 
    //    metallic = metallicRoughness.Sample(LinearSampler, iVec.UV).g;
    //    roughness = metallicRoughness.Sample(LinearSampler, iVec.UV).r;
    //}


    float3 directionalLight = float3(-1.0f, 0.0f, -1.0f);
    float3 lightIntensity = float3(0.0f, 0.0f, 0.0f);

    float3 directionalLight2 = float3(0.0f, 1.0f, -1.0f);
    float3 lightIntensity2 = float3(0.0f, 0.0f, 0.0f);

    float3 directionalLight3 = float3(0.0f, 0.0f, -1.0f);
    float3 lightIntensity3 = float3(1.75f, 2.75f, 2.75f);

    //outColor += baseColor.xyz * float3(ambient,ambient,ambient);
     brdf disneyBrdf;
        outColor = disneyBrdf.ComputeBrdf(directionalLight, iVectors, specularContribution, baseColor.xyz * lightIntensity, metallic, roughness, materials[0].emissiveColor.r, materials[0].emissiveColor.g);
        outColor += disneyBrdf.ComputeBrdf(directionalLight2, iVectors, specularContribution, baseColor.xyz * lightIntensity2, metallic, roughness, materials[0].emissiveColor.r/rtvmaterial, materials[0].emissiveColor.g);
        outColor += disneyBrdf.ComputeBrdf(directionalLight3, iVectors, specularContribution, baseColor.xyz, metallic, roughness, 0.0f, 0.0f);
        outColor += baseColor.xyz * float3(ambient,ambient,ambient);
    float shadowContribution = ComputeShadowTerm(lightClipPos, modulateEmission, float2(4096, 4096));
    outColor *= 1.0f-(shadowContribution*0.18f);
    
    GBuffer gBuffer; 
    gBuffer.mat2  = float4(outColor, 1.0f);
    gBuffer.norm = float4(N, 0.0f);
    gBuffer.pos = vsOut.WorldPosition;

    return gBuffer;
}