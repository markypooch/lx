cbuffer wvp : register(b0) {
    matrix world;
    matrix viewProj;
}

struct VertexInput {
    float4 pos    : POSITION;
    float2 tex    : TEXCOORD;
    float4 norm   : NORMAL;
    float4 tang   : TANGENT;
    float4 bitang : BITANGENT;
};

struct VertexOutput {
    float4 pos    : SV_POSITION;
    float2 tex    : TEXCOORD;
    float4 norm   : NORMAL;
    float4 tang   : TANGENT;
    float4 bitang : BITANGENT;
};

VertexOutput main(VertexInput vIn) {

     VertexOutput vOut;
   
     float4 wPos = mul(world, vIn.pos);
     vOut.pos    = mul(viewProj, wPos);

     vOut.tex = vIn.tex;
     vOut.norm = mul(world, vIn.norm);
     vOut.tang = mul(world, vIn.tang);
     vOut.bitang = mul(world, vIn.bitang);

     return vOut;
}
 