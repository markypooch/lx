struct VertexIn {
    float4 pos : POSITION;
};

struct VertexOut {
    float4 pos : SV_POSITION;
};

VertexOut main(VertexIn vIn) {
    VertexOut vOut;
    vOut.pos = vIn.pos;
   
    return vOut;
}