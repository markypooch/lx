#pragma once
#include <string>
#include <functional>

class IWindow
{
public:
	void* CreateWindow(unsigned int width, unsigned int height, std::string windowName);
	void  SetUpdateCallback(std::function<)
};

